package de.hannover.uni.se.fmse.lts.ui.check.popup.actions;

import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import de.hannover.uni.se.fmse.ctl.FormulaDefinition;
import de.hannover.uni.se.fmse.lts.xLTS.Check;
import de.hannover.uni.se.fmse.lts.xLTS.CheckSet;

public class CheckFormulaSetAction implements IObjectActionDelegate {

	private Shell shell;

	/**
	 * Constructor for Action1.
	 */
	public CheckFormulaSetAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {

		Resource selectedResource = getSelectedResource();
		CheckSet checkSet = (CheckSet) selectedResource.getContents().get(0);
		
		String output = "Check " + checkSet.getName() + ":\n";
		
		for(Check check : checkSet.getChecks()) {
			FormulaDefinition definition = check.getFormulaDefinition();
			output += definition.getName() + ": ";
			output += definition.getFormula().evaluate(check.getLts()) + "\n";
			saveToFile(check, generateCheckGraph(check));
		}
	
		MessageDialog.openInformation(shell, "Check Result",
				output);
	}

	private void saveToFile(Check check, CharSequence generateCheckGraph) {
		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		IFile resourceFile = (IFile) structuredSelection
				.getFirstElement();
		IPath newPath = resourceFile.getLocation().removeLastSegments(1);
		//newPath = newPath.append("check-out");
		newPath = newPath.append("check_" + check.getLts().getName() + "_" + check.getFormulaDefinition().getName() + ".dot");
		try {
			FileWriter fileWriter = new FileWriter(newPath.toFile());
			fileWriter.write(generateCheckGraph.toString());
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private CharSequence generateCheckGraph(Check check) {
		return new de.hannover.uni.se.fmse.lts.ui.check.popup.actions.CheckSetOutputGenerator().doGenerate(check);
	}

	private Resource getSelectedResource() {
		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		IFile resourceFile = (IFile) structuredSelection
				.getFirstElement();
		ResourceSet resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.getResource(URI
				.createPlatformResourceURI(resourceFile
						.getFullPath().toString(), true), true);
		return resource;
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}
