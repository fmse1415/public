package de.hannover.uni.se.fmse.lts.ui.check.popup.actions

import de.hannover.uni.se.fmse.lts.State
import de.hannover.uni.se.fmse.lts.xLTS.Check

class CheckSetOutputGenerator {
	
	def doGenerate(Check check) '''
	digraph {
		«val lts=check.lts»
		«val formula=check.formulaDefinition.formula»
		«FOR i: 0..lts.states.size-1»
			«val state=lts.states.get(i)»
			state_«i» [ style="filled"«IF (formula.satisfyingStates.contains(state))» fillcolor=green«ENDIF» label="«state.name» ( «getAtomicPropositions(state)»)" ]
		«ENDFOR»
		«FOR i: 0..lts.initStates.size-1»
			«val state=lts.initStates.get(i)»
			state_init_«i» [ style="invisible" ]
			state_init_«i» -> state_«lts.states.indexOf(state)»
		«ENDFOR»
		«FOR i: 0..lts.transitions.size-1»
			«val t=lts.transitions.get(i)»
			state_«lts.states.indexOf(t.source)» -> state_«lts.states.indexOf(t.destination)»
		«ENDFOR»
	}
	'''
	
	def getAtomicPropositions(State state)
		'''«IF (state.atomicPropositions.size>0)»«FOR j: 0..state.atomicPropositions.size-1»«val ap=state.atomicPropositions.get(j)»«ap.name» «ENDFOR»«ENDIF»'''
	
	
		
	
}