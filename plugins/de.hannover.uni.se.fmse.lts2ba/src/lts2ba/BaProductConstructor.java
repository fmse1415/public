package lts2ba;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import de.hannover.uni.se.fmse.buechi.*;
import de.hannover.uni.se.fmse.buechi.Transition;
import de.hannover.uni.se.fmse.lts.*;

public class BaProductConstructor {
	ArrayList<ComposedState> composedStates = new ArrayList<>();

	public Automaton construct(Automaton ba1, Automaton ba2) {
		Automaton result = BuechiFactory.eINSTANCE.createAutomaton();

		// Fügt das Kreuzprodukt I1 x I2 zum neuen BA hinzu
		for (State state1 : ba1.getInitStates()) {
			for (State state2 : ba2.getInitStates()) {
				ComposedState cs = LtsFactory.eINSTANCE.createComposedState();
				cs.setPart1(state1);
				cs.setPart2(state2);
				if (!composedStates.contains(cs)) {
					composedStates.add(cs);
					result.getInitStates().add(cs);
				}
			}
		}

		// Fügt das Kreuzprodukt Q1 x F2 zum neuen BA hinzu
		for (State state1 : ba1.getStates()) {
			for (State state2 : ba2.getFinalStates()) {
				ComposedState cs = LtsFactory.eINSTANCE.createComposedState();
				cs.setPart1(state1);
				cs.setPart2(state2);
				if (!composedStates.contains(cs)) {
					composedStates.add(cs);
					result.getFinalStates().add(cs);
				}
			}
		}

		// Fügt das Kreuzprodukt Q1 x Q2 zum neuen BA hinzu
		for (State state1 : ba1.getStates()) {
			for (State state2 : ba2.getStates()) {
				ComposedState cs = LtsFactory.eINSTANCE.createComposedState();
				cs.setPart1(state1);
				cs.setPart2(state2);
				if (!composedStates.contains(cs)) {
					composedStates.add(cs);
				}
			}
		}

		for (de.hannover.uni.se.fmse.lts.Transition t1 : ba1.getTransitions()) {
			for (de.hannover.uni.se.fmse.lts.Transition t2 : ba2
					.getTransitions()) {
				if (propositionsMatch(
						((Transition) t1).getAtomicPropositions(),
						((Transition) t2).getAtomicPropositions())) {
					Logger.getLogger(BaProductConstructor.class.getName())
							.info("Found matching transitions:\n".concat(
									getTransitionString((Transition) t1)).concat("\n").concat(getTransitionString((Transition) t2)));
					List<AtomicProposition> alist = new ArrayList<AtomicProposition>();
					alist.addAll(((Transition) t1).getAtomicPropositions());
					outer:for(AtomicProposition ap:((Transition) t2).getAtomicPropositions()){
						for(int i=0;i<alist.size();i++) {
							AtomicProposition a = alist.get(i);
							if(a.getName().equals(ap.getName()))
								continue outer; 
						}
						alist.add(ap);
					}
					Transition buechiTrans = BuechiFactory.eINSTANCE
							.createTransition();
					buechiTrans.setSource(getState(t1.getSource(),
							t2.getSource()));
					buechiTrans.setDestination(getState(t1.getDestination(),
							t2.getDestination()));
					result.getTransitions().add(buechiTrans);
					buechiTrans.getAtomicPropositions().addAll(alist);
				} else {
					Logger.getLogger(BaProductConstructor.class.getName())
					.info("Found nonmatching transitions:\n".concat(
							getTransitionString((Transition) t1)).concat("\n").concat(getTransitionString((Transition) t2)));
				}
				// }
			}
		}
		result.getStates().addAll(composedStates);
		result.getAlphabet().addAll(ba1.getAlphabet());
		return result;
	}

	private boolean propositionsMatch(List<AtomicProposition> ksProps,
			List<AtomicProposition> formulaProps) {
		if(ksProps.isEmpty()) {
			for(AtomicProposition ap:formulaProps)
				if(!ap.getName().startsWith("!"))return false;
		}
		outer: for (AtomicProposition ksprop : ksProps) {
			for (AtomicProposition fprop : formulaProps) {
				String name = fprop.getName();
				if (name.equals("___<SIGMA>"))
					return true;
				else {
					if (name.equals(ksprop.getName())) {
						continue outer;
					}
				}
			}
			return false;
		}
		return true;
	}

	private ComposedState getState(State s1, State s2) {
		for (ComposedState cs : composedStates) {
			if (cs.getPart1() == s1 && cs.getPart2() == s2)
				return cs;
		}
		ComposedState csNew = LtsFactory.eINSTANCE.createComposedState();
		csNew.setPart1(s1);
		csNew.setPart2(s2);
		composedStates.add(csNew);
		return csNew;

	}

	String getTransitionString(Transition t) {
		StringBuilder result = new StringBuilder()
				.append(t.getSource().hashCode()).append("->")
				.append(t.getDestination().hashCode()).append(": ");
		for (AtomicProposition ap : t.getAtomicPropositions())
			result.append(ap.getName());
		return result.toString();
	}
}
