package lts2ba;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.util.EcoreUtil;

import de.hannover.uni.se.fmse.buechi.Automaton;
import de.hannover.uni.se.fmse.buechi.BuechiFactory;
import de.hannover.uni.se.fmse.buechi.Transition;
import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.LtsFactory;
import de.hannover.uni.se.fmse.lts.NamedElement;
import de.hannover.uni.se.fmse.lts.State;

public final class LTS2BATransformer {
	private final Map<State, State> copiedStates = new HashMap<State, State>();

	/**
	 * Retrieves the copy of a state from the source lts, or creates one, if not
	 * yet present.
	 * 
	 * @param s
	 *            original state
	 * @return copy of s.
	 */
	private final State getState(State s) {
		if (!copiedStates.containsKey(s)) {
			copiedStates.put(s, EcoreUtil.copy(s));
		}
		return copiedStates.get(s);
	}

	/**
	 * Transforms an lts to a büchi automaton.
	 * 
	 * @param LTS
	 * @return
	 */
	public final Automaton transformLTS(LTS LTS) {
		// 1) create automaton
		final Automaton result = BuechiFactory.eINSTANCE.createAutomaton();
		result.setName(LTS.getName());
		// 2a) add new initial state.
		final State init = LtsFactory.eINSTANCE.createBasicState();
		((NamedElement) init).setName("init");
		result.getInitStates().add(getState(init));
		// 2b) link new initial state to all previous initial states.
		for (State target : LTS.getInitStates()) {
			final Transition trans = BuechiFactory.eINSTANCE.createTransition();
			((de.hannover.uni.se.fmse.lts.Transition) trans)
					.setSource(getState(init));
			((de.hannover.uni.se.fmse.lts.Transition) trans)
					.setDestination(getState(target));
			trans.getAtomicPropositions().addAll(target.getAtomicPropositions());
			// all states are accepting states.
			result.getTransitions().add(trans);
		}
		// 3) iterate over all states from the lts and copy them.
		for (State state : LTS.getStates()) {
			for (de.hannover.uni.se.fmse.lts.Transition trans : state
					.getIncoming()) {
				// copy transition, label it with APs from the target state.
				final Transition copiedTransition = copyTransition(trans);
				// add copy to transition list.
				result.getTransitions()
						.add((de.hannover.uni.se.fmse.lts.Transition) copiedTransition);
			}
		}
		// 4) add all copied states to state list.
		result.getStates().addAll(copiedStates.values());
		// 5) all states are accepting states.
		result.getFinalStates().addAll(copiedStates.values());
		result.getAtomicPropositions().addAll(LTS.getAtomicPropositions());
		return result;
	}

	/**
	 * Converts an lts transition to a büchi transition.
	 * 
	 * @param t
	 *            transition from an lts model instance.
	 * @return copy of t labeled with the propositions of the destination state.
	 */
	private final Transition copyTransition(
			de.hannover.uni.se.fmse.lts.Transition t) {
		Transition buechiTrans = BuechiFactory.eINSTANCE.createTransition();
		((de.hannover.uni.se.fmse.lts.Transition) buechiTrans)
				.setSource(getState(t.getSource()));
		((de.hannover.uni.se.fmse.lts.Transition) buechiTrans)
				.setDestination(getState(t.getDestination()));
		buechiTrans.getAtomicPropositions().addAll(
				t.getDestination().getAtomicPropositions());
		return buechiTrans;
	}
}