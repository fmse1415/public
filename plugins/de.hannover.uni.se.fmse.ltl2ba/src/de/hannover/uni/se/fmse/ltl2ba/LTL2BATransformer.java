package de.hannover.uni.se.fmse.ltl2ba;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import rwth.i2.ltl2ba4j.LTL2BA4J;
import rwth.i2.ltl2ba4j.model.IGraphProposition;
import rwth.i2.ltl2ba4j.model.ITransition;
import rwth.i2.ltl2ba4j.model.IState;
import rwth.i2.ltl2ba4j.model.impl.SigmaProposition;
import de.hannover.uni.se.fmse.lts.AtomicProposition;
import de.hannover.uni.se.fmse.lts.LtsFactory;
import de.hannover.uni.se.fmse.lts.NamedElement;
import de.hannover.uni.se.fmse.lts.State;
import de.hannover.uni.se.fmse.buechi.Automaton;
import de.hannover.uni.se.fmse.buechi.BuechiFactory;
import de.hannover.uni.se.fmse.buechi.Transition;
import de.hannover.uni.se.fmse.ltl.Formula;
import de.hannover.uni.se.fmse.lts.generator.LTLFormulaGenerator;

public class LTL2BATransformer {
	public static Automaton transformLTL(Formula ltl) {
		ctr  =0;
		String input = LTLFormulaGenerator.generateLTLString(ltl);;
		Collection<ITransition> generatedAutomaton = LTL2BA4J.formulaToBA(input);
		Automaton result = BuechiFactory.eINSTANCE.createAutomaton();
		Map<IState,State> states = new HashMap<>();
		AtomicProposition sigma = LtsFactory.eINSTANCE.createAtomicProposition();
		sigma.setName("___<SIGMA>");
		for(ITransition t : generatedAutomaton) {
			// fetch or add transformed states to automaton.
			IState src = t.getSourceState(), tget = t.getTargetState();
			State source, target;
			if(!states.containsKey(src)) {
				source = transformState(src, result); 
				states.put(src, source);
			}
			else source = states.get(src);
			if(!states.containsKey(tget)) {
				target = transformState(tget, result); 
				states.put(tget, target);
			}
			else target = states.get(tget);
			// create transition, label it and attach it to source & target state.
			Transition transition = BuechiFactory.eINSTANCE.createTransition();
			for(IGraphProposition prop : t.getLabels()) {
				if(prop instanceof SigmaProposition) {
					transition.getAtomicPropositions().add(sigma);
					if(!result.getAtomicPropositions().contains(sigma))result.getAtomicPropositions().add(sigma);
				} else {
					AtomicProposition propa = LtsFactory.eINSTANCE.createAtomicProposition();
					propa.setName(prop.getFullLabel());
					if(!result.getAtomicPropositions().contains(propa))result.getAtomicPropositions().add(propa);
					transition.getAtomicPropositions().add(propa);
										
				}
			}((de.hannover.uni.se.fmse.lts.Transition) transition).setSource(source);
					((de.hannover.uni.se.fmse.lts.Transition) transition).setDestination(target);
			result.getTransitions().add(transition);
		}
		return result;
	}
		private static State transformState(IState state, Automaton a) {
			State s = LtsFactory.eINSTANCE.createBasicState();
			((NamedElement) s).setName(""+ctr++);
			a.getStates().add(s);
			if(state.isFinal()) a.getFinalStates().add(s);
			if(state.isInitial()) a.getInitStates().add(s);
			return s;
		}
	private static int ctr=0;
}
