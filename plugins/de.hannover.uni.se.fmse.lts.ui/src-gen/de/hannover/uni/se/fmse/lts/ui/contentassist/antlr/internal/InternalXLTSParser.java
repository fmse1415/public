package de.hannover.uni.se.fmse.lts.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import de.hannover.uni.se.fmse.lts.services.XLTSGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalXLTSParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'false'", "'LTS'", "'{'", "'states'", "'initial'", "'}'", "'atomic'", "'propositions'", "','", "'alphabet'", "'->'", "':'", "'compose'", "'||'", "'as'", "'using'", "'CTL'", "'FormulaSet'", "':='", "'EX'", "'EG'", "'E'", "'['", "'U'", "']'", "'('", "'\\u22C0'", "')'", "'\\u22C1'", "'\\u00AC'", "'CheckSet'", "'check'", "'\\u22A8'", "'LTL'", "'X'", "'G'", "'F'", "'true'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalXLTSParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalXLTSParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalXLTSParser.tokenNames; }
    public String getGrammarFileName() { return "../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g"; }


     
     	private XLTSGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(XLTSGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleDocument"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:61:1: entryRuleDocument : ruleDocument EOF ;
    public final void entryRuleDocument() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:62:1: ( ruleDocument EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:63:1: ruleDocument EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentRule()); 
            }
            pushFollow(FOLLOW_ruleDocument_in_entryRuleDocument67);
            ruleDocument();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleDocument74); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDocument"


    // $ANTLR start "ruleDocument"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:70:1: ruleDocument : ( ( rule__Document__Group__0 ) ) ;
    public final void ruleDocument() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:74:2: ( ( ( rule__Document__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:75:1: ( ( rule__Document__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:75:1: ( ( rule__Document__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:76:1: ( rule__Document__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:77:1: ( rule__Document__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:77:2: rule__Document__Group__0
            {
            pushFollow(FOLLOW_rule__Document__Group__0_in_ruleDocument100);
            rule__Document__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDocument"


    // $ANTLR start "entryRuleLTS"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:89:1: entryRuleLTS : ruleLTS EOF ;
    public final void entryRuleLTS() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:90:1: ( ruleLTS EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:91:1: ruleLTS EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSRule()); 
            }
            pushFollow(FOLLOW_ruleLTS_in_entryRuleLTS127);
            ruleLTS();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTS134); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTS"


    // $ANTLR start "ruleLTS"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:98:1: ruleLTS : ( ( rule__LTS__Group__0 ) ) ;
    public final void ruleLTS() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:102:2: ( ( ( rule__LTS__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:103:1: ( ( rule__LTS__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:103:1: ( ( rule__LTS__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:104:1: ( rule__LTS__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:105:1: ( rule__LTS__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:105:2: rule__LTS__Group__0
            {
            pushFollow(FOLLOW_rule__LTS__Group__0_in_ruleLTS160);
            rule__LTS__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTS"


    // $ANTLR start "entryRuleAtomicProposition"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:117:1: entryRuleAtomicProposition : ruleAtomicProposition EOF ;
    public final void entryRuleAtomicProposition() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:118:1: ( ruleAtomicProposition EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:119:1: ruleAtomicProposition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtomicPropositionRule()); 
            }
            pushFollow(FOLLOW_ruleAtomicProposition_in_entryRuleAtomicProposition187);
            ruleAtomicProposition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtomicPropositionRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAtomicProposition194); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAtomicProposition"


    // $ANTLR start "ruleAtomicProposition"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:126:1: ruleAtomicProposition : ( ( rule__AtomicProposition__NameAssignment ) ) ;
    public final void ruleAtomicProposition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:130:2: ( ( ( rule__AtomicProposition__NameAssignment ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:131:1: ( ( rule__AtomicProposition__NameAssignment ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:131:1: ( ( rule__AtomicProposition__NameAssignment ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:132:1: ( rule__AtomicProposition__NameAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtomicPropositionAccess().getNameAssignment()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:133:1: ( rule__AtomicProposition__NameAssignment )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:133:2: rule__AtomicProposition__NameAssignment
            {
            pushFollow(FOLLOW_rule__AtomicProposition__NameAssignment_in_ruleAtomicProposition220);
            rule__AtomicProposition__NameAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtomicPropositionAccess().getNameAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAtomicProposition"


    // $ANTLR start "entryRuleBasicState"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:145:1: entryRuleBasicState : ruleBasicState EOF ;
    public final void entryRuleBasicState() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:146:1: ( ruleBasicState EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:147:1: ruleBasicState EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateRule()); 
            }
            pushFollow(FOLLOW_ruleBasicState_in_entryRuleBasicState247);
            ruleBasicState();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBasicState254); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBasicState"


    // $ANTLR start "ruleBasicState"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:154:1: ruleBasicState : ( ( rule__BasicState__Group__0 ) ) ;
    public final void ruleBasicState() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:158:2: ( ( ( rule__BasicState__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:159:1: ( ( rule__BasicState__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:159:1: ( ( rule__BasicState__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:160:1: ( rule__BasicState__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:161:1: ( rule__BasicState__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:161:2: rule__BasicState__Group__0
            {
            pushFollow(FOLLOW_rule__BasicState__Group__0_in_ruleBasicState280);
            rule__BasicState__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBasicState"


    // $ANTLR start "entryRuleEvent"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:173:1: entryRuleEvent : ruleEvent EOF ;
    public final void entryRuleEvent() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:174:1: ( ruleEvent EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:175:1: ruleEvent EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEventRule()); 
            }
            pushFollow(FOLLOW_ruleEvent_in_entryRuleEvent307);
            ruleEvent();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEventRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEvent314); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEvent"


    // $ANTLR start "ruleEvent"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:182:1: ruleEvent : ( ( rule__Event__NameAssignment ) ) ;
    public final void ruleEvent() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:186:2: ( ( ( rule__Event__NameAssignment ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:187:1: ( ( rule__Event__NameAssignment ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:187:1: ( ( rule__Event__NameAssignment ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:188:1: ( rule__Event__NameAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEventAccess().getNameAssignment()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:189:1: ( rule__Event__NameAssignment )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:189:2: rule__Event__NameAssignment
            {
            pushFollow(FOLLOW_rule__Event__NameAssignment_in_ruleEvent340);
            rule__Event__NameAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEventAccess().getNameAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEvent"


    // $ANTLR start "entryRuleTransition"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:201:1: entryRuleTransition : ruleTransition EOF ;
    public final void entryRuleTransition() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:202:1: ( ruleTransition EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:203:1: ruleTransition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTransitionRule()); 
            }
            pushFollow(FOLLOW_ruleTransition_in_entryRuleTransition367);
            ruleTransition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTransitionRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTransition374); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:210:1: ruleTransition : ( ( rule__Transition__Group__0 ) ) ;
    public final void ruleTransition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:214:2: ( ( ( rule__Transition__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:215:1: ( ( rule__Transition__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:215:1: ( ( rule__Transition__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:216:1: ( rule__Transition__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTransitionAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:217:1: ( rule__Transition__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:217:2: rule__Transition__Group__0
            {
            pushFollow(FOLLOW_rule__Transition__Group__0_in_ruleTransition400);
            rule__Transition__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTransitionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "entryRuleComposedLTS"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:229:1: entryRuleComposedLTS : ruleComposedLTS EOF ;
    public final void entryRuleComposedLTS() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:230:1: ( ruleComposedLTS EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:231:1: ruleComposedLTS EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSRule()); 
            }
            pushFollow(FOLLOW_ruleComposedLTS_in_entryRuleComposedLTS427);
            ruleComposedLTS();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleComposedLTS434); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleComposedLTS"


    // $ANTLR start "ruleComposedLTS"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:238:1: ruleComposedLTS : ( ( rule__ComposedLTS__Group__0 ) ) ;
    public final void ruleComposedLTS() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:242:2: ( ( ( rule__ComposedLTS__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:243:1: ( ( rule__ComposedLTS__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:243:1: ( ( rule__ComposedLTS__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:244:1: ( rule__ComposedLTS__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:245:1: ( rule__ComposedLTS__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:245:2: rule__ComposedLTS__Group__0
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group__0_in_ruleComposedLTS460);
            rule__ComposedLTS__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComposedLTS"


    // $ANTLR start "entryRuleFormulaSet"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:257:1: entryRuleFormulaSet : ruleFormulaSet EOF ;
    public final void entryRuleFormulaSet() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:258:1: ( ruleFormulaSet EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:259:1: ruleFormulaSet EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaSetRule()); 
            }
            pushFollow(FOLLOW_ruleFormulaSet_in_entryRuleFormulaSet487);
            ruleFormulaSet();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaSetRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFormulaSet494); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFormulaSet"


    // $ANTLR start "ruleFormulaSet"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:266:1: ruleFormulaSet : ( ( rule__FormulaSet__Group__0 ) ) ;
    public final void ruleFormulaSet() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:270:2: ( ( ( rule__FormulaSet__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:271:1: ( ( rule__FormulaSet__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:271:1: ( ( rule__FormulaSet__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:272:1: ( rule__FormulaSet__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaSetAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:273:1: ( rule__FormulaSet__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:273:2: rule__FormulaSet__Group__0
            {
            pushFollow(FOLLOW_rule__FormulaSet__Group__0_in_ruleFormulaSet520);
            rule__FormulaSet__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaSetAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFormulaSet"


    // $ANTLR start "entryRuleFormulaDefinition"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:285:1: entryRuleFormulaDefinition : ruleFormulaDefinition EOF ;
    public final void entryRuleFormulaDefinition() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:286:1: ( ruleFormulaDefinition EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:287:1: ruleFormulaDefinition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaDefinitionRule()); 
            }
            pushFollow(FOLLOW_ruleFormulaDefinition_in_entryRuleFormulaDefinition547);
            ruleFormulaDefinition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaDefinitionRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFormulaDefinition554); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFormulaDefinition"


    // $ANTLR start "ruleFormulaDefinition"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:294:1: ruleFormulaDefinition : ( ( rule__FormulaDefinition__Group__0 ) ) ;
    public final void ruleFormulaDefinition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:298:2: ( ( ( rule__FormulaDefinition__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:299:1: ( ( rule__FormulaDefinition__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:299:1: ( ( rule__FormulaDefinition__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:300:1: ( rule__FormulaDefinition__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaDefinitionAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:301:1: ( rule__FormulaDefinition__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:301:2: rule__FormulaDefinition__Group__0
            {
            pushFollow(FOLLOW_rule__FormulaDefinition__Group__0_in_ruleFormulaDefinition580);
            rule__FormulaDefinition__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaDefinitionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFormulaDefinition"


    // $ANTLR start "entryRuleFormula"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:313:1: entryRuleFormula : ruleFormula EOF ;
    public final void entryRuleFormula() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:314:1: ( ruleFormula EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:315:1: ruleFormula EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaRule()); 
            }
            pushFollow(FOLLOW_ruleFormula_in_entryRuleFormula607);
            ruleFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFormula614); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFormula"


    // $ANTLR start "ruleFormula"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:322:1: ruleFormula : ( ( rule__Formula__Alternatives ) ) ;
    public final void ruleFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:326:2: ( ( ( rule__Formula__Alternatives ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:327:1: ( ( rule__Formula__Alternatives ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:327:1: ( ( rule__Formula__Alternatives ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:328:1: ( rule__Formula__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaAccess().getAlternatives()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:329:1: ( rule__Formula__Alternatives )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:329:2: rule__Formula__Alternatives
            {
            pushFollow(FOLLOW_rule__Formula__Alternatives_in_ruleFormula640);
            rule__Formula__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFormula"


    // $ANTLR start "entryRuleEX"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:341:1: entryRuleEX : ruleEX EOF ;
    public final void entryRuleEX() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:342:1: ( ruleEX EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:343:1: ruleEX EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEXRule()); 
            }
            pushFollow(FOLLOW_ruleEX_in_entryRuleEX667);
            ruleEX();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEXRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEX674); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEX"


    // $ANTLR start "ruleEX"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:350:1: ruleEX : ( ( rule__EX__Group__0 ) ) ;
    public final void ruleEX() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:354:2: ( ( ( rule__EX__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:355:1: ( ( rule__EX__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:355:1: ( ( rule__EX__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:356:1: ( rule__EX__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEXAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:357:1: ( rule__EX__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:357:2: rule__EX__Group__0
            {
            pushFollow(FOLLOW_rule__EX__Group__0_in_ruleEX700);
            rule__EX__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEXAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEX"


    // $ANTLR start "entryRuleEG"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:369:1: entryRuleEG : ruleEG EOF ;
    public final void entryRuleEG() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:370:1: ( ruleEG EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:371:1: ruleEG EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEGRule()); 
            }
            pushFollow(FOLLOW_ruleEG_in_entryRuleEG727);
            ruleEG();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEGRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEG734); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEG"


    // $ANTLR start "ruleEG"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:378:1: ruleEG : ( ( rule__EG__Group__0 ) ) ;
    public final void ruleEG() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:382:2: ( ( ( rule__EG__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:383:1: ( ( rule__EG__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:383:1: ( ( rule__EG__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:384:1: ( rule__EG__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEGAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:385:1: ( rule__EG__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:385:2: rule__EG__Group__0
            {
            pushFollow(FOLLOW_rule__EG__Group__0_in_ruleEG760);
            rule__EG__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEGAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEG"


    // $ANTLR start "entryRuleEU"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:397:1: entryRuleEU : ruleEU EOF ;
    public final void entryRuleEU() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:398:1: ( ruleEU EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:399:1: ruleEU EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEURule()); 
            }
            pushFollow(FOLLOW_ruleEU_in_entryRuleEU787);
            ruleEU();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEURule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEU794); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEU"


    // $ANTLR start "ruleEU"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:406:1: ruleEU : ( ( rule__EU__Group__0 ) ) ;
    public final void ruleEU() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:410:2: ( ( ( rule__EU__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:411:1: ( ( rule__EU__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:411:1: ( ( rule__EU__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:412:1: ( rule__EU__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEUAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:413:1: ( rule__EU__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:413:2: rule__EU__Group__0
            {
            pushFollow(FOLLOW_rule__EU__Group__0_in_ruleEU820);
            rule__EU__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEUAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEU"


    // $ANTLR start "entryRuleAND"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:425:1: entryRuleAND : ruleAND EOF ;
    public final void entryRuleAND() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:426:1: ( ruleAND EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:427:1: ruleAND EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getANDRule()); 
            }
            pushFollow(FOLLOW_ruleAND_in_entryRuleAND847);
            ruleAND();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getANDRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAND854); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAND"


    // $ANTLR start "ruleAND"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:434:1: ruleAND : ( ( rule__AND__Group__0 ) ) ;
    public final void ruleAND() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:438:2: ( ( ( rule__AND__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:439:1: ( ( rule__AND__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:439:1: ( ( rule__AND__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:440:1: ( rule__AND__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getANDAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:441:1: ( rule__AND__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:441:2: rule__AND__Group__0
            {
            pushFollow(FOLLOW_rule__AND__Group__0_in_ruleAND880);
            rule__AND__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getANDAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAND"


    // $ANTLR start "entryRuleOR"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:453:1: entryRuleOR : ruleOR EOF ;
    public final void entryRuleOR() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:454:1: ( ruleOR EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:455:1: ruleOR EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getORRule()); 
            }
            pushFollow(FOLLOW_ruleOR_in_entryRuleOR907);
            ruleOR();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getORRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOR914); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOR"


    // $ANTLR start "ruleOR"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:462:1: ruleOR : ( ( rule__OR__Group__0 ) ) ;
    public final void ruleOR() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:466:2: ( ( ( rule__OR__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:467:1: ( ( rule__OR__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:467:1: ( ( rule__OR__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:468:1: ( rule__OR__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getORAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:469:1: ( rule__OR__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:469:2: rule__OR__Group__0
            {
            pushFollow(FOLLOW_rule__OR__Group__0_in_ruleOR940);
            rule__OR__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getORAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOR"


    // $ANTLR start "entryRuleNOT"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:481:1: entryRuleNOT : ruleNOT EOF ;
    public final void entryRuleNOT() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:482:1: ( ruleNOT EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:483:1: ruleNOT EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNOTRule()); 
            }
            pushFollow(FOLLOW_ruleNOT_in_entryRuleNOT967);
            ruleNOT();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNOTRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNOT974); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNOT"


    // $ANTLR start "ruleNOT"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:490:1: ruleNOT : ( ( rule__NOT__Group__0 ) ) ;
    public final void ruleNOT() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:494:2: ( ( ( rule__NOT__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:495:1: ( ( rule__NOT__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:495:1: ( ( rule__NOT__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:496:1: ( rule__NOT__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNOTAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:497:1: ( rule__NOT__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:497:2: rule__NOT__Group__0
            {
            pushFollow(FOLLOW_rule__NOT__Group__0_in_ruleNOT1000);
            rule__NOT__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNOTAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNOT"


    // $ANTLR start "entryRuleProposition"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:509:1: entryRuleProposition : ruleProposition EOF ;
    public final void entryRuleProposition() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:510:1: ( ruleProposition EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:511:1: ruleProposition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPropositionRule()); 
            }
            pushFollow(FOLLOW_ruleProposition_in_entryRuleProposition1027);
            ruleProposition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPropositionRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleProposition1034); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProposition"


    // $ANTLR start "ruleProposition"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:518:1: ruleProposition : ( ( rule__Proposition__AtomicPropositionAssignment ) ) ;
    public final void ruleProposition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:522:2: ( ( ( rule__Proposition__AtomicPropositionAssignment ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:523:1: ( ( rule__Proposition__AtomicPropositionAssignment ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:523:1: ( ( rule__Proposition__AtomicPropositionAssignment ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:524:1: ( rule__Proposition__AtomicPropositionAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPropositionAccess().getAtomicPropositionAssignment()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:525:1: ( rule__Proposition__AtomicPropositionAssignment )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:525:2: rule__Proposition__AtomicPropositionAssignment
            {
            pushFollow(FOLLOW_rule__Proposition__AtomicPropositionAssignment_in_ruleProposition1060);
            rule__Proposition__AtomicPropositionAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPropositionAccess().getAtomicPropositionAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProposition"


    // $ANTLR start "entryRuleConstant"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:537:1: entryRuleConstant : ruleConstant EOF ;
    public final void entryRuleConstant() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:538:1: ( ruleConstant EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:539:1: ruleConstant EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstantRule()); 
            }
            pushFollow(FOLLOW_ruleConstant_in_entryRuleConstant1087);
            ruleConstant();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstantRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleConstant1094); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:546:1: ruleConstant : ( ( rule__Constant__Group__0 ) ) ;
    public final void ruleConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:550:2: ( ( ( rule__Constant__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:551:1: ( ( rule__Constant__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:551:1: ( ( rule__Constant__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:552:1: ( rule__Constant__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstantAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:553:1: ( rule__Constant__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:553:2: rule__Constant__Group__0
            {
            pushFollow(FOLLOW_rule__Constant__Group__0_in_ruleConstant1120);
            rule__Constant__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstantAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "entryRuleCheckSet"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:565:1: entryRuleCheckSet : ruleCheckSet EOF ;
    public final void entryRuleCheckSet() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:566:1: ( ruleCheckSet EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:567:1: ruleCheckSet EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckSetRule()); 
            }
            pushFollow(FOLLOW_ruleCheckSet_in_entryRuleCheckSet1147);
            ruleCheckSet();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckSetRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheckSet1154); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCheckSet"


    // $ANTLR start "ruleCheckSet"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:574:1: ruleCheckSet : ( ( rule__CheckSet__Group__0 ) ) ;
    public final void ruleCheckSet() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:578:2: ( ( ( rule__CheckSet__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:579:1: ( ( rule__CheckSet__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:579:1: ( ( rule__CheckSet__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:580:1: ( rule__CheckSet__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckSetAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:581:1: ( rule__CheckSet__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:581:2: rule__CheckSet__Group__0
            {
            pushFollow(FOLLOW_rule__CheckSet__Group__0_in_ruleCheckSet1180);
            rule__CheckSet__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckSetAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCheckSet"


    // $ANTLR start "entryRuleCheck"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:593:1: entryRuleCheck : ruleCheck EOF ;
    public final void entryRuleCheck() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:594:1: ( ruleCheck EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:595:1: ruleCheck EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckRule()); 
            }
            pushFollow(FOLLOW_ruleCheck_in_entryRuleCheck1207);
            ruleCheck();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheck1214); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCheck"


    // $ANTLR start "ruleCheck"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:602:1: ruleCheck : ( ( rule__Check__Group__0 ) ) ;
    public final void ruleCheck() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:606:2: ( ( ( rule__Check__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:607:1: ( ( rule__Check__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:607:1: ( ( rule__Check__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:608:1: ( rule__Check__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:609:1: ( rule__Check__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:609:2: rule__Check__Group__0
            {
            pushFollow(FOLLOW_rule__Check__Group__0_in_ruleCheck1240);
            rule__Check__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCheck"


    // $ANTLR start "entryRuleLTLFormulaSet"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:621:1: entryRuleLTLFormulaSet : ruleLTLFormulaSet EOF ;
    public final void entryRuleLTLFormulaSet() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:622:1: ( ruleLTLFormulaSet EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:623:1: ruleLTLFormulaSet EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaSetRule()); 
            }
            pushFollow(FOLLOW_ruleLTLFormulaSet_in_entryRuleLTLFormulaSet1267);
            ruleLTLFormulaSet();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaSetRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLFormulaSet1274); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLFormulaSet"


    // $ANTLR start "ruleLTLFormulaSet"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:630:1: ruleLTLFormulaSet : ( ( rule__LTLFormulaSet__Group__0 ) ) ;
    public final void ruleLTLFormulaSet() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:634:2: ( ( ( rule__LTLFormulaSet__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:635:1: ( ( rule__LTLFormulaSet__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:635:1: ( ( rule__LTLFormulaSet__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:636:1: ( rule__LTLFormulaSet__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaSetAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:637:1: ( rule__LTLFormulaSet__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:637:2: rule__LTLFormulaSet__Group__0
            {
            pushFollow(FOLLOW_rule__LTLFormulaSet__Group__0_in_ruleLTLFormulaSet1300);
            rule__LTLFormulaSet__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaSetAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLFormulaSet"


    // $ANTLR start "entryRuleLTLFormulaDefinition"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:649:1: entryRuleLTLFormulaDefinition : ruleLTLFormulaDefinition EOF ;
    public final void entryRuleLTLFormulaDefinition() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:650:1: ( ruleLTLFormulaDefinition EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:651:1: ruleLTLFormulaDefinition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaDefinitionRule()); 
            }
            pushFollow(FOLLOW_ruleLTLFormulaDefinition_in_entryRuleLTLFormulaDefinition1327);
            ruleLTLFormulaDefinition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaDefinitionRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLFormulaDefinition1334); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLFormulaDefinition"


    // $ANTLR start "ruleLTLFormulaDefinition"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:658:1: ruleLTLFormulaDefinition : ( ( rule__LTLFormulaDefinition__Group__0 ) ) ;
    public final void ruleLTLFormulaDefinition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:662:2: ( ( ( rule__LTLFormulaDefinition__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:663:1: ( ( rule__LTLFormulaDefinition__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:663:1: ( ( rule__LTLFormulaDefinition__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:664:1: ( rule__LTLFormulaDefinition__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaDefinitionAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:665:1: ( rule__LTLFormulaDefinition__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:665:2: rule__LTLFormulaDefinition__Group__0
            {
            pushFollow(FOLLOW_rule__LTLFormulaDefinition__Group__0_in_ruleLTLFormulaDefinition1360);
            rule__LTLFormulaDefinition__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaDefinitionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLFormulaDefinition"


    // $ANTLR start "entryRuleLTLFormula"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:677:1: entryRuleLTLFormula : ruleLTLFormula EOF ;
    public final void entryRuleLTLFormula() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:678:1: ( ruleLTLFormula EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:679:1: ruleLTLFormula EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaRule()); 
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_entryRuleLTLFormula1387);
            ruleLTLFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLFormula1394); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLFormula"


    // $ANTLR start "ruleLTLFormula"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:686:1: ruleLTLFormula : ( ( rule__LTLFormula__Alternatives ) ) ;
    public final void ruleLTLFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:690:2: ( ( ( rule__LTLFormula__Alternatives ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:691:1: ( ( rule__LTLFormula__Alternatives ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:691:1: ( ( rule__LTLFormula__Alternatives ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:692:1: ( rule__LTLFormula__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaAccess().getAlternatives()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:693:1: ( rule__LTLFormula__Alternatives )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:693:2: rule__LTLFormula__Alternatives
            {
            pushFollow(FOLLOW_rule__LTLFormula__Alternatives_in_ruleLTLFormula1420);
            rule__LTLFormula__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLFormula"


    // $ANTLR start "entryRuleLTLX"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:705:1: entryRuleLTLX : ruleLTLX EOF ;
    public final void entryRuleLTLX() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:706:1: ( ruleLTLX EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:707:1: ruleLTLX EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLXRule()); 
            }
            pushFollow(FOLLOW_ruleLTLX_in_entryRuleLTLX1447);
            ruleLTLX();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLXRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLX1454); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLX"


    // $ANTLR start "ruleLTLX"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:714:1: ruleLTLX : ( ( rule__LTLX__Group__0 ) ) ;
    public final void ruleLTLX() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:718:2: ( ( ( rule__LTLX__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:719:1: ( ( rule__LTLX__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:719:1: ( ( rule__LTLX__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:720:1: ( rule__LTLX__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLXAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:721:1: ( rule__LTLX__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:721:2: rule__LTLX__Group__0
            {
            pushFollow(FOLLOW_rule__LTLX__Group__0_in_ruleLTLX1480);
            rule__LTLX__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLXAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLX"


    // $ANTLR start "entryRuleLTLG"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:733:1: entryRuleLTLG : ruleLTLG EOF ;
    public final void entryRuleLTLG() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:734:1: ( ruleLTLG EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:735:1: ruleLTLG EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLGRule()); 
            }
            pushFollow(FOLLOW_ruleLTLG_in_entryRuleLTLG1507);
            ruleLTLG();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLGRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLG1514); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLG"


    // $ANTLR start "ruleLTLG"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:742:1: ruleLTLG : ( ( rule__LTLG__Group__0 ) ) ;
    public final void ruleLTLG() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:746:2: ( ( ( rule__LTLG__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:747:1: ( ( rule__LTLG__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:747:1: ( ( rule__LTLG__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:748:1: ( rule__LTLG__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLGAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:749:1: ( rule__LTLG__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:749:2: rule__LTLG__Group__0
            {
            pushFollow(FOLLOW_rule__LTLG__Group__0_in_ruleLTLG1540);
            rule__LTLG__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLGAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLG"


    // $ANTLR start "entryRuleLTLU"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:761:1: entryRuleLTLU : ruleLTLU EOF ;
    public final void entryRuleLTLU() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:762:1: ( ruleLTLU EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:763:1: ruleLTLU EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLURule()); 
            }
            pushFollow(FOLLOW_ruleLTLU_in_entryRuleLTLU1567);
            ruleLTLU();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLURule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLU1574); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLU"


    // $ANTLR start "ruleLTLU"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:770:1: ruleLTLU : ( ( rule__LTLU__Group__0 ) ) ;
    public final void ruleLTLU() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:774:2: ( ( ( rule__LTLU__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:775:1: ( ( rule__LTLU__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:775:1: ( ( rule__LTLU__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:776:1: ( rule__LTLU__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLUAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:777:1: ( rule__LTLU__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:777:2: rule__LTLU__Group__0
            {
            pushFollow(FOLLOW_rule__LTLU__Group__0_in_ruleLTLU1600);
            rule__LTLU__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLUAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLU"


    // $ANTLR start "entryRuleLTLF"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:789:1: entryRuleLTLF : ruleLTLF EOF ;
    public final void entryRuleLTLF() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:790:1: ( ruleLTLF EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:791:1: ruleLTLF EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFRule()); 
            }
            pushFollow(FOLLOW_ruleLTLF_in_entryRuleLTLF1627);
            ruleLTLF();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLF1634); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLF"


    // $ANTLR start "ruleLTLF"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:798:1: ruleLTLF : ( ( rule__LTLF__Group__0 ) ) ;
    public final void ruleLTLF() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:802:2: ( ( ( rule__LTLF__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:803:1: ( ( rule__LTLF__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:803:1: ( ( rule__LTLF__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:804:1: ( rule__LTLF__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:805:1: ( rule__LTLF__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:805:2: rule__LTLF__Group__0
            {
            pushFollow(FOLLOW_rule__LTLF__Group__0_in_ruleLTLF1660);
            rule__LTLF__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLF"


    // $ANTLR start "entryRuleLTLNOT"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:817:1: entryRuleLTLNOT : ruleLTLNOT EOF ;
    public final void entryRuleLTLNOT() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:818:1: ( ruleLTLNOT EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:819:1: ruleLTLNOT EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLNOTRule()); 
            }
            pushFollow(FOLLOW_ruleLTLNOT_in_entryRuleLTLNOT1687);
            ruleLTLNOT();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLNOTRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLNOT1694); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLNOT"


    // $ANTLR start "ruleLTLNOT"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:826:1: ruleLTLNOT : ( ( rule__LTLNOT__Group__0 ) ) ;
    public final void ruleLTLNOT() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:830:2: ( ( ( rule__LTLNOT__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:831:1: ( ( rule__LTLNOT__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:831:1: ( ( rule__LTLNOT__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:832:1: ( rule__LTLNOT__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLNOTAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:833:1: ( rule__LTLNOT__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:833:2: rule__LTLNOT__Group__0
            {
            pushFollow(FOLLOW_rule__LTLNOT__Group__0_in_ruleLTLNOT1720);
            rule__LTLNOT__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLNOTAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLNOT"


    // $ANTLR start "entryRuleLTLAND"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:845:1: entryRuleLTLAND : ruleLTLAND EOF ;
    public final void entryRuleLTLAND() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:846:1: ( ruleLTLAND EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:847:1: ruleLTLAND EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLANDRule()); 
            }
            pushFollow(FOLLOW_ruleLTLAND_in_entryRuleLTLAND1747);
            ruleLTLAND();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLANDRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLAND1754); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLAND"


    // $ANTLR start "ruleLTLAND"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:854:1: ruleLTLAND : ( ( rule__LTLAND__Group__0 ) ) ;
    public final void ruleLTLAND() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:858:2: ( ( ( rule__LTLAND__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:859:1: ( ( rule__LTLAND__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:859:1: ( ( rule__LTLAND__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:860:1: ( rule__LTLAND__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLANDAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:861:1: ( rule__LTLAND__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:861:2: rule__LTLAND__Group__0
            {
            pushFollow(FOLLOW_rule__LTLAND__Group__0_in_ruleLTLAND1780);
            rule__LTLAND__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLANDAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLAND"


    // $ANTLR start "entryRuleLTLOR"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:873:1: entryRuleLTLOR : ruleLTLOR EOF ;
    public final void entryRuleLTLOR() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:874:1: ( ruleLTLOR EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:875:1: ruleLTLOR EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLORRule()); 
            }
            pushFollow(FOLLOW_ruleLTLOR_in_entryRuleLTLOR1807);
            ruleLTLOR();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLORRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLOR1814); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLOR"


    // $ANTLR start "ruleLTLOR"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:882:1: ruleLTLOR : ( ( rule__LTLOR__Group__0 ) ) ;
    public final void ruleLTLOR() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:886:2: ( ( ( rule__LTLOR__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:887:1: ( ( rule__LTLOR__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:887:1: ( ( rule__LTLOR__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:888:1: ( rule__LTLOR__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLORAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:889:1: ( rule__LTLOR__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:889:2: rule__LTLOR__Group__0
            {
            pushFollow(FOLLOW_rule__LTLOR__Group__0_in_ruleLTLOR1840);
            rule__LTLOR__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLORAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLOR"


    // $ANTLR start "entryRuleLTLProposition"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:901:1: entryRuleLTLProposition : ruleLTLProposition EOF ;
    public final void entryRuleLTLProposition() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:902:1: ( ruleLTLProposition EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:903:1: ruleLTLProposition EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLPropositionRule()); 
            }
            pushFollow(FOLLOW_ruleLTLProposition_in_entryRuleLTLProposition1867);
            ruleLTLProposition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLPropositionRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLProposition1874); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLProposition"


    // $ANTLR start "ruleLTLProposition"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:910:1: ruleLTLProposition : ( ( rule__LTLProposition__AtomicPropositionAssignment ) ) ;
    public final void ruleLTLProposition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:914:2: ( ( ( rule__LTLProposition__AtomicPropositionAssignment ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:915:1: ( ( rule__LTLProposition__AtomicPropositionAssignment ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:915:1: ( ( rule__LTLProposition__AtomicPropositionAssignment ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:916:1: ( rule__LTLProposition__AtomicPropositionAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLPropositionAccess().getAtomicPropositionAssignment()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:917:1: ( rule__LTLProposition__AtomicPropositionAssignment )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:917:2: rule__LTLProposition__AtomicPropositionAssignment
            {
            pushFollow(FOLLOW_rule__LTLProposition__AtomicPropositionAssignment_in_ruleLTLProposition1900);
            rule__LTLProposition__AtomicPropositionAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLPropositionAccess().getAtomicPropositionAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLProposition"


    // $ANTLR start "entryRuleLTLConstant"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:929:1: entryRuleLTLConstant : ruleLTLConstant EOF ;
    public final void entryRuleLTLConstant() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:930:1: ( ruleLTLConstant EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:931:1: ruleLTLConstant EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLConstantRule()); 
            }
            pushFollow(FOLLOW_ruleLTLConstant_in_entryRuleLTLConstant1927);
            ruleLTLConstant();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLConstantRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLConstant1934); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLConstant"


    // $ANTLR start "ruleLTLConstant"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:938:1: ruleLTLConstant : ( ( rule__LTLConstant__Group__0 ) ) ;
    public final void ruleLTLConstant() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:942:2: ( ( ( rule__LTLConstant__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:943:1: ( ( rule__LTLConstant__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:943:1: ( ( rule__LTLConstant__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:944:1: ( rule__LTLConstant__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLConstantAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:945:1: ( rule__LTLConstant__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:945:2: rule__LTLConstant__Group__0
            {
            pushFollow(FOLLOW_rule__LTLConstant__Group__0_in_ruleLTLConstant1960);
            rule__LTLConstant__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLConstantAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLConstant"


    // $ANTLR start "entryRuleLTLCheckSet"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:957:1: entryRuleLTLCheckSet : ruleLTLCheckSet EOF ;
    public final void entryRuleLTLCheckSet() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:958:1: ( ruleLTLCheckSet EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:959:1: ruleLTLCheckSet EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckSetRule()); 
            }
            pushFollow(FOLLOW_ruleLTLCheckSet_in_entryRuleLTLCheckSet1987);
            ruleLTLCheckSet();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckSetRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLCheckSet1994); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLCheckSet"


    // $ANTLR start "ruleLTLCheckSet"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:966:1: ruleLTLCheckSet : ( ( rule__LTLCheckSet__Group__0 ) ) ;
    public final void ruleLTLCheckSet() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:970:2: ( ( ( rule__LTLCheckSet__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:971:1: ( ( rule__LTLCheckSet__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:971:1: ( ( rule__LTLCheckSet__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:972:1: ( rule__LTLCheckSet__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckSetAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:973:1: ( rule__LTLCheckSet__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:973:2: rule__LTLCheckSet__Group__0
            {
            pushFollow(FOLLOW_rule__LTLCheckSet__Group__0_in_ruleLTLCheckSet2020);
            rule__LTLCheckSet__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckSetAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLCheckSet"


    // $ANTLR start "entryRuleLTLCheck"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:985:1: entryRuleLTLCheck : ruleLTLCheck EOF ;
    public final void entryRuleLTLCheck() throws RecognitionException {
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:986:1: ( ruleLTLCheck EOF )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:987:1: ruleLTLCheck EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckRule()); 
            }
            pushFollow(FOLLOW_ruleLTLCheck_in_entryRuleLTLCheck2047);
            ruleLTLCheck();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLCheck2054); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLTLCheck"


    // $ANTLR start "ruleLTLCheck"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:994:1: ruleLTLCheck : ( ( rule__LTLCheck__Group__0 ) ) ;
    public final void ruleLTLCheck() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:998:2: ( ( ( rule__LTLCheck__Group__0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:999:1: ( ( rule__LTLCheck__Group__0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:999:1: ( ( rule__LTLCheck__Group__0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1000:1: ( rule__LTLCheck__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckAccess().getGroup()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1001:1: ( rule__LTLCheck__Group__0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1001:2: rule__LTLCheck__Group__0
            {
            pushFollow(FOLLOW_rule__LTLCheck__Group__0_in_ruleLTLCheck2080);
            rule__LTLCheck__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLTLCheck"


    // $ANTLR start "rule__Document__LtssAlternatives_0_0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1013:1: rule__Document__LtssAlternatives_0_0 : ( ( ruleLTS ) | ( ruleComposedLTS ) );
    public final void rule__Document__LtssAlternatives_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1017:1: ( ( ruleLTS ) | ( ruleComposedLTS ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==12) ) {
                alt1=1;
            }
            else if ( (LA1_0==23) ) {
                alt1=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1018:1: ( ruleLTS )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1018:1: ( ruleLTS )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1019:1: ruleLTS
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getDocumentAccess().getLtssLTSParserRuleCall_0_0_0()); 
                    }
                    pushFollow(FOLLOW_ruleLTS_in_rule__Document__LtssAlternatives_0_02116);
                    ruleLTS();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getDocumentAccess().getLtssLTSParserRuleCall_0_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1024:6: ( ruleComposedLTS )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1024:6: ( ruleComposedLTS )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1025:1: ruleComposedLTS
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getDocumentAccess().getLtssComposedLTSParserRuleCall_0_0_1()); 
                    }
                    pushFollow(FOLLOW_ruleComposedLTS_in_rule__Document__LtssAlternatives_0_02133);
                    ruleComposedLTS();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getDocumentAccess().getLtssComposedLTSParserRuleCall_0_0_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__LtssAlternatives_0_0"


    // $ANTLR start "rule__Formula__Alternatives"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1035:1: rule__Formula__Alternatives : ( ( ruleEX ) | ( ruleEG ) | ( ruleEU ) | ( ruleNOT ) | ( ( ruleAND ) ) | ( ruleOR ) | ( ruleProposition ) | ( ruleConstant ) );
    public final void rule__Formula__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1039:1: ( ( ruleEX ) | ( ruleEG ) | ( ruleEU ) | ( ruleNOT ) | ( ( ruleAND ) ) | ( ruleOR ) | ( ruleProposition ) | ( ruleConstant ) )
            int alt2=8;
            alt2 = dfa2.predict(input);
            switch (alt2) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1040:1: ( ruleEX )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1040:1: ( ruleEX )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1041:1: ruleEX
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFormulaAccess().getEXParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_ruleEX_in_rule__Formula__Alternatives2165);
                    ruleEX();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFormulaAccess().getEXParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1046:6: ( ruleEG )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1046:6: ( ruleEG )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1047:1: ruleEG
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFormulaAccess().getEGParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_ruleEG_in_rule__Formula__Alternatives2182);
                    ruleEG();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFormulaAccess().getEGParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1052:6: ( ruleEU )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1052:6: ( ruleEU )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1053:1: ruleEU
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFormulaAccess().getEUParserRuleCall_2()); 
                    }
                    pushFollow(FOLLOW_ruleEU_in_rule__Formula__Alternatives2199);
                    ruleEU();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFormulaAccess().getEUParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1058:6: ( ruleNOT )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1058:6: ( ruleNOT )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1059:1: ruleNOT
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFormulaAccess().getNOTParserRuleCall_3()); 
                    }
                    pushFollow(FOLLOW_ruleNOT_in_rule__Formula__Alternatives2216);
                    ruleNOT();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFormulaAccess().getNOTParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1064:6: ( ( ruleAND ) )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1064:6: ( ( ruleAND ) )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1065:1: ( ruleAND )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFormulaAccess().getANDParserRuleCall_4()); 
                    }
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1066:1: ( ruleAND )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1066:3: ruleAND
                    {
                    pushFollow(FOLLOW_ruleAND_in_rule__Formula__Alternatives2234);
                    ruleAND();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFormulaAccess().getANDParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1070:6: ( ruleOR )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1070:6: ( ruleOR )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1071:1: ruleOR
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFormulaAccess().getORParserRuleCall_5()); 
                    }
                    pushFollow(FOLLOW_ruleOR_in_rule__Formula__Alternatives2252);
                    ruleOR();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFormulaAccess().getORParserRuleCall_5()); 
                    }

                    }


                    }
                    break;
                case 7 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1076:6: ( ruleProposition )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1076:6: ( ruleProposition )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1077:1: ruleProposition
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFormulaAccess().getPropositionParserRuleCall_6()); 
                    }
                    pushFollow(FOLLOW_ruleProposition_in_rule__Formula__Alternatives2269);
                    ruleProposition();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFormulaAccess().getPropositionParserRuleCall_6()); 
                    }

                    }


                    }
                    break;
                case 8 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1082:6: ( ruleConstant )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1082:6: ( ruleConstant )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1083:1: ruleConstant
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getFormulaAccess().getConstantParserRuleCall_7()); 
                    }
                    pushFollow(FOLLOW_ruleConstant_in_rule__Formula__Alternatives2286);
                    ruleConstant();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getFormulaAccess().getConstantParserRuleCall_7()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Alternatives"


    // $ANTLR start "rule__Constant__Alternatives_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1093:1: rule__Constant__Alternatives_1 : ( ( ( rule__Constant__ValueAssignment_1_0 ) ) | ( 'false' ) );
    public final void rule__Constant__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1097:1: ( ( ( rule__Constant__ValueAssignment_1_0 ) ) | ( 'false' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==48) ) {
                alt3=1;
            }
            else if ( (LA3_0==11) ) {
                alt3=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1098:1: ( ( rule__Constant__ValueAssignment_1_0 ) )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1098:1: ( ( rule__Constant__ValueAssignment_1_0 ) )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1099:1: ( rule__Constant__ValueAssignment_1_0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConstantAccess().getValueAssignment_1_0()); 
                    }
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1100:1: ( rule__Constant__ValueAssignment_1_0 )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1100:2: rule__Constant__ValueAssignment_1_0
                    {
                    pushFollow(FOLLOW_rule__Constant__ValueAssignment_1_0_in_rule__Constant__Alternatives_12318);
                    rule__Constant__ValueAssignment_1_0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConstantAccess().getValueAssignment_1_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1104:6: ( 'false' )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1104:6: ( 'false' )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1105:1: 'false'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getConstantAccess().getFalseKeyword_1_1()); 
                    }
                    match(input,11,FOLLOW_11_in_rule__Constant__Alternatives_12337); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getConstantAccess().getFalseKeyword_1_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Alternatives_1"


    // $ANTLR start "rule__LTLFormula__Alternatives"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1117:1: rule__LTLFormula__Alternatives : ( ( ruleLTLX ) | ( ruleLTLG ) | ( ruleLTLU ) | ( ruleLTLF ) | ( ruleLTLNOT ) | ( ( ruleLTLAND ) ) | ( ruleLTLOR ) | ( ruleLTLProposition ) | ( ruleLTLConstant ) );
    public final void rule__LTLFormula__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1121:1: ( ( ruleLTLX ) | ( ruleLTLG ) | ( ruleLTLU ) | ( ruleLTLF ) | ( ruleLTLNOT ) | ( ( ruleLTLAND ) ) | ( ruleLTLOR ) | ( ruleLTLProposition ) | ( ruleLTLConstant ) )
            int alt4=9;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1122:1: ( ruleLTLX )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1122:1: ( ruleLTLX )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1123:1: ruleLTLX
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLTLFormulaAccess().getLTLXParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_ruleLTLX_in_rule__LTLFormula__Alternatives2371);
                    ruleLTLX();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLTLFormulaAccess().getLTLXParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1128:6: ( ruleLTLG )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1128:6: ( ruleLTLG )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1129:1: ruleLTLG
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLTLFormulaAccess().getLTLGParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_ruleLTLG_in_rule__LTLFormula__Alternatives2388);
                    ruleLTLG();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLTLFormulaAccess().getLTLGParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1134:6: ( ruleLTLU )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1134:6: ( ruleLTLU )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1135:1: ruleLTLU
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLTLFormulaAccess().getLTLUParserRuleCall_2()); 
                    }
                    pushFollow(FOLLOW_ruleLTLU_in_rule__LTLFormula__Alternatives2405);
                    ruleLTLU();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLTLFormulaAccess().getLTLUParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1140:6: ( ruleLTLF )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1140:6: ( ruleLTLF )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1141:1: ruleLTLF
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLTLFormulaAccess().getLTLFParserRuleCall_3()); 
                    }
                    pushFollow(FOLLOW_ruleLTLF_in_rule__LTLFormula__Alternatives2422);
                    ruleLTLF();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLTLFormulaAccess().getLTLFParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1146:6: ( ruleLTLNOT )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1146:6: ( ruleLTLNOT )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1147:1: ruleLTLNOT
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLTLFormulaAccess().getLTLNOTParserRuleCall_4()); 
                    }
                    pushFollow(FOLLOW_ruleLTLNOT_in_rule__LTLFormula__Alternatives2439);
                    ruleLTLNOT();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLTLFormulaAccess().getLTLNOTParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1152:6: ( ( ruleLTLAND ) )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1152:6: ( ( ruleLTLAND ) )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1153:1: ( ruleLTLAND )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLTLFormulaAccess().getLTLANDParserRuleCall_5()); 
                    }
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1154:1: ( ruleLTLAND )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1154:3: ruleLTLAND
                    {
                    pushFollow(FOLLOW_ruleLTLAND_in_rule__LTLFormula__Alternatives2457);
                    ruleLTLAND();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLTLFormulaAccess().getLTLANDParserRuleCall_5()); 
                    }

                    }


                    }
                    break;
                case 7 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1158:6: ( ruleLTLOR )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1158:6: ( ruleLTLOR )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1159:1: ruleLTLOR
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLTLFormulaAccess().getLTLORParserRuleCall_6()); 
                    }
                    pushFollow(FOLLOW_ruleLTLOR_in_rule__LTLFormula__Alternatives2475);
                    ruleLTLOR();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLTLFormulaAccess().getLTLORParserRuleCall_6()); 
                    }

                    }


                    }
                    break;
                case 8 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1164:6: ( ruleLTLProposition )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1164:6: ( ruleLTLProposition )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1165:1: ruleLTLProposition
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLTLFormulaAccess().getLTLPropositionParserRuleCall_7()); 
                    }
                    pushFollow(FOLLOW_ruleLTLProposition_in_rule__LTLFormula__Alternatives2492);
                    ruleLTLProposition();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLTLFormulaAccess().getLTLPropositionParserRuleCall_7()); 
                    }

                    }


                    }
                    break;
                case 9 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1170:6: ( ruleLTLConstant )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1170:6: ( ruleLTLConstant )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1171:1: ruleLTLConstant
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLTLFormulaAccess().getLTLConstantParserRuleCall_8()); 
                    }
                    pushFollow(FOLLOW_ruleLTLConstant_in_rule__LTLFormula__Alternatives2509);
                    ruleLTLConstant();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLTLFormulaAccess().getLTLConstantParserRuleCall_8()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormula__Alternatives"


    // $ANTLR start "rule__LTLConstant__Alternatives_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1181:1: rule__LTLConstant__Alternatives_1 : ( ( ( rule__LTLConstant__ValueAssignment_1_0 ) ) | ( 'false' ) );
    public final void rule__LTLConstant__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1185:1: ( ( ( rule__LTLConstant__ValueAssignment_1_0 ) ) | ( 'false' ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==48) ) {
                alt5=1;
            }
            else if ( (LA5_0==11) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1186:1: ( ( rule__LTLConstant__ValueAssignment_1_0 ) )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1186:1: ( ( rule__LTLConstant__ValueAssignment_1_0 ) )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1187:1: ( rule__LTLConstant__ValueAssignment_1_0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLTLConstantAccess().getValueAssignment_1_0()); 
                    }
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1188:1: ( rule__LTLConstant__ValueAssignment_1_0 )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1188:2: rule__LTLConstant__ValueAssignment_1_0
                    {
                    pushFollow(FOLLOW_rule__LTLConstant__ValueAssignment_1_0_in_rule__LTLConstant__Alternatives_12541);
                    rule__LTLConstant__ValueAssignment_1_0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLTLConstantAccess().getValueAssignment_1_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1192:6: ( 'false' )
                    {
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1192:6: ( 'false' )
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1193:1: 'false'
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLTLConstantAccess().getFalseKeyword_1_1()); 
                    }
                    match(input,11,FOLLOW_11_in_rule__LTLConstant__Alternatives_12560); if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLTLConstantAccess().getFalseKeyword_1_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLConstant__Alternatives_1"


    // $ANTLR start "rule__Document__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1207:1: rule__Document__Group__0 : rule__Document__Group__0__Impl rule__Document__Group__1 ;
    public final void rule__Document__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1211:1: ( rule__Document__Group__0__Impl rule__Document__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1212:2: rule__Document__Group__0__Impl rule__Document__Group__1
            {
            pushFollow(FOLLOW_rule__Document__Group__0__Impl_in_rule__Document__Group__02592);
            rule__Document__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Document__Group__1_in_rule__Document__Group__02595);
            rule__Document__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__0"


    // $ANTLR start "rule__Document__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1219:1: rule__Document__Group__0__Impl : ( ( rule__Document__LtssAssignment_0 )* ) ;
    public final void rule__Document__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1223:1: ( ( ( rule__Document__LtssAssignment_0 )* ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1224:1: ( ( rule__Document__LtssAssignment_0 )* )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1224:1: ( ( rule__Document__LtssAssignment_0 )* )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1225:1: ( rule__Document__LtssAssignment_0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getLtssAssignment_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1226:1: ( rule__Document__LtssAssignment_0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==12||LA6_0==23) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1226:2: rule__Document__LtssAssignment_0
            	    {
            	    pushFollow(FOLLOW_rule__Document__LtssAssignment_0_in_rule__Document__Group__0__Impl2622);
            	    rule__Document__LtssAssignment_0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getLtssAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__0__Impl"


    // $ANTLR start "rule__Document__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1236:1: rule__Document__Group__1 : rule__Document__Group__1__Impl rule__Document__Group__2 ;
    public final void rule__Document__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1240:1: ( rule__Document__Group__1__Impl rule__Document__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1241:2: rule__Document__Group__1__Impl rule__Document__Group__2
            {
            pushFollow(FOLLOW_rule__Document__Group__1__Impl_in_rule__Document__Group__12653);
            rule__Document__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Document__Group__2_in_rule__Document__Group__12656);
            rule__Document__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__1"


    // $ANTLR start "rule__Document__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1248:1: rule__Document__Group__1__Impl : ( ( rule__Document__FormulaSetAssignment_1 )? ) ;
    public final void rule__Document__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1252:1: ( ( ( rule__Document__FormulaSetAssignment_1 )? ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1253:1: ( ( rule__Document__FormulaSetAssignment_1 )? )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1253:1: ( ( rule__Document__FormulaSetAssignment_1 )? )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1254:1: ( rule__Document__FormulaSetAssignment_1 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getFormulaSetAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1255:1: ( rule__Document__FormulaSetAssignment_1 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==27) ) {
                int LA7_1 = input.LA(2);

                if ( (LA7_1==28) ) {
                    alt7=1;
                }
            }
            switch (alt7) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1255:2: rule__Document__FormulaSetAssignment_1
                    {
                    pushFollow(FOLLOW_rule__Document__FormulaSetAssignment_1_in_rule__Document__Group__1__Impl2683);
                    rule__Document__FormulaSetAssignment_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getFormulaSetAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__1__Impl"


    // $ANTLR start "rule__Document__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1265:1: rule__Document__Group__2 : rule__Document__Group__2__Impl rule__Document__Group__3 ;
    public final void rule__Document__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1269:1: ( rule__Document__Group__2__Impl rule__Document__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1270:2: rule__Document__Group__2__Impl rule__Document__Group__3
            {
            pushFollow(FOLLOW_rule__Document__Group__2__Impl_in_rule__Document__Group__22714);
            rule__Document__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Document__Group__3_in_rule__Document__Group__22717);
            rule__Document__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__2"


    // $ANTLR start "rule__Document__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1277:1: rule__Document__Group__2__Impl : ( ( rule__Document__CheckSetAssignment_2 )? ) ;
    public final void rule__Document__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1281:1: ( ( ( rule__Document__CheckSetAssignment_2 )? ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1282:1: ( ( rule__Document__CheckSetAssignment_2 )? )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1282:1: ( ( rule__Document__CheckSetAssignment_2 )? )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1283:1: ( rule__Document__CheckSetAssignment_2 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getCheckSetAssignment_2()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1284:1: ( rule__Document__CheckSetAssignment_2 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==27) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1284:2: rule__Document__CheckSetAssignment_2
                    {
                    pushFollow(FOLLOW_rule__Document__CheckSetAssignment_2_in_rule__Document__Group__2__Impl2744);
                    rule__Document__CheckSetAssignment_2();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getCheckSetAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__2__Impl"


    // $ANTLR start "rule__Document__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1294:1: rule__Document__Group__3 : rule__Document__Group__3__Impl rule__Document__Group__4 ;
    public final void rule__Document__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1298:1: ( rule__Document__Group__3__Impl rule__Document__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1299:2: rule__Document__Group__3__Impl rule__Document__Group__4
            {
            pushFollow(FOLLOW_rule__Document__Group__3__Impl_in_rule__Document__Group__32775);
            rule__Document__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Document__Group__4_in_rule__Document__Group__32778);
            rule__Document__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__3"


    // $ANTLR start "rule__Document__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1306:1: rule__Document__Group__3__Impl : ( ( rule__Document__LTLformulaSetAssignment_3 )? ) ;
    public final void rule__Document__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1310:1: ( ( ( rule__Document__LTLformulaSetAssignment_3 )? ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1311:1: ( ( rule__Document__LTLformulaSetAssignment_3 )? )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1311:1: ( ( rule__Document__LTLformulaSetAssignment_3 )? )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1312:1: ( rule__Document__LTLformulaSetAssignment_3 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getLTLformulaSetAssignment_3()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1313:1: ( rule__Document__LTLformulaSetAssignment_3 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==44) ) {
                int LA9_1 = input.LA(2);

                if ( (LA9_1==28) ) {
                    alt9=1;
                }
            }
            switch (alt9) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1313:2: rule__Document__LTLformulaSetAssignment_3
                    {
                    pushFollow(FOLLOW_rule__Document__LTLformulaSetAssignment_3_in_rule__Document__Group__3__Impl2805);
                    rule__Document__LTLformulaSetAssignment_3();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getLTLformulaSetAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__3__Impl"


    // $ANTLR start "rule__Document__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1323:1: rule__Document__Group__4 : rule__Document__Group__4__Impl ;
    public final void rule__Document__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1327:1: ( rule__Document__Group__4__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1328:2: rule__Document__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Document__Group__4__Impl_in_rule__Document__Group__42836);
            rule__Document__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__4"


    // $ANTLR start "rule__Document__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1334:1: rule__Document__Group__4__Impl : ( ( rule__Document__LTLcheckSetAssignment_4 )? ) ;
    public final void rule__Document__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1338:1: ( ( ( rule__Document__LTLcheckSetAssignment_4 )? ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1339:1: ( ( rule__Document__LTLcheckSetAssignment_4 )? )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1339:1: ( ( rule__Document__LTLcheckSetAssignment_4 )? )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1340:1: ( rule__Document__LTLcheckSetAssignment_4 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getLTLcheckSetAssignment_4()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1341:1: ( rule__Document__LTLcheckSetAssignment_4 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==44) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1341:2: rule__Document__LTLcheckSetAssignment_4
                    {
                    pushFollow(FOLLOW_rule__Document__LTLcheckSetAssignment_4_in_rule__Document__Group__4__Impl2863);
                    rule__Document__LTLcheckSetAssignment_4();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getLTLcheckSetAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__Group__4__Impl"


    // $ANTLR start "rule__LTS__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1361:1: rule__LTS__Group__0 : rule__LTS__Group__0__Impl rule__LTS__Group__1 ;
    public final void rule__LTS__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1365:1: ( rule__LTS__Group__0__Impl rule__LTS__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1366:2: rule__LTS__Group__0__Impl rule__LTS__Group__1
            {
            pushFollow(FOLLOW_rule__LTS__Group__0__Impl_in_rule__LTS__Group__02904);
            rule__LTS__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group__1_in_rule__LTS__Group__02907);
            rule__LTS__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__0"


    // $ANTLR start "rule__LTS__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1373:1: rule__LTS__Group__0__Impl : ( 'LTS' ) ;
    public final void rule__LTS__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1377:1: ( ( 'LTS' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1378:1: ( 'LTS' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1378:1: ( 'LTS' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1379:1: 'LTS'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getLTSKeyword_0()); 
            }
            match(input,12,FOLLOW_12_in_rule__LTS__Group__0__Impl2935); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getLTSKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__0__Impl"


    // $ANTLR start "rule__LTS__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1392:1: rule__LTS__Group__1 : rule__LTS__Group__1__Impl rule__LTS__Group__2 ;
    public final void rule__LTS__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1396:1: ( rule__LTS__Group__1__Impl rule__LTS__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1397:2: rule__LTS__Group__1__Impl rule__LTS__Group__2
            {
            pushFollow(FOLLOW_rule__LTS__Group__1__Impl_in_rule__LTS__Group__12966);
            rule__LTS__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group__2_in_rule__LTS__Group__12969);
            rule__LTS__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__1"


    // $ANTLR start "rule__LTS__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1404:1: rule__LTS__Group__1__Impl : ( ( rule__LTS__NameAssignment_1 ) ) ;
    public final void rule__LTS__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1408:1: ( ( ( rule__LTS__NameAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1409:1: ( ( rule__LTS__NameAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1409:1: ( ( rule__LTS__NameAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1410:1: ( rule__LTS__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getNameAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1411:1: ( rule__LTS__NameAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1411:2: rule__LTS__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__LTS__NameAssignment_1_in_rule__LTS__Group__1__Impl2996);
            rule__LTS__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__1__Impl"


    // $ANTLR start "rule__LTS__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1421:1: rule__LTS__Group__2 : rule__LTS__Group__2__Impl rule__LTS__Group__3 ;
    public final void rule__LTS__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1425:1: ( rule__LTS__Group__2__Impl rule__LTS__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1426:2: rule__LTS__Group__2__Impl rule__LTS__Group__3
            {
            pushFollow(FOLLOW_rule__LTS__Group__2__Impl_in_rule__LTS__Group__23026);
            rule__LTS__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group__3_in_rule__LTS__Group__23029);
            rule__LTS__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__2"


    // $ANTLR start "rule__LTS__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1433:1: rule__LTS__Group__2__Impl : ( '{' ) ;
    public final void rule__LTS__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1437:1: ( ( '{' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1438:1: ( '{' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1438:1: ( '{' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1439:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getLeftCurlyBracketKeyword_2()); 
            }
            match(input,13,FOLLOW_13_in_rule__LTS__Group__2__Impl3057); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getLeftCurlyBracketKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__2__Impl"


    // $ANTLR start "rule__LTS__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1452:1: rule__LTS__Group__3 : rule__LTS__Group__3__Impl rule__LTS__Group__4 ;
    public final void rule__LTS__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1456:1: ( rule__LTS__Group__3__Impl rule__LTS__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1457:2: rule__LTS__Group__3__Impl rule__LTS__Group__4
            {
            pushFollow(FOLLOW_rule__LTS__Group__3__Impl_in_rule__LTS__Group__33088);
            rule__LTS__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group__4_in_rule__LTS__Group__33091);
            rule__LTS__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__3"


    // $ANTLR start "rule__LTS__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1464:1: rule__LTS__Group__3__Impl : ( ( rule__LTS__Group_3__0 )? ) ;
    public final void rule__LTS__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1468:1: ( ( ( rule__LTS__Group_3__0 )? ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1469:1: ( ( rule__LTS__Group_3__0 )? )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1469:1: ( ( rule__LTS__Group_3__0 )? )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1470:1: ( rule__LTS__Group_3__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getGroup_3()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1471:1: ( rule__LTS__Group_3__0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==17) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1471:2: rule__LTS__Group_3__0
                    {
                    pushFollow(FOLLOW_rule__LTS__Group_3__0_in_rule__LTS__Group__3__Impl3118);
                    rule__LTS__Group_3__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getGroup_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__3__Impl"


    // $ANTLR start "rule__LTS__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1481:1: rule__LTS__Group__4 : rule__LTS__Group__4__Impl rule__LTS__Group__5 ;
    public final void rule__LTS__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1485:1: ( rule__LTS__Group__4__Impl rule__LTS__Group__5 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1486:2: rule__LTS__Group__4__Impl rule__LTS__Group__5
            {
            pushFollow(FOLLOW_rule__LTS__Group__4__Impl_in_rule__LTS__Group__43149);
            rule__LTS__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group__5_in_rule__LTS__Group__43152);
            rule__LTS__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__4"


    // $ANTLR start "rule__LTS__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1493:1: rule__LTS__Group__4__Impl : ( 'states' ) ;
    public final void rule__LTS__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1497:1: ( ( 'states' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1498:1: ( 'states' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1498:1: ( 'states' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1499:1: 'states'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getStatesKeyword_4()); 
            }
            match(input,14,FOLLOW_14_in_rule__LTS__Group__4__Impl3180); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getStatesKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__4__Impl"


    // $ANTLR start "rule__LTS__Group__5"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1512:1: rule__LTS__Group__5 : rule__LTS__Group__5__Impl rule__LTS__Group__6 ;
    public final void rule__LTS__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1516:1: ( rule__LTS__Group__5__Impl rule__LTS__Group__6 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1517:2: rule__LTS__Group__5__Impl rule__LTS__Group__6
            {
            pushFollow(FOLLOW_rule__LTS__Group__5__Impl_in_rule__LTS__Group__53211);
            rule__LTS__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group__6_in_rule__LTS__Group__53214);
            rule__LTS__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__5"


    // $ANTLR start "rule__LTS__Group__5__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1524:1: rule__LTS__Group__5__Impl : ( ( rule__LTS__StatesAssignment_5 ) ) ;
    public final void rule__LTS__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1528:1: ( ( ( rule__LTS__StatesAssignment_5 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1529:1: ( ( rule__LTS__StatesAssignment_5 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1529:1: ( ( rule__LTS__StatesAssignment_5 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1530:1: ( rule__LTS__StatesAssignment_5 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getStatesAssignment_5()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1531:1: ( rule__LTS__StatesAssignment_5 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1531:2: rule__LTS__StatesAssignment_5
            {
            pushFollow(FOLLOW_rule__LTS__StatesAssignment_5_in_rule__LTS__Group__5__Impl3241);
            rule__LTS__StatesAssignment_5();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getStatesAssignment_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__5__Impl"


    // $ANTLR start "rule__LTS__Group__6"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1541:1: rule__LTS__Group__6 : rule__LTS__Group__6__Impl rule__LTS__Group__7 ;
    public final void rule__LTS__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1545:1: ( rule__LTS__Group__6__Impl rule__LTS__Group__7 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1546:2: rule__LTS__Group__6__Impl rule__LTS__Group__7
            {
            pushFollow(FOLLOW_rule__LTS__Group__6__Impl_in_rule__LTS__Group__63271);
            rule__LTS__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group__7_in_rule__LTS__Group__63274);
            rule__LTS__Group__7();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__6"


    // $ANTLR start "rule__LTS__Group__6__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1553:1: rule__LTS__Group__6__Impl : ( ( rule__LTS__Group_6__0 )* ) ;
    public final void rule__LTS__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1557:1: ( ( ( rule__LTS__Group_6__0 )* ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1558:1: ( ( rule__LTS__Group_6__0 )* )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1558:1: ( ( rule__LTS__Group_6__0 )* )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1559:1: ( rule__LTS__Group_6__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getGroup_6()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1560:1: ( rule__LTS__Group_6__0 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==19) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1560:2: rule__LTS__Group_6__0
            	    {
            	    pushFollow(FOLLOW_rule__LTS__Group_6__0_in_rule__LTS__Group__6__Impl3301);
            	    rule__LTS__Group_6__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getGroup_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__6__Impl"


    // $ANTLR start "rule__LTS__Group__7"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1570:1: rule__LTS__Group__7 : rule__LTS__Group__7__Impl rule__LTS__Group__8 ;
    public final void rule__LTS__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1574:1: ( rule__LTS__Group__7__Impl rule__LTS__Group__8 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1575:2: rule__LTS__Group__7__Impl rule__LTS__Group__8
            {
            pushFollow(FOLLOW_rule__LTS__Group__7__Impl_in_rule__LTS__Group__73332);
            rule__LTS__Group__7__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group__8_in_rule__LTS__Group__73335);
            rule__LTS__Group__8();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__7"


    // $ANTLR start "rule__LTS__Group__7__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1582:1: rule__LTS__Group__7__Impl : ( 'initial' ) ;
    public final void rule__LTS__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1586:1: ( ( 'initial' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1587:1: ( 'initial' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1587:1: ( 'initial' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1588:1: 'initial'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getInitialKeyword_7()); 
            }
            match(input,15,FOLLOW_15_in_rule__LTS__Group__7__Impl3363); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getInitialKeyword_7()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__7__Impl"


    // $ANTLR start "rule__LTS__Group__8"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1601:1: rule__LTS__Group__8 : rule__LTS__Group__8__Impl rule__LTS__Group__9 ;
    public final void rule__LTS__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1605:1: ( rule__LTS__Group__8__Impl rule__LTS__Group__9 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1606:2: rule__LTS__Group__8__Impl rule__LTS__Group__9
            {
            pushFollow(FOLLOW_rule__LTS__Group__8__Impl_in_rule__LTS__Group__83394);
            rule__LTS__Group__8__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group__9_in_rule__LTS__Group__83397);
            rule__LTS__Group__9();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__8"


    // $ANTLR start "rule__LTS__Group__8__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1613:1: rule__LTS__Group__8__Impl : ( ( rule__LTS__InitStatesAssignment_8 ) ) ;
    public final void rule__LTS__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1617:1: ( ( ( rule__LTS__InitStatesAssignment_8 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1618:1: ( ( rule__LTS__InitStatesAssignment_8 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1618:1: ( ( rule__LTS__InitStatesAssignment_8 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1619:1: ( rule__LTS__InitStatesAssignment_8 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getInitStatesAssignment_8()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1620:1: ( rule__LTS__InitStatesAssignment_8 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1620:2: rule__LTS__InitStatesAssignment_8
            {
            pushFollow(FOLLOW_rule__LTS__InitStatesAssignment_8_in_rule__LTS__Group__8__Impl3424);
            rule__LTS__InitStatesAssignment_8();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getInitStatesAssignment_8()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__8__Impl"


    // $ANTLR start "rule__LTS__Group__9"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1630:1: rule__LTS__Group__9 : rule__LTS__Group__9__Impl rule__LTS__Group__10 ;
    public final void rule__LTS__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1634:1: ( rule__LTS__Group__9__Impl rule__LTS__Group__10 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1635:2: rule__LTS__Group__9__Impl rule__LTS__Group__10
            {
            pushFollow(FOLLOW_rule__LTS__Group__9__Impl_in_rule__LTS__Group__93454);
            rule__LTS__Group__9__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group__10_in_rule__LTS__Group__93457);
            rule__LTS__Group__10();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__9"


    // $ANTLR start "rule__LTS__Group__9__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1642:1: rule__LTS__Group__9__Impl : ( ( rule__LTS__Group_9__0 )* ) ;
    public final void rule__LTS__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1646:1: ( ( ( rule__LTS__Group_9__0 )* ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1647:1: ( ( rule__LTS__Group_9__0 )* )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1647:1: ( ( rule__LTS__Group_9__0 )* )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1648:1: ( rule__LTS__Group_9__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getGroup_9()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1649:1: ( rule__LTS__Group_9__0 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==19) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1649:2: rule__LTS__Group_9__0
            	    {
            	    pushFollow(FOLLOW_rule__LTS__Group_9__0_in_rule__LTS__Group__9__Impl3484);
            	    rule__LTS__Group_9__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getGroup_9()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__9__Impl"


    // $ANTLR start "rule__LTS__Group__10"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1659:1: rule__LTS__Group__10 : rule__LTS__Group__10__Impl rule__LTS__Group__11 ;
    public final void rule__LTS__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1663:1: ( rule__LTS__Group__10__Impl rule__LTS__Group__11 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1664:2: rule__LTS__Group__10__Impl rule__LTS__Group__11
            {
            pushFollow(FOLLOW_rule__LTS__Group__10__Impl_in_rule__LTS__Group__103515);
            rule__LTS__Group__10__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group__11_in_rule__LTS__Group__103518);
            rule__LTS__Group__11();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__10"


    // $ANTLR start "rule__LTS__Group__10__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1671:1: rule__LTS__Group__10__Impl : ( ( rule__LTS__Group_10__0 )? ) ;
    public final void rule__LTS__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1675:1: ( ( ( rule__LTS__Group_10__0 )? ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1676:1: ( ( rule__LTS__Group_10__0 )? )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1676:1: ( ( rule__LTS__Group_10__0 )? )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1677:1: ( rule__LTS__Group_10__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getGroup_10()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1678:1: ( rule__LTS__Group_10__0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==20) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1678:2: rule__LTS__Group_10__0
                    {
                    pushFollow(FOLLOW_rule__LTS__Group_10__0_in_rule__LTS__Group__10__Impl3545);
                    rule__LTS__Group_10__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getGroup_10()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__10__Impl"


    // $ANTLR start "rule__LTS__Group__11"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1688:1: rule__LTS__Group__11 : rule__LTS__Group__11__Impl rule__LTS__Group__12 ;
    public final void rule__LTS__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1692:1: ( rule__LTS__Group__11__Impl rule__LTS__Group__12 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1693:2: rule__LTS__Group__11__Impl rule__LTS__Group__12
            {
            pushFollow(FOLLOW_rule__LTS__Group__11__Impl_in_rule__LTS__Group__113576);
            rule__LTS__Group__11__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group__12_in_rule__LTS__Group__113579);
            rule__LTS__Group__12();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__11"


    // $ANTLR start "rule__LTS__Group__11__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1700:1: rule__LTS__Group__11__Impl : ( ( rule__LTS__TransitionsAssignment_11 )* ) ;
    public final void rule__LTS__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1704:1: ( ( ( rule__LTS__TransitionsAssignment_11 )* ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1705:1: ( ( rule__LTS__TransitionsAssignment_11 )* )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1705:1: ( ( rule__LTS__TransitionsAssignment_11 )* )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1706:1: ( rule__LTS__TransitionsAssignment_11 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getTransitionsAssignment_11()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1707:1: ( rule__LTS__TransitionsAssignment_11 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_ID) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1707:2: rule__LTS__TransitionsAssignment_11
            	    {
            	    pushFollow(FOLLOW_rule__LTS__TransitionsAssignment_11_in_rule__LTS__Group__11__Impl3606);
            	    rule__LTS__TransitionsAssignment_11();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getTransitionsAssignment_11()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__11__Impl"


    // $ANTLR start "rule__LTS__Group__12"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1717:1: rule__LTS__Group__12 : rule__LTS__Group__12__Impl ;
    public final void rule__LTS__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1721:1: ( rule__LTS__Group__12__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1722:2: rule__LTS__Group__12__Impl
            {
            pushFollow(FOLLOW_rule__LTS__Group__12__Impl_in_rule__LTS__Group__123637);
            rule__LTS__Group__12__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__12"


    // $ANTLR start "rule__LTS__Group__12__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1728:1: rule__LTS__Group__12__Impl : ( '}' ) ;
    public final void rule__LTS__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1732:1: ( ( '}' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1733:1: ( '}' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1733:1: ( '}' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1734:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getRightCurlyBracketKeyword_12()); 
            }
            match(input,16,FOLLOW_16_in_rule__LTS__Group__12__Impl3665); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getRightCurlyBracketKeyword_12()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group__12__Impl"


    // $ANTLR start "rule__LTS__Group_3__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1773:1: rule__LTS__Group_3__0 : rule__LTS__Group_3__0__Impl rule__LTS__Group_3__1 ;
    public final void rule__LTS__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1777:1: ( rule__LTS__Group_3__0__Impl rule__LTS__Group_3__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1778:2: rule__LTS__Group_3__0__Impl rule__LTS__Group_3__1
            {
            pushFollow(FOLLOW_rule__LTS__Group_3__0__Impl_in_rule__LTS__Group_3__03722);
            rule__LTS__Group_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group_3__1_in_rule__LTS__Group_3__03725);
            rule__LTS__Group_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_3__0"


    // $ANTLR start "rule__LTS__Group_3__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1785:1: rule__LTS__Group_3__0__Impl : ( 'atomic' ) ;
    public final void rule__LTS__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1789:1: ( ( 'atomic' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1790:1: ( 'atomic' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1790:1: ( 'atomic' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1791:1: 'atomic'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getAtomicKeyword_3_0()); 
            }
            match(input,17,FOLLOW_17_in_rule__LTS__Group_3__0__Impl3753); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getAtomicKeyword_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_3__0__Impl"


    // $ANTLR start "rule__LTS__Group_3__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1804:1: rule__LTS__Group_3__1 : rule__LTS__Group_3__1__Impl rule__LTS__Group_3__2 ;
    public final void rule__LTS__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1808:1: ( rule__LTS__Group_3__1__Impl rule__LTS__Group_3__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1809:2: rule__LTS__Group_3__1__Impl rule__LTS__Group_3__2
            {
            pushFollow(FOLLOW_rule__LTS__Group_3__1__Impl_in_rule__LTS__Group_3__13784);
            rule__LTS__Group_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group_3__2_in_rule__LTS__Group_3__13787);
            rule__LTS__Group_3__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_3__1"


    // $ANTLR start "rule__LTS__Group_3__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1816:1: rule__LTS__Group_3__1__Impl : ( 'propositions' ) ;
    public final void rule__LTS__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1820:1: ( ( 'propositions' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1821:1: ( 'propositions' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1821:1: ( 'propositions' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1822:1: 'propositions'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getPropositionsKeyword_3_1()); 
            }
            match(input,18,FOLLOW_18_in_rule__LTS__Group_3__1__Impl3815); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getPropositionsKeyword_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_3__1__Impl"


    // $ANTLR start "rule__LTS__Group_3__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1835:1: rule__LTS__Group_3__2 : rule__LTS__Group_3__2__Impl rule__LTS__Group_3__3 ;
    public final void rule__LTS__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1839:1: ( rule__LTS__Group_3__2__Impl rule__LTS__Group_3__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1840:2: rule__LTS__Group_3__2__Impl rule__LTS__Group_3__3
            {
            pushFollow(FOLLOW_rule__LTS__Group_3__2__Impl_in_rule__LTS__Group_3__23846);
            rule__LTS__Group_3__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group_3__3_in_rule__LTS__Group_3__23849);
            rule__LTS__Group_3__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_3__2"


    // $ANTLR start "rule__LTS__Group_3__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1847:1: rule__LTS__Group_3__2__Impl : ( ( rule__LTS__AtomicPropositionsAssignment_3_2 ) ) ;
    public final void rule__LTS__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1851:1: ( ( ( rule__LTS__AtomicPropositionsAssignment_3_2 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1852:1: ( ( rule__LTS__AtomicPropositionsAssignment_3_2 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1852:1: ( ( rule__LTS__AtomicPropositionsAssignment_3_2 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1853:1: ( rule__LTS__AtomicPropositionsAssignment_3_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getAtomicPropositionsAssignment_3_2()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1854:1: ( rule__LTS__AtomicPropositionsAssignment_3_2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1854:2: rule__LTS__AtomicPropositionsAssignment_3_2
            {
            pushFollow(FOLLOW_rule__LTS__AtomicPropositionsAssignment_3_2_in_rule__LTS__Group_3__2__Impl3876);
            rule__LTS__AtomicPropositionsAssignment_3_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getAtomicPropositionsAssignment_3_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_3__2__Impl"


    // $ANTLR start "rule__LTS__Group_3__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1864:1: rule__LTS__Group_3__3 : rule__LTS__Group_3__3__Impl ;
    public final void rule__LTS__Group_3__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1868:1: ( rule__LTS__Group_3__3__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1869:2: rule__LTS__Group_3__3__Impl
            {
            pushFollow(FOLLOW_rule__LTS__Group_3__3__Impl_in_rule__LTS__Group_3__33906);
            rule__LTS__Group_3__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_3__3"


    // $ANTLR start "rule__LTS__Group_3__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1875:1: rule__LTS__Group_3__3__Impl : ( ( rule__LTS__Group_3_3__0 )* ) ;
    public final void rule__LTS__Group_3__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1879:1: ( ( ( rule__LTS__Group_3_3__0 )* ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1880:1: ( ( rule__LTS__Group_3_3__0 )* )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1880:1: ( ( rule__LTS__Group_3_3__0 )* )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1881:1: ( rule__LTS__Group_3_3__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getGroup_3_3()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1882:1: ( rule__LTS__Group_3_3__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==19) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1882:2: rule__LTS__Group_3_3__0
            	    {
            	    pushFollow(FOLLOW_rule__LTS__Group_3_3__0_in_rule__LTS__Group_3__3__Impl3933);
            	    rule__LTS__Group_3_3__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getGroup_3_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_3__3__Impl"


    // $ANTLR start "rule__LTS__Group_3_3__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1900:1: rule__LTS__Group_3_3__0 : rule__LTS__Group_3_3__0__Impl rule__LTS__Group_3_3__1 ;
    public final void rule__LTS__Group_3_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1904:1: ( rule__LTS__Group_3_3__0__Impl rule__LTS__Group_3_3__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1905:2: rule__LTS__Group_3_3__0__Impl rule__LTS__Group_3_3__1
            {
            pushFollow(FOLLOW_rule__LTS__Group_3_3__0__Impl_in_rule__LTS__Group_3_3__03972);
            rule__LTS__Group_3_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group_3_3__1_in_rule__LTS__Group_3_3__03975);
            rule__LTS__Group_3_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_3_3__0"


    // $ANTLR start "rule__LTS__Group_3_3__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1912:1: rule__LTS__Group_3_3__0__Impl : ( ',' ) ;
    public final void rule__LTS__Group_3_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1916:1: ( ( ',' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1917:1: ( ',' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1917:1: ( ',' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1918:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getCommaKeyword_3_3_0()); 
            }
            match(input,19,FOLLOW_19_in_rule__LTS__Group_3_3__0__Impl4003); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getCommaKeyword_3_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_3_3__0__Impl"


    // $ANTLR start "rule__LTS__Group_3_3__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1931:1: rule__LTS__Group_3_3__1 : rule__LTS__Group_3_3__1__Impl ;
    public final void rule__LTS__Group_3_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1935:1: ( rule__LTS__Group_3_3__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1936:2: rule__LTS__Group_3_3__1__Impl
            {
            pushFollow(FOLLOW_rule__LTS__Group_3_3__1__Impl_in_rule__LTS__Group_3_3__14034);
            rule__LTS__Group_3_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_3_3__1"


    // $ANTLR start "rule__LTS__Group_3_3__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1942:1: rule__LTS__Group_3_3__1__Impl : ( ( rule__LTS__AtomicPropositionsAssignment_3_3_1 ) ) ;
    public final void rule__LTS__Group_3_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1946:1: ( ( ( rule__LTS__AtomicPropositionsAssignment_3_3_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1947:1: ( ( rule__LTS__AtomicPropositionsAssignment_3_3_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1947:1: ( ( rule__LTS__AtomicPropositionsAssignment_3_3_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1948:1: ( rule__LTS__AtomicPropositionsAssignment_3_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getAtomicPropositionsAssignment_3_3_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1949:1: ( rule__LTS__AtomicPropositionsAssignment_3_3_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1949:2: rule__LTS__AtomicPropositionsAssignment_3_3_1
            {
            pushFollow(FOLLOW_rule__LTS__AtomicPropositionsAssignment_3_3_1_in_rule__LTS__Group_3_3__1__Impl4061);
            rule__LTS__AtomicPropositionsAssignment_3_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getAtomicPropositionsAssignment_3_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_3_3__1__Impl"


    // $ANTLR start "rule__LTS__Group_6__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1963:1: rule__LTS__Group_6__0 : rule__LTS__Group_6__0__Impl rule__LTS__Group_6__1 ;
    public final void rule__LTS__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1967:1: ( rule__LTS__Group_6__0__Impl rule__LTS__Group_6__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1968:2: rule__LTS__Group_6__0__Impl rule__LTS__Group_6__1
            {
            pushFollow(FOLLOW_rule__LTS__Group_6__0__Impl_in_rule__LTS__Group_6__04095);
            rule__LTS__Group_6__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group_6__1_in_rule__LTS__Group_6__04098);
            rule__LTS__Group_6__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_6__0"


    // $ANTLR start "rule__LTS__Group_6__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1975:1: rule__LTS__Group_6__0__Impl : ( ',' ) ;
    public final void rule__LTS__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1979:1: ( ( ',' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1980:1: ( ',' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1980:1: ( ',' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1981:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getCommaKeyword_6_0()); 
            }
            match(input,19,FOLLOW_19_in_rule__LTS__Group_6__0__Impl4126); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getCommaKeyword_6_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_6__0__Impl"


    // $ANTLR start "rule__LTS__Group_6__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1994:1: rule__LTS__Group_6__1 : rule__LTS__Group_6__1__Impl ;
    public final void rule__LTS__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1998:1: ( rule__LTS__Group_6__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1999:2: rule__LTS__Group_6__1__Impl
            {
            pushFollow(FOLLOW_rule__LTS__Group_6__1__Impl_in_rule__LTS__Group_6__14157);
            rule__LTS__Group_6__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_6__1"


    // $ANTLR start "rule__LTS__Group_6__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2005:1: rule__LTS__Group_6__1__Impl : ( ( rule__LTS__StatesAssignment_6_1 ) ) ;
    public final void rule__LTS__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2009:1: ( ( ( rule__LTS__StatesAssignment_6_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2010:1: ( ( rule__LTS__StatesAssignment_6_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2010:1: ( ( rule__LTS__StatesAssignment_6_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2011:1: ( rule__LTS__StatesAssignment_6_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getStatesAssignment_6_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2012:1: ( rule__LTS__StatesAssignment_6_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2012:2: rule__LTS__StatesAssignment_6_1
            {
            pushFollow(FOLLOW_rule__LTS__StatesAssignment_6_1_in_rule__LTS__Group_6__1__Impl4184);
            rule__LTS__StatesAssignment_6_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getStatesAssignment_6_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_6__1__Impl"


    // $ANTLR start "rule__LTS__Group_9__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2026:1: rule__LTS__Group_9__0 : rule__LTS__Group_9__0__Impl rule__LTS__Group_9__1 ;
    public final void rule__LTS__Group_9__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2030:1: ( rule__LTS__Group_9__0__Impl rule__LTS__Group_9__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2031:2: rule__LTS__Group_9__0__Impl rule__LTS__Group_9__1
            {
            pushFollow(FOLLOW_rule__LTS__Group_9__0__Impl_in_rule__LTS__Group_9__04218);
            rule__LTS__Group_9__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group_9__1_in_rule__LTS__Group_9__04221);
            rule__LTS__Group_9__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_9__0"


    // $ANTLR start "rule__LTS__Group_9__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2038:1: rule__LTS__Group_9__0__Impl : ( ',' ) ;
    public final void rule__LTS__Group_9__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2042:1: ( ( ',' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2043:1: ( ',' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2043:1: ( ',' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2044:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getCommaKeyword_9_0()); 
            }
            match(input,19,FOLLOW_19_in_rule__LTS__Group_9__0__Impl4249); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getCommaKeyword_9_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_9__0__Impl"


    // $ANTLR start "rule__LTS__Group_9__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2057:1: rule__LTS__Group_9__1 : rule__LTS__Group_9__1__Impl ;
    public final void rule__LTS__Group_9__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2061:1: ( rule__LTS__Group_9__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2062:2: rule__LTS__Group_9__1__Impl
            {
            pushFollow(FOLLOW_rule__LTS__Group_9__1__Impl_in_rule__LTS__Group_9__14280);
            rule__LTS__Group_9__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_9__1"


    // $ANTLR start "rule__LTS__Group_9__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2068:1: rule__LTS__Group_9__1__Impl : ( ( rule__LTS__InitStatesAssignment_9_1 ) ) ;
    public final void rule__LTS__Group_9__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2072:1: ( ( ( rule__LTS__InitStatesAssignment_9_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2073:1: ( ( rule__LTS__InitStatesAssignment_9_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2073:1: ( ( rule__LTS__InitStatesAssignment_9_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2074:1: ( rule__LTS__InitStatesAssignment_9_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getInitStatesAssignment_9_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2075:1: ( rule__LTS__InitStatesAssignment_9_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2075:2: rule__LTS__InitStatesAssignment_9_1
            {
            pushFollow(FOLLOW_rule__LTS__InitStatesAssignment_9_1_in_rule__LTS__Group_9__1__Impl4307);
            rule__LTS__InitStatesAssignment_9_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getInitStatesAssignment_9_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_9__1__Impl"


    // $ANTLR start "rule__LTS__Group_10__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2089:1: rule__LTS__Group_10__0 : rule__LTS__Group_10__0__Impl rule__LTS__Group_10__1 ;
    public final void rule__LTS__Group_10__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2093:1: ( rule__LTS__Group_10__0__Impl rule__LTS__Group_10__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2094:2: rule__LTS__Group_10__0__Impl rule__LTS__Group_10__1
            {
            pushFollow(FOLLOW_rule__LTS__Group_10__0__Impl_in_rule__LTS__Group_10__04341);
            rule__LTS__Group_10__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group_10__1_in_rule__LTS__Group_10__04344);
            rule__LTS__Group_10__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_10__0"


    // $ANTLR start "rule__LTS__Group_10__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2101:1: rule__LTS__Group_10__0__Impl : ( 'alphabet' ) ;
    public final void rule__LTS__Group_10__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2105:1: ( ( 'alphabet' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2106:1: ( 'alphabet' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2106:1: ( 'alphabet' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2107:1: 'alphabet'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getAlphabetKeyword_10_0()); 
            }
            match(input,20,FOLLOW_20_in_rule__LTS__Group_10__0__Impl4372); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getAlphabetKeyword_10_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_10__0__Impl"


    // $ANTLR start "rule__LTS__Group_10__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2120:1: rule__LTS__Group_10__1 : rule__LTS__Group_10__1__Impl rule__LTS__Group_10__2 ;
    public final void rule__LTS__Group_10__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2124:1: ( rule__LTS__Group_10__1__Impl rule__LTS__Group_10__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2125:2: rule__LTS__Group_10__1__Impl rule__LTS__Group_10__2
            {
            pushFollow(FOLLOW_rule__LTS__Group_10__1__Impl_in_rule__LTS__Group_10__14403);
            rule__LTS__Group_10__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group_10__2_in_rule__LTS__Group_10__14406);
            rule__LTS__Group_10__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_10__1"


    // $ANTLR start "rule__LTS__Group_10__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2132:1: rule__LTS__Group_10__1__Impl : ( ( rule__LTS__AlphabetAssignment_10_1 ) ) ;
    public final void rule__LTS__Group_10__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2136:1: ( ( ( rule__LTS__AlphabetAssignment_10_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2137:1: ( ( rule__LTS__AlphabetAssignment_10_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2137:1: ( ( rule__LTS__AlphabetAssignment_10_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2138:1: ( rule__LTS__AlphabetAssignment_10_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getAlphabetAssignment_10_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2139:1: ( rule__LTS__AlphabetAssignment_10_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2139:2: rule__LTS__AlphabetAssignment_10_1
            {
            pushFollow(FOLLOW_rule__LTS__AlphabetAssignment_10_1_in_rule__LTS__Group_10__1__Impl4433);
            rule__LTS__AlphabetAssignment_10_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getAlphabetAssignment_10_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_10__1__Impl"


    // $ANTLR start "rule__LTS__Group_10__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2149:1: rule__LTS__Group_10__2 : rule__LTS__Group_10__2__Impl ;
    public final void rule__LTS__Group_10__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2153:1: ( rule__LTS__Group_10__2__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2154:2: rule__LTS__Group_10__2__Impl
            {
            pushFollow(FOLLOW_rule__LTS__Group_10__2__Impl_in_rule__LTS__Group_10__24463);
            rule__LTS__Group_10__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_10__2"


    // $ANTLR start "rule__LTS__Group_10__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2160:1: rule__LTS__Group_10__2__Impl : ( ( rule__LTS__Group_10_2__0 )* ) ;
    public final void rule__LTS__Group_10__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2164:1: ( ( ( rule__LTS__Group_10_2__0 )* ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2165:1: ( ( rule__LTS__Group_10_2__0 )* )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2165:1: ( ( rule__LTS__Group_10_2__0 )* )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2166:1: ( rule__LTS__Group_10_2__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getGroup_10_2()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2167:1: ( rule__LTS__Group_10_2__0 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==19) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2167:2: rule__LTS__Group_10_2__0
            	    {
            	    pushFollow(FOLLOW_rule__LTS__Group_10_2__0_in_rule__LTS__Group_10__2__Impl4490);
            	    rule__LTS__Group_10_2__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getGroup_10_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_10__2__Impl"


    // $ANTLR start "rule__LTS__Group_10_2__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2183:1: rule__LTS__Group_10_2__0 : rule__LTS__Group_10_2__0__Impl rule__LTS__Group_10_2__1 ;
    public final void rule__LTS__Group_10_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2187:1: ( rule__LTS__Group_10_2__0__Impl rule__LTS__Group_10_2__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2188:2: rule__LTS__Group_10_2__0__Impl rule__LTS__Group_10_2__1
            {
            pushFollow(FOLLOW_rule__LTS__Group_10_2__0__Impl_in_rule__LTS__Group_10_2__04527);
            rule__LTS__Group_10_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTS__Group_10_2__1_in_rule__LTS__Group_10_2__04530);
            rule__LTS__Group_10_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_10_2__0"


    // $ANTLR start "rule__LTS__Group_10_2__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2195:1: rule__LTS__Group_10_2__0__Impl : ( ',' ) ;
    public final void rule__LTS__Group_10_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2199:1: ( ( ',' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2200:1: ( ',' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2200:1: ( ',' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2201:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getCommaKeyword_10_2_0()); 
            }
            match(input,19,FOLLOW_19_in_rule__LTS__Group_10_2__0__Impl4558); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getCommaKeyword_10_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_10_2__0__Impl"


    // $ANTLR start "rule__LTS__Group_10_2__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2214:1: rule__LTS__Group_10_2__1 : rule__LTS__Group_10_2__1__Impl ;
    public final void rule__LTS__Group_10_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2218:1: ( rule__LTS__Group_10_2__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2219:2: rule__LTS__Group_10_2__1__Impl
            {
            pushFollow(FOLLOW_rule__LTS__Group_10_2__1__Impl_in_rule__LTS__Group_10_2__14589);
            rule__LTS__Group_10_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_10_2__1"


    // $ANTLR start "rule__LTS__Group_10_2__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2225:1: rule__LTS__Group_10_2__1__Impl : ( ( rule__LTS__AlphabetAssignment_10_2_1 ) ) ;
    public final void rule__LTS__Group_10_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2229:1: ( ( ( rule__LTS__AlphabetAssignment_10_2_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2230:1: ( ( rule__LTS__AlphabetAssignment_10_2_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2230:1: ( ( rule__LTS__AlphabetAssignment_10_2_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2231:1: ( rule__LTS__AlphabetAssignment_10_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getAlphabetAssignment_10_2_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2232:1: ( rule__LTS__AlphabetAssignment_10_2_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2232:2: rule__LTS__AlphabetAssignment_10_2_1
            {
            pushFollow(FOLLOW_rule__LTS__AlphabetAssignment_10_2_1_in_rule__LTS__Group_10_2__1__Impl4616);
            rule__LTS__AlphabetAssignment_10_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getAlphabetAssignment_10_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__Group_10_2__1__Impl"


    // $ANTLR start "rule__BasicState__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2246:1: rule__BasicState__Group__0 : rule__BasicState__Group__0__Impl rule__BasicState__Group__1 ;
    public final void rule__BasicState__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2250:1: ( rule__BasicState__Group__0__Impl rule__BasicState__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2251:2: rule__BasicState__Group__0__Impl rule__BasicState__Group__1
            {
            pushFollow(FOLLOW_rule__BasicState__Group__0__Impl_in_rule__BasicState__Group__04650);
            rule__BasicState__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__BasicState__Group__1_in_rule__BasicState__Group__04653);
            rule__BasicState__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group__0"


    // $ANTLR start "rule__BasicState__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2258:1: rule__BasicState__Group__0__Impl : ( ( rule__BasicState__NameAssignment_0 ) ) ;
    public final void rule__BasicState__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2262:1: ( ( ( rule__BasicState__NameAssignment_0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2263:1: ( ( rule__BasicState__NameAssignment_0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2263:1: ( ( rule__BasicState__NameAssignment_0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2264:1: ( rule__BasicState__NameAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getNameAssignment_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2265:1: ( rule__BasicState__NameAssignment_0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2265:2: rule__BasicState__NameAssignment_0
            {
            pushFollow(FOLLOW_rule__BasicState__NameAssignment_0_in_rule__BasicState__Group__0__Impl4680);
            rule__BasicState__NameAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getNameAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group__0__Impl"


    // $ANTLR start "rule__BasicState__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2275:1: rule__BasicState__Group__1 : rule__BasicState__Group__1__Impl ;
    public final void rule__BasicState__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2279:1: ( rule__BasicState__Group__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2280:2: rule__BasicState__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__BasicState__Group__1__Impl_in_rule__BasicState__Group__14710);
            rule__BasicState__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group__1"


    // $ANTLR start "rule__BasicState__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2286:1: rule__BasicState__Group__1__Impl : ( ( rule__BasicState__Group_1__0 )* ) ;
    public final void rule__BasicState__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2290:1: ( ( ( rule__BasicState__Group_1__0 )* ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2291:1: ( ( rule__BasicState__Group_1__0 )* )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2291:1: ( ( rule__BasicState__Group_1__0 )* )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2292:1: ( rule__BasicState__Group_1__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getGroup_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2293:1: ( rule__BasicState__Group_1__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==13) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2293:2: rule__BasicState__Group_1__0
            	    {
            	    pushFollow(FOLLOW_rule__BasicState__Group_1__0_in_rule__BasicState__Group__1__Impl4737);
            	    rule__BasicState__Group_1__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group__1__Impl"


    // $ANTLR start "rule__BasicState__Group_1__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2307:1: rule__BasicState__Group_1__0 : rule__BasicState__Group_1__0__Impl rule__BasicState__Group_1__1 ;
    public final void rule__BasicState__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2311:1: ( rule__BasicState__Group_1__0__Impl rule__BasicState__Group_1__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2312:2: rule__BasicState__Group_1__0__Impl rule__BasicState__Group_1__1
            {
            pushFollow(FOLLOW_rule__BasicState__Group_1__0__Impl_in_rule__BasicState__Group_1__04772);
            rule__BasicState__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__BasicState__Group_1__1_in_rule__BasicState__Group_1__04775);
            rule__BasicState__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group_1__0"


    // $ANTLR start "rule__BasicState__Group_1__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2319:1: rule__BasicState__Group_1__0__Impl : ( '{' ) ;
    public final void rule__BasicState__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2323:1: ( ( '{' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2324:1: ( '{' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2324:1: ( '{' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2325:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getLeftCurlyBracketKeyword_1_0()); 
            }
            match(input,13,FOLLOW_13_in_rule__BasicState__Group_1__0__Impl4803); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getLeftCurlyBracketKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group_1__0__Impl"


    // $ANTLR start "rule__BasicState__Group_1__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2338:1: rule__BasicState__Group_1__1 : rule__BasicState__Group_1__1__Impl rule__BasicState__Group_1__2 ;
    public final void rule__BasicState__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2342:1: ( rule__BasicState__Group_1__1__Impl rule__BasicState__Group_1__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2343:2: rule__BasicState__Group_1__1__Impl rule__BasicState__Group_1__2
            {
            pushFollow(FOLLOW_rule__BasicState__Group_1__1__Impl_in_rule__BasicState__Group_1__14834);
            rule__BasicState__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__BasicState__Group_1__2_in_rule__BasicState__Group_1__14837);
            rule__BasicState__Group_1__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group_1__1"


    // $ANTLR start "rule__BasicState__Group_1__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2350:1: rule__BasicState__Group_1__1__Impl : ( ( rule__BasicState__AtomicPropositionsAssignment_1_1 ) ) ;
    public final void rule__BasicState__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2354:1: ( ( ( rule__BasicState__AtomicPropositionsAssignment_1_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2355:1: ( ( rule__BasicState__AtomicPropositionsAssignment_1_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2355:1: ( ( rule__BasicState__AtomicPropositionsAssignment_1_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2356:1: ( rule__BasicState__AtomicPropositionsAssignment_1_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getAtomicPropositionsAssignment_1_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2357:1: ( rule__BasicState__AtomicPropositionsAssignment_1_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2357:2: rule__BasicState__AtomicPropositionsAssignment_1_1
            {
            pushFollow(FOLLOW_rule__BasicState__AtomicPropositionsAssignment_1_1_in_rule__BasicState__Group_1__1__Impl4864);
            rule__BasicState__AtomicPropositionsAssignment_1_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getAtomicPropositionsAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group_1__1__Impl"


    // $ANTLR start "rule__BasicState__Group_1__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2367:1: rule__BasicState__Group_1__2 : rule__BasicState__Group_1__2__Impl rule__BasicState__Group_1__3 ;
    public final void rule__BasicState__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2371:1: ( rule__BasicState__Group_1__2__Impl rule__BasicState__Group_1__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2372:2: rule__BasicState__Group_1__2__Impl rule__BasicState__Group_1__3
            {
            pushFollow(FOLLOW_rule__BasicState__Group_1__2__Impl_in_rule__BasicState__Group_1__24894);
            rule__BasicState__Group_1__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__BasicState__Group_1__3_in_rule__BasicState__Group_1__24897);
            rule__BasicState__Group_1__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group_1__2"


    // $ANTLR start "rule__BasicState__Group_1__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2379:1: rule__BasicState__Group_1__2__Impl : ( ( rule__BasicState__Group_1_2__0 )* ) ;
    public final void rule__BasicState__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2383:1: ( ( ( rule__BasicState__Group_1_2__0 )* ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2384:1: ( ( rule__BasicState__Group_1_2__0 )* )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2384:1: ( ( rule__BasicState__Group_1_2__0 )* )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2385:1: ( rule__BasicState__Group_1_2__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getGroup_1_2()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2386:1: ( rule__BasicState__Group_1_2__0 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==19) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2386:2: rule__BasicState__Group_1_2__0
            	    {
            	    pushFollow(FOLLOW_rule__BasicState__Group_1_2__0_in_rule__BasicState__Group_1__2__Impl4924);
            	    rule__BasicState__Group_1_2__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getGroup_1_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group_1__2__Impl"


    // $ANTLR start "rule__BasicState__Group_1__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2396:1: rule__BasicState__Group_1__3 : rule__BasicState__Group_1__3__Impl ;
    public final void rule__BasicState__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2400:1: ( rule__BasicState__Group_1__3__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2401:2: rule__BasicState__Group_1__3__Impl
            {
            pushFollow(FOLLOW_rule__BasicState__Group_1__3__Impl_in_rule__BasicState__Group_1__34955);
            rule__BasicState__Group_1__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group_1__3"


    // $ANTLR start "rule__BasicState__Group_1__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2407:1: rule__BasicState__Group_1__3__Impl : ( '}' ) ;
    public final void rule__BasicState__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2411:1: ( ( '}' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2412:1: ( '}' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2412:1: ( '}' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2413:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getRightCurlyBracketKeyword_1_3()); 
            }
            match(input,16,FOLLOW_16_in_rule__BasicState__Group_1__3__Impl4983); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getRightCurlyBracketKeyword_1_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group_1__3__Impl"


    // $ANTLR start "rule__BasicState__Group_1_2__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2434:1: rule__BasicState__Group_1_2__0 : rule__BasicState__Group_1_2__0__Impl rule__BasicState__Group_1_2__1 ;
    public final void rule__BasicState__Group_1_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2438:1: ( rule__BasicState__Group_1_2__0__Impl rule__BasicState__Group_1_2__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2439:2: rule__BasicState__Group_1_2__0__Impl rule__BasicState__Group_1_2__1
            {
            pushFollow(FOLLOW_rule__BasicState__Group_1_2__0__Impl_in_rule__BasicState__Group_1_2__05022);
            rule__BasicState__Group_1_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__BasicState__Group_1_2__1_in_rule__BasicState__Group_1_2__05025);
            rule__BasicState__Group_1_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group_1_2__0"


    // $ANTLR start "rule__BasicState__Group_1_2__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2446:1: rule__BasicState__Group_1_2__0__Impl : ( ',' ) ;
    public final void rule__BasicState__Group_1_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2450:1: ( ( ',' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2451:1: ( ',' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2451:1: ( ',' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2452:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getCommaKeyword_1_2_0()); 
            }
            match(input,19,FOLLOW_19_in_rule__BasicState__Group_1_2__0__Impl5053); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getCommaKeyword_1_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group_1_2__0__Impl"


    // $ANTLR start "rule__BasicState__Group_1_2__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2465:1: rule__BasicState__Group_1_2__1 : rule__BasicState__Group_1_2__1__Impl ;
    public final void rule__BasicState__Group_1_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2469:1: ( rule__BasicState__Group_1_2__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2470:2: rule__BasicState__Group_1_2__1__Impl
            {
            pushFollow(FOLLOW_rule__BasicState__Group_1_2__1__Impl_in_rule__BasicState__Group_1_2__15084);
            rule__BasicState__Group_1_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group_1_2__1"


    // $ANTLR start "rule__BasicState__Group_1_2__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2476:1: rule__BasicState__Group_1_2__1__Impl : ( ( rule__BasicState__AtomicPropositionsAssignment_1_2_1 ) ) ;
    public final void rule__BasicState__Group_1_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2480:1: ( ( ( rule__BasicState__AtomicPropositionsAssignment_1_2_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2481:1: ( ( rule__BasicState__AtomicPropositionsAssignment_1_2_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2481:1: ( ( rule__BasicState__AtomicPropositionsAssignment_1_2_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2482:1: ( rule__BasicState__AtomicPropositionsAssignment_1_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getAtomicPropositionsAssignment_1_2_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2483:1: ( rule__BasicState__AtomicPropositionsAssignment_1_2_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2483:2: rule__BasicState__AtomicPropositionsAssignment_1_2_1
            {
            pushFollow(FOLLOW_rule__BasicState__AtomicPropositionsAssignment_1_2_1_in_rule__BasicState__Group_1_2__1__Impl5111);
            rule__BasicState__AtomicPropositionsAssignment_1_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getAtomicPropositionsAssignment_1_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__Group_1_2__1__Impl"


    // $ANTLR start "rule__Transition__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2497:1: rule__Transition__Group__0 : rule__Transition__Group__0__Impl rule__Transition__Group__1 ;
    public final void rule__Transition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2501:1: ( rule__Transition__Group__0__Impl rule__Transition__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2502:2: rule__Transition__Group__0__Impl rule__Transition__Group__1
            {
            pushFollow(FOLLOW_rule__Transition__Group__0__Impl_in_rule__Transition__Group__05145);
            rule__Transition__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Transition__Group__1_in_rule__Transition__Group__05148);
            rule__Transition__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0"


    // $ANTLR start "rule__Transition__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2509:1: rule__Transition__Group__0__Impl : ( ( rule__Transition__SourceAssignment_0 ) ) ;
    public final void rule__Transition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2513:1: ( ( ( rule__Transition__SourceAssignment_0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2514:1: ( ( rule__Transition__SourceAssignment_0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2514:1: ( ( rule__Transition__SourceAssignment_0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2515:1: ( rule__Transition__SourceAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTransitionAccess().getSourceAssignment_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2516:1: ( rule__Transition__SourceAssignment_0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2516:2: rule__Transition__SourceAssignment_0
            {
            pushFollow(FOLLOW_rule__Transition__SourceAssignment_0_in_rule__Transition__Group__0__Impl5175);
            rule__Transition__SourceAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTransitionAccess().getSourceAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__0__Impl"


    // $ANTLR start "rule__Transition__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2526:1: rule__Transition__Group__1 : rule__Transition__Group__1__Impl rule__Transition__Group__2 ;
    public final void rule__Transition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2530:1: ( rule__Transition__Group__1__Impl rule__Transition__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2531:2: rule__Transition__Group__1__Impl rule__Transition__Group__2
            {
            pushFollow(FOLLOW_rule__Transition__Group__1__Impl_in_rule__Transition__Group__15205);
            rule__Transition__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Transition__Group__2_in_rule__Transition__Group__15208);
            rule__Transition__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1"


    // $ANTLR start "rule__Transition__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2538:1: rule__Transition__Group__1__Impl : ( '->' ) ;
    public final void rule__Transition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2542:1: ( ( '->' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2543:1: ( '->' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2543:1: ( '->' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2544:1: '->'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTransitionAccess().getHyphenMinusGreaterThanSignKeyword_1()); 
            }
            match(input,21,FOLLOW_21_in_rule__Transition__Group__1__Impl5236); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTransitionAccess().getHyphenMinusGreaterThanSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__1__Impl"


    // $ANTLR start "rule__Transition__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2557:1: rule__Transition__Group__2 : rule__Transition__Group__2__Impl rule__Transition__Group__3 ;
    public final void rule__Transition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2561:1: ( rule__Transition__Group__2__Impl rule__Transition__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2562:2: rule__Transition__Group__2__Impl rule__Transition__Group__3
            {
            pushFollow(FOLLOW_rule__Transition__Group__2__Impl_in_rule__Transition__Group__25267);
            rule__Transition__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Transition__Group__3_in_rule__Transition__Group__25270);
            rule__Transition__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2"


    // $ANTLR start "rule__Transition__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2569:1: rule__Transition__Group__2__Impl : ( ( rule__Transition__DestinationAssignment_2 ) ) ;
    public final void rule__Transition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2573:1: ( ( ( rule__Transition__DestinationAssignment_2 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2574:1: ( ( rule__Transition__DestinationAssignment_2 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2574:1: ( ( rule__Transition__DestinationAssignment_2 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2575:1: ( rule__Transition__DestinationAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTransitionAccess().getDestinationAssignment_2()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2576:1: ( rule__Transition__DestinationAssignment_2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2576:2: rule__Transition__DestinationAssignment_2
            {
            pushFollow(FOLLOW_rule__Transition__DestinationAssignment_2_in_rule__Transition__Group__2__Impl5297);
            rule__Transition__DestinationAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTransitionAccess().getDestinationAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__2__Impl"


    // $ANTLR start "rule__Transition__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2586:1: rule__Transition__Group__3 : rule__Transition__Group__3__Impl rule__Transition__Group__4 ;
    public final void rule__Transition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2590:1: ( rule__Transition__Group__3__Impl rule__Transition__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2591:2: rule__Transition__Group__3__Impl rule__Transition__Group__4
            {
            pushFollow(FOLLOW_rule__Transition__Group__3__Impl_in_rule__Transition__Group__35327);
            rule__Transition__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Transition__Group__4_in_rule__Transition__Group__35330);
            rule__Transition__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3"


    // $ANTLR start "rule__Transition__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2598:1: rule__Transition__Group__3__Impl : ( ':' ) ;
    public final void rule__Transition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2602:1: ( ( ':' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2603:1: ( ':' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2603:1: ( ':' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2604:1: ':'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTransitionAccess().getColonKeyword_3()); 
            }
            match(input,22,FOLLOW_22_in_rule__Transition__Group__3__Impl5358); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTransitionAccess().getColonKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__3__Impl"


    // $ANTLR start "rule__Transition__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2617:1: rule__Transition__Group__4 : rule__Transition__Group__4__Impl ;
    public final void rule__Transition__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2621:1: ( rule__Transition__Group__4__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2622:2: rule__Transition__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Transition__Group__4__Impl_in_rule__Transition__Group__45389);
            rule__Transition__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__4"


    // $ANTLR start "rule__Transition__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2628:1: rule__Transition__Group__4__Impl : ( ( rule__Transition__EventAssignment_4 ) ) ;
    public final void rule__Transition__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2632:1: ( ( ( rule__Transition__EventAssignment_4 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2633:1: ( ( rule__Transition__EventAssignment_4 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2633:1: ( ( rule__Transition__EventAssignment_4 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2634:1: ( rule__Transition__EventAssignment_4 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTransitionAccess().getEventAssignment_4()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2635:1: ( rule__Transition__EventAssignment_4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2635:2: rule__Transition__EventAssignment_4
            {
            pushFollow(FOLLOW_rule__Transition__EventAssignment_4_in_rule__Transition__Group__4__Impl5416);
            rule__Transition__EventAssignment_4();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTransitionAccess().getEventAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__Group__4__Impl"


    // $ANTLR start "rule__ComposedLTS__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2655:1: rule__ComposedLTS__Group__0 : rule__ComposedLTS__Group__0__Impl rule__ComposedLTS__Group__1 ;
    public final void rule__ComposedLTS__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2659:1: ( rule__ComposedLTS__Group__0__Impl rule__ComposedLTS__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2660:2: rule__ComposedLTS__Group__0__Impl rule__ComposedLTS__Group__1
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group__0__Impl_in_rule__ComposedLTS__Group__05456);
            rule__ComposedLTS__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__ComposedLTS__Group__1_in_rule__ComposedLTS__Group__05459);
            rule__ComposedLTS__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__0"


    // $ANTLR start "rule__ComposedLTS__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2667:1: rule__ComposedLTS__Group__0__Impl : ( 'compose' ) ;
    public final void rule__ComposedLTS__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2671:1: ( ( 'compose' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2672:1: ( 'compose' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2672:1: ( 'compose' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2673:1: 'compose'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getComposeKeyword_0()); 
            }
            match(input,23,FOLLOW_23_in_rule__ComposedLTS__Group__0__Impl5487); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getComposeKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__0__Impl"


    // $ANTLR start "rule__ComposedLTS__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2686:1: rule__ComposedLTS__Group__1 : rule__ComposedLTS__Group__1__Impl rule__ComposedLTS__Group__2 ;
    public final void rule__ComposedLTS__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2690:1: ( rule__ComposedLTS__Group__1__Impl rule__ComposedLTS__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2691:2: rule__ComposedLTS__Group__1__Impl rule__ComposedLTS__Group__2
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group__1__Impl_in_rule__ComposedLTS__Group__15518);
            rule__ComposedLTS__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__ComposedLTS__Group__2_in_rule__ComposedLTS__Group__15521);
            rule__ComposedLTS__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__1"


    // $ANTLR start "rule__ComposedLTS__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2698:1: rule__ComposedLTS__Group__1__Impl : ( ( rule__ComposedLTS__LtsLeftAssignment_1 ) ) ;
    public final void rule__ComposedLTS__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2702:1: ( ( ( rule__ComposedLTS__LtsLeftAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2703:1: ( ( rule__ComposedLTS__LtsLeftAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2703:1: ( ( rule__ComposedLTS__LtsLeftAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2704:1: ( rule__ComposedLTS__LtsLeftAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getLtsLeftAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2705:1: ( rule__ComposedLTS__LtsLeftAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2705:2: rule__ComposedLTS__LtsLeftAssignment_1
            {
            pushFollow(FOLLOW_rule__ComposedLTS__LtsLeftAssignment_1_in_rule__ComposedLTS__Group__1__Impl5548);
            rule__ComposedLTS__LtsLeftAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getLtsLeftAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__1__Impl"


    // $ANTLR start "rule__ComposedLTS__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2715:1: rule__ComposedLTS__Group__2 : rule__ComposedLTS__Group__2__Impl rule__ComposedLTS__Group__3 ;
    public final void rule__ComposedLTS__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2719:1: ( rule__ComposedLTS__Group__2__Impl rule__ComposedLTS__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2720:2: rule__ComposedLTS__Group__2__Impl rule__ComposedLTS__Group__3
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group__2__Impl_in_rule__ComposedLTS__Group__25578);
            rule__ComposedLTS__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__ComposedLTS__Group__3_in_rule__ComposedLTS__Group__25581);
            rule__ComposedLTS__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__2"


    // $ANTLR start "rule__ComposedLTS__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2727:1: rule__ComposedLTS__Group__2__Impl : ( '||' ) ;
    public final void rule__ComposedLTS__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2731:1: ( ( '||' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2732:1: ( '||' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2732:1: ( '||' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2733:1: '||'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getVerticalLineVerticalLineKeyword_2()); 
            }
            match(input,24,FOLLOW_24_in_rule__ComposedLTS__Group__2__Impl5609); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getVerticalLineVerticalLineKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__2__Impl"


    // $ANTLR start "rule__ComposedLTS__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2746:1: rule__ComposedLTS__Group__3 : rule__ComposedLTS__Group__3__Impl rule__ComposedLTS__Group__4 ;
    public final void rule__ComposedLTS__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2750:1: ( rule__ComposedLTS__Group__3__Impl rule__ComposedLTS__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2751:2: rule__ComposedLTS__Group__3__Impl rule__ComposedLTS__Group__4
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group__3__Impl_in_rule__ComposedLTS__Group__35640);
            rule__ComposedLTS__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__ComposedLTS__Group__4_in_rule__ComposedLTS__Group__35643);
            rule__ComposedLTS__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__3"


    // $ANTLR start "rule__ComposedLTS__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2758:1: rule__ComposedLTS__Group__3__Impl : ( ( rule__ComposedLTS__LtsRightAssignment_3 ) ) ;
    public final void rule__ComposedLTS__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2762:1: ( ( ( rule__ComposedLTS__LtsRightAssignment_3 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2763:1: ( ( rule__ComposedLTS__LtsRightAssignment_3 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2763:1: ( ( rule__ComposedLTS__LtsRightAssignment_3 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2764:1: ( rule__ComposedLTS__LtsRightAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getLtsRightAssignment_3()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2765:1: ( rule__ComposedLTS__LtsRightAssignment_3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2765:2: rule__ComposedLTS__LtsRightAssignment_3
            {
            pushFollow(FOLLOW_rule__ComposedLTS__LtsRightAssignment_3_in_rule__ComposedLTS__Group__3__Impl5670);
            rule__ComposedLTS__LtsRightAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getLtsRightAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__3__Impl"


    // $ANTLR start "rule__ComposedLTS__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2775:1: rule__ComposedLTS__Group__4 : rule__ComposedLTS__Group__4__Impl rule__ComposedLTS__Group__5 ;
    public final void rule__ComposedLTS__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2779:1: ( rule__ComposedLTS__Group__4__Impl rule__ComposedLTS__Group__5 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2780:2: rule__ComposedLTS__Group__4__Impl rule__ComposedLTS__Group__5
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group__4__Impl_in_rule__ComposedLTS__Group__45700);
            rule__ComposedLTS__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__ComposedLTS__Group__5_in_rule__ComposedLTS__Group__45703);
            rule__ComposedLTS__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__4"


    // $ANTLR start "rule__ComposedLTS__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2787:1: rule__ComposedLTS__Group__4__Impl : ( 'as' ) ;
    public final void rule__ComposedLTS__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2791:1: ( ( 'as' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2792:1: ( 'as' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2792:1: ( 'as' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2793:1: 'as'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getAsKeyword_4()); 
            }
            match(input,25,FOLLOW_25_in_rule__ComposedLTS__Group__4__Impl5731); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getAsKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__4__Impl"


    // $ANTLR start "rule__ComposedLTS__Group__5"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2806:1: rule__ComposedLTS__Group__5 : rule__ComposedLTS__Group__5__Impl rule__ComposedLTS__Group__6 ;
    public final void rule__ComposedLTS__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2810:1: ( rule__ComposedLTS__Group__5__Impl rule__ComposedLTS__Group__6 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2811:2: rule__ComposedLTS__Group__5__Impl rule__ComposedLTS__Group__6
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group__5__Impl_in_rule__ComposedLTS__Group__55762);
            rule__ComposedLTS__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__ComposedLTS__Group__6_in_rule__ComposedLTS__Group__55765);
            rule__ComposedLTS__Group__6();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__5"


    // $ANTLR start "rule__ComposedLTS__Group__5__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2818:1: rule__ComposedLTS__Group__5__Impl : ( ( rule__ComposedLTS__NameAssignment_5 ) ) ;
    public final void rule__ComposedLTS__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2822:1: ( ( ( rule__ComposedLTS__NameAssignment_5 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2823:1: ( ( rule__ComposedLTS__NameAssignment_5 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2823:1: ( ( rule__ComposedLTS__NameAssignment_5 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2824:1: ( rule__ComposedLTS__NameAssignment_5 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getNameAssignment_5()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2825:1: ( rule__ComposedLTS__NameAssignment_5 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2825:2: rule__ComposedLTS__NameAssignment_5
            {
            pushFollow(FOLLOW_rule__ComposedLTS__NameAssignment_5_in_rule__ComposedLTS__Group__5__Impl5792);
            rule__ComposedLTS__NameAssignment_5();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getNameAssignment_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__5__Impl"


    // $ANTLR start "rule__ComposedLTS__Group__6"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2835:1: rule__ComposedLTS__Group__6 : rule__ComposedLTS__Group__6__Impl ;
    public final void rule__ComposedLTS__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2839:1: ( rule__ComposedLTS__Group__6__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2840:2: rule__ComposedLTS__Group__6__Impl
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group__6__Impl_in_rule__ComposedLTS__Group__65822);
            rule__ComposedLTS__Group__6__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__6"


    // $ANTLR start "rule__ComposedLTS__Group__6__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2846:1: rule__ComposedLTS__Group__6__Impl : ( ( rule__ComposedLTS__Group_6__0 )? ) ;
    public final void rule__ComposedLTS__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2850:1: ( ( ( rule__ComposedLTS__Group_6__0 )? ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2851:1: ( ( rule__ComposedLTS__Group_6__0 )? )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2851:1: ( ( rule__ComposedLTS__Group_6__0 )? )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2852:1: ( rule__ComposedLTS__Group_6__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getGroup_6()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2853:1: ( rule__ComposedLTS__Group_6__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==26) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2853:2: rule__ComposedLTS__Group_6__0
                    {
                    pushFollow(FOLLOW_rule__ComposedLTS__Group_6__0_in_rule__ComposedLTS__Group__6__Impl5849);
                    rule__ComposedLTS__Group_6__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getGroup_6()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group__6__Impl"


    // $ANTLR start "rule__ComposedLTS__Group_6__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2877:1: rule__ComposedLTS__Group_6__0 : rule__ComposedLTS__Group_6__0__Impl rule__ComposedLTS__Group_6__1 ;
    public final void rule__ComposedLTS__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2881:1: ( rule__ComposedLTS__Group_6__0__Impl rule__ComposedLTS__Group_6__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2882:2: rule__ComposedLTS__Group_6__0__Impl rule__ComposedLTS__Group_6__1
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group_6__0__Impl_in_rule__ComposedLTS__Group_6__05894);
            rule__ComposedLTS__Group_6__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__ComposedLTS__Group_6__1_in_rule__ComposedLTS__Group_6__05897);
            rule__ComposedLTS__Group_6__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6__0"


    // $ANTLR start "rule__ComposedLTS__Group_6__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2889:1: rule__ComposedLTS__Group_6__0__Impl : ( 'using' ) ;
    public final void rule__ComposedLTS__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2893:1: ( ( 'using' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2894:1: ( 'using' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2894:1: ( 'using' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2895:1: 'using'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getUsingKeyword_6_0()); 
            }
            match(input,26,FOLLOW_26_in_rule__ComposedLTS__Group_6__0__Impl5925); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getUsingKeyword_6_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6__0__Impl"


    // $ANTLR start "rule__ComposedLTS__Group_6__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2908:1: rule__ComposedLTS__Group_6__1 : rule__ComposedLTS__Group_6__1__Impl rule__ComposedLTS__Group_6__2 ;
    public final void rule__ComposedLTS__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2912:1: ( rule__ComposedLTS__Group_6__1__Impl rule__ComposedLTS__Group_6__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2913:2: rule__ComposedLTS__Group_6__1__Impl rule__ComposedLTS__Group_6__2
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group_6__1__Impl_in_rule__ComposedLTS__Group_6__15956);
            rule__ComposedLTS__Group_6__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__ComposedLTS__Group_6__2_in_rule__ComposedLTS__Group_6__15959);
            rule__ComposedLTS__Group_6__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6__1"


    // $ANTLR start "rule__ComposedLTS__Group_6__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2920:1: rule__ComposedLTS__Group_6__1__Impl : ( '{' ) ;
    public final void rule__ComposedLTS__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2924:1: ( ( '{' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2925:1: ( '{' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2925:1: ( '{' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2926:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getLeftCurlyBracketKeyword_6_1()); 
            }
            match(input,13,FOLLOW_13_in_rule__ComposedLTS__Group_6__1__Impl5987); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getLeftCurlyBracketKeyword_6_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6__1__Impl"


    // $ANTLR start "rule__ComposedLTS__Group_6__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2939:1: rule__ComposedLTS__Group_6__2 : rule__ComposedLTS__Group_6__2__Impl rule__ComposedLTS__Group_6__3 ;
    public final void rule__ComposedLTS__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2943:1: ( rule__ComposedLTS__Group_6__2__Impl rule__ComposedLTS__Group_6__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2944:2: rule__ComposedLTS__Group_6__2__Impl rule__ComposedLTS__Group_6__3
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group_6__2__Impl_in_rule__ComposedLTS__Group_6__26018);
            rule__ComposedLTS__Group_6__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__ComposedLTS__Group_6__3_in_rule__ComposedLTS__Group_6__26021);
            rule__ComposedLTS__Group_6__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6__2"


    // $ANTLR start "rule__ComposedLTS__Group_6__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2951:1: rule__ComposedLTS__Group_6__2__Impl : ( ( rule__ComposedLTS__EventsAssignment_6_2 ) ) ;
    public final void rule__ComposedLTS__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2955:1: ( ( ( rule__ComposedLTS__EventsAssignment_6_2 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2956:1: ( ( rule__ComposedLTS__EventsAssignment_6_2 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2956:1: ( ( rule__ComposedLTS__EventsAssignment_6_2 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2957:1: ( rule__ComposedLTS__EventsAssignment_6_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getEventsAssignment_6_2()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2958:1: ( rule__ComposedLTS__EventsAssignment_6_2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2958:2: rule__ComposedLTS__EventsAssignment_6_2
            {
            pushFollow(FOLLOW_rule__ComposedLTS__EventsAssignment_6_2_in_rule__ComposedLTS__Group_6__2__Impl6048);
            rule__ComposedLTS__EventsAssignment_6_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getEventsAssignment_6_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6__2__Impl"


    // $ANTLR start "rule__ComposedLTS__Group_6__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2968:1: rule__ComposedLTS__Group_6__3 : rule__ComposedLTS__Group_6__3__Impl rule__ComposedLTS__Group_6__4 ;
    public final void rule__ComposedLTS__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2972:1: ( rule__ComposedLTS__Group_6__3__Impl rule__ComposedLTS__Group_6__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2973:2: rule__ComposedLTS__Group_6__3__Impl rule__ComposedLTS__Group_6__4
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group_6__3__Impl_in_rule__ComposedLTS__Group_6__36078);
            rule__ComposedLTS__Group_6__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__ComposedLTS__Group_6__4_in_rule__ComposedLTS__Group_6__36081);
            rule__ComposedLTS__Group_6__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6__3"


    // $ANTLR start "rule__ComposedLTS__Group_6__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2980:1: rule__ComposedLTS__Group_6__3__Impl : ( ( rule__ComposedLTS__Group_6_3__0 )* ) ;
    public final void rule__ComposedLTS__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2984:1: ( ( ( rule__ComposedLTS__Group_6_3__0 )* ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2985:1: ( ( rule__ComposedLTS__Group_6_3__0 )* )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2985:1: ( ( rule__ComposedLTS__Group_6_3__0 )* )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2986:1: ( rule__ComposedLTS__Group_6_3__0 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getGroup_6_3()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2987:1: ( rule__ComposedLTS__Group_6_3__0 )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==19) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2987:2: rule__ComposedLTS__Group_6_3__0
            	    {
            	    pushFollow(FOLLOW_rule__ComposedLTS__Group_6_3__0_in_rule__ComposedLTS__Group_6__3__Impl6108);
            	    rule__ComposedLTS__Group_6_3__0();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getGroup_6_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6__3__Impl"


    // $ANTLR start "rule__ComposedLTS__Group_6__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:2997:1: rule__ComposedLTS__Group_6__4 : rule__ComposedLTS__Group_6__4__Impl ;
    public final void rule__ComposedLTS__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3001:1: ( rule__ComposedLTS__Group_6__4__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3002:2: rule__ComposedLTS__Group_6__4__Impl
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group_6__4__Impl_in_rule__ComposedLTS__Group_6__46139);
            rule__ComposedLTS__Group_6__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6__4"


    // $ANTLR start "rule__ComposedLTS__Group_6__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3008:1: rule__ComposedLTS__Group_6__4__Impl : ( '}' ) ;
    public final void rule__ComposedLTS__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3012:1: ( ( '}' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3013:1: ( '}' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3013:1: ( '}' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3014:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getRightCurlyBracketKeyword_6_4()); 
            }
            match(input,16,FOLLOW_16_in_rule__ComposedLTS__Group_6__4__Impl6167); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getRightCurlyBracketKeyword_6_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6__4__Impl"


    // $ANTLR start "rule__ComposedLTS__Group_6_3__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3037:1: rule__ComposedLTS__Group_6_3__0 : rule__ComposedLTS__Group_6_3__0__Impl rule__ComposedLTS__Group_6_3__1 ;
    public final void rule__ComposedLTS__Group_6_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3041:1: ( rule__ComposedLTS__Group_6_3__0__Impl rule__ComposedLTS__Group_6_3__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3042:2: rule__ComposedLTS__Group_6_3__0__Impl rule__ComposedLTS__Group_6_3__1
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group_6_3__0__Impl_in_rule__ComposedLTS__Group_6_3__06208);
            rule__ComposedLTS__Group_6_3__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__ComposedLTS__Group_6_3__1_in_rule__ComposedLTS__Group_6_3__06211);
            rule__ComposedLTS__Group_6_3__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6_3__0"


    // $ANTLR start "rule__ComposedLTS__Group_6_3__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3049:1: rule__ComposedLTS__Group_6_3__0__Impl : ( ',' ) ;
    public final void rule__ComposedLTS__Group_6_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3053:1: ( ( ',' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3054:1: ( ',' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3054:1: ( ',' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3055:1: ','
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getCommaKeyword_6_3_0()); 
            }
            match(input,19,FOLLOW_19_in_rule__ComposedLTS__Group_6_3__0__Impl6239); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getCommaKeyword_6_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6_3__0__Impl"


    // $ANTLR start "rule__ComposedLTS__Group_6_3__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3068:1: rule__ComposedLTS__Group_6_3__1 : rule__ComposedLTS__Group_6_3__1__Impl ;
    public final void rule__ComposedLTS__Group_6_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3072:1: ( rule__ComposedLTS__Group_6_3__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3073:2: rule__ComposedLTS__Group_6_3__1__Impl
            {
            pushFollow(FOLLOW_rule__ComposedLTS__Group_6_3__1__Impl_in_rule__ComposedLTS__Group_6_3__16270);
            rule__ComposedLTS__Group_6_3__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6_3__1"


    // $ANTLR start "rule__ComposedLTS__Group_6_3__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3079:1: rule__ComposedLTS__Group_6_3__1__Impl : ( ( rule__ComposedLTS__EventsAssignment_6_3_1 ) ) ;
    public final void rule__ComposedLTS__Group_6_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3083:1: ( ( ( rule__ComposedLTS__EventsAssignment_6_3_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3084:1: ( ( rule__ComposedLTS__EventsAssignment_6_3_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3084:1: ( ( rule__ComposedLTS__EventsAssignment_6_3_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3085:1: ( rule__ComposedLTS__EventsAssignment_6_3_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getEventsAssignment_6_3_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3086:1: ( rule__ComposedLTS__EventsAssignment_6_3_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3086:2: rule__ComposedLTS__EventsAssignment_6_3_1
            {
            pushFollow(FOLLOW_rule__ComposedLTS__EventsAssignment_6_3_1_in_rule__ComposedLTS__Group_6_3__1__Impl6297);
            rule__ComposedLTS__EventsAssignment_6_3_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getEventsAssignment_6_3_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__Group_6_3__1__Impl"


    // $ANTLR start "rule__FormulaSet__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3100:1: rule__FormulaSet__Group__0 : rule__FormulaSet__Group__0__Impl rule__FormulaSet__Group__1 ;
    public final void rule__FormulaSet__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3104:1: ( rule__FormulaSet__Group__0__Impl rule__FormulaSet__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3105:2: rule__FormulaSet__Group__0__Impl rule__FormulaSet__Group__1
            {
            pushFollow(FOLLOW_rule__FormulaSet__Group__0__Impl_in_rule__FormulaSet__Group__06331);
            rule__FormulaSet__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__FormulaSet__Group__1_in_rule__FormulaSet__Group__06334);
            rule__FormulaSet__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__Group__0"


    // $ANTLR start "rule__FormulaSet__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3112:1: rule__FormulaSet__Group__0__Impl : ( 'CTL' ) ;
    public final void rule__FormulaSet__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3116:1: ( ( 'CTL' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3117:1: ( 'CTL' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3117:1: ( 'CTL' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3118:1: 'CTL'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaSetAccess().getCTLKeyword_0()); 
            }
            match(input,27,FOLLOW_27_in_rule__FormulaSet__Group__0__Impl6362); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaSetAccess().getCTLKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__Group__0__Impl"


    // $ANTLR start "rule__FormulaSet__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3131:1: rule__FormulaSet__Group__1 : rule__FormulaSet__Group__1__Impl rule__FormulaSet__Group__2 ;
    public final void rule__FormulaSet__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3135:1: ( rule__FormulaSet__Group__1__Impl rule__FormulaSet__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3136:2: rule__FormulaSet__Group__1__Impl rule__FormulaSet__Group__2
            {
            pushFollow(FOLLOW_rule__FormulaSet__Group__1__Impl_in_rule__FormulaSet__Group__16393);
            rule__FormulaSet__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__FormulaSet__Group__2_in_rule__FormulaSet__Group__16396);
            rule__FormulaSet__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__Group__1"


    // $ANTLR start "rule__FormulaSet__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3143:1: rule__FormulaSet__Group__1__Impl : ( 'FormulaSet' ) ;
    public final void rule__FormulaSet__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3147:1: ( ( 'FormulaSet' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3148:1: ( 'FormulaSet' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3148:1: ( 'FormulaSet' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3149:1: 'FormulaSet'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaSetAccess().getFormulaSetKeyword_1()); 
            }
            match(input,28,FOLLOW_28_in_rule__FormulaSet__Group__1__Impl6424); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaSetAccess().getFormulaSetKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__Group__1__Impl"


    // $ANTLR start "rule__FormulaSet__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3162:1: rule__FormulaSet__Group__2 : rule__FormulaSet__Group__2__Impl rule__FormulaSet__Group__3 ;
    public final void rule__FormulaSet__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3166:1: ( rule__FormulaSet__Group__2__Impl rule__FormulaSet__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3167:2: rule__FormulaSet__Group__2__Impl rule__FormulaSet__Group__3
            {
            pushFollow(FOLLOW_rule__FormulaSet__Group__2__Impl_in_rule__FormulaSet__Group__26455);
            rule__FormulaSet__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__FormulaSet__Group__3_in_rule__FormulaSet__Group__26458);
            rule__FormulaSet__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__Group__2"


    // $ANTLR start "rule__FormulaSet__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3174:1: rule__FormulaSet__Group__2__Impl : ( ( rule__FormulaSet__NameAssignment_2 ) ) ;
    public final void rule__FormulaSet__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3178:1: ( ( ( rule__FormulaSet__NameAssignment_2 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3179:1: ( ( rule__FormulaSet__NameAssignment_2 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3179:1: ( ( rule__FormulaSet__NameAssignment_2 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3180:1: ( rule__FormulaSet__NameAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaSetAccess().getNameAssignment_2()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3181:1: ( rule__FormulaSet__NameAssignment_2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3181:2: rule__FormulaSet__NameAssignment_2
            {
            pushFollow(FOLLOW_rule__FormulaSet__NameAssignment_2_in_rule__FormulaSet__Group__2__Impl6485);
            rule__FormulaSet__NameAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaSetAccess().getNameAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__Group__2__Impl"


    // $ANTLR start "rule__FormulaSet__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3191:1: rule__FormulaSet__Group__3 : rule__FormulaSet__Group__3__Impl rule__FormulaSet__Group__4 ;
    public final void rule__FormulaSet__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3195:1: ( rule__FormulaSet__Group__3__Impl rule__FormulaSet__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3196:2: rule__FormulaSet__Group__3__Impl rule__FormulaSet__Group__4
            {
            pushFollow(FOLLOW_rule__FormulaSet__Group__3__Impl_in_rule__FormulaSet__Group__36515);
            rule__FormulaSet__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__FormulaSet__Group__4_in_rule__FormulaSet__Group__36518);
            rule__FormulaSet__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__Group__3"


    // $ANTLR start "rule__FormulaSet__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3203:1: rule__FormulaSet__Group__3__Impl : ( '{' ) ;
    public final void rule__FormulaSet__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3207:1: ( ( '{' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3208:1: ( '{' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3208:1: ( '{' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3209:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaSetAccess().getLeftCurlyBracketKeyword_3()); 
            }
            match(input,13,FOLLOW_13_in_rule__FormulaSet__Group__3__Impl6546); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaSetAccess().getLeftCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__Group__3__Impl"


    // $ANTLR start "rule__FormulaSet__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3222:1: rule__FormulaSet__Group__4 : rule__FormulaSet__Group__4__Impl rule__FormulaSet__Group__5 ;
    public final void rule__FormulaSet__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3226:1: ( rule__FormulaSet__Group__4__Impl rule__FormulaSet__Group__5 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3227:2: rule__FormulaSet__Group__4__Impl rule__FormulaSet__Group__5
            {
            pushFollow(FOLLOW_rule__FormulaSet__Group__4__Impl_in_rule__FormulaSet__Group__46577);
            rule__FormulaSet__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__FormulaSet__Group__5_in_rule__FormulaSet__Group__46580);
            rule__FormulaSet__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__Group__4"


    // $ANTLR start "rule__FormulaSet__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3234:1: rule__FormulaSet__Group__4__Impl : ( ( rule__FormulaSet__FormulaeAssignment_4 )* ) ;
    public final void rule__FormulaSet__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3238:1: ( ( ( rule__FormulaSet__FormulaeAssignment_4 )* ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3239:1: ( ( rule__FormulaSet__FormulaeAssignment_4 )* )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3239:1: ( ( rule__FormulaSet__FormulaeAssignment_4 )* )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3240:1: ( rule__FormulaSet__FormulaeAssignment_4 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaSetAccess().getFormulaeAssignment_4()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3241:1: ( rule__FormulaSet__FormulaeAssignment_4 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==RULE_ID) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3241:2: rule__FormulaSet__FormulaeAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__FormulaSet__FormulaeAssignment_4_in_rule__FormulaSet__Group__4__Impl6607);
            	    rule__FormulaSet__FormulaeAssignment_4();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaSetAccess().getFormulaeAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__Group__4__Impl"


    // $ANTLR start "rule__FormulaSet__Group__5"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3251:1: rule__FormulaSet__Group__5 : rule__FormulaSet__Group__5__Impl ;
    public final void rule__FormulaSet__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3255:1: ( rule__FormulaSet__Group__5__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3256:2: rule__FormulaSet__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__FormulaSet__Group__5__Impl_in_rule__FormulaSet__Group__56638);
            rule__FormulaSet__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__Group__5"


    // $ANTLR start "rule__FormulaSet__Group__5__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3262:1: rule__FormulaSet__Group__5__Impl : ( '}' ) ;
    public final void rule__FormulaSet__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3266:1: ( ( '}' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3267:1: ( '}' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3267:1: ( '}' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3268:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaSetAccess().getRightCurlyBracketKeyword_5()); 
            }
            match(input,16,FOLLOW_16_in_rule__FormulaSet__Group__5__Impl6666); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaSetAccess().getRightCurlyBracketKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__Group__5__Impl"


    // $ANTLR start "rule__FormulaDefinition__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3293:1: rule__FormulaDefinition__Group__0 : rule__FormulaDefinition__Group__0__Impl rule__FormulaDefinition__Group__1 ;
    public final void rule__FormulaDefinition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3297:1: ( rule__FormulaDefinition__Group__0__Impl rule__FormulaDefinition__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3298:2: rule__FormulaDefinition__Group__0__Impl rule__FormulaDefinition__Group__1
            {
            pushFollow(FOLLOW_rule__FormulaDefinition__Group__0__Impl_in_rule__FormulaDefinition__Group__06709);
            rule__FormulaDefinition__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__FormulaDefinition__Group__1_in_rule__FormulaDefinition__Group__06712);
            rule__FormulaDefinition__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaDefinition__Group__0"


    // $ANTLR start "rule__FormulaDefinition__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3305:1: rule__FormulaDefinition__Group__0__Impl : ( ( rule__FormulaDefinition__NameAssignment_0 ) ) ;
    public final void rule__FormulaDefinition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3309:1: ( ( ( rule__FormulaDefinition__NameAssignment_0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3310:1: ( ( rule__FormulaDefinition__NameAssignment_0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3310:1: ( ( rule__FormulaDefinition__NameAssignment_0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3311:1: ( rule__FormulaDefinition__NameAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaDefinitionAccess().getNameAssignment_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3312:1: ( rule__FormulaDefinition__NameAssignment_0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3312:2: rule__FormulaDefinition__NameAssignment_0
            {
            pushFollow(FOLLOW_rule__FormulaDefinition__NameAssignment_0_in_rule__FormulaDefinition__Group__0__Impl6739);
            rule__FormulaDefinition__NameAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaDefinitionAccess().getNameAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaDefinition__Group__0__Impl"


    // $ANTLR start "rule__FormulaDefinition__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3322:1: rule__FormulaDefinition__Group__1 : rule__FormulaDefinition__Group__1__Impl rule__FormulaDefinition__Group__2 ;
    public final void rule__FormulaDefinition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3326:1: ( rule__FormulaDefinition__Group__1__Impl rule__FormulaDefinition__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3327:2: rule__FormulaDefinition__Group__1__Impl rule__FormulaDefinition__Group__2
            {
            pushFollow(FOLLOW_rule__FormulaDefinition__Group__1__Impl_in_rule__FormulaDefinition__Group__16769);
            rule__FormulaDefinition__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__FormulaDefinition__Group__2_in_rule__FormulaDefinition__Group__16772);
            rule__FormulaDefinition__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaDefinition__Group__1"


    // $ANTLR start "rule__FormulaDefinition__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3334:1: rule__FormulaDefinition__Group__1__Impl : ( ':=' ) ;
    public final void rule__FormulaDefinition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3338:1: ( ( ':=' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3339:1: ( ':=' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3339:1: ( ':=' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3340:1: ':='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaDefinitionAccess().getColonEqualsSignKeyword_1()); 
            }
            match(input,29,FOLLOW_29_in_rule__FormulaDefinition__Group__1__Impl6800); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaDefinitionAccess().getColonEqualsSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaDefinition__Group__1__Impl"


    // $ANTLR start "rule__FormulaDefinition__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3353:1: rule__FormulaDefinition__Group__2 : rule__FormulaDefinition__Group__2__Impl ;
    public final void rule__FormulaDefinition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3357:1: ( rule__FormulaDefinition__Group__2__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3358:2: rule__FormulaDefinition__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__FormulaDefinition__Group__2__Impl_in_rule__FormulaDefinition__Group__26831);
            rule__FormulaDefinition__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaDefinition__Group__2"


    // $ANTLR start "rule__FormulaDefinition__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3364:1: rule__FormulaDefinition__Group__2__Impl : ( ( rule__FormulaDefinition__FormulaAssignment_2 ) ) ;
    public final void rule__FormulaDefinition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3368:1: ( ( ( rule__FormulaDefinition__FormulaAssignment_2 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3369:1: ( ( rule__FormulaDefinition__FormulaAssignment_2 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3369:1: ( ( rule__FormulaDefinition__FormulaAssignment_2 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3370:1: ( rule__FormulaDefinition__FormulaAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaDefinitionAccess().getFormulaAssignment_2()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3371:1: ( rule__FormulaDefinition__FormulaAssignment_2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3371:2: rule__FormulaDefinition__FormulaAssignment_2
            {
            pushFollow(FOLLOW_rule__FormulaDefinition__FormulaAssignment_2_in_rule__FormulaDefinition__Group__2__Impl6858);
            rule__FormulaDefinition__FormulaAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaDefinitionAccess().getFormulaAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaDefinition__Group__2__Impl"


    // $ANTLR start "rule__EX__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3387:1: rule__EX__Group__0 : rule__EX__Group__0__Impl rule__EX__Group__1 ;
    public final void rule__EX__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3391:1: ( rule__EX__Group__0__Impl rule__EX__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3392:2: rule__EX__Group__0__Impl rule__EX__Group__1
            {
            pushFollow(FOLLOW_rule__EX__Group__0__Impl_in_rule__EX__Group__06894);
            rule__EX__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__EX__Group__1_in_rule__EX__Group__06897);
            rule__EX__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EX__Group__0"


    // $ANTLR start "rule__EX__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3399:1: rule__EX__Group__0__Impl : ( 'EX' ) ;
    public final void rule__EX__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3403:1: ( ( 'EX' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3404:1: ( 'EX' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3404:1: ( 'EX' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3405:1: 'EX'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEXAccess().getEXKeyword_0()); 
            }
            match(input,30,FOLLOW_30_in_rule__EX__Group__0__Impl6925); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEXAccess().getEXKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EX__Group__0__Impl"


    // $ANTLR start "rule__EX__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3418:1: rule__EX__Group__1 : rule__EX__Group__1__Impl ;
    public final void rule__EX__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3422:1: ( rule__EX__Group__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3423:2: rule__EX__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__EX__Group__1__Impl_in_rule__EX__Group__16956);
            rule__EX__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EX__Group__1"


    // $ANTLR start "rule__EX__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3429:1: rule__EX__Group__1__Impl : ( ( rule__EX__NestedAssignment_1 ) ) ;
    public final void rule__EX__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3433:1: ( ( ( rule__EX__NestedAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3434:1: ( ( rule__EX__NestedAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3434:1: ( ( rule__EX__NestedAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3435:1: ( rule__EX__NestedAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEXAccess().getNestedAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3436:1: ( rule__EX__NestedAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3436:2: rule__EX__NestedAssignment_1
            {
            pushFollow(FOLLOW_rule__EX__NestedAssignment_1_in_rule__EX__Group__1__Impl6983);
            rule__EX__NestedAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEXAccess().getNestedAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EX__Group__1__Impl"


    // $ANTLR start "rule__EG__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3450:1: rule__EG__Group__0 : rule__EG__Group__0__Impl rule__EG__Group__1 ;
    public final void rule__EG__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3454:1: ( rule__EG__Group__0__Impl rule__EG__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3455:2: rule__EG__Group__0__Impl rule__EG__Group__1
            {
            pushFollow(FOLLOW_rule__EG__Group__0__Impl_in_rule__EG__Group__07017);
            rule__EG__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__EG__Group__1_in_rule__EG__Group__07020);
            rule__EG__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EG__Group__0"


    // $ANTLR start "rule__EG__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3462:1: rule__EG__Group__0__Impl : ( 'EG' ) ;
    public final void rule__EG__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3466:1: ( ( 'EG' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3467:1: ( 'EG' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3467:1: ( 'EG' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3468:1: 'EG'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEGAccess().getEGKeyword_0()); 
            }
            match(input,31,FOLLOW_31_in_rule__EG__Group__0__Impl7048); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEGAccess().getEGKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EG__Group__0__Impl"


    // $ANTLR start "rule__EG__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3481:1: rule__EG__Group__1 : rule__EG__Group__1__Impl ;
    public final void rule__EG__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3485:1: ( rule__EG__Group__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3486:2: rule__EG__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__EG__Group__1__Impl_in_rule__EG__Group__17079);
            rule__EG__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EG__Group__1"


    // $ANTLR start "rule__EG__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3492:1: rule__EG__Group__1__Impl : ( ( rule__EG__NestedAssignment_1 ) ) ;
    public final void rule__EG__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3496:1: ( ( ( rule__EG__NestedAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3497:1: ( ( rule__EG__NestedAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3497:1: ( ( rule__EG__NestedAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3498:1: ( rule__EG__NestedAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEGAccess().getNestedAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3499:1: ( rule__EG__NestedAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3499:2: rule__EG__NestedAssignment_1
            {
            pushFollow(FOLLOW_rule__EG__NestedAssignment_1_in_rule__EG__Group__1__Impl7106);
            rule__EG__NestedAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEGAccess().getNestedAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EG__Group__1__Impl"


    // $ANTLR start "rule__EU__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3513:1: rule__EU__Group__0 : rule__EU__Group__0__Impl rule__EU__Group__1 ;
    public final void rule__EU__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3517:1: ( rule__EU__Group__0__Impl rule__EU__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3518:2: rule__EU__Group__0__Impl rule__EU__Group__1
            {
            pushFollow(FOLLOW_rule__EU__Group__0__Impl_in_rule__EU__Group__07140);
            rule__EU__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__EU__Group__1_in_rule__EU__Group__07143);
            rule__EU__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__Group__0"


    // $ANTLR start "rule__EU__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3525:1: rule__EU__Group__0__Impl : ( 'E' ) ;
    public final void rule__EU__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3529:1: ( ( 'E' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3530:1: ( 'E' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3530:1: ( 'E' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3531:1: 'E'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEUAccess().getEKeyword_0()); 
            }
            match(input,32,FOLLOW_32_in_rule__EU__Group__0__Impl7171); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEUAccess().getEKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__Group__0__Impl"


    // $ANTLR start "rule__EU__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3544:1: rule__EU__Group__1 : rule__EU__Group__1__Impl rule__EU__Group__2 ;
    public final void rule__EU__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3548:1: ( rule__EU__Group__1__Impl rule__EU__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3549:2: rule__EU__Group__1__Impl rule__EU__Group__2
            {
            pushFollow(FOLLOW_rule__EU__Group__1__Impl_in_rule__EU__Group__17202);
            rule__EU__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__EU__Group__2_in_rule__EU__Group__17205);
            rule__EU__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__Group__1"


    // $ANTLR start "rule__EU__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3556:1: rule__EU__Group__1__Impl : ( '[' ) ;
    public final void rule__EU__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3560:1: ( ( '[' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3561:1: ( '[' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3561:1: ( '[' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3562:1: '['
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEUAccess().getLeftSquareBracketKeyword_1()); 
            }
            match(input,33,FOLLOW_33_in_rule__EU__Group__1__Impl7233); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEUAccess().getLeftSquareBracketKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__Group__1__Impl"


    // $ANTLR start "rule__EU__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3575:1: rule__EU__Group__2 : rule__EU__Group__2__Impl rule__EU__Group__3 ;
    public final void rule__EU__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3579:1: ( rule__EU__Group__2__Impl rule__EU__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3580:2: rule__EU__Group__2__Impl rule__EU__Group__3
            {
            pushFollow(FOLLOW_rule__EU__Group__2__Impl_in_rule__EU__Group__27264);
            rule__EU__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__EU__Group__3_in_rule__EU__Group__27267);
            rule__EU__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__Group__2"


    // $ANTLR start "rule__EU__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3587:1: rule__EU__Group__2__Impl : ( ( rule__EU__LeftAssignment_2 ) ) ;
    public final void rule__EU__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3591:1: ( ( ( rule__EU__LeftAssignment_2 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3592:1: ( ( rule__EU__LeftAssignment_2 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3592:1: ( ( rule__EU__LeftAssignment_2 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3593:1: ( rule__EU__LeftAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEUAccess().getLeftAssignment_2()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3594:1: ( rule__EU__LeftAssignment_2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3594:2: rule__EU__LeftAssignment_2
            {
            pushFollow(FOLLOW_rule__EU__LeftAssignment_2_in_rule__EU__Group__2__Impl7294);
            rule__EU__LeftAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEUAccess().getLeftAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__Group__2__Impl"


    // $ANTLR start "rule__EU__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3604:1: rule__EU__Group__3 : rule__EU__Group__3__Impl rule__EU__Group__4 ;
    public final void rule__EU__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3608:1: ( rule__EU__Group__3__Impl rule__EU__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3609:2: rule__EU__Group__3__Impl rule__EU__Group__4
            {
            pushFollow(FOLLOW_rule__EU__Group__3__Impl_in_rule__EU__Group__37324);
            rule__EU__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__EU__Group__4_in_rule__EU__Group__37327);
            rule__EU__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__Group__3"


    // $ANTLR start "rule__EU__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3616:1: rule__EU__Group__3__Impl : ( 'U' ) ;
    public final void rule__EU__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3620:1: ( ( 'U' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3621:1: ( 'U' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3621:1: ( 'U' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3622:1: 'U'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEUAccess().getUKeyword_3()); 
            }
            match(input,34,FOLLOW_34_in_rule__EU__Group__3__Impl7355); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEUAccess().getUKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__Group__3__Impl"


    // $ANTLR start "rule__EU__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3635:1: rule__EU__Group__4 : rule__EU__Group__4__Impl rule__EU__Group__5 ;
    public final void rule__EU__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3639:1: ( rule__EU__Group__4__Impl rule__EU__Group__5 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3640:2: rule__EU__Group__4__Impl rule__EU__Group__5
            {
            pushFollow(FOLLOW_rule__EU__Group__4__Impl_in_rule__EU__Group__47386);
            rule__EU__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__EU__Group__5_in_rule__EU__Group__47389);
            rule__EU__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__Group__4"


    // $ANTLR start "rule__EU__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3647:1: rule__EU__Group__4__Impl : ( ( rule__EU__RightAssignment_4 ) ) ;
    public final void rule__EU__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3651:1: ( ( ( rule__EU__RightAssignment_4 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3652:1: ( ( rule__EU__RightAssignment_4 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3652:1: ( ( rule__EU__RightAssignment_4 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3653:1: ( rule__EU__RightAssignment_4 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEUAccess().getRightAssignment_4()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3654:1: ( rule__EU__RightAssignment_4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3654:2: rule__EU__RightAssignment_4
            {
            pushFollow(FOLLOW_rule__EU__RightAssignment_4_in_rule__EU__Group__4__Impl7416);
            rule__EU__RightAssignment_4();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEUAccess().getRightAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__Group__4__Impl"


    // $ANTLR start "rule__EU__Group__5"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3664:1: rule__EU__Group__5 : rule__EU__Group__5__Impl ;
    public final void rule__EU__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3668:1: ( rule__EU__Group__5__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3669:2: rule__EU__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__EU__Group__5__Impl_in_rule__EU__Group__57446);
            rule__EU__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__Group__5"


    // $ANTLR start "rule__EU__Group__5__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3675:1: rule__EU__Group__5__Impl : ( ']' ) ;
    public final void rule__EU__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3679:1: ( ( ']' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3680:1: ( ']' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3680:1: ( ']' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3681:1: ']'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEUAccess().getRightSquareBracketKeyword_5()); 
            }
            match(input,35,FOLLOW_35_in_rule__EU__Group__5__Impl7474); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEUAccess().getRightSquareBracketKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__Group__5__Impl"


    // $ANTLR start "rule__AND__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3706:1: rule__AND__Group__0 : rule__AND__Group__0__Impl rule__AND__Group__1 ;
    public final void rule__AND__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3710:1: ( rule__AND__Group__0__Impl rule__AND__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3711:2: rule__AND__Group__0__Impl rule__AND__Group__1
            {
            pushFollow(FOLLOW_rule__AND__Group__0__Impl_in_rule__AND__Group__07517);
            rule__AND__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__AND__Group__1_in_rule__AND__Group__07520);
            rule__AND__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AND__Group__0"


    // $ANTLR start "rule__AND__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3718:1: rule__AND__Group__0__Impl : ( '(' ) ;
    public final void rule__AND__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3722:1: ( ( '(' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3723:1: ( '(' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3723:1: ( '(' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3724:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getANDAccess().getLeftParenthesisKeyword_0()); 
            }
            match(input,36,FOLLOW_36_in_rule__AND__Group__0__Impl7548); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getANDAccess().getLeftParenthesisKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AND__Group__0__Impl"


    // $ANTLR start "rule__AND__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3737:1: rule__AND__Group__1 : rule__AND__Group__1__Impl rule__AND__Group__2 ;
    public final void rule__AND__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3741:1: ( rule__AND__Group__1__Impl rule__AND__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3742:2: rule__AND__Group__1__Impl rule__AND__Group__2
            {
            pushFollow(FOLLOW_rule__AND__Group__1__Impl_in_rule__AND__Group__17579);
            rule__AND__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__AND__Group__2_in_rule__AND__Group__17582);
            rule__AND__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AND__Group__1"


    // $ANTLR start "rule__AND__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3749:1: rule__AND__Group__1__Impl : ( ( rule__AND__LeftAssignment_1 ) ) ;
    public final void rule__AND__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3753:1: ( ( ( rule__AND__LeftAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3754:1: ( ( rule__AND__LeftAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3754:1: ( ( rule__AND__LeftAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3755:1: ( rule__AND__LeftAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getANDAccess().getLeftAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3756:1: ( rule__AND__LeftAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3756:2: rule__AND__LeftAssignment_1
            {
            pushFollow(FOLLOW_rule__AND__LeftAssignment_1_in_rule__AND__Group__1__Impl7609);
            rule__AND__LeftAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getANDAccess().getLeftAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AND__Group__1__Impl"


    // $ANTLR start "rule__AND__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3766:1: rule__AND__Group__2 : rule__AND__Group__2__Impl rule__AND__Group__3 ;
    public final void rule__AND__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3770:1: ( rule__AND__Group__2__Impl rule__AND__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3771:2: rule__AND__Group__2__Impl rule__AND__Group__3
            {
            pushFollow(FOLLOW_rule__AND__Group__2__Impl_in_rule__AND__Group__27639);
            rule__AND__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__AND__Group__3_in_rule__AND__Group__27642);
            rule__AND__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AND__Group__2"


    // $ANTLR start "rule__AND__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3778:1: rule__AND__Group__2__Impl : ( '\\u22C0' ) ;
    public final void rule__AND__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3782:1: ( ( '\\u22C0' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3783:1: ( '\\u22C0' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3783:1: ( '\\u22C0' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3784:1: '\\u22C0'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getANDAccess().getNAryLogicalAndKeyword_2()); 
            }
            match(input,37,FOLLOW_37_in_rule__AND__Group__2__Impl7670); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getANDAccess().getNAryLogicalAndKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AND__Group__2__Impl"


    // $ANTLR start "rule__AND__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3797:1: rule__AND__Group__3 : rule__AND__Group__3__Impl rule__AND__Group__4 ;
    public final void rule__AND__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3801:1: ( rule__AND__Group__3__Impl rule__AND__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3802:2: rule__AND__Group__3__Impl rule__AND__Group__4
            {
            pushFollow(FOLLOW_rule__AND__Group__3__Impl_in_rule__AND__Group__37701);
            rule__AND__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__AND__Group__4_in_rule__AND__Group__37704);
            rule__AND__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AND__Group__3"


    // $ANTLR start "rule__AND__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3809:1: rule__AND__Group__3__Impl : ( ( rule__AND__RightAssignment_3 ) ) ;
    public final void rule__AND__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3813:1: ( ( ( rule__AND__RightAssignment_3 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3814:1: ( ( rule__AND__RightAssignment_3 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3814:1: ( ( rule__AND__RightAssignment_3 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3815:1: ( rule__AND__RightAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getANDAccess().getRightAssignment_3()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3816:1: ( rule__AND__RightAssignment_3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3816:2: rule__AND__RightAssignment_3
            {
            pushFollow(FOLLOW_rule__AND__RightAssignment_3_in_rule__AND__Group__3__Impl7731);
            rule__AND__RightAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getANDAccess().getRightAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AND__Group__3__Impl"


    // $ANTLR start "rule__AND__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3826:1: rule__AND__Group__4 : rule__AND__Group__4__Impl ;
    public final void rule__AND__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3830:1: ( rule__AND__Group__4__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3831:2: rule__AND__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__AND__Group__4__Impl_in_rule__AND__Group__47761);
            rule__AND__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AND__Group__4"


    // $ANTLR start "rule__AND__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3837:1: rule__AND__Group__4__Impl : ( ')' ) ;
    public final void rule__AND__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3841:1: ( ( ')' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3842:1: ( ')' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3842:1: ( ')' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3843:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getANDAccess().getRightParenthesisKeyword_4()); 
            }
            match(input,38,FOLLOW_38_in_rule__AND__Group__4__Impl7789); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getANDAccess().getRightParenthesisKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AND__Group__4__Impl"


    // $ANTLR start "rule__OR__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3866:1: rule__OR__Group__0 : rule__OR__Group__0__Impl rule__OR__Group__1 ;
    public final void rule__OR__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3870:1: ( rule__OR__Group__0__Impl rule__OR__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3871:2: rule__OR__Group__0__Impl rule__OR__Group__1
            {
            pushFollow(FOLLOW_rule__OR__Group__0__Impl_in_rule__OR__Group__07830);
            rule__OR__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__OR__Group__1_in_rule__OR__Group__07833);
            rule__OR__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OR__Group__0"


    // $ANTLR start "rule__OR__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3878:1: rule__OR__Group__0__Impl : ( '(' ) ;
    public final void rule__OR__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3882:1: ( ( '(' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3883:1: ( '(' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3883:1: ( '(' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3884:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getORAccess().getLeftParenthesisKeyword_0()); 
            }
            match(input,36,FOLLOW_36_in_rule__OR__Group__0__Impl7861); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getORAccess().getLeftParenthesisKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OR__Group__0__Impl"


    // $ANTLR start "rule__OR__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3897:1: rule__OR__Group__1 : rule__OR__Group__1__Impl rule__OR__Group__2 ;
    public final void rule__OR__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3901:1: ( rule__OR__Group__1__Impl rule__OR__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3902:2: rule__OR__Group__1__Impl rule__OR__Group__2
            {
            pushFollow(FOLLOW_rule__OR__Group__1__Impl_in_rule__OR__Group__17892);
            rule__OR__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__OR__Group__2_in_rule__OR__Group__17895);
            rule__OR__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OR__Group__1"


    // $ANTLR start "rule__OR__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3909:1: rule__OR__Group__1__Impl : ( ( rule__OR__LeftAssignment_1 ) ) ;
    public final void rule__OR__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3913:1: ( ( ( rule__OR__LeftAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3914:1: ( ( rule__OR__LeftAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3914:1: ( ( rule__OR__LeftAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3915:1: ( rule__OR__LeftAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getORAccess().getLeftAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3916:1: ( rule__OR__LeftAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3916:2: rule__OR__LeftAssignment_1
            {
            pushFollow(FOLLOW_rule__OR__LeftAssignment_1_in_rule__OR__Group__1__Impl7922);
            rule__OR__LeftAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getORAccess().getLeftAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OR__Group__1__Impl"


    // $ANTLR start "rule__OR__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3926:1: rule__OR__Group__2 : rule__OR__Group__2__Impl rule__OR__Group__3 ;
    public final void rule__OR__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3930:1: ( rule__OR__Group__2__Impl rule__OR__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3931:2: rule__OR__Group__2__Impl rule__OR__Group__3
            {
            pushFollow(FOLLOW_rule__OR__Group__2__Impl_in_rule__OR__Group__27952);
            rule__OR__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__OR__Group__3_in_rule__OR__Group__27955);
            rule__OR__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OR__Group__2"


    // $ANTLR start "rule__OR__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3938:1: rule__OR__Group__2__Impl : ( '\\u22C1' ) ;
    public final void rule__OR__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3942:1: ( ( '\\u22C1' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3943:1: ( '\\u22C1' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3943:1: ( '\\u22C1' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3944:1: '\\u22C1'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getORAccess().getNAryLogicalOrKeyword_2()); 
            }
            match(input,39,FOLLOW_39_in_rule__OR__Group__2__Impl7983); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getORAccess().getNAryLogicalOrKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OR__Group__2__Impl"


    // $ANTLR start "rule__OR__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3957:1: rule__OR__Group__3 : rule__OR__Group__3__Impl rule__OR__Group__4 ;
    public final void rule__OR__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3961:1: ( rule__OR__Group__3__Impl rule__OR__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3962:2: rule__OR__Group__3__Impl rule__OR__Group__4
            {
            pushFollow(FOLLOW_rule__OR__Group__3__Impl_in_rule__OR__Group__38014);
            rule__OR__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__OR__Group__4_in_rule__OR__Group__38017);
            rule__OR__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OR__Group__3"


    // $ANTLR start "rule__OR__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3969:1: rule__OR__Group__3__Impl : ( ( rule__OR__RightAssignment_3 ) ) ;
    public final void rule__OR__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3973:1: ( ( ( rule__OR__RightAssignment_3 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3974:1: ( ( rule__OR__RightAssignment_3 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3974:1: ( ( rule__OR__RightAssignment_3 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3975:1: ( rule__OR__RightAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getORAccess().getRightAssignment_3()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3976:1: ( rule__OR__RightAssignment_3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3976:2: rule__OR__RightAssignment_3
            {
            pushFollow(FOLLOW_rule__OR__RightAssignment_3_in_rule__OR__Group__3__Impl8044);
            rule__OR__RightAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getORAccess().getRightAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OR__Group__3__Impl"


    // $ANTLR start "rule__OR__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3986:1: rule__OR__Group__4 : rule__OR__Group__4__Impl ;
    public final void rule__OR__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3990:1: ( rule__OR__Group__4__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3991:2: rule__OR__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__OR__Group__4__Impl_in_rule__OR__Group__48074);
            rule__OR__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OR__Group__4"


    // $ANTLR start "rule__OR__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:3997:1: rule__OR__Group__4__Impl : ( ')' ) ;
    public final void rule__OR__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4001:1: ( ( ')' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4002:1: ( ')' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4002:1: ( ')' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4003:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getORAccess().getRightParenthesisKeyword_4()); 
            }
            match(input,38,FOLLOW_38_in_rule__OR__Group__4__Impl8102); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getORAccess().getRightParenthesisKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OR__Group__4__Impl"


    // $ANTLR start "rule__NOT__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4026:1: rule__NOT__Group__0 : rule__NOT__Group__0__Impl rule__NOT__Group__1 ;
    public final void rule__NOT__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4030:1: ( rule__NOT__Group__0__Impl rule__NOT__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4031:2: rule__NOT__Group__0__Impl rule__NOT__Group__1
            {
            pushFollow(FOLLOW_rule__NOT__Group__0__Impl_in_rule__NOT__Group__08143);
            rule__NOT__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__NOT__Group__1_in_rule__NOT__Group__08146);
            rule__NOT__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NOT__Group__0"


    // $ANTLR start "rule__NOT__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4038:1: rule__NOT__Group__0__Impl : ( '\\u00AC' ) ;
    public final void rule__NOT__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4042:1: ( ( '\\u00AC' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4043:1: ( '\\u00AC' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4043:1: ( '\\u00AC' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4044:1: '\\u00AC'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNOTAccess().getNotSignKeyword_0()); 
            }
            match(input,40,FOLLOW_40_in_rule__NOT__Group__0__Impl8174); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNOTAccess().getNotSignKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NOT__Group__0__Impl"


    // $ANTLR start "rule__NOT__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4057:1: rule__NOT__Group__1 : rule__NOT__Group__1__Impl ;
    public final void rule__NOT__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4061:1: ( rule__NOT__Group__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4062:2: rule__NOT__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__NOT__Group__1__Impl_in_rule__NOT__Group__18205);
            rule__NOT__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NOT__Group__1"


    // $ANTLR start "rule__NOT__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4068:1: rule__NOT__Group__1__Impl : ( ( rule__NOT__NestedAssignment_1 ) ) ;
    public final void rule__NOT__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4072:1: ( ( ( rule__NOT__NestedAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4073:1: ( ( rule__NOT__NestedAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4073:1: ( ( rule__NOT__NestedAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4074:1: ( rule__NOT__NestedAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNOTAccess().getNestedAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4075:1: ( rule__NOT__NestedAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4075:2: rule__NOT__NestedAssignment_1
            {
            pushFollow(FOLLOW_rule__NOT__NestedAssignment_1_in_rule__NOT__Group__1__Impl8232);
            rule__NOT__NestedAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNOTAccess().getNestedAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NOT__Group__1__Impl"


    // $ANTLR start "rule__Constant__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4089:1: rule__Constant__Group__0 : rule__Constant__Group__0__Impl rule__Constant__Group__1 ;
    public final void rule__Constant__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4093:1: ( rule__Constant__Group__0__Impl rule__Constant__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4094:2: rule__Constant__Group__0__Impl rule__Constant__Group__1
            {
            pushFollow(FOLLOW_rule__Constant__Group__0__Impl_in_rule__Constant__Group__08266);
            rule__Constant__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Constant__Group__1_in_rule__Constant__Group__08269);
            rule__Constant__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group__0"


    // $ANTLR start "rule__Constant__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4101:1: rule__Constant__Group__0__Impl : ( () ) ;
    public final void rule__Constant__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4105:1: ( ( () ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4106:1: ( () )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4106:1: ( () )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4107:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstantAccess().getConstantAction_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4108:1: ()
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4110:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstantAccess().getConstantAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group__0__Impl"


    // $ANTLR start "rule__Constant__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4120:1: rule__Constant__Group__1 : rule__Constant__Group__1__Impl ;
    public final void rule__Constant__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4124:1: ( rule__Constant__Group__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4125:2: rule__Constant__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Constant__Group__1__Impl_in_rule__Constant__Group__18327);
            rule__Constant__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group__1"


    // $ANTLR start "rule__Constant__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4131:1: rule__Constant__Group__1__Impl : ( ( rule__Constant__Alternatives_1 ) ) ;
    public final void rule__Constant__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4135:1: ( ( ( rule__Constant__Alternatives_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4136:1: ( ( rule__Constant__Alternatives_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4136:1: ( ( rule__Constant__Alternatives_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4137:1: ( rule__Constant__Alternatives_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstantAccess().getAlternatives_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4138:1: ( rule__Constant__Alternatives_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4138:2: rule__Constant__Alternatives_1
            {
            pushFollow(FOLLOW_rule__Constant__Alternatives_1_in_rule__Constant__Group__1__Impl8354);
            rule__Constant__Alternatives_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstantAccess().getAlternatives_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__Group__1__Impl"


    // $ANTLR start "rule__CheckSet__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4152:1: rule__CheckSet__Group__0 : rule__CheckSet__Group__0__Impl rule__CheckSet__Group__1 ;
    public final void rule__CheckSet__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4156:1: ( rule__CheckSet__Group__0__Impl rule__CheckSet__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4157:2: rule__CheckSet__Group__0__Impl rule__CheckSet__Group__1
            {
            pushFollow(FOLLOW_rule__CheckSet__Group__0__Impl_in_rule__CheckSet__Group__08388);
            rule__CheckSet__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__CheckSet__Group__1_in_rule__CheckSet__Group__08391);
            rule__CheckSet__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__Group__0"


    // $ANTLR start "rule__CheckSet__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4164:1: rule__CheckSet__Group__0__Impl : ( 'CTL' ) ;
    public final void rule__CheckSet__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4168:1: ( ( 'CTL' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4169:1: ( 'CTL' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4169:1: ( 'CTL' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4170:1: 'CTL'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckSetAccess().getCTLKeyword_0()); 
            }
            match(input,27,FOLLOW_27_in_rule__CheckSet__Group__0__Impl8419); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckSetAccess().getCTLKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__Group__0__Impl"


    // $ANTLR start "rule__CheckSet__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4183:1: rule__CheckSet__Group__1 : rule__CheckSet__Group__1__Impl rule__CheckSet__Group__2 ;
    public final void rule__CheckSet__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4187:1: ( rule__CheckSet__Group__1__Impl rule__CheckSet__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4188:2: rule__CheckSet__Group__1__Impl rule__CheckSet__Group__2
            {
            pushFollow(FOLLOW_rule__CheckSet__Group__1__Impl_in_rule__CheckSet__Group__18450);
            rule__CheckSet__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__CheckSet__Group__2_in_rule__CheckSet__Group__18453);
            rule__CheckSet__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__Group__1"


    // $ANTLR start "rule__CheckSet__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4195:1: rule__CheckSet__Group__1__Impl : ( 'CheckSet' ) ;
    public final void rule__CheckSet__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4199:1: ( ( 'CheckSet' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4200:1: ( 'CheckSet' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4200:1: ( 'CheckSet' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4201:1: 'CheckSet'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckSetAccess().getCheckSetKeyword_1()); 
            }
            match(input,41,FOLLOW_41_in_rule__CheckSet__Group__1__Impl8481); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckSetAccess().getCheckSetKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__Group__1__Impl"


    // $ANTLR start "rule__CheckSet__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4214:1: rule__CheckSet__Group__2 : rule__CheckSet__Group__2__Impl rule__CheckSet__Group__3 ;
    public final void rule__CheckSet__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4218:1: ( rule__CheckSet__Group__2__Impl rule__CheckSet__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4219:2: rule__CheckSet__Group__2__Impl rule__CheckSet__Group__3
            {
            pushFollow(FOLLOW_rule__CheckSet__Group__2__Impl_in_rule__CheckSet__Group__28512);
            rule__CheckSet__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__CheckSet__Group__3_in_rule__CheckSet__Group__28515);
            rule__CheckSet__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__Group__2"


    // $ANTLR start "rule__CheckSet__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4226:1: rule__CheckSet__Group__2__Impl : ( ( rule__CheckSet__NameAssignment_2 ) ) ;
    public final void rule__CheckSet__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4230:1: ( ( ( rule__CheckSet__NameAssignment_2 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4231:1: ( ( rule__CheckSet__NameAssignment_2 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4231:1: ( ( rule__CheckSet__NameAssignment_2 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4232:1: ( rule__CheckSet__NameAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckSetAccess().getNameAssignment_2()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4233:1: ( rule__CheckSet__NameAssignment_2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4233:2: rule__CheckSet__NameAssignment_2
            {
            pushFollow(FOLLOW_rule__CheckSet__NameAssignment_2_in_rule__CheckSet__Group__2__Impl8542);
            rule__CheckSet__NameAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckSetAccess().getNameAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__Group__2__Impl"


    // $ANTLR start "rule__CheckSet__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4243:1: rule__CheckSet__Group__3 : rule__CheckSet__Group__3__Impl rule__CheckSet__Group__4 ;
    public final void rule__CheckSet__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4247:1: ( rule__CheckSet__Group__3__Impl rule__CheckSet__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4248:2: rule__CheckSet__Group__3__Impl rule__CheckSet__Group__4
            {
            pushFollow(FOLLOW_rule__CheckSet__Group__3__Impl_in_rule__CheckSet__Group__38572);
            rule__CheckSet__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__CheckSet__Group__4_in_rule__CheckSet__Group__38575);
            rule__CheckSet__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__Group__3"


    // $ANTLR start "rule__CheckSet__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4255:1: rule__CheckSet__Group__3__Impl : ( '{' ) ;
    public final void rule__CheckSet__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4259:1: ( ( '{' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4260:1: ( '{' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4260:1: ( '{' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4261:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckSetAccess().getLeftCurlyBracketKeyword_3()); 
            }
            match(input,13,FOLLOW_13_in_rule__CheckSet__Group__3__Impl8603); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckSetAccess().getLeftCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__Group__3__Impl"


    // $ANTLR start "rule__CheckSet__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4274:1: rule__CheckSet__Group__4 : rule__CheckSet__Group__4__Impl rule__CheckSet__Group__5 ;
    public final void rule__CheckSet__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4278:1: ( rule__CheckSet__Group__4__Impl rule__CheckSet__Group__5 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4279:2: rule__CheckSet__Group__4__Impl rule__CheckSet__Group__5
            {
            pushFollow(FOLLOW_rule__CheckSet__Group__4__Impl_in_rule__CheckSet__Group__48634);
            rule__CheckSet__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__CheckSet__Group__5_in_rule__CheckSet__Group__48637);
            rule__CheckSet__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__Group__4"


    // $ANTLR start "rule__CheckSet__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4286:1: rule__CheckSet__Group__4__Impl : ( ( rule__CheckSet__ChecksAssignment_4 )* ) ;
    public final void rule__CheckSet__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4290:1: ( ( ( rule__CheckSet__ChecksAssignment_4 )* ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4291:1: ( ( rule__CheckSet__ChecksAssignment_4 )* )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4291:1: ( ( rule__CheckSet__ChecksAssignment_4 )* )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4292:1: ( rule__CheckSet__ChecksAssignment_4 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckSetAccess().getChecksAssignment_4()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4293:1: ( rule__CheckSet__ChecksAssignment_4 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==42) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4293:2: rule__CheckSet__ChecksAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__CheckSet__ChecksAssignment_4_in_rule__CheckSet__Group__4__Impl8664);
            	    rule__CheckSet__ChecksAssignment_4();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckSetAccess().getChecksAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__Group__4__Impl"


    // $ANTLR start "rule__CheckSet__Group__5"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4303:1: rule__CheckSet__Group__5 : rule__CheckSet__Group__5__Impl ;
    public final void rule__CheckSet__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4307:1: ( rule__CheckSet__Group__5__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4308:2: rule__CheckSet__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__CheckSet__Group__5__Impl_in_rule__CheckSet__Group__58695);
            rule__CheckSet__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__Group__5"


    // $ANTLR start "rule__CheckSet__Group__5__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4314:1: rule__CheckSet__Group__5__Impl : ( '}' ) ;
    public final void rule__CheckSet__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4318:1: ( ( '}' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4319:1: ( '}' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4319:1: ( '}' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4320:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckSetAccess().getRightCurlyBracketKeyword_5()); 
            }
            match(input,16,FOLLOW_16_in_rule__CheckSet__Group__5__Impl8723); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckSetAccess().getRightCurlyBracketKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__Group__5__Impl"


    // $ANTLR start "rule__Check__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4345:1: rule__Check__Group__0 : rule__Check__Group__0__Impl rule__Check__Group__1 ;
    public final void rule__Check__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4349:1: ( rule__Check__Group__0__Impl rule__Check__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4350:2: rule__Check__Group__0__Impl rule__Check__Group__1
            {
            pushFollow(FOLLOW_rule__Check__Group__0__Impl_in_rule__Check__Group__08766);
            rule__Check__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Check__Group__1_in_rule__Check__Group__08769);
            rule__Check__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__0"


    // $ANTLR start "rule__Check__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4357:1: rule__Check__Group__0__Impl : ( 'check' ) ;
    public final void rule__Check__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4361:1: ( ( 'check' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4362:1: ( 'check' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4362:1: ( 'check' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4363:1: 'check'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckAccess().getCheckKeyword_0()); 
            }
            match(input,42,FOLLOW_42_in_rule__Check__Group__0__Impl8797); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckAccess().getCheckKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__0__Impl"


    // $ANTLR start "rule__Check__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4376:1: rule__Check__Group__1 : rule__Check__Group__1__Impl rule__Check__Group__2 ;
    public final void rule__Check__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4380:1: ( rule__Check__Group__1__Impl rule__Check__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4381:2: rule__Check__Group__1__Impl rule__Check__Group__2
            {
            pushFollow(FOLLOW_rule__Check__Group__1__Impl_in_rule__Check__Group__18828);
            rule__Check__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Check__Group__2_in_rule__Check__Group__18831);
            rule__Check__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__1"


    // $ANTLR start "rule__Check__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4388:1: rule__Check__Group__1__Impl : ( ( rule__Check__LtsAssignment_1 ) ) ;
    public final void rule__Check__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4392:1: ( ( ( rule__Check__LtsAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4393:1: ( ( rule__Check__LtsAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4393:1: ( ( rule__Check__LtsAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4394:1: ( rule__Check__LtsAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckAccess().getLtsAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4395:1: ( rule__Check__LtsAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4395:2: rule__Check__LtsAssignment_1
            {
            pushFollow(FOLLOW_rule__Check__LtsAssignment_1_in_rule__Check__Group__1__Impl8858);
            rule__Check__LtsAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckAccess().getLtsAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__1__Impl"


    // $ANTLR start "rule__Check__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4405:1: rule__Check__Group__2 : rule__Check__Group__2__Impl rule__Check__Group__3 ;
    public final void rule__Check__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4409:1: ( rule__Check__Group__2__Impl rule__Check__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4410:2: rule__Check__Group__2__Impl rule__Check__Group__3
            {
            pushFollow(FOLLOW_rule__Check__Group__2__Impl_in_rule__Check__Group__28888);
            rule__Check__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Check__Group__3_in_rule__Check__Group__28891);
            rule__Check__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__2"


    // $ANTLR start "rule__Check__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4417:1: rule__Check__Group__2__Impl : ( '\\u22A8' ) ;
    public final void rule__Check__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4421:1: ( ( '\\u22A8' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4422:1: ( '\\u22A8' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4422:1: ( '\\u22A8' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4423:1: '\\u22A8'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckAccess().getTrueKeyword_2()); 
            }
            match(input,43,FOLLOW_43_in_rule__Check__Group__2__Impl8919); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckAccess().getTrueKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__2__Impl"


    // $ANTLR start "rule__Check__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4436:1: rule__Check__Group__3 : rule__Check__Group__3__Impl ;
    public final void rule__Check__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4440:1: ( rule__Check__Group__3__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4441:2: rule__Check__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__Check__Group__3__Impl_in_rule__Check__Group__38950);
            rule__Check__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__3"


    // $ANTLR start "rule__Check__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4447:1: rule__Check__Group__3__Impl : ( ( rule__Check__FormulaDefinitionAssignment_3 ) ) ;
    public final void rule__Check__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4451:1: ( ( ( rule__Check__FormulaDefinitionAssignment_3 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4452:1: ( ( rule__Check__FormulaDefinitionAssignment_3 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4452:1: ( ( rule__Check__FormulaDefinitionAssignment_3 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4453:1: ( rule__Check__FormulaDefinitionAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckAccess().getFormulaDefinitionAssignment_3()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4454:1: ( rule__Check__FormulaDefinitionAssignment_3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4454:2: rule__Check__FormulaDefinitionAssignment_3
            {
            pushFollow(FOLLOW_rule__Check__FormulaDefinitionAssignment_3_in_rule__Check__Group__3__Impl8977);
            rule__Check__FormulaDefinitionAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckAccess().getFormulaDefinitionAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__Group__3__Impl"


    // $ANTLR start "rule__LTLFormulaSet__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4472:1: rule__LTLFormulaSet__Group__0 : rule__LTLFormulaSet__Group__0__Impl rule__LTLFormulaSet__Group__1 ;
    public final void rule__LTLFormulaSet__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4476:1: ( rule__LTLFormulaSet__Group__0__Impl rule__LTLFormulaSet__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4477:2: rule__LTLFormulaSet__Group__0__Impl rule__LTLFormulaSet__Group__1
            {
            pushFollow(FOLLOW_rule__LTLFormulaSet__Group__0__Impl_in_rule__LTLFormulaSet__Group__09015);
            rule__LTLFormulaSet__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLFormulaSet__Group__1_in_rule__LTLFormulaSet__Group__09018);
            rule__LTLFormulaSet__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__Group__0"


    // $ANTLR start "rule__LTLFormulaSet__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4484:1: rule__LTLFormulaSet__Group__0__Impl : ( 'LTL' ) ;
    public final void rule__LTLFormulaSet__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4488:1: ( ( 'LTL' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4489:1: ( 'LTL' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4489:1: ( 'LTL' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4490:1: 'LTL'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaSetAccess().getLTLKeyword_0()); 
            }
            match(input,44,FOLLOW_44_in_rule__LTLFormulaSet__Group__0__Impl9046); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaSetAccess().getLTLKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__Group__0__Impl"


    // $ANTLR start "rule__LTLFormulaSet__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4503:1: rule__LTLFormulaSet__Group__1 : rule__LTLFormulaSet__Group__1__Impl rule__LTLFormulaSet__Group__2 ;
    public final void rule__LTLFormulaSet__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4507:1: ( rule__LTLFormulaSet__Group__1__Impl rule__LTLFormulaSet__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4508:2: rule__LTLFormulaSet__Group__1__Impl rule__LTLFormulaSet__Group__2
            {
            pushFollow(FOLLOW_rule__LTLFormulaSet__Group__1__Impl_in_rule__LTLFormulaSet__Group__19077);
            rule__LTLFormulaSet__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLFormulaSet__Group__2_in_rule__LTLFormulaSet__Group__19080);
            rule__LTLFormulaSet__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__Group__1"


    // $ANTLR start "rule__LTLFormulaSet__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4515:1: rule__LTLFormulaSet__Group__1__Impl : ( 'FormulaSet' ) ;
    public final void rule__LTLFormulaSet__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4519:1: ( ( 'FormulaSet' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4520:1: ( 'FormulaSet' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4520:1: ( 'FormulaSet' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4521:1: 'FormulaSet'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaSetAccess().getFormulaSetKeyword_1()); 
            }
            match(input,28,FOLLOW_28_in_rule__LTLFormulaSet__Group__1__Impl9108); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaSetAccess().getFormulaSetKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__Group__1__Impl"


    // $ANTLR start "rule__LTLFormulaSet__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4534:1: rule__LTLFormulaSet__Group__2 : rule__LTLFormulaSet__Group__2__Impl rule__LTLFormulaSet__Group__3 ;
    public final void rule__LTLFormulaSet__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4538:1: ( rule__LTLFormulaSet__Group__2__Impl rule__LTLFormulaSet__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4539:2: rule__LTLFormulaSet__Group__2__Impl rule__LTLFormulaSet__Group__3
            {
            pushFollow(FOLLOW_rule__LTLFormulaSet__Group__2__Impl_in_rule__LTLFormulaSet__Group__29139);
            rule__LTLFormulaSet__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLFormulaSet__Group__3_in_rule__LTLFormulaSet__Group__29142);
            rule__LTLFormulaSet__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__Group__2"


    // $ANTLR start "rule__LTLFormulaSet__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4546:1: rule__LTLFormulaSet__Group__2__Impl : ( ( rule__LTLFormulaSet__NameAssignment_2 ) ) ;
    public final void rule__LTLFormulaSet__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4550:1: ( ( ( rule__LTLFormulaSet__NameAssignment_2 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4551:1: ( ( rule__LTLFormulaSet__NameAssignment_2 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4551:1: ( ( rule__LTLFormulaSet__NameAssignment_2 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4552:1: ( rule__LTLFormulaSet__NameAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaSetAccess().getNameAssignment_2()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4553:1: ( rule__LTLFormulaSet__NameAssignment_2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4553:2: rule__LTLFormulaSet__NameAssignment_2
            {
            pushFollow(FOLLOW_rule__LTLFormulaSet__NameAssignment_2_in_rule__LTLFormulaSet__Group__2__Impl9169);
            rule__LTLFormulaSet__NameAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaSetAccess().getNameAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__Group__2__Impl"


    // $ANTLR start "rule__LTLFormulaSet__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4563:1: rule__LTLFormulaSet__Group__3 : rule__LTLFormulaSet__Group__3__Impl rule__LTLFormulaSet__Group__4 ;
    public final void rule__LTLFormulaSet__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4567:1: ( rule__LTLFormulaSet__Group__3__Impl rule__LTLFormulaSet__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4568:2: rule__LTLFormulaSet__Group__3__Impl rule__LTLFormulaSet__Group__4
            {
            pushFollow(FOLLOW_rule__LTLFormulaSet__Group__3__Impl_in_rule__LTLFormulaSet__Group__39199);
            rule__LTLFormulaSet__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLFormulaSet__Group__4_in_rule__LTLFormulaSet__Group__39202);
            rule__LTLFormulaSet__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__Group__3"


    // $ANTLR start "rule__LTLFormulaSet__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4575:1: rule__LTLFormulaSet__Group__3__Impl : ( '{' ) ;
    public final void rule__LTLFormulaSet__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4579:1: ( ( '{' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4580:1: ( '{' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4580:1: ( '{' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4581:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaSetAccess().getLeftCurlyBracketKeyword_3()); 
            }
            match(input,13,FOLLOW_13_in_rule__LTLFormulaSet__Group__3__Impl9230); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaSetAccess().getLeftCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__Group__3__Impl"


    // $ANTLR start "rule__LTLFormulaSet__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4594:1: rule__LTLFormulaSet__Group__4 : rule__LTLFormulaSet__Group__4__Impl rule__LTLFormulaSet__Group__5 ;
    public final void rule__LTLFormulaSet__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4598:1: ( rule__LTLFormulaSet__Group__4__Impl rule__LTLFormulaSet__Group__5 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4599:2: rule__LTLFormulaSet__Group__4__Impl rule__LTLFormulaSet__Group__5
            {
            pushFollow(FOLLOW_rule__LTLFormulaSet__Group__4__Impl_in_rule__LTLFormulaSet__Group__49261);
            rule__LTLFormulaSet__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLFormulaSet__Group__5_in_rule__LTLFormulaSet__Group__49264);
            rule__LTLFormulaSet__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__Group__4"


    // $ANTLR start "rule__LTLFormulaSet__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4606:1: rule__LTLFormulaSet__Group__4__Impl : ( ( rule__LTLFormulaSet__FormulaeAssignment_4 )* ) ;
    public final void rule__LTLFormulaSet__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4610:1: ( ( ( rule__LTLFormulaSet__FormulaeAssignment_4 )* ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4611:1: ( ( rule__LTLFormulaSet__FormulaeAssignment_4 )* )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4611:1: ( ( rule__LTLFormulaSet__FormulaeAssignment_4 )* )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4612:1: ( rule__LTLFormulaSet__FormulaeAssignment_4 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaSetAccess().getFormulaeAssignment_4()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4613:1: ( rule__LTLFormulaSet__FormulaeAssignment_4 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==RULE_ID) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4613:2: rule__LTLFormulaSet__FormulaeAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__LTLFormulaSet__FormulaeAssignment_4_in_rule__LTLFormulaSet__Group__4__Impl9291);
            	    rule__LTLFormulaSet__FormulaeAssignment_4();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaSetAccess().getFormulaeAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__Group__4__Impl"


    // $ANTLR start "rule__LTLFormulaSet__Group__5"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4623:1: rule__LTLFormulaSet__Group__5 : rule__LTLFormulaSet__Group__5__Impl ;
    public final void rule__LTLFormulaSet__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4627:1: ( rule__LTLFormulaSet__Group__5__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4628:2: rule__LTLFormulaSet__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__LTLFormulaSet__Group__5__Impl_in_rule__LTLFormulaSet__Group__59322);
            rule__LTLFormulaSet__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__Group__5"


    // $ANTLR start "rule__LTLFormulaSet__Group__5__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4634:1: rule__LTLFormulaSet__Group__5__Impl : ( '}' ) ;
    public final void rule__LTLFormulaSet__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4638:1: ( ( '}' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4639:1: ( '}' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4639:1: ( '}' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4640:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaSetAccess().getRightCurlyBracketKeyword_5()); 
            }
            match(input,16,FOLLOW_16_in_rule__LTLFormulaSet__Group__5__Impl9350); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaSetAccess().getRightCurlyBracketKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__Group__5__Impl"


    // $ANTLR start "rule__LTLFormulaDefinition__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4665:1: rule__LTLFormulaDefinition__Group__0 : rule__LTLFormulaDefinition__Group__0__Impl rule__LTLFormulaDefinition__Group__1 ;
    public final void rule__LTLFormulaDefinition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4669:1: ( rule__LTLFormulaDefinition__Group__0__Impl rule__LTLFormulaDefinition__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4670:2: rule__LTLFormulaDefinition__Group__0__Impl rule__LTLFormulaDefinition__Group__1
            {
            pushFollow(FOLLOW_rule__LTLFormulaDefinition__Group__0__Impl_in_rule__LTLFormulaDefinition__Group__09393);
            rule__LTLFormulaDefinition__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLFormulaDefinition__Group__1_in_rule__LTLFormulaDefinition__Group__09396);
            rule__LTLFormulaDefinition__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaDefinition__Group__0"


    // $ANTLR start "rule__LTLFormulaDefinition__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4677:1: rule__LTLFormulaDefinition__Group__0__Impl : ( ( rule__LTLFormulaDefinition__NameAssignment_0 ) ) ;
    public final void rule__LTLFormulaDefinition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4681:1: ( ( ( rule__LTLFormulaDefinition__NameAssignment_0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4682:1: ( ( rule__LTLFormulaDefinition__NameAssignment_0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4682:1: ( ( rule__LTLFormulaDefinition__NameAssignment_0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4683:1: ( rule__LTLFormulaDefinition__NameAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaDefinitionAccess().getNameAssignment_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4684:1: ( rule__LTLFormulaDefinition__NameAssignment_0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4684:2: rule__LTLFormulaDefinition__NameAssignment_0
            {
            pushFollow(FOLLOW_rule__LTLFormulaDefinition__NameAssignment_0_in_rule__LTLFormulaDefinition__Group__0__Impl9423);
            rule__LTLFormulaDefinition__NameAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaDefinitionAccess().getNameAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaDefinition__Group__0__Impl"


    // $ANTLR start "rule__LTLFormulaDefinition__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4694:1: rule__LTLFormulaDefinition__Group__1 : rule__LTLFormulaDefinition__Group__1__Impl rule__LTLFormulaDefinition__Group__2 ;
    public final void rule__LTLFormulaDefinition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4698:1: ( rule__LTLFormulaDefinition__Group__1__Impl rule__LTLFormulaDefinition__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4699:2: rule__LTLFormulaDefinition__Group__1__Impl rule__LTLFormulaDefinition__Group__2
            {
            pushFollow(FOLLOW_rule__LTLFormulaDefinition__Group__1__Impl_in_rule__LTLFormulaDefinition__Group__19453);
            rule__LTLFormulaDefinition__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLFormulaDefinition__Group__2_in_rule__LTLFormulaDefinition__Group__19456);
            rule__LTLFormulaDefinition__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaDefinition__Group__1"


    // $ANTLR start "rule__LTLFormulaDefinition__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4706:1: rule__LTLFormulaDefinition__Group__1__Impl : ( ':=' ) ;
    public final void rule__LTLFormulaDefinition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4710:1: ( ( ':=' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4711:1: ( ':=' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4711:1: ( ':=' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4712:1: ':='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaDefinitionAccess().getColonEqualsSignKeyword_1()); 
            }
            match(input,29,FOLLOW_29_in_rule__LTLFormulaDefinition__Group__1__Impl9484); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaDefinitionAccess().getColonEqualsSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaDefinition__Group__1__Impl"


    // $ANTLR start "rule__LTLFormulaDefinition__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4725:1: rule__LTLFormulaDefinition__Group__2 : rule__LTLFormulaDefinition__Group__2__Impl ;
    public final void rule__LTLFormulaDefinition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4729:1: ( rule__LTLFormulaDefinition__Group__2__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4730:2: rule__LTLFormulaDefinition__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__LTLFormulaDefinition__Group__2__Impl_in_rule__LTLFormulaDefinition__Group__29515);
            rule__LTLFormulaDefinition__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaDefinition__Group__2"


    // $ANTLR start "rule__LTLFormulaDefinition__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4736:1: rule__LTLFormulaDefinition__Group__2__Impl : ( ( rule__LTLFormulaDefinition__FormulaAssignment_2 ) ) ;
    public final void rule__LTLFormulaDefinition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4740:1: ( ( ( rule__LTLFormulaDefinition__FormulaAssignment_2 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4741:1: ( ( rule__LTLFormulaDefinition__FormulaAssignment_2 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4741:1: ( ( rule__LTLFormulaDefinition__FormulaAssignment_2 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4742:1: ( rule__LTLFormulaDefinition__FormulaAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaDefinitionAccess().getFormulaAssignment_2()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4743:1: ( rule__LTLFormulaDefinition__FormulaAssignment_2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4743:2: rule__LTLFormulaDefinition__FormulaAssignment_2
            {
            pushFollow(FOLLOW_rule__LTLFormulaDefinition__FormulaAssignment_2_in_rule__LTLFormulaDefinition__Group__2__Impl9542);
            rule__LTLFormulaDefinition__FormulaAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaDefinitionAccess().getFormulaAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaDefinition__Group__2__Impl"


    // $ANTLR start "rule__LTLX__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4759:1: rule__LTLX__Group__0 : rule__LTLX__Group__0__Impl rule__LTLX__Group__1 ;
    public final void rule__LTLX__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4763:1: ( rule__LTLX__Group__0__Impl rule__LTLX__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4764:2: rule__LTLX__Group__0__Impl rule__LTLX__Group__1
            {
            pushFollow(FOLLOW_rule__LTLX__Group__0__Impl_in_rule__LTLX__Group__09578);
            rule__LTLX__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLX__Group__1_in_rule__LTLX__Group__09581);
            rule__LTLX__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLX__Group__0"


    // $ANTLR start "rule__LTLX__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4771:1: rule__LTLX__Group__0__Impl : ( 'X' ) ;
    public final void rule__LTLX__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4775:1: ( ( 'X' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4776:1: ( 'X' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4776:1: ( 'X' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4777:1: 'X'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLXAccess().getXKeyword_0()); 
            }
            match(input,45,FOLLOW_45_in_rule__LTLX__Group__0__Impl9609); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLXAccess().getXKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLX__Group__0__Impl"


    // $ANTLR start "rule__LTLX__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4790:1: rule__LTLX__Group__1 : rule__LTLX__Group__1__Impl ;
    public final void rule__LTLX__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4794:1: ( rule__LTLX__Group__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4795:2: rule__LTLX__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__LTLX__Group__1__Impl_in_rule__LTLX__Group__19640);
            rule__LTLX__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLX__Group__1"


    // $ANTLR start "rule__LTLX__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4801:1: rule__LTLX__Group__1__Impl : ( ( rule__LTLX__NestedAssignment_1 ) ) ;
    public final void rule__LTLX__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4805:1: ( ( ( rule__LTLX__NestedAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4806:1: ( ( rule__LTLX__NestedAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4806:1: ( ( rule__LTLX__NestedAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4807:1: ( rule__LTLX__NestedAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLXAccess().getNestedAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4808:1: ( rule__LTLX__NestedAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4808:2: rule__LTLX__NestedAssignment_1
            {
            pushFollow(FOLLOW_rule__LTLX__NestedAssignment_1_in_rule__LTLX__Group__1__Impl9667);
            rule__LTLX__NestedAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLXAccess().getNestedAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLX__Group__1__Impl"


    // $ANTLR start "rule__LTLG__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4822:1: rule__LTLG__Group__0 : rule__LTLG__Group__0__Impl rule__LTLG__Group__1 ;
    public final void rule__LTLG__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4826:1: ( rule__LTLG__Group__0__Impl rule__LTLG__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4827:2: rule__LTLG__Group__0__Impl rule__LTLG__Group__1
            {
            pushFollow(FOLLOW_rule__LTLG__Group__0__Impl_in_rule__LTLG__Group__09701);
            rule__LTLG__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLG__Group__1_in_rule__LTLG__Group__09704);
            rule__LTLG__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLG__Group__0"


    // $ANTLR start "rule__LTLG__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4834:1: rule__LTLG__Group__0__Impl : ( 'G' ) ;
    public final void rule__LTLG__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4838:1: ( ( 'G' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4839:1: ( 'G' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4839:1: ( 'G' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4840:1: 'G'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLGAccess().getGKeyword_0()); 
            }
            match(input,46,FOLLOW_46_in_rule__LTLG__Group__0__Impl9732); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLGAccess().getGKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLG__Group__0__Impl"


    // $ANTLR start "rule__LTLG__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4853:1: rule__LTLG__Group__1 : rule__LTLG__Group__1__Impl ;
    public final void rule__LTLG__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4857:1: ( rule__LTLG__Group__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4858:2: rule__LTLG__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__LTLG__Group__1__Impl_in_rule__LTLG__Group__19763);
            rule__LTLG__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLG__Group__1"


    // $ANTLR start "rule__LTLG__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4864:1: rule__LTLG__Group__1__Impl : ( ( rule__LTLG__NestedAssignment_1 ) ) ;
    public final void rule__LTLG__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4868:1: ( ( ( rule__LTLG__NestedAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4869:1: ( ( rule__LTLG__NestedAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4869:1: ( ( rule__LTLG__NestedAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4870:1: ( rule__LTLG__NestedAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLGAccess().getNestedAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4871:1: ( rule__LTLG__NestedAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4871:2: rule__LTLG__NestedAssignment_1
            {
            pushFollow(FOLLOW_rule__LTLG__NestedAssignment_1_in_rule__LTLG__Group__1__Impl9790);
            rule__LTLG__NestedAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLGAccess().getNestedAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLG__Group__1__Impl"


    // $ANTLR start "rule__LTLU__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4885:1: rule__LTLU__Group__0 : rule__LTLU__Group__0__Impl rule__LTLU__Group__1 ;
    public final void rule__LTLU__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4889:1: ( rule__LTLU__Group__0__Impl rule__LTLU__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4890:2: rule__LTLU__Group__0__Impl rule__LTLU__Group__1
            {
            pushFollow(FOLLOW_rule__LTLU__Group__0__Impl_in_rule__LTLU__Group__09824);
            rule__LTLU__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLU__Group__1_in_rule__LTLU__Group__09827);
            rule__LTLU__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLU__Group__0"


    // $ANTLR start "rule__LTLU__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4897:1: rule__LTLU__Group__0__Impl : ( '[' ) ;
    public final void rule__LTLU__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4901:1: ( ( '[' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4902:1: ( '[' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4902:1: ( '[' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4903:1: '['
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLUAccess().getLeftSquareBracketKeyword_0()); 
            }
            match(input,33,FOLLOW_33_in_rule__LTLU__Group__0__Impl9855); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLUAccess().getLeftSquareBracketKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLU__Group__0__Impl"


    // $ANTLR start "rule__LTLU__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4916:1: rule__LTLU__Group__1 : rule__LTLU__Group__1__Impl rule__LTLU__Group__2 ;
    public final void rule__LTLU__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4920:1: ( rule__LTLU__Group__1__Impl rule__LTLU__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4921:2: rule__LTLU__Group__1__Impl rule__LTLU__Group__2
            {
            pushFollow(FOLLOW_rule__LTLU__Group__1__Impl_in_rule__LTLU__Group__19886);
            rule__LTLU__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLU__Group__2_in_rule__LTLU__Group__19889);
            rule__LTLU__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLU__Group__1"


    // $ANTLR start "rule__LTLU__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4928:1: rule__LTLU__Group__1__Impl : ( ( rule__LTLU__LeftAssignment_1 ) ) ;
    public final void rule__LTLU__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4932:1: ( ( ( rule__LTLU__LeftAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4933:1: ( ( rule__LTLU__LeftAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4933:1: ( ( rule__LTLU__LeftAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4934:1: ( rule__LTLU__LeftAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLUAccess().getLeftAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4935:1: ( rule__LTLU__LeftAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4935:2: rule__LTLU__LeftAssignment_1
            {
            pushFollow(FOLLOW_rule__LTLU__LeftAssignment_1_in_rule__LTLU__Group__1__Impl9916);
            rule__LTLU__LeftAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLUAccess().getLeftAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLU__Group__1__Impl"


    // $ANTLR start "rule__LTLU__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4945:1: rule__LTLU__Group__2 : rule__LTLU__Group__2__Impl rule__LTLU__Group__3 ;
    public final void rule__LTLU__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4949:1: ( rule__LTLU__Group__2__Impl rule__LTLU__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4950:2: rule__LTLU__Group__2__Impl rule__LTLU__Group__3
            {
            pushFollow(FOLLOW_rule__LTLU__Group__2__Impl_in_rule__LTLU__Group__29946);
            rule__LTLU__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLU__Group__3_in_rule__LTLU__Group__29949);
            rule__LTLU__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLU__Group__2"


    // $ANTLR start "rule__LTLU__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4957:1: rule__LTLU__Group__2__Impl : ( 'U' ) ;
    public final void rule__LTLU__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4961:1: ( ( 'U' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4962:1: ( 'U' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4962:1: ( 'U' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4963:1: 'U'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLUAccess().getUKeyword_2()); 
            }
            match(input,34,FOLLOW_34_in_rule__LTLU__Group__2__Impl9977); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLUAccess().getUKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLU__Group__2__Impl"


    // $ANTLR start "rule__LTLU__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4976:1: rule__LTLU__Group__3 : rule__LTLU__Group__3__Impl rule__LTLU__Group__4 ;
    public final void rule__LTLU__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4980:1: ( rule__LTLU__Group__3__Impl rule__LTLU__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4981:2: rule__LTLU__Group__3__Impl rule__LTLU__Group__4
            {
            pushFollow(FOLLOW_rule__LTLU__Group__3__Impl_in_rule__LTLU__Group__310008);
            rule__LTLU__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLU__Group__4_in_rule__LTLU__Group__310011);
            rule__LTLU__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLU__Group__3"


    // $ANTLR start "rule__LTLU__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4988:1: rule__LTLU__Group__3__Impl : ( ( rule__LTLU__RightAssignment_3 ) ) ;
    public final void rule__LTLU__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4992:1: ( ( ( rule__LTLU__RightAssignment_3 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4993:1: ( ( rule__LTLU__RightAssignment_3 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4993:1: ( ( rule__LTLU__RightAssignment_3 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4994:1: ( rule__LTLU__RightAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLUAccess().getRightAssignment_3()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4995:1: ( rule__LTLU__RightAssignment_3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:4995:2: rule__LTLU__RightAssignment_3
            {
            pushFollow(FOLLOW_rule__LTLU__RightAssignment_3_in_rule__LTLU__Group__3__Impl10038);
            rule__LTLU__RightAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLUAccess().getRightAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLU__Group__3__Impl"


    // $ANTLR start "rule__LTLU__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5005:1: rule__LTLU__Group__4 : rule__LTLU__Group__4__Impl ;
    public final void rule__LTLU__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5009:1: ( rule__LTLU__Group__4__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5010:2: rule__LTLU__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__LTLU__Group__4__Impl_in_rule__LTLU__Group__410068);
            rule__LTLU__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLU__Group__4"


    // $ANTLR start "rule__LTLU__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5016:1: rule__LTLU__Group__4__Impl : ( ']' ) ;
    public final void rule__LTLU__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5020:1: ( ( ']' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5021:1: ( ']' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5021:1: ( ']' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5022:1: ']'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLUAccess().getRightSquareBracketKeyword_4()); 
            }
            match(input,35,FOLLOW_35_in_rule__LTLU__Group__4__Impl10096); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLUAccess().getRightSquareBracketKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLU__Group__4__Impl"


    // $ANTLR start "rule__LTLF__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5045:1: rule__LTLF__Group__0 : rule__LTLF__Group__0__Impl rule__LTLF__Group__1 ;
    public final void rule__LTLF__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5049:1: ( rule__LTLF__Group__0__Impl rule__LTLF__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5050:2: rule__LTLF__Group__0__Impl rule__LTLF__Group__1
            {
            pushFollow(FOLLOW_rule__LTLF__Group__0__Impl_in_rule__LTLF__Group__010137);
            rule__LTLF__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLF__Group__1_in_rule__LTLF__Group__010140);
            rule__LTLF__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLF__Group__0"


    // $ANTLR start "rule__LTLF__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5057:1: rule__LTLF__Group__0__Impl : ( 'F' ) ;
    public final void rule__LTLF__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5061:1: ( ( 'F' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5062:1: ( 'F' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5062:1: ( 'F' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5063:1: 'F'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFAccess().getFKeyword_0()); 
            }
            match(input,47,FOLLOW_47_in_rule__LTLF__Group__0__Impl10168); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFAccess().getFKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLF__Group__0__Impl"


    // $ANTLR start "rule__LTLF__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5076:1: rule__LTLF__Group__1 : rule__LTLF__Group__1__Impl ;
    public final void rule__LTLF__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5080:1: ( rule__LTLF__Group__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5081:2: rule__LTLF__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__LTLF__Group__1__Impl_in_rule__LTLF__Group__110199);
            rule__LTLF__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLF__Group__1"


    // $ANTLR start "rule__LTLF__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5087:1: rule__LTLF__Group__1__Impl : ( ( rule__LTLF__NestedAssignment_1 ) ) ;
    public final void rule__LTLF__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5091:1: ( ( ( rule__LTLF__NestedAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5092:1: ( ( rule__LTLF__NestedAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5092:1: ( ( rule__LTLF__NestedAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5093:1: ( rule__LTLF__NestedAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFAccess().getNestedAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5094:1: ( rule__LTLF__NestedAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5094:2: rule__LTLF__NestedAssignment_1
            {
            pushFollow(FOLLOW_rule__LTLF__NestedAssignment_1_in_rule__LTLF__Group__1__Impl10226);
            rule__LTLF__NestedAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFAccess().getNestedAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLF__Group__1__Impl"


    // $ANTLR start "rule__LTLNOT__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5108:1: rule__LTLNOT__Group__0 : rule__LTLNOT__Group__0__Impl rule__LTLNOT__Group__1 ;
    public final void rule__LTLNOT__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5112:1: ( rule__LTLNOT__Group__0__Impl rule__LTLNOT__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5113:2: rule__LTLNOT__Group__0__Impl rule__LTLNOT__Group__1
            {
            pushFollow(FOLLOW_rule__LTLNOT__Group__0__Impl_in_rule__LTLNOT__Group__010260);
            rule__LTLNOT__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLNOT__Group__1_in_rule__LTLNOT__Group__010263);
            rule__LTLNOT__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLNOT__Group__0"


    // $ANTLR start "rule__LTLNOT__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5120:1: rule__LTLNOT__Group__0__Impl : ( '\\u00AC' ) ;
    public final void rule__LTLNOT__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5124:1: ( ( '\\u00AC' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5125:1: ( '\\u00AC' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5125:1: ( '\\u00AC' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5126:1: '\\u00AC'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLNOTAccess().getNotSignKeyword_0()); 
            }
            match(input,40,FOLLOW_40_in_rule__LTLNOT__Group__0__Impl10291); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLNOTAccess().getNotSignKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLNOT__Group__0__Impl"


    // $ANTLR start "rule__LTLNOT__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5139:1: rule__LTLNOT__Group__1 : rule__LTLNOT__Group__1__Impl ;
    public final void rule__LTLNOT__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5143:1: ( rule__LTLNOT__Group__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5144:2: rule__LTLNOT__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__LTLNOT__Group__1__Impl_in_rule__LTLNOT__Group__110322);
            rule__LTLNOT__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLNOT__Group__1"


    // $ANTLR start "rule__LTLNOT__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5150:1: rule__LTLNOT__Group__1__Impl : ( ( rule__LTLNOT__NestedAssignment_1 ) ) ;
    public final void rule__LTLNOT__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5154:1: ( ( ( rule__LTLNOT__NestedAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5155:1: ( ( rule__LTLNOT__NestedAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5155:1: ( ( rule__LTLNOT__NestedAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5156:1: ( rule__LTLNOT__NestedAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLNOTAccess().getNestedAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5157:1: ( rule__LTLNOT__NestedAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5157:2: rule__LTLNOT__NestedAssignment_1
            {
            pushFollow(FOLLOW_rule__LTLNOT__NestedAssignment_1_in_rule__LTLNOT__Group__1__Impl10349);
            rule__LTLNOT__NestedAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLNOTAccess().getNestedAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLNOT__Group__1__Impl"


    // $ANTLR start "rule__LTLAND__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5171:1: rule__LTLAND__Group__0 : rule__LTLAND__Group__0__Impl rule__LTLAND__Group__1 ;
    public final void rule__LTLAND__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5175:1: ( rule__LTLAND__Group__0__Impl rule__LTLAND__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5176:2: rule__LTLAND__Group__0__Impl rule__LTLAND__Group__1
            {
            pushFollow(FOLLOW_rule__LTLAND__Group__0__Impl_in_rule__LTLAND__Group__010383);
            rule__LTLAND__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLAND__Group__1_in_rule__LTLAND__Group__010386);
            rule__LTLAND__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLAND__Group__0"


    // $ANTLR start "rule__LTLAND__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5183:1: rule__LTLAND__Group__0__Impl : ( '(' ) ;
    public final void rule__LTLAND__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5187:1: ( ( '(' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5188:1: ( '(' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5188:1: ( '(' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5189:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLANDAccess().getLeftParenthesisKeyword_0()); 
            }
            match(input,36,FOLLOW_36_in_rule__LTLAND__Group__0__Impl10414); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLANDAccess().getLeftParenthesisKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLAND__Group__0__Impl"


    // $ANTLR start "rule__LTLAND__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5202:1: rule__LTLAND__Group__1 : rule__LTLAND__Group__1__Impl rule__LTLAND__Group__2 ;
    public final void rule__LTLAND__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5206:1: ( rule__LTLAND__Group__1__Impl rule__LTLAND__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5207:2: rule__LTLAND__Group__1__Impl rule__LTLAND__Group__2
            {
            pushFollow(FOLLOW_rule__LTLAND__Group__1__Impl_in_rule__LTLAND__Group__110445);
            rule__LTLAND__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLAND__Group__2_in_rule__LTLAND__Group__110448);
            rule__LTLAND__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLAND__Group__1"


    // $ANTLR start "rule__LTLAND__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5214:1: rule__LTLAND__Group__1__Impl : ( ( rule__LTLAND__LeftAssignment_1 ) ) ;
    public final void rule__LTLAND__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5218:1: ( ( ( rule__LTLAND__LeftAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5219:1: ( ( rule__LTLAND__LeftAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5219:1: ( ( rule__LTLAND__LeftAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5220:1: ( rule__LTLAND__LeftAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLANDAccess().getLeftAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5221:1: ( rule__LTLAND__LeftAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5221:2: rule__LTLAND__LeftAssignment_1
            {
            pushFollow(FOLLOW_rule__LTLAND__LeftAssignment_1_in_rule__LTLAND__Group__1__Impl10475);
            rule__LTLAND__LeftAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLANDAccess().getLeftAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLAND__Group__1__Impl"


    // $ANTLR start "rule__LTLAND__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5231:1: rule__LTLAND__Group__2 : rule__LTLAND__Group__2__Impl rule__LTLAND__Group__3 ;
    public final void rule__LTLAND__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5235:1: ( rule__LTLAND__Group__2__Impl rule__LTLAND__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5236:2: rule__LTLAND__Group__2__Impl rule__LTLAND__Group__3
            {
            pushFollow(FOLLOW_rule__LTLAND__Group__2__Impl_in_rule__LTLAND__Group__210505);
            rule__LTLAND__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLAND__Group__3_in_rule__LTLAND__Group__210508);
            rule__LTLAND__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLAND__Group__2"


    // $ANTLR start "rule__LTLAND__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5243:1: rule__LTLAND__Group__2__Impl : ( '\\u22C0' ) ;
    public final void rule__LTLAND__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5247:1: ( ( '\\u22C0' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5248:1: ( '\\u22C0' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5248:1: ( '\\u22C0' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5249:1: '\\u22C0'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLANDAccess().getNAryLogicalAndKeyword_2()); 
            }
            match(input,37,FOLLOW_37_in_rule__LTLAND__Group__2__Impl10536); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLANDAccess().getNAryLogicalAndKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLAND__Group__2__Impl"


    // $ANTLR start "rule__LTLAND__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5262:1: rule__LTLAND__Group__3 : rule__LTLAND__Group__3__Impl rule__LTLAND__Group__4 ;
    public final void rule__LTLAND__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5266:1: ( rule__LTLAND__Group__3__Impl rule__LTLAND__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5267:2: rule__LTLAND__Group__3__Impl rule__LTLAND__Group__4
            {
            pushFollow(FOLLOW_rule__LTLAND__Group__3__Impl_in_rule__LTLAND__Group__310567);
            rule__LTLAND__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLAND__Group__4_in_rule__LTLAND__Group__310570);
            rule__LTLAND__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLAND__Group__3"


    // $ANTLR start "rule__LTLAND__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5274:1: rule__LTLAND__Group__3__Impl : ( ( rule__LTLAND__RightAssignment_3 ) ) ;
    public final void rule__LTLAND__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5278:1: ( ( ( rule__LTLAND__RightAssignment_3 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5279:1: ( ( rule__LTLAND__RightAssignment_3 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5279:1: ( ( rule__LTLAND__RightAssignment_3 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5280:1: ( rule__LTLAND__RightAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLANDAccess().getRightAssignment_3()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5281:1: ( rule__LTLAND__RightAssignment_3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5281:2: rule__LTLAND__RightAssignment_3
            {
            pushFollow(FOLLOW_rule__LTLAND__RightAssignment_3_in_rule__LTLAND__Group__3__Impl10597);
            rule__LTLAND__RightAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLANDAccess().getRightAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLAND__Group__3__Impl"


    // $ANTLR start "rule__LTLAND__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5291:1: rule__LTLAND__Group__4 : rule__LTLAND__Group__4__Impl ;
    public final void rule__LTLAND__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5295:1: ( rule__LTLAND__Group__4__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5296:2: rule__LTLAND__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__LTLAND__Group__4__Impl_in_rule__LTLAND__Group__410627);
            rule__LTLAND__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLAND__Group__4"


    // $ANTLR start "rule__LTLAND__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5302:1: rule__LTLAND__Group__4__Impl : ( ')' ) ;
    public final void rule__LTLAND__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5306:1: ( ( ')' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5307:1: ( ')' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5307:1: ( ')' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5308:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLANDAccess().getRightParenthesisKeyword_4()); 
            }
            match(input,38,FOLLOW_38_in_rule__LTLAND__Group__4__Impl10655); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLANDAccess().getRightParenthesisKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLAND__Group__4__Impl"


    // $ANTLR start "rule__LTLOR__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5331:1: rule__LTLOR__Group__0 : rule__LTLOR__Group__0__Impl rule__LTLOR__Group__1 ;
    public final void rule__LTLOR__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5335:1: ( rule__LTLOR__Group__0__Impl rule__LTLOR__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5336:2: rule__LTLOR__Group__0__Impl rule__LTLOR__Group__1
            {
            pushFollow(FOLLOW_rule__LTLOR__Group__0__Impl_in_rule__LTLOR__Group__010696);
            rule__LTLOR__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLOR__Group__1_in_rule__LTLOR__Group__010699);
            rule__LTLOR__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLOR__Group__0"


    // $ANTLR start "rule__LTLOR__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5343:1: rule__LTLOR__Group__0__Impl : ( '(' ) ;
    public final void rule__LTLOR__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5347:1: ( ( '(' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5348:1: ( '(' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5348:1: ( '(' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5349:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLORAccess().getLeftParenthesisKeyword_0()); 
            }
            match(input,36,FOLLOW_36_in_rule__LTLOR__Group__0__Impl10727); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLORAccess().getLeftParenthesisKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLOR__Group__0__Impl"


    // $ANTLR start "rule__LTLOR__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5362:1: rule__LTLOR__Group__1 : rule__LTLOR__Group__1__Impl rule__LTLOR__Group__2 ;
    public final void rule__LTLOR__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5366:1: ( rule__LTLOR__Group__1__Impl rule__LTLOR__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5367:2: rule__LTLOR__Group__1__Impl rule__LTLOR__Group__2
            {
            pushFollow(FOLLOW_rule__LTLOR__Group__1__Impl_in_rule__LTLOR__Group__110758);
            rule__LTLOR__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLOR__Group__2_in_rule__LTLOR__Group__110761);
            rule__LTLOR__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLOR__Group__1"


    // $ANTLR start "rule__LTLOR__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5374:1: rule__LTLOR__Group__1__Impl : ( ( rule__LTLOR__LeftAssignment_1 ) ) ;
    public final void rule__LTLOR__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5378:1: ( ( ( rule__LTLOR__LeftAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5379:1: ( ( rule__LTLOR__LeftAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5379:1: ( ( rule__LTLOR__LeftAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5380:1: ( rule__LTLOR__LeftAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLORAccess().getLeftAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5381:1: ( rule__LTLOR__LeftAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5381:2: rule__LTLOR__LeftAssignment_1
            {
            pushFollow(FOLLOW_rule__LTLOR__LeftAssignment_1_in_rule__LTLOR__Group__1__Impl10788);
            rule__LTLOR__LeftAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLORAccess().getLeftAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLOR__Group__1__Impl"


    // $ANTLR start "rule__LTLOR__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5391:1: rule__LTLOR__Group__2 : rule__LTLOR__Group__2__Impl rule__LTLOR__Group__3 ;
    public final void rule__LTLOR__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5395:1: ( rule__LTLOR__Group__2__Impl rule__LTLOR__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5396:2: rule__LTLOR__Group__2__Impl rule__LTLOR__Group__3
            {
            pushFollow(FOLLOW_rule__LTLOR__Group__2__Impl_in_rule__LTLOR__Group__210818);
            rule__LTLOR__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLOR__Group__3_in_rule__LTLOR__Group__210821);
            rule__LTLOR__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLOR__Group__2"


    // $ANTLR start "rule__LTLOR__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5403:1: rule__LTLOR__Group__2__Impl : ( '\\u22C1' ) ;
    public final void rule__LTLOR__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5407:1: ( ( '\\u22C1' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5408:1: ( '\\u22C1' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5408:1: ( '\\u22C1' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5409:1: '\\u22C1'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLORAccess().getNAryLogicalOrKeyword_2()); 
            }
            match(input,39,FOLLOW_39_in_rule__LTLOR__Group__2__Impl10849); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLORAccess().getNAryLogicalOrKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLOR__Group__2__Impl"


    // $ANTLR start "rule__LTLOR__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5422:1: rule__LTLOR__Group__3 : rule__LTLOR__Group__3__Impl rule__LTLOR__Group__4 ;
    public final void rule__LTLOR__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5426:1: ( rule__LTLOR__Group__3__Impl rule__LTLOR__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5427:2: rule__LTLOR__Group__3__Impl rule__LTLOR__Group__4
            {
            pushFollow(FOLLOW_rule__LTLOR__Group__3__Impl_in_rule__LTLOR__Group__310880);
            rule__LTLOR__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLOR__Group__4_in_rule__LTLOR__Group__310883);
            rule__LTLOR__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLOR__Group__3"


    // $ANTLR start "rule__LTLOR__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5434:1: rule__LTLOR__Group__3__Impl : ( ( rule__LTLOR__RightAssignment_3 ) ) ;
    public final void rule__LTLOR__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5438:1: ( ( ( rule__LTLOR__RightAssignment_3 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5439:1: ( ( rule__LTLOR__RightAssignment_3 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5439:1: ( ( rule__LTLOR__RightAssignment_3 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5440:1: ( rule__LTLOR__RightAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLORAccess().getRightAssignment_3()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5441:1: ( rule__LTLOR__RightAssignment_3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5441:2: rule__LTLOR__RightAssignment_3
            {
            pushFollow(FOLLOW_rule__LTLOR__RightAssignment_3_in_rule__LTLOR__Group__3__Impl10910);
            rule__LTLOR__RightAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLORAccess().getRightAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLOR__Group__3__Impl"


    // $ANTLR start "rule__LTLOR__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5451:1: rule__LTLOR__Group__4 : rule__LTLOR__Group__4__Impl ;
    public final void rule__LTLOR__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5455:1: ( rule__LTLOR__Group__4__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5456:2: rule__LTLOR__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__LTLOR__Group__4__Impl_in_rule__LTLOR__Group__410940);
            rule__LTLOR__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLOR__Group__4"


    // $ANTLR start "rule__LTLOR__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5462:1: rule__LTLOR__Group__4__Impl : ( ')' ) ;
    public final void rule__LTLOR__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5466:1: ( ( ')' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5467:1: ( ')' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5467:1: ( ')' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5468:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLORAccess().getRightParenthesisKeyword_4()); 
            }
            match(input,38,FOLLOW_38_in_rule__LTLOR__Group__4__Impl10968); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLORAccess().getRightParenthesisKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLOR__Group__4__Impl"


    // $ANTLR start "rule__LTLConstant__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5491:1: rule__LTLConstant__Group__0 : rule__LTLConstant__Group__0__Impl rule__LTLConstant__Group__1 ;
    public final void rule__LTLConstant__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5495:1: ( rule__LTLConstant__Group__0__Impl rule__LTLConstant__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5496:2: rule__LTLConstant__Group__0__Impl rule__LTLConstant__Group__1
            {
            pushFollow(FOLLOW_rule__LTLConstant__Group__0__Impl_in_rule__LTLConstant__Group__011009);
            rule__LTLConstant__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLConstant__Group__1_in_rule__LTLConstant__Group__011012);
            rule__LTLConstant__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLConstant__Group__0"


    // $ANTLR start "rule__LTLConstant__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5503:1: rule__LTLConstant__Group__0__Impl : ( () ) ;
    public final void rule__LTLConstant__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5507:1: ( ( () ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5508:1: ( () )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5508:1: ( () )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5509:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLConstantAccess().getConstantAction_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5510:1: ()
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5512:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLConstantAccess().getConstantAction_0()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLConstant__Group__0__Impl"


    // $ANTLR start "rule__LTLConstant__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5522:1: rule__LTLConstant__Group__1 : rule__LTLConstant__Group__1__Impl ;
    public final void rule__LTLConstant__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5526:1: ( rule__LTLConstant__Group__1__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5527:2: rule__LTLConstant__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__LTLConstant__Group__1__Impl_in_rule__LTLConstant__Group__111070);
            rule__LTLConstant__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLConstant__Group__1"


    // $ANTLR start "rule__LTLConstant__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5533:1: rule__LTLConstant__Group__1__Impl : ( ( rule__LTLConstant__Alternatives_1 ) ) ;
    public final void rule__LTLConstant__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5537:1: ( ( ( rule__LTLConstant__Alternatives_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5538:1: ( ( rule__LTLConstant__Alternatives_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5538:1: ( ( rule__LTLConstant__Alternatives_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5539:1: ( rule__LTLConstant__Alternatives_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLConstantAccess().getAlternatives_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5540:1: ( rule__LTLConstant__Alternatives_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5540:2: rule__LTLConstant__Alternatives_1
            {
            pushFollow(FOLLOW_rule__LTLConstant__Alternatives_1_in_rule__LTLConstant__Group__1__Impl11097);
            rule__LTLConstant__Alternatives_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLConstantAccess().getAlternatives_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLConstant__Group__1__Impl"


    // $ANTLR start "rule__LTLCheckSet__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5554:1: rule__LTLCheckSet__Group__0 : rule__LTLCheckSet__Group__0__Impl rule__LTLCheckSet__Group__1 ;
    public final void rule__LTLCheckSet__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5558:1: ( rule__LTLCheckSet__Group__0__Impl rule__LTLCheckSet__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5559:2: rule__LTLCheckSet__Group__0__Impl rule__LTLCheckSet__Group__1
            {
            pushFollow(FOLLOW_rule__LTLCheckSet__Group__0__Impl_in_rule__LTLCheckSet__Group__011131);
            rule__LTLCheckSet__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLCheckSet__Group__1_in_rule__LTLCheckSet__Group__011134);
            rule__LTLCheckSet__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__Group__0"


    // $ANTLR start "rule__LTLCheckSet__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5566:1: rule__LTLCheckSet__Group__0__Impl : ( 'LTL' ) ;
    public final void rule__LTLCheckSet__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5570:1: ( ( 'LTL' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5571:1: ( 'LTL' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5571:1: ( 'LTL' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5572:1: 'LTL'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckSetAccess().getLTLKeyword_0()); 
            }
            match(input,44,FOLLOW_44_in_rule__LTLCheckSet__Group__0__Impl11162); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckSetAccess().getLTLKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__Group__0__Impl"


    // $ANTLR start "rule__LTLCheckSet__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5585:1: rule__LTLCheckSet__Group__1 : rule__LTLCheckSet__Group__1__Impl rule__LTLCheckSet__Group__2 ;
    public final void rule__LTLCheckSet__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5589:1: ( rule__LTLCheckSet__Group__1__Impl rule__LTLCheckSet__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5590:2: rule__LTLCheckSet__Group__1__Impl rule__LTLCheckSet__Group__2
            {
            pushFollow(FOLLOW_rule__LTLCheckSet__Group__1__Impl_in_rule__LTLCheckSet__Group__111193);
            rule__LTLCheckSet__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLCheckSet__Group__2_in_rule__LTLCheckSet__Group__111196);
            rule__LTLCheckSet__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__Group__1"


    // $ANTLR start "rule__LTLCheckSet__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5597:1: rule__LTLCheckSet__Group__1__Impl : ( 'CheckSet' ) ;
    public final void rule__LTLCheckSet__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5601:1: ( ( 'CheckSet' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5602:1: ( 'CheckSet' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5602:1: ( 'CheckSet' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5603:1: 'CheckSet'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckSetAccess().getCheckSetKeyword_1()); 
            }
            match(input,41,FOLLOW_41_in_rule__LTLCheckSet__Group__1__Impl11224); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckSetAccess().getCheckSetKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__Group__1__Impl"


    // $ANTLR start "rule__LTLCheckSet__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5616:1: rule__LTLCheckSet__Group__2 : rule__LTLCheckSet__Group__2__Impl rule__LTLCheckSet__Group__3 ;
    public final void rule__LTLCheckSet__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5620:1: ( rule__LTLCheckSet__Group__2__Impl rule__LTLCheckSet__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5621:2: rule__LTLCheckSet__Group__2__Impl rule__LTLCheckSet__Group__3
            {
            pushFollow(FOLLOW_rule__LTLCheckSet__Group__2__Impl_in_rule__LTLCheckSet__Group__211255);
            rule__LTLCheckSet__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLCheckSet__Group__3_in_rule__LTLCheckSet__Group__211258);
            rule__LTLCheckSet__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__Group__2"


    // $ANTLR start "rule__LTLCheckSet__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5628:1: rule__LTLCheckSet__Group__2__Impl : ( ( rule__LTLCheckSet__NameAssignment_2 ) ) ;
    public final void rule__LTLCheckSet__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5632:1: ( ( ( rule__LTLCheckSet__NameAssignment_2 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5633:1: ( ( rule__LTLCheckSet__NameAssignment_2 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5633:1: ( ( rule__LTLCheckSet__NameAssignment_2 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5634:1: ( rule__LTLCheckSet__NameAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckSetAccess().getNameAssignment_2()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5635:1: ( rule__LTLCheckSet__NameAssignment_2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5635:2: rule__LTLCheckSet__NameAssignment_2
            {
            pushFollow(FOLLOW_rule__LTLCheckSet__NameAssignment_2_in_rule__LTLCheckSet__Group__2__Impl11285);
            rule__LTLCheckSet__NameAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckSetAccess().getNameAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__Group__2__Impl"


    // $ANTLR start "rule__LTLCheckSet__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5645:1: rule__LTLCheckSet__Group__3 : rule__LTLCheckSet__Group__3__Impl rule__LTLCheckSet__Group__4 ;
    public final void rule__LTLCheckSet__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5649:1: ( rule__LTLCheckSet__Group__3__Impl rule__LTLCheckSet__Group__4 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5650:2: rule__LTLCheckSet__Group__3__Impl rule__LTLCheckSet__Group__4
            {
            pushFollow(FOLLOW_rule__LTLCheckSet__Group__3__Impl_in_rule__LTLCheckSet__Group__311315);
            rule__LTLCheckSet__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLCheckSet__Group__4_in_rule__LTLCheckSet__Group__311318);
            rule__LTLCheckSet__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__Group__3"


    // $ANTLR start "rule__LTLCheckSet__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5657:1: rule__LTLCheckSet__Group__3__Impl : ( '{' ) ;
    public final void rule__LTLCheckSet__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5661:1: ( ( '{' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5662:1: ( '{' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5662:1: ( '{' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5663:1: '{'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckSetAccess().getLeftCurlyBracketKeyword_3()); 
            }
            match(input,13,FOLLOW_13_in_rule__LTLCheckSet__Group__3__Impl11346); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckSetAccess().getLeftCurlyBracketKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__Group__3__Impl"


    // $ANTLR start "rule__LTLCheckSet__Group__4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5676:1: rule__LTLCheckSet__Group__4 : rule__LTLCheckSet__Group__4__Impl rule__LTLCheckSet__Group__5 ;
    public final void rule__LTLCheckSet__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5680:1: ( rule__LTLCheckSet__Group__4__Impl rule__LTLCheckSet__Group__5 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5681:2: rule__LTLCheckSet__Group__4__Impl rule__LTLCheckSet__Group__5
            {
            pushFollow(FOLLOW_rule__LTLCheckSet__Group__4__Impl_in_rule__LTLCheckSet__Group__411377);
            rule__LTLCheckSet__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLCheckSet__Group__5_in_rule__LTLCheckSet__Group__411380);
            rule__LTLCheckSet__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__Group__4"


    // $ANTLR start "rule__LTLCheckSet__Group__4__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5688:1: rule__LTLCheckSet__Group__4__Impl : ( ( rule__LTLCheckSet__ChecksAssignment_4 )* ) ;
    public final void rule__LTLCheckSet__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5692:1: ( ( ( rule__LTLCheckSet__ChecksAssignment_4 )* ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5693:1: ( ( rule__LTLCheckSet__ChecksAssignment_4 )* )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5693:1: ( ( rule__LTLCheckSet__ChecksAssignment_4 )* )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5694:1: ( rule__LTLCheckSet__ChecksAssignment_4 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckSetAccess().getChecksAssignment_4()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5695:1: ( rule__LTLCheckSet__ChecksAssignment_4 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==42) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5695:2: rule__LTLCheckSet__ChecksAssignment_4
            	    {
            	    pushFollow(FOLLOW_rule__LTLCheckSet__ChecksAssignment_4_in_rule__LTLCheckSet__Group__4__Impl11407);
            	    rule__LTLCheckSet__ChecksAssignment_4();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckSetAccess().getChecksAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__Group__4__Impl"


    // $ANTLR start "rule__LTLCheckSet__Group__5"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5705:1: rule__LTLCheckSet__Group__5 : rule__LTLCheckSet__Group__5__Impl ;
    public final void rule__LTLCheckSet__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5709:1: ( rule__LTLCheckSet__Group__5__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5710:2: rule__LTLCheckSet__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__LTLCheckSet__Group__5__Impl_in_rule__LTLCheckSet__Group__511438);
            rule__LTLCheckSet__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__Group__5"


    // $ANTLR start "rule__LTLCheckSet__Group__5__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5716:1: rule__LTLCheckSet__Group__5__Impl : ( '}' ) ;
    public final void rule__LTLCheckSet__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5720:1: ( ( '}' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5721:1: ( '}' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5721:1: ( '}' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5722:1: '}'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckSetAccess().getRightCurlyBracketKeyword_5()); 
            }
            match(input,16,FOLLOW_16_in_rule__LTLCheckSet__Group__5__Impl11466); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckSetAccess().getRightCurlyBracketKeyword_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__Group__5__Impl"


    // $ANTLR start "rule__LTLCheck__Group__0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5747:1: rule__LTLCheck__Group__0 : rule__LTLCheck__Group__0__Impl rule__LTLCheck__Group__1 ;
    public final void rule__LTLCheck__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5751:1: ( rule__LTLCheck__Group__0__Impl rule__LTLCheck__Group__1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5752:2: rule__LTLCheck__Group__0__Impl rule__LTLCheck__Group__1
            {
            pushFollow(FOLLOW_rule__LTLCheck__Group__0__Impl_in_rule__LTLCheck__Group__011509);
            rule__LTLCheck__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLCheck__Group__1_in_rule__LTLCheck__Group__011512);
            rule__LTLCheck__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheck__Group__0"


    // $ANTLR start "rule__LTLCheck__Group__0__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5759:1: rule__LTLCheck__Group__0__Impl : ( 'check' ) ;
    public final void rule__LTLCheck__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5763:1: ( ( 'check' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5764:1: ( 'check' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5764:1: ( 'check' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5765:1: 'check'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckAccess().getCheckKeyword_0()); 
            }
            match(input,42,FOLLOW_42_in_rule__LTLCheck__Group__0__Impl11540); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckAccess().getCheckKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheck__Group__0__Impl"


    // $ANTLR start "rule__LTLCheck__Group__1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5778:1: rule__LTLCheck__Group__1 : rule__LTLCheck__Group__1__Impl rule__LTLCheck__Group__2 ;
    public final void rule__LTLCheck__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5782:1: ( rule__LTLCheck__Group__1__Impl rule__LTLCheck__Group__2 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5783:2: rule__LTLCheck__Group__1__Impl rule__LTLCheck__Group__2
            {
            pushFollow(FOLLOW_rule__LTLCheck__Group__1__Impl_in_rule__LTLCheck__Group__111571);
            rule__LTLCheck__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLCheck__Group__2_in_rule__LTLCheck__Group__111574);
            rule__LTLCheck__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheck__Group__1"


    // $ANTLR start "rule__LTLCheck__Group__1__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5790:1: rule__LTLCheck__Group__1__Impl : ( ( rule__LTLCheck__LtsAssignment_1 ) ) ;
    public final void rule__LTLCheck__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5794:1: ( ( ( rule__LTLCheck__LtsAssignment_1 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5795:1: ( ( rule__LTLCheck__LtsAssignment_1 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5795:1: ( ( rule__LTLCheck__LtsAssignment_1 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5796:1: ( rule__LTLCheck__LtsAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckAccess().getLtsAssignment_1()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5797:1: ( rule__LTLCheck__LtsAssignment_1 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5797:2: rule__LTLCheck__LtsAssignment_1
            {
            pushFollow(FOLLOW_rule__LTLCheck__LtsAssignment_1_in_rule__LTLCheck__Group__1__Impl11601);
            rule__LTLCheck__LtsAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckAccess().getLtsAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheck__Group__1__Impl"


    // $ANTLR start "rule__LTLCheck__Group__2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5807:1: rule__LTLCheck__Group__2 : rule__LTLCheck__Group__2__Impl rule__LTLCheck__Group__3 ;
    public final void rule__LTLCheck__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5811:1: ( rule__LTLCheck__Group__2__Impl rule__LTLCheck__Group__3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5812:2: rule__LTLCheck__Group__2__Impl rule__LTLCheck__Group__3
            {
            pushFollow(FOLLOW_rule__LTLCheck__Group__2__Impl_in_rule__LTLCheck__Group__211631);
            rule__LTLCheck__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__LTLCheck__Group__3_in_rule__LTLCheck__Group__211634);
            rule__LTLCheck__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheck__Group__2"


    // $ANTLR start "rule__LTLCheck__Group__2__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5819:1: rule__LTLCheck__Group__2__Impl : ( '\\u22A8' ) ;
    public final void rule__LTLCheck__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5823:1: ( ( '\\u22A8' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5824:1: ( '\\u22A8' )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5824:1: ( '\\u22A8' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5825:1: '\\u22A8'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckAccess().getTrueKeyword_2()); 
            }
            match(input,43,FOLLOW_43_in_rule__LTLCheck__Group__2__Impl11662); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckAccess().getTrueKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheck__Group__2__Impl"


    // $ANTLR start "rule__LTLCheck__Group__3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5838:1: rule__LTLCheck__Group__3 : rule__LTLCheck__Group__3__Impl ;
    public final void rule__LTLCheck__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5842:1: ( rule__LTLCheck__Group__3__Impl )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5843:2: rule__LTLCheck__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__LTLCheck__Group__3__Impl_in_rule__LTLCheck__Group__311693);
            rule__LTLCheck__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheck__Group__3"


    // $ANTLR start "rule__LTLCheck__Group__3__Impl"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5849:1: rule__LTLCheck__Group__3__Impl : ( ( rule__LTLCheck__FormulaDefinitionAssignment_3 ) ) ;
    public final void rule__LTLCheck__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5853:1: ( ( ( rule__LTLCheck__FormulaDefinitionAssignment_3 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5854:1: ( ( rule__LTLCheck__FormulaDefinitionAssignment_3 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5854:1: ( ( rule__LTLCheck__FormulaDefinitionAssignment_3 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5855:1: ( rule__LTLCheck__FormulaDefinitionAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckAccess().getFormulaDefinitionAssignment_3()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5856:1: ( rule__LTLCheck__FormulaDefinitionAssignment_3 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5856:2: rule__LTLCheck__FormulaDefinitionAssignment_3
            {
            pushFollow(FOLLOW_rule__LTLCheck__FormulaDefinitionAssignment_3_in_rule__LTLCheck__Group__3__Impl11720);
            rule__LTLCheck__FormulaDefinitionAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckAccess().getFormulaDefinitionAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheck__Group__3__Impl"


    // $ANTLR start "rule__Document__LtssAssignment_0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5875:1: rule__Document__LtssAssignment_0 : ( ( rule__Document__LtssAlternatives_0_0 ) ) ;
    public final void rule__Document__LtssAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5879:1: ( ( ( rule__Document__LtssAlternatives_0_0 ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5880:1: ( ( rule__Document__LtssAlternatives_0_0 ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5880:1: ( ( rule__Document__LtssAlternatives_0_0 ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5881:1: ( rule__Document__LtssAlternatives_0_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getLtssAlternatives_0_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5882:1: ( rule__Document__LtssAlternatives_0_0 )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5882:2: rule__Document__LtssAlternatives_0_0
            {
            pushFollow(FOLLOW_rule__Document__LtssAlternatives_0_0_in_rule__Document__LtssAssignment_011763);
            rule__Document__LtssAlternatives_0_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getLtssAlternatives_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__LtssAssignment_0"


    // $ANTLR start "rule__Document__FormulaSetAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5891:1: rule__Document__FormulaSetAssignment_1 : ( ruleFormulaSet ) ;
    public final void rule__Document__FormulaSetAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5895:1: ( ( ruleFormulaSet ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5896:1: ( ruleFormulaSet )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5896:1: ( ruleFormulaSet )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5897:1: ruleFormulaSet
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getFormulaSetFormulaSetParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleFormulaSet_in_rule__Document__FormulaSetAssignment_111796);
            ruleFormulaSet();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getFormulaSetFormulaSetParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__FormulaSetAssignment_1"


    // $ANTLR start "rule__Document__CheckSetAssignment_2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5906:1: rule__Document__CheckSetAssignment_2 : ( ruleCheckSet ) ;
    public final void rule__Document__CheckSetAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5910:1: ( ( ruleCheckSet ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5911:1: ( ruleCheckSet )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5911:1: ( ruleCheckSet )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5912:1: ruleCheckSet
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getCheckSetCheckSetParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleCheckSet_in_rule__Document__CheckSetAssignment_211827);
            ruleCheckSet();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getCheckSetCheckSetParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__CheckSetAssignment_2"


    // $ANTLR start "rule__Document__LTLformulaSetAssignment_3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5921:1: rule__Document__LTLformulaSetAssignment_3 : ( ruleLTLFormulaSet ) ;
    public final void rule__Document__LTLformulaSetAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5925:1: ( ( ruleLTLFormulaSet ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5926:1: ( ruleLTLFormulaSet )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5926:1: ( ruleLTLFormulaSet )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5927:1: ruleLTLFormulaSet
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getLTLformulaSetLTLFormulaSetParserRuleCall_3_0()); 
            }
            pushFollow(FOLLOW_ruleLTLFormulaSet_in_rule__Document__LTLformulaSetAssignment_311858);
            ruleLTLFormulaSet();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getLTLformulaSetLTLFormulaSetParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__LTLformulaSetAssignment_3"


    // $ANTLR start "rule__Document__LTLcheckSetAssignment_4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5936:1: rule__Document__LTLcheckSetAssignment_4 : ( ruleLTLCheckSet ) ;
    public final void rule__Document__LTLcheckSetAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5940:1: ( ( ruleLTLCheckSet ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5941:1: ( ruleLTLCheckSet )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5941:1: ( ruleLTLCheckSet )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5942:1: ruleLTLCheckSet
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getDocumentAccess().getLTLcheckSetLTLCheckSetParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_ruleLTLCheckSet_in_rule__Document__LTLcheckSetAssignment_411889);
            ruleLTLCheckSet();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getDocumentAccess().getLTLcheckSetLTLCheckSetParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Document__LTLcheckSetAssignment_4"


    // $ANTLR start "rule__LTS__NameAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5951:1: rule__LTS__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__LTS__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5955:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5956:1: ( RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5956:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5957:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__LTS__NameAssignment_111920); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__NameAssignment_1"


    // $ANTLR start "rule__LTS__AtomicPropositionsAssignment_3_2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5966:1: rule__LTS__AtomicPropositionsAssignment_3_2 : ( ruleAtomicProposition ) ;
    public final void rule__LTS__AtomicPropositionsAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5970:1: ( ( ruleAtomicProposition ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5971:1: ( ruleAtomicProposition )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5971:1: ( ruleAtomicProposition )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5972:1: ruleAtomicProposition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getAtomicPropositionsAtomicPropositionParserRuleCall_3_2_0()); 
            }
            pushFollow(FOLLOW_ruleAtomicProposition_in_rule__LTS__AtomicPropositionsAssignment_3_211951);
            ruleAtomicProposition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getAtomicPropositionsAtomicPropositionParserRuleCall_3_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__AtomicPropositionsAssignment_3_2"


    // $ANTLR start "rule__LTS__AtomicPropositionsAssignment_3_3_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5981:1: rule__LTS__AtomicPropositionsAssignment_3_3_1 : ( ruleAtomicProposition ) ;
    public final void rule__LTS__AtomicPropositionsAssignment_3_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5985:1: ( ( ruleAtomicProposition ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5986:1: ( ruleAtomicProposition )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5986:1: ( ruleAtomicProposition )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5987:1: ruleAtomicProposition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getAtomicPropositionsAtomicPropositionParserRuleCall_3_3_1_0()); 
            }
            pushFollow(FOLLOW_ruleAtomicProposition_in_rule__LTS__AtomicPropositionsAssignment_3_3_111982);
            ruleAtomicProposition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getAtomicPropositionsAtomicPropositionParserRuleCall_3_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__AtomicPropositionsAssignment_3_3_1"


    // $ANTLR start "rule__LTS__StatesAssignment_5"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:5996:1: rule__LTS__StatesAssignment_5 : ( ruleBasicState ) ;
    public final void rule__LTS__StatesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6000:1: ( ( ruleBasicState ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6001:1: ( ruleBasicState )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6001:1: ( ruleBasicState )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6002:1: ruleBasicState
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getStatesBasicStateParserRuleCall_5_0()); 
            }
            pushFollow(FOLLOW_ruleBasicState_in_rule__LTS__StatesAssignment_512013);
            ruleBasicState();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getStatesBasicStateParserRuleCall_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__StatesAssignment_5"


    // $ANTLR start "rule__LTS__StatesAssignment_6_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6011:1: rule__LTS__StatesAssignment_6_1 : ( ruleBasicState ) ;
    public final void rule__LTS__StatesAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6015:1: ( ( ruleBasicState ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6016:1: ( ruleBasicState )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6016:1: ( ruleBasicState )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6017:1: ruleBasicState
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getStatesBasicStateParserRuleCall_6_1_0()); 
            }
            pushFollow(FOLLOW_ruleBasicState_in_rule__LTS__StatesAssignment_6_112044);
            ruleBasicState();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getStatesBasicStateParserRuleCall_6_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__StatesAssignment_6_1"


    // $ANTLR start "rule__LTS__InitStatesAssignment_8"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6026:1: rule__LTS__InitStatesAssignment_8 : ( ( RULE_ID ) ) ;
    public final void rule__LTS__InitStatesAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6030:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6031:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6031:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6032:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getInitStatesStateCrossReference_8_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6033:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6034:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getInitStatesStateIDTerminalRuleCall_8_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__LTS__InitStatesAssignment_812079); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getInitStatesStateIDTerminalRuleCall_8_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getInitStatesStateCrossReference_8_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__InitStatesAssignment_8"


    // $ANTLR start "rule__LTS__InitStatesAssignment_9_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6045:1: rule__LTS__InitStatesAssignment_9_1 : ( ( RULE_ID ) ) ;
    public final void rule__LTS__InitStatesAssignment_9_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6049:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6050:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6050:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6051:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getInitStatesStateCrossReference_9_1_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6052:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6053:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getInitStatesStateIDTerminalRuleCall_9_1_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__LTS__InitStatesAssignment_9_112118); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getInitStatesStateIDTerminalRuleCall_9_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getInitStatesStateCrossReference_9_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__InitStatesAssignment_9_1"


    // $ANTLR start "rule__LTS__AlphabetAssignment_10_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6064:1: rule__LTS__AlphabetAssignment_10_1 : ( ruleEvent ) ;
    public final void rule__LTS__AlphabetAssignment_10_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6068:1: ( ( ruleEvent ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6069:1: ( ruleEvent )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6069:1: ( ruleEvent )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6070:1: ruleEvent
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getAlphabetEventParserRuleCall_10_1_0()); 
            }
            pushFollow(FOLLOW_ruleEvent_in_rule__LTS__AlphabetAssignment_10_112153);
            ruleEvent();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getAlphabetEventParserRuleCall_10_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__AlphabetAssignment_10_1"


    // $ANTLR start "rule__LTS__AlphabetAssignment_10_2_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6079:1: rule__LTS__AlphabetAssignment_10_2_1 : ( ruleEvent ) ;
    public final void rule__LTS__AlphabetAssignment_10_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6083:1: ( ( ruleEvent ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6084:1: ( ruleEvent )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6084:1: ( ruleEvent )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6085:1: ruleEvent
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getAlphabetEventParserRuleCall_10_2_1_0()); 
            }
            pushFollow(FOLLOW_ruleEvent_in_rule__LTS__AlphabetAssignment_10_2_112184);
            ruleEvent();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getAlphabetEventParserRuleCall_10_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__AlphabetAssignment_10_2_1"


    // $ANTLR start "rule__LTS__TransitionsAssignment_11"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6094:1: rule__LTS__TransitionsAssignment_11 : ( ruleTransition ) ;
    public final void rule__LTS__TransitionsAssignment_11() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6098:1: ( ( ruleTransition ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6099:1: ( ruleTransition )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6099:1: ( ruleTransition )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6100:1: ruleTransition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTSAccess().getTransitionsTransitionParserRuleCall_11_0()); 
            }
            pushFollow(FOLLOW_ruleTransition_in_rule__LTS__TransitionsAssignment_1112215);
            ruleTransition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTSAccess().getTransitionsTransitionParserRuleCall_11_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTS__TransitionsAssignment_11"


    // $ANTLR start "rule__AtomicProposition__NameAssignment"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6109:1: rule__AtomicProposition__NameAssignment : ( RULE_ID ) ;
    public final void rule__AtomicProposition__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6113:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6114:1: ( RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6114:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6115:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAtomicPropositionAccess().getNameIDTerminalRuleCall_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__AtomicProposition__NameAssignment12246); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAtomicPropositionAccess().getNameIDTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AtomicProposition__NameAssignment"


    // $ANTLR start "rule__BasicState__NameAssignment_0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6124:1: rule__BasicState__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__BasicState__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6128:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6129:1: ( RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6129:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6130:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getNameIDTerminalRuleCall_0_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__BasicState__NameAssignment_012277); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getNameIDTerminalRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__NameAssignment_0"


    // $ANTLR start "rule__BasicState__AtomicPropositionsAssignment_1_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6139:1: rule__BasicState__AtomicPropositionsAssignment_1_1 : ( ( RULE_ID ) ) ;
    public final void rule__BasicState__AtomicPropositionsAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6143:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6144:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6144:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6145:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getAtomicPropositionsAtomicPropositionCrossReference_1_1_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6146:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6147:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getAtomicPropositionsAtomicPropositionIDTerminalRuleCall_1_1_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__BasicState__AtomicPropositionsAssignment_1_112312); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getAtomicPropositionsAtomicPropositionIDTerminalRuleCall_1_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getAtomicPropositionsAtomicPropositionCrossReference_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__AtomicPropositionsAssignment_1_1"


    // $ANTLR start "rule__BasicState__AtomicPropositionsAssignment_1_2_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6158:1: rule__BasicState__AtomicPropositionsAssignment_1_2_1 : ( ( RULE_ID ) ) ;
    public final void rule__BasicState__AtomicPropositionsAssignment_1_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6162:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6163:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6163:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6164:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getAtomicPropositionsAtomicPropositionCrossReference_1_2_1_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6165:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6166:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBasicStateAccess().getAtomicPropositionsAtomicPropositionIDTerminalRuleCall_1_2_1_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__BasicState__AtomicPropositionsAssignment_1_2_112351); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getAtomicPropositionsAtomicPropositionIDTerminalRuleCall_1_2_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBasicStateAccess().getAtomicPropositionsAtomicPropositionCrossReference_1_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BasicState__AtomicPropositionsAssignment_1_2_1"


    // $ANTLR start "rule__Event__NameAssignment"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6177:1: rule__Event__NameAssignment : ( RULE_ID ) ;
    public final void rule__Event__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6181:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6182:1: ( RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6182:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6183:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEventAccess().getNameIDTerminalRuleCall_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Event__NameAssignment12386); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEventAccess().getNameIDTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Event__NameAssignment"


    // $ANTLR start "rule__Transition__SourceAssignment_0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6192:1: rule__Transition__SourceAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__Transition__SourceAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6196:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6197:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6197:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6198:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTransitionAccess().getSourceStateCrossReference_0_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6199:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6200:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTransitionAccess().getSourceStateIDTerminalRuleCall_0_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Transition__SourceAssignment_012421); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTransitionAccess().getSourceStateIDTerminalRuleCall_0_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTransitionAccess().getSourceStateCrossReference_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__SourceAssignment_0"


    // $ANTLR start "rule__Transition__DestinationAssignment_2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6211:1: rule__Transition__DestinationAssignment_2 : ( ( RULE_ID ) ) ;
    public final void rule__Transition__DestinationAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6215:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6216:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6216:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6217:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTransitionAccess().getDestinationStateCrossReference_2_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6218:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6219:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTransitionAccess().getDestinationStateIDTerminalRuleCall_2_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Transition__DestinationAssignment_212460); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTransitionAccess().getDestinationStateIDTerminalRuleCall_2_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTransitionAccess().getDestinationStateCrossReference_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__DestinationAssignment_2"


    // $ANTLR start "rule__Transition__EventAssignment_4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6230:1: rule__Transition__EventAssignment_4 : ( ( RULE_ID ) ) ;
    public final void rule__Transition__EventAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6234:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6235:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6235:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6236:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTransitionAccess().getEventEventCrossReference_4_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6237:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6238:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTransitionAccess().getEventEventIDTerminalRuleCall_4_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Transition__EventAssignment_412499); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTransitionAccess().getEventEventIDTerminalRuleCall_4_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTransitionAccess().getEventEventCrossReference_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Transition__EventAssignment_4"


    // $ANTLR start "rule__ComposedLTS__LtsLeftAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6249:1: rule__ComposedLTS__LtsLeftAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__ComposedLTS__LtsLeftAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6253:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6254:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6254:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6255:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getLtsLeftLTSCrossReference_1_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6256:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6257:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getLtsLeftLTSIDTerminalRuleCall_1_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__ComposedLTS__LtsLeftAssignment_112538); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getLtsLeftLTSIDTerminalRuleCall_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getLtsLeftLTSCrossReference_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__LtsLeftAssignment_1"


    // $ANTLR start "rule__ComposedLTS__LtsRightAssignment_3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6268:1: rule__ComposedLTS__LtsRightAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__ComposedLTS__LtsRightAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6272:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6273:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6273:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6274:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getLtsRightLTSCrossReference_3_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6275:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6276:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getLtsRightLTSIDTerminalRuleCall_3_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__ComposedLTS__LtsRightAssignment_312577); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getLtsRightLTSIDTerminalRuleCall_3_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getLtsRightLTSCrossReference_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__LtsRightAssignment_3"


    // $ANTLR start "rule__ComposedLTS__NameAssignment_5"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6287:1: rule__ComposedLTS__NameAssignment_5 : ( RULE_ID ) ;
    public final void rule__ComposedLTS__NameAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6291:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6292:1: ( RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6292:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6293:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getNameIDTerminalRuleCall_5_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__ComposedLTS__NameAssignment_512612); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getNameIDTerminalRuleCall_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__NameAssignment_5"


    // $ANTLR start "rule__ComposedLTS__EventsAssignment_6_2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6302:1: rule__ComposedLTS__EventsAssignment_6_2 : ( ( RULE_ID ) ) ;
    public final void rule__ComposedLTS__EventsAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6306:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6307:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6307:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6308:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getEventsEventCrossReference_6_2_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6309:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6310:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getEventsEventIDTerminalRuleCall_6_2_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__ComposedLTS__EventsAssignment_6_212647); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getEventsEventIDTerminalRuleCall_6_2_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getEventsEventCrossReference_6_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__EventsAssignment_6_2"


    // $ANTLR start "rule__ComposedLTS__EventsAssignment_6_3_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6321:1: rule__ComposedLTS__EventsAssignment_6_3_1 : ( ( RULE_ID ) ) ;
    public final void rule__ComposedLTS__EventsAssignment_6_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6325:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6326:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6326:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6327:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getEventsEventCrossReference_6_3_1_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6328:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6329:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getComposedLTSAccess().getEventsEventIDTerminalRuleCall_6_3_1_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__ComposedLTS__EventsAssignment_6_3_112686); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getEventsEventIDTerminalRuleCall_6_3_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getComposedLTSAccess().getEventsEventCrossReference_6_3_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComposedLTS__EventsAssignment_6_3_1"


    // $ANTLR start "rule__FormulaSet__NameAssignment_2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6340:1: rule__FormulaSet__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__FormulaSet__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6344:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6345:1: ( RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6345:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6346:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaSetAccess().getNameIDTerminalRuleCall_2_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__FormulaSet__NameAssignment_212721); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaSetAccess().getNameIDTerminalRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__NameAssignment_2"


    // $ANTLR start "rule__FormulaSet__FormulaeAssignment_4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6355:1: rule__FormulaSet__FormulaeAssignment_4 : ( ruleFormulaDefinition ) ;
    public final void rule__FormulaSet__FormulaeAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6359:1: ( ( ruleFormulaDefinition ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6360:1: ( ruleFormulaDefinition )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6360:1: ( ruleFormulaDefinition )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6361:1: ruleFormulaDefinition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaSetAccess().getFormulaeFormulaDefinitionParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_ruleFormulaDefinition_in_rule__FormulaSet__FormulaeAssignment_412752);
            ruleFormulaDefinition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaSetAccess().getFormulaeFormulaDefinitionParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaSet__FormulaeAssignment_4"


    // $ANTLR start "rule__FormulaDefinition__NameAssignment_0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6370:1: rule__FormulaDefinition__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__FormulaDefinition__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6374:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6375:1: ( RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6375:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6376:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaDefinitionAccess().getNameIDTerminalRuleCall_0_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__FormulaDefinition__NameAssignment_012783); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaDefinitionAccess().getNameIDTerminalRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaDefinition__NameAssignment_0"


    // $ANTLR start "rule__FormulaDefinition__FormulaAssignment_2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6385:1: rule__FormulaDefinition__FormulaAssignment_2 : ( ruleFormula ) ;
    public final void rule__FormulaDefinition__FormulaAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6389:1: ( ( ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6390:1: ( ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6390:1: ( ruleFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6391:1: ruleFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getFormulaDefinitionAccess().getFormulaFormulaParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleFormula_in_rule__FormulaDefinition__FormulaAssignment_212814);
            ruleFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getFormulaDefinitionAccess().getFormulaFormulaParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FormulaDefinition__FormulaAssignment_2"


    // $ANTLR start "rule__EX__NestedAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6400:1: rule__EX__NestedAssignment_1 : ( ruleFormula ) ;
    public final void rule__EX__NestedAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6404:1: ( ( ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6405:1: ( ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6405:1: ( ruleFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6406:1: ruleFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEXAccess().getNestedFormulaParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleFormula_in_rule__EX__NestedAssignment_112845);
            ruleFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEXAccess().getNestedFormulaParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EX__NestedAssignment_1"


    // $ANTLR start "rule__EG__NestedAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6415:1: rule__EG__NestedAssignment_1 : ( ruleFormula ) ;
    public final void rule__EG__NestedAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6419:1: ( ( ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6420:1: ( ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6420:1: ( ruleFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6421:1: ruleFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEGAccess().getNestedFormulaParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleFormula_in_rule__EG__NestedAssignment_112876);
            ruleFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEGAccess().getNestedFormulaParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EG__NestedAssignment_1"


    // $ANTLR start "rule__EU__LeftAssignment_2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6430:1: rule__EU__LeftAssignment_2 : ( ruleFormula ) ;
    public final void rule__EU__LeftAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6434:1: ( ( ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6435:1: ( ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6435:1: ( ruleFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6436:1: ruleFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEUAccess().getLeftFormulaParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleFormula_in_rule__EU__LeftAssignment_212907);
            ruleFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEUAccess().getLeftFormulaParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__LeftAssignment_2"


    // $ANTLR start "rule__EU__RightAssignment_4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6445:1: rule__EU__RightAssignment_4 : ( ruleFormula ) ;
    public final void rule__EU__RightAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6449:1: ( ( ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6450:1: ( ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6450:1: ( ruleFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6451:1: ruleFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEUAccess().getRightFormulaParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_ruleFormula_in_rule__EU__RightAssignment_412938);
            ruleFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEUAccess().getRightFormulaParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EU__RightAssignment_4"


    // $ANTLR start "rule__AND__LeftAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6460:1: rule__AND__LeftAssignment_1 : ( ruleFormula ) ;
    public final void rule__AND__LeftAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6464:1: ( ( ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6465:1: ( ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6465:1: ( ruleFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6466:1: ruleFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getANDAccess().getLeftFormulaParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleFormula_in_rule__AND__LeftAssignment_112969);
            ruleFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getANDAccess().getLeftFormulaParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AND__LeftAssignment_1"


    // $ANTLR start "rule__AND__RightAssignment_3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6475:1: rule__AND__RightAssignment_3 : ( ruleFormula ) ;
    public final void rule__AND__RightAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6479:1: ( ( ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6480:1: ( ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6480:1: ( ruleFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6481:1: ruleFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getANDAccess().getRightFormulaParserRuleCall_3_0()); 
            }
            pushFollow(FOLLOW_ruleFormula_in_rule__AND__RightAssignment_313000);
            ruleFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getANDAccess().getRightFormulaParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AND__RightAssignment_3"


    // $ANTLR start "rule__OR__LeftAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6490:1: rule__OR__LeftAssignment_1 : ( ruleFormula ) ;
    public final void rule__OR__LeftAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6494:1: ( ( ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6495:1: ( ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6495:1: ( ruleFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6496:1: ruleFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getORAccess().getLeftFormulaParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleFormula_in_rule__OR__LeftAssignment_113031);
            ruleFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getORAccess().getLeftFormulaParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OR__LeftAssignment_1"


    // $ANTLR start "rule__OR__RightAssignment_3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6505:1: rule__OR__RightAssignment_3 : ( ruleFormula ) ;
    public final void rule__OR__RightAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6509:1: ( ( ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6510:1: ( ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6510:1: ( ruleFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6511:1: ruleFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getORAccess().getRightFormulaParserRuleCall_3_0()); 
            }
            pushFollow(FOLLOW_ruleFormula_in_rule__OR__RightAssignment_313062);
            ruleFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getORAccess().getRightFormulaParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OR__RightAssignment_3"


    // $ANTLR start "rule__NOT__NestedAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6520:1: rule__NOT__NestedAssignment_1 : ( ruleFormula ) ;
    public final void rule__NOT__NestedAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6524:1: ( ( ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6525:1: ( ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6525:1: ( ruleFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6526:1: ruleFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNOTAccess().getNestedFormulaParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleFormula_in_rule__NOT__NestedAssignment_113093);
            ruleFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNOTAccess().getNestedFormulaParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NOT__NestedAssignment_1"


    // $ANTLR start "rule__Proposition__AtomicPropositionAssignment"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6535:1: rule__Proposition__AtomicPropositionAssignment : ( ( RULE_ID ) ) ;
    public final void rule__Proposition__AtomicPropositionAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6539:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6540:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6540:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6541:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPropositionAccess().getAtomicPropositionAtomicPropositionCrossReference_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6542:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6543:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPropositionAccess().getAtomicPropositionAtomicPropositionIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Proposition__AtomicPropositionAssignment13128); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPropositionAccess().getAtomicPropositionAtomicPropositionIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPropositionAccess().getAtomicPropositionAtomicPropositionCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Proposition__AtomicPropositionAssignment"


    // $ANTLR start "rule__Constant__ValueAssignment_1_0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6554:1: rule__Constant__ValueAssignment_1_0 : ( ( 'true' ) ) ;
    public final void rule__Constant__ValueAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6558:1: ( ( ( 'true' ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6559:1: ( ( 'true' ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6559:1: ( ( 'true' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6560:1: ( 'true' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstantAccess().getValueTrueKeyword_1_0_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6561:1: ( 'true' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6562:1: 'true'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getConstantAccess().getValueTrueKeyword_1_0_0()); 
            }
            match(input,48,FOLLOW_48_in_rule__Constant__ValueAssignment_1_013168); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstantAccess().getValueTrueKeyword_1_0_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getConstantAccess().getValueTrueKeyword_1_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constant__ValueAssignment_1_0"


    // $ANTLR start "rule__CheckSet__NameAssignment_2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6577:1: rule__CheckSet__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__CheckSet__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6581:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6582:1: ( RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6582:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6583:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckSetAccess().getNameIDTerminalRuleCall_2_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__CheckSet__NameAssignment_213207); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckSetAccess().getNameIDTerminalRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__NameAssignment_2"


    // $ANTLR start "rule__CheckSet__ChecksAssignment_4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6592:1: rule__CheckSet__ChecksAssignment_4 : ( ruleCheck ) ;
    public final void rule__CheckSet__ChecksAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6596:1: ( ( ruleCheck ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6597:1: ( ruleCheck )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6597:1: ( ruleCheck )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6598:1: ruleCheck
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckSetAccess().getChecksCheckParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_ruleCheck_in_rule__CheckSet__ChecksAssignment_413238);
            ruleCheck();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckSetAccess().getChecksCheckParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckSet__ChecksAssignment_4"


    // $ANTLR start "rule__Check__LtsAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6607:1: rule__Check__LtsAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__Check__LtsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6611:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6612:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6612:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6613:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckAccess().getLtsLTSCrossReference_1_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6614:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6615:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckAccess().getLtsLTSIDTerminalRuleCall_1_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Check__LtsAssignment_113273); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckAccess().getLtsLTSIDTerminalRuleCall_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckAccess().getLtsLTSCrossReference_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__LtsAssignment_1"


    // $ANTLR start "rule__Check__FormulaDefinitionAssignment_3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6626:1: rule__Check__FormulaDefinitionAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__Check__FormulaDefinitionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6630:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6631:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6631:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6632:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckAccess().getFormulaDefinitionFormulaDefinitionCrossReference_3_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6633:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6634:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getCheckAccess().getFormulaDefinitionFormulaDefinitionIDTerminalRuleCall_3_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Check__FormulaDefinitionAssignment_313312); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckAccess().getFormulaDefinitionFormulaDefinitionIDTerminalRuleCall_3_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getCheckAccess().getFormulaDefinitionFormulaDefinitionCrossReference_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Check__FormulaDefinitionAssignment_3"


    // $ANTLR start "rule__LTLFormulaSet__NameAssignment_2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6645:1: rule__LTLFormulaSet__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__LTLFormulaSet__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6649:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6650:1: ( RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6650:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6651:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaSetAccess().getNameIDTerminalRuleCall_2_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__LTLFormulaSet__NameAssignment_213347); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaSetAccess().getNameIDTerminalRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__NameAssignment_2"


    // $ANTLR start "rule__LTLFormulaSet__FormulaeAssignment_4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6660:1: rule__LTLFormulaSet__FormulaeAssignment_4 : ( ruleLTLFormulaDefinition ) ;
    public final void rule__LTLFormulaSet__FormulaeAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6664:1: ( ( ruleLTLFormulaDefinition ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6665:1: ( ruleLTLFormulaDefinition )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6665:1: ( ruleLTLFormulaDefinition )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6666:1: ruleLTLFormulaDefinition
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaSetAccess().getFormulaeLTLFormulaDefinitionParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_ruleLTLFormulaDefinition_in_rule__LTLFormulaSet__FormulaeAssignment_413378);
            ruleLTLFormulaDefinition();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaSetAccess().getFormulaeLTLFormulaDefinitionParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaSet__FormulaeAssignment_4"


    // $ANTLR start "rule__LTLFormulaDefinition__NameAssignment_0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6675:1: rule__LTLFormulaDefinition__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__LTLFormulaDefinition__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6679:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6680:1: ( RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6680:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6681:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaDefinitionAccess().getNameIDTerminalRuleCall_0_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__LTLFormulaDefinition__NameAssignment_013409); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaDefinitionAccess().getNameIDTerminalRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaDefinition__NameAssignment_0"


    // $ANTLR start "rule__LTLFormulaDefinition__FormulaAssignment_2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6690:1: rule__LTLFormulaDefinition__FormulaAssignment_2 : ( ruleLTLFormula ) ;
    public final void rule__LTLFormulaDefinition__FormulaAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6694:1: ( ( ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6695:1: ( ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6695:1: ( ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6696:1: ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFormulaDefinitionAccess().getFormulaLTLFormulaParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_rule__LTLFormulaDefinition__FormulaAssignment_213440);
            ruleLTLFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFormulaDefinitionAccess().getFormulaLTLFormulaParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLFormulaDefinition__FormulaAssignment_2"


    // $ANTLR start "rule__LTLX__NestedAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6705:1: rule__LTLX__NestedAssignment_1 : ( ruleLTLFormula ) ;
    public final void rule__LTLX__NestedAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6709:1: ( ( ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6710:1: ( ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6710:1: ( ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6711:1: ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLXAccess().getNestedLTLFormulaParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_rule__LTLX__NestedAssignment_113471);
            ruleLTLFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLXAccess().getNestedLTLFormulaParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLX__NestedAssignment_1"


    // $ANTLR start "rule__LTLG__NestedAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6720:1: rule__LTLG__NestedAssignment_1 : ( ruleLTLFormula ) ;
    public final void rule__LTLG__NestedAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6724:1: ( ( ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6725:1: ( ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6725:1: ( ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6726:1: ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLGAccess().getNestedLTLFormulaParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_rule__LTLG__NestedAssignment_113502);
            ruleLTLFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLGAccess().getNestedLTLFormulaParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLG__NestedAssignment_1"


    // $ANTLR start "rule__LTLU__LeftAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6735:1: rule__LTLU__LeftAssignment_1 : ( ruleLTLFormula ) ;
    public final void rule__LTLU__LeftAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6739:1: ( ( ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6740:1: ( ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6740:1: ( ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6741:1: ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLUAccess().getLeftLTLFormulaParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_rule__LTLU__LeftAssignment_113533);
            ruleLTLFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLUAccess().getLeftLTLFormulaParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLU__LeftAssignment_1"


    // $ANTLR start "rule__LTLU__RightAssignment_3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6750:1: rule__LTLU__RightAssignment_3 : ( ruleLTLFormula ) ;
    public final void rule__LTLU__RightAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6754:1: ( ( ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6755:1: ( ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6755:1: ( ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6756:1: ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLUAccess().getRightLTLFormulaParserRuleCall_3_0()); 
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_rule__LTLU__RightAssignment_313564);
            ruleLTLFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLUAccess().getRightLTLFormulaParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLU__RightAssignment_3"


    // $ANTLR start "rule__LTLF__NestedAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6765:1: rule__LTLF__NestedAssignment_1 : ( ruleLTLFormula ) ;
    public final void rule__LTLF__NestedAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6769:1: ( ( ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6770:1: ( ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6770:1: ( ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6771:1: ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLFAccess().getNestedLTLFormulaParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_rule__LTLF__NestedAssignment_113595);
            ruleLTLFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLFAccess().getNestedLTLFormulaParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLF__NestedAssignment_1"


    // $ANTLR start "rule__LTLNOT__NestedAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6780:1: rule__LTLNOT__NestedAssignment_1 : ( ruleLTLFormula ) ;
    public final void rule__LTLNOT__NestedAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6784:1: ( ( ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6785:1: ( ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6785:1: ( ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6786:1: ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLNOTAccess().getNestedLTLFormulaParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_rule__LTLNOT__NestedAssignment_113626);
            ruleLTLFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLNOTAccess().getNestedLTLFormulaParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLNOT__NestedAssignment_1"


    // $ANTLR start "rule__LTLAND__LeftAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6795:1: rule__LTLAND__LeftAssignment_1 : ( ruleLTLFormula ) ;
    public final void rule__LTLAND__LeftAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6799:1: ( ( ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6800:1: ( ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6800:1: ( ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6801:1: ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLANDAccess().getLeftLTLFormulaParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_rule__LTLAND__LeftAssignment_113657);
            ruleLTLFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLANDAccess().getLeftLTLFormulaParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLAND__LeftAssignment_1"


    // $ANTLR start "rule__LTLAND__RightAssignment_3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6810:1: rule__LTLAND__RightAssignment_3 : ( ruleLTLFormula ) ;
    public final void rule__LTLAND__RightAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6814:1: ( ( ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6815:1: ( ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6815:1: ( ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6816:1: ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLANDAccess().getRightLTLFormulaParserRuleCall_3_0()); 
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_rule__LTLAND__RightAssignment_313688);
            ruleLTLFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLANDAccess().getRightLTLFormulaParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLAND__RightAssignment_3"


    // $ANTLR start "rule__LTLOR__LeftAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6825:1: rule__LTLOR__LeftAssignment_1 : ( ruleLTLFormula ) ;
    public final void rule__LTLOR__LeftAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6829:1: ( ( ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6830:1: ( ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6830:1: ( ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6831:1: ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLORAccess().getLeftLTLFormulaParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_rule__LTLOR__LeftAssignment_113719);
            ruleLTLFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLORAccess().getLeftLTLFormulaParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLOR__LeftAssignment_1"


    // $ANTLR start "rule__LTLOR__RightAssignment_3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6840:1: rule__LTLOR__RightAssignment_3 : ( ruleLTLFormula ) ;
    public final void rule__LTLOR__RightAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6844:1: ( ( ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6845:1: ( ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6845:1: ( ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6846:1: ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLORAccess().getRightLTLFormulaParserRuleCall_3_0()); 
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_rule__LTLOR__RightAssignment_313750);
            ruleLTLFormula();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLORAccess().getRightLTLFormulaParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLOR__RightAssignment_3"


    // $ANTLR start "rule__LTLProposition__AtomicPropositionAssignment"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6855:1: rule__LTLProposition__AtomicPropositionAssignment : ( ( RULE_ID ) ) ;
    public final void rule__LTLProposition__AtomicPropositionAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6859:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6860:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6860:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6861:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLPropositionAccess().getAtomicPropositionAtomicPropositionCrossReference_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6862:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6863:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLPropositionAccess().getAtomicPropositionAtomicPropositionIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__LTLProposition__AtomicPropositionAssignment13785); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLPropositionAccess().getAtomicPropositionAtomicPropositionIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLPropositionAccess().getAtomicPropositionAtomicPropositionCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLProposition__AtomicPropositionAssignment"


    // $ANTLR start "rule__LTLConstant__ValueAssignment_1_0"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6874:1: rule__LTLConstant__ValueAssignment_1_0 : ( ( 'true' ) ) ;
    public final void rule__LTLConstant__ValueAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6878:1: ( ( ( 'true' ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6879:1: ( ( 'true' ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6879:1: ( ( 'true' ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6880:1: ( 'true' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLConstantAccess().getValueTrueKeyword_1_0_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6881:1: ( 'true' )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6882:1: 'true'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLConstantAccess().getValueTrueKeyword_1_0_0()); 
            }
            match(input,48,FOLLOW_48_in_rule__LTLConstant__ValueAssignment_1_013825); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLConstantAccess().getValueTrueKeyword_1_0_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLConstantAccess().getValueTrueKeyword_1_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLConstant__ValueAssignment_1_0"


    // $ANTLR start "rule__LTLCheckSet__NameAssignment_2"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6897:1: rule__LTLCheckSet__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__LTLCheckSet__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6901:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6902:1: ( RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6902:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6903:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckSetAccess().getNameIDTerminalRuleCall_2_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__LTLCheckSet__NameAssignment_213864); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckSetAccess().getNameIDTerminalRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__NameAssignment_2"


    // $ANTLR start "rule__LTLCheckSet__ChecksAssignment_4"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6912:1: rule__LTLCheckSet__ChecksAssignment_4 : ( ruleLTLCheck ) ;
    public final void rule__LTLCheckSet__ChecksAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6916:1: ( ( ruleLTLCheck ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6917:1: ( ruleLTLCheck )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6917:1: ( ruleLTLCheck )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6918:1: ruleLTLCheck
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckSetAccess().getChecksLTLCheckParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_ruleLTLCheck_in_rule__LTLCheckSet__ChecksAssignment_413895);
            ruleLTLCheck();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckSetAccess().getChecksLTLCheckParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheckSet__ChecksAssignment_4"


    // $ANTLR start "rule__LTLCheck__LtsAssignment_1"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6927:1: rule__LTLCheck__LtsAssignment_1 : ( ( RULE_ID ) ) ;
    public final void rule__LTLCheck__LtsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6931:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6932:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6932:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6933:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckAccess().getLtsLTSCrossReference_1_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6934:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6935:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckAccess().getLtsLTSIDTerminalRuleCall_1_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__LTLCheck__LtsAssignment_113930); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckAccess().getLtsLTSIDTerminalRuleCall_1_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckAccess().getLtsLTSCrossReference_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheck__LtsAssignment_1"


    // $ANTLR start "rule__LTLCheck__FormulaDefinitionAssignment_3"
    // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6946:1: rule__LTLCheck__FormulaDefinitionAssignment_3 : ( ( RULE_ID ) ) ;
    public final void rule__LTLCheck__FormulaDefinitionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6950:1: ( ( ( RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6951:1: ( ( RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6951:1: ( ( RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6952:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckAccess().getFormulaDefinitionLTLFormulaDefinitionCrossReference_3_0()); 
            }
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6953:1: ( RULE_ID )
            // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:6954:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLTLCheckAccess().getFormulaDefinitionLTLFormulaDefinitionIDTerminalRuleCall_3_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__LTLCheck__FormulaDefinitionAssignment_313969); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckAccess().getFormulaDefinitionLTLFormulaDefinitionIDTerminalRuleCall_3_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLTLCheckAccess().getFormulaDefinitionLTLFormulaDefinitionCrossReference_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LTLCheck__FormulaDefinitionAssignment_3"

    // $ANTLR start synpred6_InternalXLTS
    public final void synpred6_InternalXLTS_fragment() throws RecognitionException {   
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1064:6: ( ( ( ruleAND ) ) )
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1064:6: ( ( ruleAND ) )
        {
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1064:6: ( ( ruleAND ) )
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1065:1: ( ruleAND )
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getFormulaAccess().getANDParserRuleCall_4()); 
        }
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1066:1: ( ruleAND )
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1066:3: ruleAND
        {
        pushFollow(FOLLOW_ruleAND_in_synpred6_InternalXLTS2234);
        ruleAND();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }
    }
    // $ANTLR end synpred6_InternalXLTS

    // $ANTLR start synpred7_InternalXLTS
    public final void synpred7_InternalXLTS_fragment() throws RecognitionException {   
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1070:6: ( ( ruleOR ) )
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1070:6: ( ruleOR )
        {
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1070:6: ( ruleOR )
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1071:1: ruleOR
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getFormulaAccess().getORParserRuleCall_5()); 
        }
        pushFollow(FOLLOW_ruleOR_in_synpred7_InternalXLTS2252);
        ruleOR();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred7_InternalXLTS

    // $ANTLR start synpred15_InternalXLTS
    public final void synpred15_InternalXLTS_fragment() throws RecognitionException {   
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1152:6: ( ( ( ruleLTLAND ) ) )
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1152:6: ( ( ruleLTLAND ) )
        {
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1152:6: ( ( ruleLTLAND ) )
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1153:1: ( ruleLTLAND )
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getLTLFormulaAccess().getLTLANDParserRuleCall_5()); 
        }
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1154:1: ( ruleLTLAND )
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1154:3: ruleLTLAND
        {
        pushFollow(FOLLOW_ruleLTLAND_in_synpred15_InternalXLTS2457);
        ruleLTLAND();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }
    }
    // $ANTLR end synpred15_InternalXLTS

    // $ANTLR start synpred16_InternalXLTS
    public final void synpred16_InternalXLTS_fragment() throws RecognitionException {   
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1158:6: ( ( ruleLTLOR ) )
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1158:6: ( ruleLTLOR )
        {
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1158:6: ( ruleLTLOR )
        // ../de.hannover.uni.se.fmse.lts.ui/src-gen/de/hannover/uni/se/fmse/lts/ui/contentassist/antlr/internal/InternalXLTS.g:1159:1: ruleLTLOR
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getLTLFormulaAccess().getLTLORParserRuleCall_6()); 
        }
        pushFollow(FOLLOW_ruleLTLOR_in_synpred16_InternalXLTS2475);
        ruleLTLOR();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred16_InternalXLTS

    // Delegated rules

    public final boolean synpred7_InternalXLTS() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred7_InternalXLTS_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred6_InternalXLTS() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred6_InternalXLTS_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred16_InternalXLTS() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred16_InternalXLTS_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred15_InternalXLTS() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred15_InternalXLTS_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA2 dfa2 = new DFA2(this);
    protected DFA4 dfa4 = new DFA4(this);
    static final String DFA2_eotS =
        "\13\uffff";
    static final String DFA2_eofS =
        "\13\uffff";
    static final String DFA2_minS =
        "\1\4\4\uffff\1\0\5\uffff";
    static final String DFA2_maxS =
        "\1\60\4\uffff\1\0\5\uffff";
    static final String DFA2_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\1\uffff\1\7\1\10\1\uffff\1\5\1\6";
    static final String DFA2_specialS =
        "\5\uffff\1\0\5\uffff}>";
    static final String[] DFA2_transitionS = {
            "\1\6\6\uffff\1\7\22\uffff\1\1\1\2\1\3\3\uffff\1\5\3\uffff\1"+
            "\4\7\uffff\1\7",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA2_eot = DFA.unpackEncodedString(DFA2_eotS);
    static final short[] DFA2_eof = DFA.unpackEncodedString(DFA2_eofS);
    static final char[] DFA2_min = DFA.unpackEncodedStringToUnsignedChars(DFA2_minS);
    static final char[] DFA2_max = DFA.unpackEncodedStringToUnsignedChars(DFA2_maxS);
    static final short[] DFA2_accept = DFA.unpackEncodedString(DFA2_acceptS);
    static final short[] DFA2_special = DFA.unpackEncodedString(DFA2_specialS);
    static final short[][] DFA2_transition;

    static {
        int numStates = DFA2_transitionS.length;
        DFA2_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA2_transition[i] = DFA.unpackEncodedString(DFA2_transitionS[i]);
        }
    }

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = DFA2_eot;
            this.eof = DFA2_eof;
            this.min = DFA2_min;
            this.max = DFA2_max;
            this.accept = DFA2_accept;
            this.special = DFA2_special;
            this.transition = DFA2_transition;
        }
        public String getDescription() {
            return "1035:1: rule__Formula__Alternatives : ( ( ruleEX ) | ( ruleEG ) | ( ruleEU ) | ( ruleNOT ) | ( ( ruleAND ) ) | ( ruleOR ) | ( ruleProposition ) | ( ruleConstant ) );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA2_5 = input.LA(1);

                         
                        int index2_5 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred6_InternalXLTS()) ) {s = 9;}

                        else if ( (synpred7_InternalXLTS()) ) {s = 10;}

                         
                        input.seek(index2_5);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 2, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA4_eotS =
        "\14\uffff";
    static final String DFA4_eofS =
        "\14\uffff";
    static final String DFA4_minS =
        "\1\4\5\uffff\1\0\5\uffff";
    static final String DFA4_maxS =
        "\1\60\5\uffff\1\0\5\uffff";
    static final String DFA4_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\1\5\1\uffff\1\10\1\11\1\uffff\1\6\1\7";
    static final String DFA4_specialS =
        "\6\uffff\1\0\5\uffff}>";
    static final String[] DFA4_transitionS = {
            "\1\7\6\uffff\1\10\25\uffff\1\3\2\uffff\1\6\3\uffff\1\5\4\uffff"+
            "\1\1\1\2\1\4\1\10",
            "",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA4_eot = DFA.unpackEncodedString(DFA4_eotS);
    static final short[] DFA4_eof = DFA.unpackEncodedString(DFA4_eofS);
    static final char[] DFA4_min = DFA.unpackEncodedStringToUnsignedChars(DFA4_minS);
    static final char[] DFA4_max = DFA.unpackEncodedStringToUnsignedChars(DFA4_maxS);
    static final short[] DFA4_accept = DFA.unpackEncodedString(DFA4_acceptS);
    static final short[] DFA4_special = DFA.unpackEncodedString(DFA4_specialS);
    static final short[][] DFA4_transition;

    static {
        int numStates = DFA4_transitionS.length;
        DFA4_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA4_transition[i] = DFA.unpackEncodedString(DFA4_transitionS[i]);
        }
    }

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = DFA4_eot;
            this.eof = DFA4_eof;
            this.min = DFA4_min;
            this.max = DFA4_max;
            this.accept = DFA4_accept;
            this.special = DFA4_special;
            this.transition = DFA4_transition;
        }
        public String getDescription() {
            return "1117:1: rule__LTLFormula__Alternatives : ( ( ruleLTLX ) | ( ruleLTLG ) | ( ruleLTLU ) | ( ruleLTLF ) | ( ruleLTLNOT ) | ( ( ruleLTLAND ) ) | ( ruleLTLOR ) | ( ruleLTLProposition ) | ( ruleLTLConstant ) );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA4_6 = input.LA(1);

                         
                        int index4_6 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred15_InternalXLTS()) ) {s = 10;}

                        else if ( (synpred16_InternalXLTS()) ) {s = 11;}

                         
                        input.seek(index4_6);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 4, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_ruleDocument_in_entryRuleDocument67 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDocument74 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Document__Group__0_in_ruleDocument100 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTS_in_entryRuleLTS127 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTS134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group__0_in_ruleLTS160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAtomicProposition_in_entryRuleAtomicProposition187 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAtomicProposition194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__AtomicProposition__NameAssignment_in_ruleAtomicProposition220 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBasicState_in_entryRuleBasicState247 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBasicState254 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicState__Group__0_in_ruleBasicState280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEvent_in_entryRuleEvent307 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEvent314 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Event__NameAssignment_in_ruleEvent340 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTransition_in_entryRuleTransition367 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTransition374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__0_in_ruleTransition400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleComposedLTS_in_entryRuleComposedLTS427 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleComposedLTS434 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__0_in_ruleComposedLTS460 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormulaSet_in_entryRuleFormulaSet487 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFormulaSet494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaSet__Group__0_in_ruleFormulaSet520 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormulaDefinition_in_entryRuleFormulaDefinition547 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFormulaDefinition554 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaDefinition__Group__0_in_ruleFormulaDefinition580 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormula_in_entryRuleFormula607 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFormula614 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Formula__Alternatives_in_ruleFormula640 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEX_in_entryRuleEX667 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEX674 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EX__Group__0_in_ruleEX700 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEG_in_entryRuleEG727 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEG734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EG__Group__0_in_ruleEG760 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEU_in_entryRuleEU787 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEU794 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EU__Group__0_in_ruleEU820 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAND_in_entryRuleAND847 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAND854 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__AND__Group__0_in_ruleAND880 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOR_in_entryRuleOR907 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOR914 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OR__Group__0_in_ruleOR940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNOT_in_entryRuleNOT967 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNOT974 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NOT__Group__0_in_ruleNOT1000 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProposition_in_entryRuleProposition1027 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProposition1034 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Proposition__AtomicPropositionAssignment_in_ruleProposition1060 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_entryRuleConstant1087 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleConstant1094 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constant__Group__0_in_ruleConstant1120 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckSet_in_entryRuleCheckSet1147 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheckSet1154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckSet__Group__0_in_ruleCheckSet1180 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheck_in_entryRuleCheck1207 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheck1214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Check__Group__0_in_ruleCheck1240 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormulaSet_in_entryRuleLTLFormulaSet1267 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLFormulaSet1274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__Group__0_in_ruleLTLFormulaSet1300 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormulaDefinition_in_entryRuleLTLFormulaDefinition1327 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLFormulaDefinition1334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaDefinition__Group__0_in_ruleLTLFormulaDefinition1360 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_entryRuleLTLFormula1387 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLFormula1394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormula__Alternatives_in_ruleLTLFormula1420 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLX_in_entryRuleLTLX1447 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLX1454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLX__Group__0_in_ruleLTLX1480 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLG_in_entryRuleLTLG1507 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLG1514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLG__Group__0_in_ruleLTLG1540 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLU_in_entryRuleLTLU1567 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLU1574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLU__Group__0_in_ruleLTLU1600 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLF_in_entryRuleLTLF1627 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLF1634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLF__Group__0_in_ruleLTLF1660 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLNOT_in_entryRuleLTLNOT1687 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLNOT1694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLNOT__Group__0_in_ruleLTLNOT1720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLAND_in_entryRuleLTLAND1747 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLAND1754 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLAND__Group__0_in_ruleLTLAND1780 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLOR_in_entryRuleLTLOR1807 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLOR1814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLOR__Group__0_in_ruleLTLOR1840 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLProposition_in_entryRuleLTLProposition1867 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLProposition1874 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLProposition__AtomicPropositionAssignment_in_ruleLTLProposition1900 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLConstant_in_entryRuleLTLConstant1927 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLConstant1934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLConstant__Group__0_in_ruleLTLConstant1960 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLCheckSet_in_entryRuleLTLCheckSet1987 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLCheckSet1994 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__Group__0_in_ruleLTLCheckSet2020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLCheck_in_entryRuleLTLCheck2047 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLCheck2054 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheck__Group__0_in_ruleLTLCheck2080 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTS_in_rule__Document__LtssAlternatives_0_02116 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleComposedLTS_in_rule__Document__LtssAlternatives_0_02133 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEX_in_rule__Formula__Alternatives2165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEG_in_rule__Formula__Alternatives2182 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEU_in_rule__Formula__Alternatives2199 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNOT_in_rule__Formula__Alternatives2216 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAND_in_rule__Formula__Alternatives2234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOR_in_rule__Formula__Alternatives2252 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProposition_in_rule__Formula__Alternatives2269 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_rule__Formula__Alternatives2286 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constant__ValueAssignment_1_0_in_rule__Constant__Alternatives_12318 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__Constant__Alternatives_12337 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLX_in_rule__LTLFormula__Alternatives2371 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLG_in_rule__LTLFormula__Alternatives2388 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLU_in_rule__LTLFormula__Alternatives2405 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLF_in_rule__LTLFormula__Alternatives2422 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLNOT_in_rule__LTLFormula__Alternatives2439 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLAND_in_rule__LTLFormula__Alternatives2457 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLOR_in_rule__LTLFormula__Alternatives2475 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLProposition_in_rule__LTLFormula__Alternatives2492 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLConstant_in_rule__LTLFormula__Alternatives2509 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLConstant__ValueAssignment_1_0_in_rule__LTLConstant__Alternatives_12541 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__LTLConstant__Alternatives_12560 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Document__Group__0__Impl_in_rule__Document__Group__02592 = new BitSet(new long[]{0x0000100008000000L});
    public static final BitSet FOLLOW_rule__Document__Group__1_in_rule__Document__Group__02595 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Document__LtssAssignment_0_in_rule__Document__Group__0__Impl2622 = new BitSet(new long[]{0x0000000000801002L});
    public static final BitSet FOLLOW_rule__Document__Group__1__Impl_in_rule__Document__Group__12653 = new BitSet(new long[]{0x0000100008000000L});
    public static final BitSet FOLLOW_rule__Document__Group__2_in_rule__Document__Group__12656 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Document__FormulaSetAssignment_1_in_rule__Document__Group__1__Impl2683 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Document__Group__2__Impl_in_rule__Document__Group__22714 = new BitSet(new long[]{0x0000100008000000L});
    public static final BitSet FOLLOW_rule__Document__Group__3_in_rule__Document__Group__22717 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Document__CheckSetAssignment_2_in_rule__Document__Group__2__Impl2744 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Document__Group__3__Impl_in_rule__Document__Group__32775 = new BitSet(new long[]{0x0000100008000000L});
    public static final BitSet FOLLOW_rule__Document__Group__4_in_rule__Document__Group__32778 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Document__LTLformulaSetAssignment_3_in_rule__Document__Group__3__Impl2805 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Document__Group__4__Impl_in_rule__Document__Group__42836 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Document__LTLcheckSetAssignment_4_in_rule__Document__Group__4__Impl2863 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group__0__Impl_in_rule__LTS__Group__02904 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LTS__Group__1_in_rule__LTS__Group__02907 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__LTS__Group__0__Impl2935 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group__1__Impl_in_rule__LTS__Group__12966 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__LTS__Group__2_in_rule__LTS__Group__12969 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__NameAssignment_1_in_rule__LTS__Group__1__Impl2996 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group__2__Impl_in_rule__LTS__Group__23026 = new BitSet(new long[]{0x0000000000024000L});
    public static final BitSet FOLLOW_rule__LTS__Group__3_in_rule__LTS__Group__23029 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__LTS__Group__2__Impl3057 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group__3__Impl_in_rule__LTS__Group__33088 = new BitSet(new long[]{0x0000000000024000L});
    public static final BitSet FOLLOW_rule__LTS__Group__4_in_rule__LTS__Group__33091 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_3__0_in_rule__LTS__Group__3__Impl3118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group__4__Impl_in_rule__LTS__Group__43149 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LTS__Group__5_in_rule__LTS__Group__43152 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__LTS__Group__4__Impl3180 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group__5__Impl_in_rule__LTS__Group__53211 = new BitSet(new long[]{0x0000000000088000L});
    public static final BitSet FOLLOW_rule__LTS__Group__6_in_rule__LTS__Group__53214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__StatesAssignment_5_in_rule__LTS__Group__5__Impl3241 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group__6__Impl_in_rule__LTS__Group__63271 = new BitSet(new long[]{0x0000000000088000L});
    public static final BitSet FOLLOW_rule__LTS__Group__7_in_rule__LTS__Group__63274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_6__0_in_rule__LTS__Group__6__Impl3301 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_rule__LTS__Group__7__Impl_in_rule__LTS__Group__73332 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LTS__Group__8_in_rule__LTS__Group__73335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__LTS__Group__7__Impl3363 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group__8__Impl_in_rule__LTS__Group__83394 = new BitSet(new long[]{0x0000000000190010L});
    public static final BitSet FOLLOW_rule__LTS__Group__9_in_rule__LTS__Group__83397 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__InitStatesAssignment_8_in_rule__LTS__Group__8__Impl3424 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group__9__Impl_in_rule__LTS__Group__93454 = new BitSet(new long[]{0x0000000000190010L});
    public static final BitSet FOLLOW_rule__LTS__Group__10_in_rule__LTS__Group__93457 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_9__0_in_rule__LTS__Group__9__Impl3484 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_rule__LTS__Group__10__Impl_in_rule__LTS__Group__103515 = new BitSet(new long[]{0x0000000000190010L});
    public static final BitSet FOLLOW_rule__LTS__Group__11_in_rule__LTS__Group__103518 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_10__0_in_rule__LTS__Group__10__Impl3545 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group__11__Impl_in_rule__LTS__Group__113576 = new BitSet(new long[]{0x0000000000190010L});
    public static final BitSet FOLLOW_rule__LTS__Group__12_in_rule__LTS__Group__113579 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__TransitionsAssignment_11_in_rule__LTS__Group__11__Impl3606 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__LTS__Group__12__Impl_in_rule__LTS__Group__123637 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__LTS__Group__12__Impl3665 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_3__0__Impl_in_rule__LTS__Group_3__03722 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__LTS__Group_3__1_in_rule__LTS__Group_3__03725 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__LTS__Group_3__0__Impl3753 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_3__1__Impl_in_rule__LTS__Group_3__13784 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LTS__Group_3__2_in_rule__LTS__Group_3__13787 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__LTS__Group_3__1__Impl3815 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_3__2__Impl_in_rule__LTS__Group_3__23846 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__LTS__Group_3__3_in_rule__LTS__Group_3__23849 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__AtomicPropositionsAssignment_3_2_in_rule__LTS__Group_3__2__Impl3876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_3__3__Impl_in_rule__LTS__Group_3__33906 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_3_3__0_in_rule__LTS__Group_3__3__Impl3933 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_rule__LTS__Group_3_3__0__Impl_in_rule__LTS__Group_3_3__03972 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LTS__Group_3_3__1_in_rule__LTS__Group_3_3__03975 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__LTS__Group_3_3__0__Impl4003 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_3_3__1__Impl_in_rule__LTS__Group_3_3__14034 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__AtomicPropositionsAssignment_3_3_1_in_rule__LTS__Group_3_3__1__Impl4061 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_6__0__Impl_in_rule__LTS__Group_6__04095 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LTS__Group_6__1_in_rule__LTS__Group_6__04098 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__LTS__Group_6__0__Impl4126 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_6__1__Impl_in_rule__LTS__Group_6__14157 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__StatesAssignment_6_1_in_rule__LTS__Group_6__1__Impl4184 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_9__0__Impl_in_rule__LTS__Group_9__04218 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LTS__Group_9__1_in_rule__LTS__Group_9__04221 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__LTS__Group_9__0__Impl4249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_9__1__Impl_in_rule__LTS__Group_9__14280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__InitStatesAssignment_9_1_in_rule__LTS__Group_9__1__Impl4307 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_10__0__Impl_in_rule__LTS__Group_10__04341 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LTS__Group_10__1_in_rule__LTS__Group_10__04344 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__LTS__Group_10__0__Impl4372 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_10__1__Impl_in_rule__LTS__Group_10__14403 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__LTS__Group_10__2_in_rule__LTS__Group_10__14406 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__AlphabetAssignment_10_1_in_rule__LTS__Group_10__1__Impl4433 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_10__2__Impl_in_rule__LTS__Group_10__24463 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_10_2__0_in_rule__LTS__Group_10__2__Impl4490 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_rule__LTS__Group_10_2__0__Impl_in_rule__LTS__Group_10_2__04527 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LTS__Group_10_2__1_in_rule__LTS__Group_10_2__04530 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__LTS__Group_10_2__0__Impl4558 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__Group_10_2__1__Impl_in_rule__LTS__Group_10_2__14589 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTS__AlphabetAssignment_10_2_1_in_rule__LTS__Group_10_2__1__Impl4616 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicState__Group__0__Impl_in_rule__BasicState__Group__04650 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__BasicState__Group__1_in_rule__BasicState__Group__04653 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicState__NameAssignment_0_in_rule__BasicState__Group__0__Impl4680 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicState__Group__1__Impl_in_rule__BasicState__Group__14710 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicState__Group_1__0_in_rule__BasicState__Group__1__Impl4737 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_rule__BasicState__Group_1__0__Impl_in_rule__BasicState__Group_1__04772 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__BasicState__Group_1__1_in_rule__BasicState__Group_1__04775 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__BasicState__Group_1__0__Impl4803 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicState__Group_1__1__Impl_in_rule__BasicState__Group_1__14834 = new BitSet(new long[]{0x0000000000090000L});
    public static final BitSet FOLLOW_rule__BasicState__Group_1__2_in_rule__BasicState__Group_1__14837 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicState__AtomicPropositionsAssignment_1_1_in_rule__BasicState__Group_1__1__Impl4864 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicState__Group_1__2__Impl_in_rule__BasicState__Group_1__24894 = new BitSet(new long[]{0x0000000000090000L});
    public static final BitSet FOLLOW_rule__BasicState__Group_1__3_in_rule__BasicState__Group_1__24897 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicState__Group_1_2__0_in_rule__BasicState__Group_1__2__Impl4924 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_rule__BasicState__Group_1__3__Impl_in_rule__BasicState__Group_1__34955 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__BasicState__Group_1__3__Impl4983 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicState__Group_1_2__0__Impl_in_rule__BasicState__Group_1_2__05022 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__BasicState__Group_1_2__1_in_rule__BasicState__Group_1_2__05025 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__BasicState__Group_1_2__0__Impl5053 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicState__Group_1_2__1__Impl_in_rule__BasicState__Group_1_2__15084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BasicState__AtomicPropositionsAssignment_1_2_1_in_rule__BasicState__Group_1_2__1__Impl5111 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__0__Impl_in_rule__Transition__Group__05145 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__Transition__Group__1_in_rule__Transition__Group__05148 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__SourceAssignment_0_in_rule__Transition__Group__0__Impl5175 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__1__Impl_in_rule__Transition__Group__15205 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Transition__Group__2_in_rule__Transition__Group__15208 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Transition__Group__1__Impl5236 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__2__Impl_in_rule__Transition__Group__25267 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__Transition__Group__3_in_rule__Transition__Group__25270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__DestinationAssignment_2_in_rule__Transition__Group__2__Impl5297 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__3__Impl_in_rule__Transition__Group__35327 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Transition__Group__4_in_rule__Transition__Group__35330 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Transition__Group__3__Impl5358 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__Group__4__Impl_in_rule__Transition__Group__45389 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Transition__EventAssignment_4_in_rule__Transition__Group__4__Impl5416 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__0__Impl_in_rule__ComposedLTS__Group__05456 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__1_in_rule__ComposedLTS__Group__05459 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__ComposedLTS__Group__0__Impl5487 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__1__Impl_in_rule__ComposedLTS__Group__15518 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__2_in_rule__ComposedLTS__Group__15521 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__LtsLeftAssignment_1_in_rule__ComposedLTS__Group__1__Impl5548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__2__Impl_in_rule__ComposedLTS__Group__25578 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__3_in_rule__ComposedLTS__Group__25581 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__ComposedLTS__Group__2__Impl5609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__3__Impl_in_rule__ComposedLTS__Group__35640 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__4_in_rule__ComposedLTS__Group__35643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__LtsRightAssignment_3_in_rule__ComposedLTS__Group__3__Impl5670 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__4__Impl_in_rule__ComposedLTS__Group__45700 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__5_in_rule__ComposedLTS__Group__45703 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__ComposedLTS__Group__4__Impl5731 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__5__Impl_in_rule__ComposedLTS__Group__55762 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__6_in_rule__ComposedLTS__Group__55765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__NameAssignment_5_in_rule__ComposedLTS__Group__5__Impl5792 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group__6__Impl_in_rule__ComposedLTS__Group__65822 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6__0_in_rule__ComposedLTS__Group__6__Impl5849 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6__0__Impl_in_rule__ComposedLTS__Group_6__05894 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6__1_in_rule__ComposedLTS__Group_6__05897 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__ComposedLTS__Group_6__0__Impl5925 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6__1__Impl_in_rule__ComposedLTS__Group_6__15956 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6__2_in_rule__ComposedLTS__Group_6__15959 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__ComposedLTS__Group_6__1__Impl5987 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6__2__Impl_in_rule__ComposedLTS__Group_6__26018 = new BitSet(new long[]{0x0000000000090000L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6__3_in_rule__ComposedLTS__Group_6__26021 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__EventsAssignment_6_2_in_rule__ComposedLTS__Group_6__2__Impl6048 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6__3__Impl_in_rule__ComposedLTS__Group_6__36078 = new BitSet(new long[]{0x0000000000090000L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6__4_in_rule__ComposedLTS__Group_6__36081 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6_3__0_in_rule__ComposedLTS__Group_6__3__Impl6108 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6__4__Impl_in_rule__ComposedLTS__Group_6__46139 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__ComposedLTS__Group_6__4__Impl6167 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6_3__0__Impl_in_rule__ComposedLTS__Group_6_3__06208 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6_3__1_in_rule__ComposedLTS__Group_6_3__06211 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__ComposedLTS__Group_6_3__0__Impl6239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__Group_6_3__1__Impl_in_rule__ComposedLTS__Group_6_3__16270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ComposedLTS__EventsAssignment_6_3_1_in_rule__ComposedLTS__Group_6_3__1__Impl6297 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaSet__Group__0__Impl_in_rule__FormulaSet__Group__06331 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rule__FormulaSet__Group__1_in_rule__FormulaSet__Group__06334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__FormulaSet__Group__0__Impl6362 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaSet__Group__1__Impl_in_rule__FormulaSet__Group__16393 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__FormulaSet__Group__2_in_rule__FormulaSet__Group__16396 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__FormulaSet__Group__1__Impl6424 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaSet__Group__2__Impl_in_rule__FormulaSet__Group__26455 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__FormulaSet__Group__3_in_rule__FormulaSet__Group__26458 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaSet__NameAssignment_2_in_rule__FormulaSet__Group__2__Impl6485 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaSet__Group__3__Impl_in_rule__FormulaSet__Group__36515 = new BitSet(new long[]{0x0000000000010010L});
    public static final BitSet FOLLOW_rule__FormulaSet__Group__4_in_rule__FormulaSet__Group__36518 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__FormulaSet__Group__3__Impl6546 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaSet__Group__4__Impl_in_rule__FormulaSet__Group__46577 = new BitSet(new long[]{0x0000000000010010L});
    public static final BitSet FOLLOW_rule__FormulaSet__Group__5_in_rule__FormulaSet__Group__46580 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaSet__FormulaeAssignment_4_in_rule__FormulaSet__Group__4__Impl6607 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__FormulaSet__Group__5__Impl_in_rule__FormulaSet__Group__56638 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__FormulaSet__Group__5__Impl6666 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaDefinition__Group__0__Impl_in_rule__FormulaDefinition__Group__06709 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_rule__FormulaDefinition__Group__1_in_rule__FormulaDefinition__Group__06712 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaDefinition__NameAssignment_0_in_rule__FormulaDefinition__Group__0__Impl6739 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaDefinition__Group__1__Impl_in_rule__FormulaDefinition__Group__16769 = new BitSet(new long[]{0x00010111C0000810L});
    public static final BitSet FOLLOW_rule__FormulaDefinition__Group__2_in_rule__FormulaDefinition__Group__16772 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rule__FormulaDefinition__Group__1__Impl6800 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaDefinition__Group__2__Impl_in_rule__FormulaDefinition__Group__26831 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FormulaDefinition__FormulaAssignment_2_in_rule__FormulaDefinition__Group__2__Impl6858 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EX__Group__0__Impl_in_rule__EX__Group__06894 = new BitSet(new long[]{0x00010111C0000810L});
    public static final BitSet FOLLOW_rule__EX__Group__1_in_rule__EX__Group__06897 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__EX__Group__0__Impl6925 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EX__Group__1__Impl_in_rule__EX__Group__16956 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EX__NestedAssignment_1_in_rule__EX__Group__1__Impl6983 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EG__Group__0__Impl_in_rule__EG__Group__07017 = new BitSet(new long[]{0x00010111C0000810L});
    public static final BitSet FOLLOW_rule__EG__Group__1_in_rule__EG__Group__07020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__EG__Group__0__Impl7048 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EG__Group__1__Impl_in_rule__EG__Group__17079 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EG__NestedAssignment_1_in_rule__EG__Group__1__Impl7106 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EU__Group__0__Impl_in_rule__EU__Group__07140 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_rule__EU__Group__1_in_rule__EU__Group__07143 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_rule__EU__Group__0__Impl7171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EU__Group__1__Impl_in_rule__EU__Group__17202 = new BitSet(new long[]{0x00010111C0000810L});
    public static final BitSet FOLLOW_rule__EU__Group__2_in_rule__EU__Group__17205 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_rule__EU__Group__1__Impl7233 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EU__Group__2__Impl_in_rule__EU__Group__27264 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_rule__EU__Group__3_in_rule__EU__Group__27267 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EU__LeftAssignment_2_in_rule__EU__Group__2__Impl7294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EU__Group__3__Impl_in_rule__EU__Group__37324 = new BitSet(new long[]{0x00010111C0000810L});
    public static final BitSet FOLLOW_rule__EU__Group__4_in_rule__EU__Group__37327 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rule__EU__Group__3__Impl7355 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EU__Group__4__Impl_in_rule__EU__Group__47386 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_rule__EU__Group__5_in_rule__EU__Group__47389 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EU__RightAssignment_4_in_rule__EU__Group__4__Impl7416 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__EU__Group__5__Impl_in_rule__EU__Group__57446 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_rule__EU__Group__5__Impl7474 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__AND__Group__0__Impl_in_rule__AND__Group__07517 = new BitSet(new long[]{0x00010111C0000810L});
    public static final BitSet FOLLOW_rule__AND__Group__1_in_rule__AND__Group__07520 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_rule__AND__Group__0__Impl7548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__AND__Group__1__Impl_in_rule__AND__Group__17579 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_rule__AND__Group__2_in_rule__AND__Group__17582 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__AND__LeftAssignment_1_in_rule__AND__Group__1__Impl7609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__AND__Group__2__Impl_in_rule__AND__Group__27639 = new BitSet(new long[]{0x00010111C0000810L});
    public static final BitSet FOLLOW_rule__AND__Group__3_in_rule__AND__Group__27642 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_rule__AND__Group__2__Impl7670 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__AND__Group__3__Impl_in_rule__AND__Group__37701 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_rule__AND__Group__4_in_rule__AND__Group__37704 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__AND__RightAssignment_3_in_rule__AND__Group__3__Impl7731 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__AND__Group__4__Impl_in_rule__AND__Group__47761 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__AND__Group__4__Impl7789 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OR__Group__0__Impl_in_rule__OR__Group__07830 = new BitSet(new long[]{0x00010111C0000810L});
    public static final BitSet FOLLOW_rule__OR__Group__1_in_rule__OR__Group__07833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_rule__OR__Group__0__Impl7861 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OR__Group__1__Impl_in_rule__OR__Group__17892 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_rule__OR__Group__2_in_rule__OR__Group__17895 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OR__LeftAssignment_1_in_rule__OR__Group__1__Impl7922 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OR__Group__2__Impl_in_rule__OR__Group__27952 = new BitSet(new long[]{0x00010111C0000810L});
    public static final BitSet FOLLOW_rule__OR__Group__3_in_rule__OR__Group__27955 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_rule__OR__Group__2__Impl7983 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OR__Group__3__Impl_in_rule__OR__Group__38014 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_rule__OR__Group__4_in_rule__OR__Group__38017 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OR__RightAssignment_3_in_rule__OR__Group__3__Impl8044 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__OR__Group__4__Impl_in_rule__OR__Group__48074 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__OR__Group__4__Impl8102 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NOT__Group__0__Impl_in_rule__NOT__Group__08143 = new BitSet(new long[]{0x00010111C0000810L});
    public static final BitSet FOLLOW_rule__NOT__Group__1_in_rule__NOT__Group__08146 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_rule__NOT__Group__0__Impl8174 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NOT__Group__1__Impl_in_rule__NOT__Group__18205 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NOT__NestedAssignment_1_in_rule__NOT__Group__1__Impl8232 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constant__Group__0__Impl_in_rule__Constant__Group__08266 = new BitSet(new long[]{0x00010111C0000810L});
    public static final BitSet FOLLOW_rule__Constant__Group__1_in_rule__Constant__Group__08269 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constant__Group__1__Impl_in_rule__Constant__Group__18327 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Constant__Alternatives_1_in_rule__Constant__Group__1__Impl8354 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckSet__Group__0__Impl_in_rule__CheckSet__Group__08388 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_rule__CheckSet__Group__1_in_rule__CheckSet__Group__08391 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__CheckSet__Group__0__Impl8419 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckSet__Group__1__Impl_in_rule__CheckSet__Group__18450 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__CheckSet__Group__2_in_rule__CheckSet__Group__18453 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_rule__CheckSet__Group__1__Impl8481 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckSet__Group__2__Impl_in_rule__CheckSet__Group__28512 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__CheckSet__Group__3_in_rule__CheckSet__Group__28515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckSet__NameAssignment_2_in_rule__CheckSet__Group__2__Impl8542 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckSet__Group__3__Impl_in_rule__CheckSet__Group__38572 = new BitSet(new long[]{0x0000040000010000L});
    public static final BitSet FOLLOW_rule__CheckSet__Group__4_in_rule__CheckSet__Group__38575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__CheckSet__Group__3__Impl8603 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckSet__Group__4__Impl_in_rule__CheckSet__Group__48634 = new BitSet(new long[]{0x0000040000010000L});
    public static final BitSet FOLLOW_rule__CheckSet__Group__5_in_rule__CheckSet__Group__48637 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckSet__ChecksAssignment_4_in_rule__CheckSet__Group__4__Impl8664 = new BitSet(new long[]{0x0000040000000002L});
    public static final BitSet FOLLOW_rule__CheckSet__Group__5__Impl_in_rule__CheckSet__Group__58695 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__CheckSet__Group__5__Impl8723 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Check__Group__0__Impl_in_rule__Check__Group__08766 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Check__Group__1_in_rule__Check__Group__08769 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_rule__Check__Group__0__Impl8797 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Check__Group__1__Impl_in_rule__Check__Group__18828 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_rule__Check__Group__2_in_rule__Check__Group__18831 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Check__LtsAssignment_1_in_rule__Check__Group__1__Impl8858 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Check__Group__2__Impl_in_rule__Check__Group__28888 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Check__Group__3_in_rule__Check__Group__28891 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_rule__Check__Group__2__Impl8919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Check__Group__3__Impl_in_rule__Check__Group__38950 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Check__FormulaDefinitionAssignment_3_in_rule__Check__Group__3__Impl8977 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__Group__0__Impl_in_rule__LTLFormulaSet__Group__09015 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__Group__1_in_rule__LTLFormulaSet__Group__09018 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_rule__LTLFormulaSet__Group__0__Impl9046 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__Group__1__Impl_in_rule__LTLFormulaSet__Group__19077 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__Group__2_in_rule__LTLFormulaSet__Group__19080 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__LTLFormulaSet__Group__1__Impl9108 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__Group__2__Impl_in_rule__LTLFormulaSet__Group__29139 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__Group__3_in_rule__LTLFormulaSet__Group__29142 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__NameAssignment_2_in_rule__LTLFormulaSet__Group__2__Impl9169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__Group__3__Impl_in_rule__LTLFormulaSet__Group__39199 = new BitSet(new long[]{0x0000000000010010L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__Group__4_in_rule__LTLFormulaSet__Group__39202 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__LTLFormulaSet__Group__3__Impl9230 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__Group__4__Impl_in_rule__LTLFormulaSet__Group__49261 = new BitSet(new long[]{0x0000000000010010L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__Group__5_in_rule__LTLFormulaSet__Group__49264 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__FormulaeAssignment_4_in_rule__LTLFormulaSet__Group__4__Impl9291 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_rule__LTLFormulaSet__Group__5__Impl_in_rule__LTLFormulaSet__Group__59322 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__LTLFormulaSet__Group__5__Impl9350 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaDefinition__Group__0__Impl_in_rule__LTLFormulaDefinition__Group__09393 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_rule__LTLFormulaDefinition__Group__1_in_rule__LTLFormulaDefinition__Group__09396 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaDefinition__NameAssignment_0_in_rule__LTLFormulaDefinition__Group__0__Impl9423 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaDefinition__Group__1__Impl_in_rule__LTLFormulaDefinition__Group__19453 = new BitSet(new long[]{0x0001E11200000810L});
    public static final BitSet FOLLOW_rule__LTLFormulaDefinition__Group__2_in_rule__LTLFormulaDefinition__Group__19456 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rule__LTLFormulaDefinition__Group__1__Impl9484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaDefinition__Group__2__Impl_in_rule__LTLFormulaDefinition__Group__29515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLFormulaDefinition__FormulaAssignment_2_in_rule__LTLFormulaDefinition__Group__2__Impl9542 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLX__Group__0__Impl_in_rule__LTLX__Group__09578 = new BitSet(new long[]{0x0001E11200000810L});
    public static final BitSet FOLLOW_rule__LTLX__Group__1_in_rule__LTLX__Group__09581 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_rule__LTLX__Group__0__Impl9609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLX__Group__1__Impl_in_rule__LTLX__Group__19640 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLX__NestedAssignment_1_in_rule__LTLX__Group__1__Impl9667 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLG__Group__0__Impl_in_rule__LTLG__Group__09701 = new BitSet(new long[]{0x0001E11200000810L});
    public static final BitSet FOLLOW_rule__LTLG__Group__1_in_rule__LTLG__Group__09704 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_rule__LTLG__Group__0__Impl9732 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLG__Group__1__Impl_in_rule__LTLG__Group__19763 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLG__NestedAssignment_1_in_rule__LTLG__Group__1__Impl9790 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLU__Group__0__Impl_in_rule__LTLU__Group__09824 = new BitSet(new long[]{0x0001E11200000810L});
    public static final BitSet FOLLOW_rule__LTLU__Group__1_in_rule__LTLU__Group__09827 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_rule__LTLU__Group__0__Impl9855 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLU__Group__1__Impl_in_rule__LTLU__Group__19886 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_rule__LTLU__Group__2_in_rule__LTLU__Group__19889 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLU__LeftAssignment_1_in_rule__LTLU__Group__1__Impl9916 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLU__Group__2__Impl_in_rule__LTLU__Group__29946 = new BitSet(new long[]{0x0001E11200000810L});
    public static final BitSet FOLLOW_rule__LTLU__Group__3_in_rule__LTLU__Group__29949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rule__LTLU__Group__2__Impl9977 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLU__Group__3__Impl_in_rule__LTLU__Group__310008 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_rule__LTLU__Group__4_in_rule__LTLU__Group__310011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLU__RightAssignment_3_in_rule__LTLU__Group__3__Impl10038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLU__Group__4__Impl_in_rule__LTLU__Group__410068 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_rule__LTLU__Group__4__Impl10096 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLF__Group__0__Impl_in_rule__LTLF__Group__010137 = new BitSet(new long[]{0x0001E11200000810L});
    public static final BitSet FOLLOW_rule__LTLF__Group__1_in_rule__LTLF__Group__010140 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_rule__LTLF__Group__0__Impl10168 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLF__Group__1__Impl_in_rule__LTLF__Group__110199 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLF__NestedAssignment_1_in_rule__LTLF__Group__1__Impl10226 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLNOT__Group__0__Impl_in_rule__LTLNOT__Group__010260 = new BitSet(new long[]{0x0001E11200000810L});
    public static final BitSet FOLLOW_rule__LTLNOT__Group__1_in_rule__LTLNOT__Group__010263 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_rule__LTLNOT__Group__0__Impl10291 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLNOT__Group__1__Impl_in_rule__LTLNOT__Group__110322 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLNOT__NestedAssignment_1_in_rule__LTLNOT__Group__1__Impl10349 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLAND__Group__0__Impl_in_rule__LTLAND__Group__010383 = new BitSet(new long[]{0x0001E11200000810L});
    public static final BitSet FOLLOW_rule__LTLAND__Group__1_in_rule__LTLAND__Group__010386 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_rule__LTLAND__Group__0__Impl10414 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLAND__Group__1__Impl_in_rule__LTLAND__Group__110445 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_rule__LTLAND__Group__2_in_rule__LTLAND__Group__110448 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLAND__LeftAssignment_1_in_rule__LTLAND__Group__1__Impl10475 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLAND__Group__2__Impl_in_rule__LTLAND__Group__210505 = new BitSet(new long[]{0x0001E11200000810L});
    public static final BitSet FOLLOW_rule__LTLAND__Group__3_in_rule__LTLAND__Group__210508 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_rule__LTLAND__Group__2__Impl10536 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLAND__Group__3__Impl_in_rule__LTLAND__Group__310567 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_rule__LTLAND__Group__4_in_rule__LTLAND__Group__310570 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLAND__RightAssignment_3_in_rule__LTLAND__Group__3__Impl10597 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLAND__Group__4__Impl_in_rule__LTLAND__Group__410627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__LTLAND__Group__4__Impl10655 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLOR__Group__0__Impl_in_rule__LTLOR__Group__010696 = new BitSet(new long[]{0x0001E11200000810L});
    public static final BitSet FOLLOW_rule__LTLOR__Group__1_in_rule__LTLOR__Group__010699 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_rule__LTLOR__Group__0__Impl10727 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLOR__Group__1__Impl_in_rule__LTLOR__Group__110758 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_rule__LTLOR__Group__2_in_rule__LTLOR__Group__110761 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLOR__LeftAssignment_1_in_rule__LTLOR__Group__1__Impl10788 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLOR__Group__2__Impl_in_rule__LTLOR__Group__210818 = new BitSet(new long[]{0x0001E11200000810L});
    public static final BitSet FOLLOW_rule__LTLOR__Group__3_in_rule__LTLOR__Group__210821 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_rule__LTLOR__Group__2__Impl10849 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLOR__Group__3__Impl_in_rule__LTLOR__Group__310880 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_rule__LTLOR__Group__4_in_rule__LTLOR__Group__310883 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLOR__RightAssignment_3_in_rule__LTLOR__Group__3__Impl10910 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLOR__Group__4__Impl_in_rule__LTLOR__Group__410940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__LTLOR__Group__4__Impl10968 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLConstant__Group__0__Impl_in_rule__LTLConstant__Group__011009 = new BitSet(new long[]{0x0001E11200000810L});
    public static final BitSet FOLLOW_rule__LTLConstant__Group__1_in_rule__LTLConstant__Group__011012 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLConstant__Group__1__Impl_in_rule__LTLConstant__Group__111070 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLConstant__Alternatives_1_in_rule__LTLConstant__Group__1__Impl11097 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__Group__0__Impl_in_rule__LTLCheckSet__Group__011131 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__Group__1_in_rule__LTLCheckSet__Group__011134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_rule__LTLCheckSet__Group__0__Impl11162 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__Group__1__Impl_in_rule__LTLCheckSet__Group__111193 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__Group__2_in_rule__LTLCheckSet__Group__111196 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_rule__LTLCheckSet__Group__1__Impl11224 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__Group__2__Impl_in_rule__LTLCheckSet__Group__211255 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__Group__3_in_rule__LTLCheckSet__Group__211258 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__NameAssignment_2_in_rule__LTLCheckSet__Group__2__Impl11285 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__Group__3__Impl_in_rule__LTLCheckSet__Group__311315 = new BitSet(new long[]{0x0000040000010000L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__Group__4_in_rule__LTLCheckSet__Group__311318 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__LTLCheckSet__Group__3__Impl11346 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__Group__4__Impl_in_rule__LTLCheckSet__Group__411377 = new BitSet(new long[]{0x0000040000010000L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__Group__5_in_rule__LTLCheckSet__Group__411380 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__ChecksAssignment_4_in_rule__LTLCheckSet__Group__4__Impl11407 = new BitSet(new long[]{0x0000040000000002L});
    public static final BitSet FOLLOW_rule__LTLCheckSet__Group__5__Impl_in_rule__LTLCheckSet__Group__511438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__LTLCheckSet__Group__5__Impl11466 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheck__Group__0__Impl_in_rule__LTLCheck__Group__011509 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LTLCheck__Group__1_in_rule__LTLCheck__Group__011512 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_rule__LTLCheck__Group__0__Impl11540 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheck__Group__1__Impl_in_rule__LTLCheck__Group__111571 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_rule__LTLCheck__Group__2_in_rule__LTLCheck__Group__111574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheck__LtsAssignment_1_in_rule__LTLCheck__Group__1__Impl11601 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheck__Group__2__Impl_in_rule__LTLCheck__Group__211631 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__LTLCheck__Group__3_in_rule__LTLCheck__Group__211634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_rule__LTLCheck__Group__2__Impl11662 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheck__Group__3__Impl_in_rule__LTLCheck__Group__311693 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__LTLCheck__FormulaDefinitionAssignment_3_in_rule__LTLCheck__Group__3__Impl11720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Document__LtssAlternatives_0_0_in_rule__Document__LtssAssignment_011763 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormulaSet_in_rule__Document__FormulaSetAssignment_111796 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckSet_in_rule__Document__CheckSetAssignment_211827 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormulaSet_in_rule__Document__LTLformulaSetAssignment_311858 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLCheckSet_in_rule__Document__LTLcheckSetAssignment_411889 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__LTS__NameAssignment_111920 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAtomicProposition_in_rule__LTS__AtomicPropositionsAssignment_3_211951 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAtomicProposition_in_rule__LTS__AtomicPropositionsAssignment_3_3_111982 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBasicState_in_rule__LTS__StatesAssignment_512013 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBasicState_in_rule__LTS__StatesAssignment_6_112044 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__LTS__InitStatesAssignment_812079 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__LTS__InitStatesAssignment_9_112118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEvent_in_rule__LTS__AlphabetAssignment_10_112153 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEvent_in_rule__LTS__AlphabetAssignment_10_2_112184 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTransition_in_rule__LTS__TransitionsAssignment_1112215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__AtomicProposition__NameAssignment12246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__BasicState__NameAssignment_012277 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__BasicState__AtomicPropositionsAssignment_1_112312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__BasicState__AtomicPropositionsAssignment_1_2_112351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Event__NameAssignment12386 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Transition__SourceAssignment_012421 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Transition__DestinationAssignment_212460 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Transition__EventAssignment_412499 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__ComposedLTS__LtsLeftAssignment_112538 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__ComposedLTS__LtsRightAssignment_312577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__ComposedLTS__NameAssignment_512612 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__ComposedLTS__EventsAssignment_6_212647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__ComposedLTS__EventsAssignment_6_3_112686 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__FormulaSet__NameAssignment_212721 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormulaDefinition_in_rule__FormulaSet__FormulaeAssignment_412752 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__FormulaDefinition__NameAssignment_012783 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormula_in_rule__FormulaDefinition__FormulaAssignment_212814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormula_in_rule__EX__NestedAssignment_112845 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormula_in_rule__EG__NestedAssignment_112876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormula_in_rule__EU__LeftAssignment_212907 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormula_in_rule__EU__RightAssignment_412938 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormula_in_rule__AND__LeftAssignment_112969 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormula_in_rule__AND__RightAssignment_313000 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormula_in_rule__OR__LeftAssignment_113031 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormula_in_rule__OR__RightAssignment_313062 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormula_in_rule__NOT__NestedAssignment_113093 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Proposition__AtomicPropositionAssignment13128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_rule__Constant__ValueAssignment_1_013168 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__CheckSet__NameAssignment_213207 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheck_in_rule__CheckSet__ChecksAssignment_413238 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Check__LtsAssignment_113273 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Check__FormulaDefinitionAssignment_313312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__LTLFormulaSet__NameAssignment_213347 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormulaDefinition_in_rule__LTLFormulaSet__FormulaeAssignment_413378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__LTLFormulaDefinition__NameAssignment_013409 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_rule__LTLFormulaDefinition__FormulaAssignment_213440 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_rule__LTLX__NestedAssignment_113471 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_rule__LTLG__NestedAssignment_113502 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_rule__LTLU__LeftAssignment_113533 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_rule__LTLU__RightAssignment_313564 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_rule__LTLF__NestedAssignment_113595 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_rule__LTLNOT__NestedAssignment_113626 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_rule__LTLAND__LeftAssignment_113657 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_rule__LTLAND__RightAssignment_313688 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_rule__LTLOR__LeftAssignment_113719 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_rule__LTLOR__RightAssignment_313750 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__LTLProposition__AtomicPropositionAssignment13785 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_rule__LTLConstant__ValueAssignment_1_013825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__LTLCheckSet__NameAssignment_213864 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLCheck_in_rule__LTLCheckSet__ChecksAssignment_413895 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__LTLCheck__LtsAssignment_113930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__LTLCheck__FormulaDefinitionAssignment_313969 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAND_in_synpred6_InternalXLTS2234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOR_in_synpred7_InternalXLTS2252 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLAND_in_synpred15_InternalXLTS2457 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLOR_in_synpred16_InternalXLTS2475 = new BitSet(new long[]{0x0000000000000002L});

}