package de.hannover.se.fmse.ltl.ui.popup.actions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import lts2ba.BaProductConstructor;
import lts2ba.LTS2BATransformer;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

import de.hannover.uni.se.fmse.buechi.Automaton;
import de.hannover.uni.se.fmse.ltl.Formula;
import de.hannover.uni.se.fmse.ltl.LtlFactory;
import de.hannover.uni.se.fmse.ltl2ba.LTL2BATransformer;
import de.hannover.uni.se.fmse.lts.AtomicProposition;
import de.hannover.uni.se.fmse.lts.DepthFirstSearchAlgorithm;
import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.State;
import de.hannover.uni.se.fmse.lts.generator.BuechiDotOutputGenerator;
import de.hannover.uni.se.fmse.lts.xLTS.LTLCheck;
import de.hannover.uni.se.fmse.lts.xLTS.LTLCheckSet;

public class LTLCheckAction implements IObjectActionDelegate {
	private String dir = "";
	private Shell shell;

	/**
	 * Constructor for Action1.
	 */
	public LTLCheckAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {

		Resource r = getSelectedResource();
		LTLCheckSet cs = (LTLCheckSet) r.getAllContents().next();
		for (LTLCheck check : cs.getChecks()) {
			LTS lts = EcoreUtil.copy(check.getLts());
			
			Automaton ba = new LTS2BATransformer().transformLTS(lts), formulaAutomaton, productAutomaton;
			de.hannover.uni.se.fmse.ltl.NOT not = LtlFactory.eINSTANCE
					.createNOT();
			Formula f = check.getFormulaDefinition().getFormula();
			not.setNested(f);
			formulaAutomaton = LTL2BATransformer.transformLTL(not);
			productAutomaton = new BaProductConstructor().construct(ba,
					formulaAutomaton);
			boolean result = true;
			DepthFirstSearchAlgorithm search;
			for (State initState : productAutomaton.getInitStates()) {
				search = new DepthFirstSearchAlgorithm(productAutomaton);
				if (search.search(initState)) {
					try {
						new FileWriter(new File(dir+"/counter_ex_"+check.getFormulaDefinition().getName()+".dot")).append(BuechiDotOutputGenerator.generateCounterexample(productAutomaton, search.getBlueTransitions(), search.getRedTransitions())).close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					result = false;
					break;
				}
			}
			// report that the formula is valid here
			if (result)
				MessageDialog.openInformation(shell, "Success",
						"Formula is valid.");
			else
				MessageDialog.openInformation(shell, "Fail",
						"Formula is not valid.");
			File baFile = new File(dir + "/"+lts.getName()+ ".dot"), formulaFile = new File(
					dir + "/"+lts.getName()+"_"+check.getFormulaDefinition().getName() + ".dot"), productFile = new File(
					dir + "/"+lts.getName()+"_"+check.getFormulaDefinition().getName() +"_product" +  ".dot");

			try {
				baFile.createNewFile();
				if(ba.getStates().size()>0)new FileWriter(baFile).append(
						BuechiDotOutputGenerator.generate(ba)).close();
				if(formulaAutomaton.getStates().size()>0)new FileWriter(formulaFile).append(
						BuechiDotOutputGenerator.generate(formulaAutomaton))
						.close();
 				if(productAutomaton.getTransitions().size()>0)new FileWriter(productFile).append(
						BuechiDotOutputGenerator.generate(productAutomaton))
						.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		MessageDialog.openInformation(shell, "LTLUI",
				"Check Formulae was executed.");
	}

	private Resource getSelectedResource() {
		IStructuredSelection structuredSelection = (IStructuredSelection) PlatformUI
				.getWorkbench().getActiveWorkbenchWindow()
				.getSelectionService().getSelection();
		IFile resourceFile = (IFile) structuredSelection.getFirstElement();
		dir = resourceFile.getRawLocation().toFile().getParent();
		ResourceSet resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.getResource(URI
				.createPlatformResourceURI(resourceFile.getFullPath()
						.toString(), true), true);
		EcoreUtil.resolveAll(resourceSet);
		return resource;
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

}
