/**
 */
package de.hannover.uni.se.fmse.lts.provider;


import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.LtsFactory;
import de.hannover.uni.se.fmse.lts.LtsPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link de.hannover.uni.se.fmse.lts.LTS} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class LTSItemProvider extends NamedElementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LTSItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addInitStatesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Init States feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInitStatesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LTS_initStates_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LTS_initStates_feature", "_UI_LTS_type"),
				 LtsPackage.Literals.LTS__INIT_STATES,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(LtsPackage.Literals.LTS__STATES);
			childrenFeatures.add(LtsPackage.Literals.LTS__ALPHABET);
			childrenFeatures.add(LtsPackage.Literals.LTS__TRANSITIONS);
			childrenFeatures.add(LtsPackage.Literals.LTS__ATOMIC_PROPOSITIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns LTS.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/LTS"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((LTS)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_LTS_type") :
			getString("_UI_LTS_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(LTS.class)) {
			case LtsPackage.LTS__STATES:
			case LtsPackage.LTS__ALPHABET:
			case LtsPackage.LTS__TRANSITIONS:
			case LtsPackage.LTS__ATOMIC_PROPOSITIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(LtsPackage.Literals.LTS__STATES,
				 LtsFactory.eINSTANCE.createBasicState()));

		newChildDescriptors.add
			(createChildParameter
				(LtsPackage.Literals.LTS__STATES,
				 LtsFactory.eINSTANCE.createComposedState()));

		newChildDescriptors.add
			(createChildParameter
				(LtsPackage.Literals.LTS__ALPHABET,
				 LtsFactory.eINSTANCE.createEvent()));

		newChildDescriptors.add
			(createChildParameter
				(LtsPackage.Literals.LTS__TRANSITIONS,
				 LtsFactory.eINSTANCE.createTransition()));

		newChildDescriptors.add
			(createChildParameter
				(LtsPackage.Literals.LTS__ATOMIC_PROPOSITIONS,
				 LtsFactory.eINSTANCE.createAtomicProposition()));
	}

}
