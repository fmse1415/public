/**
 */
package de.hannover.uni.se.fmse.lts.xLTS;

import de.hannover.uni.se.fmse.lts.LTS;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getLtss <em>Ltss</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getFormulaSet <em>Formula Set</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getCheckSet <em>Check Set</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getLTLformulaSet <em>LT Lformula Set</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getLTLcheckSet <em>LT Lcheck Set</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getDocument()
 * @model
 * @generated
 */
public interface Document extends EObject
{
  /**
   * Returns the value of the '<em><b>Ltss</b></em>' containment reference list.
   * The list contents are of type {@link de.hannover.uni.se.fmse.lts.LTS}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Ltss</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ltss</em>' containment reference list.
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getDocument_Ltss()
   * @model containment="true"
   * @generated
   */
  EList<LTS> getLtss();

  /**
   * Returns the value of the '<em><b>Formula Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Formula Set</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Formula Set</em>' containment reference.
   * @see #setFormulaSet(FormulaSet)
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getDocument_FormulaSet()
   * @model containment="true"
   * @generated
   */
  FormulaSet getFormulaSet();

  /**
   * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getFormulaSet <em>Formula Set</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Formula Set</em>' containment reference.
   * @see #getFormulaSet()
   * @generated
   */
  void setFormulaSet(FormulaSet value);

  /**
   * Returns the value of the '<em><b>Check Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Check Set</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Check Set</em>' containment reference.
   * @see #setCheckSet(CheckSet)
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getDocument_CheckSet()
   * @model containment="true"
   * @generated
   */
  CheckSet getCheckSet();

  /**
   * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getCheckSet <em>Check Set</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Check Set</em>' containment reference.
   * @see #getCheckSet()
   * @generated
   */
  void setCheckSet(CheckSet value);

  /**
   * Returns the value of the '<em><b>LT Lformula Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>LT Lformula Set</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>LT Lformula Set</em>' containment reference.
   * @see #setLTLformulaSet(LTLFormulaSet)
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getDocument_LTLformulaSet()
   * @model containment="true"
   * @generated
   */
  LTLFormulaSet getLTLformulaSet();

  /**
   * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getLTLformulaSet <em>LT Lformula Set</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>LT Lformula Set</em>' containment reference.
   * @see #getLTLformulaSet()
   * @generated
   */
  void setLTLformulaSet(LTLFormulaSet value);

  /**
   * Returns the value of the '<em><b>LT Lcheck Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>LT Lcheck Set</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>LT Lcheck Set</em>' containment reference.
   * @see #setLTLcheckSet(LTLCheckSet)
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getDocument_LTLcheckSet()
   * @model containment="true"
   * @generated
   */
  LTLCheckSet getLTLcheckSet();

  /**
   * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getLTLcheckSet <em>LT Lcheck Set</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>LT Lcheck Set</em>' containment reference.
   * @see #getLTLcheckSet()
   * @generated
   */
  void setLTLcheckSet(LTLCheckSet value);

} // Document
