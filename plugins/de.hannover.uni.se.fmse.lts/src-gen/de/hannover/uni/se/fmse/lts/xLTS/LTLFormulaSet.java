/**
 */
package de.hannover.uni.se.fmse.lts.xLTS;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LTL Formula Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet#getName <em>Name</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet#getFormulae <em>Formulae</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getLTLFormulaSet()
 * @model
 * @generated
 */
public interface LTLFormulaSet extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getLTLFormulaSet_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Formulae</b></em>' containment reference list.
   * The list contents are of type {@link de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaDefinition}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Formulae</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Formulae</em>' containment reference list.
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getLTLFormulaSet_Formulae()
   * @model containment="true"
   * @generated
   */
  EList<LTLFormulaDefinition> getFormulae();

} // LTLFormulaSet
