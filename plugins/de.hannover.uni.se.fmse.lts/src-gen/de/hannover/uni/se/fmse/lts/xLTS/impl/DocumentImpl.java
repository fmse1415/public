/**
 */
package de.hannover.uni.se.fmse.lts.xLTS.impl;

import de.hannover.uni.se.fmse.lts.LTS;

import de.hannover.uni.se.fmse.lts.xLTS.CheckSet;
import de.hannover.uni.se.fmse.lts.xLTS.Document;
import de.hannover.uni.se.fmse.lts.xLTS.FormulaSet;
import de.hannover.uni.se.fmse.lts.xLTS.LTLCheckSet;
import de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet;
import de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.impl.DocumentImpl#getLtss <em>Ltss</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.impl.DocumentImpl#getFormulaSet <em>Formula Set</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.impl.DocumentImpl#getCheckSet <em>Check Set</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.impl.DocumentImpl#getLTLformulaSet <em>LT Lformula Set</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.impl.DocumentImpl#getLTLcheckSet <em>LT Lcheck Set</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DocumentImpl extends MinimalEObjectImpl.Container implements Document
{
  /**
   * The cached value of the '{@link #getLtss() <em>Ltss</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLtss()
   * @generated
   * @ordered
   */
  protected EList<LTS> ltss;

  /**
   * The cached value of the '{@link #getFormulaSet() <em>Formula Set</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFormulaSet()
   * @generated
   * @ordered
   */
  protected FormulaSet formulaSet;

  /**
   * The cached value of the '{@link #getCheckSet() <em>Check Set</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCheckSet()
   * @generated
   * @ordered
   */
  protected CheckSet checkSet;

  /**
   * The cached value of the '{@link #getLTLformulaSet() <em>LT Lformula Set</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLTLformulaSet()
   * @generated
   * @ordered
   */
  protected LTLFormulaSet ltLformulaSet;

  /**
   * The cached value of the '{@link #getLTLcheckSet() <em>LT Lcheck Set</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLTLcheckSet()
   * @generated
   * @ordered
   */
  protected LTLCheckSet ltLcheckSet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DocumentImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XLTSPackage.Literals.DOCUMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<LTS> getLtss()
  {
    if (ltss == null)
    {
      ltss = new EObjectContainmentEList<LTS>(LTS.class, this, XLTSPackage.DOCUMENT__LTSS);
    }
    return ltss;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FormulaSet getFormulaSet()
  {
    return formulaSet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFormulaSet(FormulaSet newFormulaSet, NotificationChain msgs)
  {
    FormulaSet oldFormulaSet = formulaSet;
    formulaSet = newFormulaSet;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XLTSPackage.DOCUMENT__FORMULA_SET, oldFormulaSet, newFormulaSet);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFormulaSet(FormulaSet newFormulaSet)
  {
    if (newFormulaSet != formulaSet)
    {
      NotificationChain msgs = null;
      if (formulaSet != null)
        msgs = ((InternalEObject)formulaSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XLTSPackage.DOCUMENT__FORMULA_SET, null, msgs);
      if (newFormulaSet != null)
        msgs = ((InternalEObject)newFormulaSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XLTSPackage.DOCUMENT__FORMULA_SET, null, msgs);
      msgs = basicSetFormulaSet(newFormulaSet, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XLTSPackage.DOCUMENT__FORMULA_SET, newFormulaSet, newFormulaSet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CheckSet getCheckSet()
  {
    return checkSet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetCheckSet(CheckSet newCheckSet, NotificationChain msgs)
  {
    CheckSet oldCheckSet = checkSet;
    checkSet = newCheckSet;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XLTSPackage.DOCUMENT__CHECK_SET, oldCheckSet, newCheckSet);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCheckSet(CheckSet newCheckSet)
  {
    if (newCheckSet != checkSet)
    {
      NotificationChain msgs = null;
      if (checkSet != null)
        msgs = ((InternalEObject)checkSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XLTSPackage.DOCUMENT__CHECK_SET, null, msgs);
      if (newCheckSet != null)
        msgs = ((InternalEObject)newCheckSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XLTSPackage.DOCUMENT__CHECK_SET, null, msgs);
      msgs = basicSetCheckSet(newCheckSet, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XLTSPackage.DOCUMENT__CHECK_SET, newCheckSet, newCheckSet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLFormulaSet getLTLformulaSet()
  {
    return ltLformulaSet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLTLformulaSet(LTLFormulaSet newLTLformulaSet, NotificationChain msgs)
  {
    LTLFormulaSet oldLTLformulaSet = ltLformulaSet;
    ltLformulaSet = newLTLformulaSet;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XLTSPackage.DOCUMENT__LT_LFORMULA_SET, oldLTLformulaSet, newLTLformulaSet);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLTLformulaSet(LTLFormulaSet newLTLformulaSet)
  {
    if (newLTLformulaSet != ltLformulaSet)
    {
      NotificationChain msgs = null;
      if (ltLformulaSet != null)
        msgs = ((InternalEObject)ltLformulaSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XLTSPackage.DOCUMENT__LT_LFORMULA_SET, null, msgs);
      if (newLTLformulaSet != null)
        msgs = ((InternalEObject)newLTLformulaSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XLTSPackage.DOCUMENT__LT_LFORMULA_SET, null, msgs);
      msgs = basicSetLTLformulaSet(newLTLformulaSet, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XLTSPackage.DOCUMENT__LT_LFORMULA_SET, newLTLformulaSet, newLTLformulaSet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLCheckSet getLTLcheckSet()
  {
    return ltLcheckSet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLTLcheckSet(LTLCheckSet newLTLcheckSet, NotificationChain msgs)
  {
    LTLCheckSet oldLTLcheckSet = ltLcheckSet;
    ltLcheckSet = newLTLcheckSet;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XLTSPackage.DOCUMENT__LT_LCHECK_SET, oldLTLcheckSet, newLTLcheckSet);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLTLcheckSet(LTLCheckSet newLTLcheckSet)
  {
    if (newLTLcheckSet != ltLcheckSet)
    {
      NotificationChain msgs = null;
      if (ltLcheckSet != null)
        msgs = ((InternalEObject)ltLcheckSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XLTSPackage.DOCUMENT__LT_LCHECK_SET, null, msgs);
      if (newLTLcheckSet != null)
        msgs = ((InternalEObject)newLTLcheckSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XLTSPackage.DOCUMENT__LT_LCHECK_SET, null, msgs);
      msgs = basicSetLTLcheckSet(newLTLcheckSet, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XLTSPackage.DOCUMENT__LT_LCHECK_SET, newLTLcheckSet, newLTLcheckSet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case XLTSPackage.DOCUMENT__LTSS:
        return ((InternalEList<?>)getLtss()).basicRemove(otherEnd, msgs);
      case XLTSPackage.DOCUMENT__FORMULA_SET:
        return basicSetFormulaSet(null, msgs);
      case XLTSPackage.DOCUMENT__CHECK_SET:
        return basicSetCheckSet(null, msgs);
      case XLTSPackage.DOCUMENT__LT_LFORMULA_SET:
        return basicSetLTLformulaSet(null, msgs);
      case XLTSPackage.DOCUMENT__LT_LCHECK_SET:
        return basicSetLTLcheckSet(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case XLTSPackage.DOCUMENT__LTSS:
        return getLtss();
      case XLTSPackage.DOCUMENT__FORMULA_SET:
        return getFormulaSet();
      case XLTSPackage.DOCUMENT__CHECK_SET:
        return getCheckSet();
      case XLTSPackage.DOCUMENT__LT_LFORMULA_SET:
        return getLTLformulaSet();
      case XLTSPackage.DOCUMENT__LT_LCHECK_SET:
        return getLTLcheckSet();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case XLTSPackage.DOCUMENT__LTSS:
        getLtss().clear();
        getLtss().addAll((Collection<? extends LTS>)newValue);
        return;
      case XLTSPackage.DOCUMENT__FORMULA_SET:
        setFormulaSet((FormulaSet)newValue);
        return;
      case XLTSPackage.DOCUMENT__CHECK_SET:
        setCheckSet((CheckSet)newValue);
        return;
      case XLTSPackage.DOCUMENT__LT_LFORMULA_SET:
        setLTLformulaSet((LTLFormulaSet)newValue);
        return;
      case XLTSPackage.DOCUMENT__LT_LCHECK_SET:
        setLTLcheckSet((LTLCheckSet)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case XLTSPackage.DOCUMENT__LTSS:
        getLtss().clear();
        return;
      case XLTSPackage.DOCUMENT__FORMULA_SET:
        setFormulaSet((FormulaSet)null);
        return;
      case XLTSPackage.DOCUMENT__CHECK_SET:
        setCheckSet((CheckSet)null);
        return;
      case XLTSPackage.DOCUMENT__LT_LFORMULA_SET:
        setLTLformulaSet((LTLFormulaSet)null);
        return;
      case XLTSPackage.DOCUMENT__LT_LCHECK_SET:
        setLTLcheckSet((LTLCheckSet)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case XLTSPackage.DOCUMENT__LTSS:
        return ltss != null && !ltss.isEmpty();
      case XLTSPackage.DOCUMENT__FORMULA_SET:
        return formulaSet != null;
      case XLTSPackage.DOCUMENT__CHECK_SET:
        return checkSet != null;
      case XLTSPackage.DOCUMENT__LT_LFORMULA_SET:
        return ltLformulaSet != null;
      case XLTSPackage.DOCUMENT__LT_LCHECK_SET:
        return ltLcheckSet != null;
    }
    return super.eIsSet(featureID);
  }

} //DocumentImpl
