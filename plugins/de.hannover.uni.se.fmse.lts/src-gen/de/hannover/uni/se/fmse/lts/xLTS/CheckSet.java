/**
 */
package de.hannover.uni.se.fmse.lts.xLTS;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Check Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.CheckSet#getName <em>Name</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.CheckSet#getChecks <em>Checks</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getCheckSet()
 * @model
 * @generated
 */
public interface CheckSet extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getCheckSet_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.xLTS.CheckSet#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Checks</b></em>' containment reference list.
   * The list contents are of type {@link de.hannover.uni.se.fmse.lts.xLTS.Check}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Checks</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Checks</em>' containment reference list.
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getCheckSet_Checks()
   * @model containment="true"
   * @generated
   */
  EList<Check> getChecks();

} // CheckSet
