/**
 */
package de.hannover.uni.se.fmse.lts.xLTS;

import de.hannover.uni.se.fmse.ctl.FormulaDefinition;

import de.hannover.uni.se.fmse.lts.LTS;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Check</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.Check#getLts <em>Lts</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.Check#getFormulaDefinition <em>Formula Definition</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getCheck()
 * @model
 * @generated
 */
public interface Check extends EObject
{
  /**
   * Returns the value of the '<em><b>Lts</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Lts</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Lts</em>' reference.
   * @see #setLts(LTS)
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getCheck_Lts()
   * @model
   * @generated
   */
  LTS getLts();

  /**
   * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.xLTS.Check#getLts <em>Lts</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Lts</em>' reference.
   * @see #getLts()
   * @generated
   */
  void setLts(LTS value);

  /**
   * Returns the value of the '<em><b>Formula Definition</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Formula Definition</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Formula Definition</em>' reference.
   * @see #setFormulaDefinition(FormulaDefinition)
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getCheck_FormulaDefinition()
   * @model
   * @generated
   */
  FormulaDefinition getFormulaDefinition();

  /**
   * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.xLTS.Check#getFormulaDefinition <em>Formula Definition</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Formula Definition</em>' reference.
   * @see #getFormulaDefinition()
   * @generated
   */
  void setFormulaDefinition(FormulaDefinition value);

} // Check
