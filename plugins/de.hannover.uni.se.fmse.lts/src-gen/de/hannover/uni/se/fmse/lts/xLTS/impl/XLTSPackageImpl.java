/**
 */
package de.hannover.uni.se.fmse.lts.xLTS.impl;

import de.hannover.uni.se.fmse.ctl.CtlPackage;

import de.hannover.uni.se.fmse.ltl.LtlPackage;

import de.hannover.uni.se.fmse.lts.LtsPackage;

import de.hannover.uni.se.fmse.lts.xLTS.Check;
import de.hannover.uni.se.fmse.lts.xLTS.CheckSet;
import de.hannover.uni.se.fmse.lts.xLTS.Document;
import de.hannover.uni.se.fmse.lts.xLTS.FormulaSet;
import de.hannover.uni.se.fmse.lts.xLTS.LTLCheck;
import de.hannover.uni.se.fmse.lts.xLTS.LTLCheckSet;
import de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaDefinition;
import de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet;
import de.hannover.uni.se.fmse.lts.xLTS.XLTSFactory;
import de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class XLTSPackageImpl extends EPackageImpl implements XLTSPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass documentEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass formulaSetEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass checkSetEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass checkEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlFormulaSetEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlFormulaDefinitionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlCheckSetEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass ltlCheckEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private XLTSPackageImpl()
  {
    super(eNS_URI, XLTSFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link XLTSPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static XLTSPackage init()
  {
    if (isInited) return (XLTSPackage)EPackage.Registry.INSTANCE.getEPackage(XLTSPackage.eNS_URI);

    // Obtain or create and register package
    XLTSPackageImpl theXLTSPackage = (XLTSPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof XLTSPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new XLTSPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    CtlPackage.eINSTANCE.eClass();
    LtlPackage.eINSTANCE.eClass();

    // Create package meta-data objects
    theXLTSPackage.createPackageContents();

    // Initialize created meta-data
    theXLTSPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theXLTSPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(XLTSPackage.eNS_URI, theXLTSPackage);
    return theXLTSPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDocument()
  {
    return documentEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocument_Ltss()
  {
    return (EReference)documentEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocument_FormulaSet()
  {
    return (EReference)documentEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocument_CheckSet()
  {
    return (EReference)documentEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocument_LTLformulaSet()
  {
    return (EReference)documentEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocument_LTLcheckSet()
  {
    return (EReference)documentEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getFormulaSet()
  {
    return formulaSetEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getFormulaSet_Name()
  {
    return (EAttribute)formulaSetEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getFormulaSet_Formulae()
  {
    return (EReference)formulaSetEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCheckSet()
  {
    return checkSetEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCheckSet_Name()
  {
    return (EAttribute)checkSetEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCheckSet_Checks()
  {
    return (EReference)checkSetEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCheck()
  {
    return checkEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCheck_Lts()
  {
    return (EReference)checkEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCheck_FormulaDefinition()
  {
    return (EReference)checkEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLFormulaSet()
  {
    return ltlFormulaSetEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLTLFormulaSet_Name()
  {
    return (EAttribute)ltlFormulaSetEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLFormulaSet_Formulae()
  {
    return (EReference)ltlFormulaSetEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLFormulaDefinition()
  {
    return ltlFormulaDefinitionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLTLFormulaDefinition_Name()
  {
    return (EAttribute)ltlFormulaDefinitionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLFormulaDefinition_Formula()
  {
    return (EReference)ltlFormulaDefinitionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLCheckSet()
  {
    return ltlCheckSetEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLTLCheckSet_Name()
  {
    return (EAttribute)ltlCheckSetEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLCheckSet_Checks()
  {
    return (EReference)ltlCheckSetEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLTLCheck()
  {
    return ltlCheckEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLCheck_Lts()
  {
    return (EReference)ltlCheckEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLTLCheck_FormulaDefinition()
  {
    return (EReference)ltlCheckEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public XLTSFactory getXLTSFactory()
  {
    return (XLTSFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    documentEClass = createEClass(DOCUMENT);
    createEReference(documentEClass, DOCUMENT__LTSS);
    createEReference(documentEClass, DOCUMENT__FORMULA_SET);
    createEReference(documentEClass, DOCUMENT__CHECK_SET);
    createEReference(documentEClass, DOCUMENT__LT_LFORMULA_SET);
    createEReference(documentEClass, DOCUMENT__LT_LCHECK_SET);

    formulaSetEClass = createEClass(FORMULA_SET);
    createEAttribute(formulaSetEClass, FORMULA_SET__NAME);
    createEReference(formulaSetEClass, FORMULA_SET__FORMULAE);

    checkSetEClass = createEClass(CHECK_SET);
    createEAttribute(checkSetEClass, CHECK_SET__NAME);
    createEReference(checkSetEClass, CHECK_SET__CHECKS);

    checkEClass = createEClass(CHECK);
    createEReference(checkEClass, CHECK__LTS);
    createEReference(checkEClass, CHECK__FORMULA_DEFINITION);

    ltlFormulaSetEClass = createEClass(LTL_FORMULA_SET);
    createEAttribute(ltlFormulaSetEClass, LTL_FORMULA_SET__NAME);
    createEReference(ltlFormulaSetEClass, LTL_FORMULA_SET__FORMULAE);

    ltlFormulaDefinitionEClass = createEClass(LTL_FORMULA_DEFINITION);
    createEAttribute(ltlFormulaDefinitionEClass, LTL_FORMULA_DEFINITION__NAME);
    createEReference(ltlFormulaDefinitionEClass, LTL_FORMULA_DEFINITION__FORMULA);

    ltlCheckSetEClass = createEClass(LTL_CHECK_SET);
    createEAttribute(ltlCheckSetEClass, LTL_CHECK_SET__NAME);
    createEReference(ltlCheckSetEClass, LTL_CHECK_SET__CHECKS);

    ltlCheckEClass = createEClass(LTL_CHECK);
    createEReference(ltlCheckEClass, LTL_CHECK__LTS);
    createEReference(ltlCheckEClass, LTL_CHECK__FORMULA_DEFINITION);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    LtsPackage theLtsPackage = (LtsPackage)EPackage.Registry.INSTANCE.getEPackage(LtsPackage.eNS_URI);
    CtlPackage theCtlPackage = (CtlPackage)EPackage.Registry.INSTANCE.getEPackage(CtlPackage.eNS_URI);
    LtlPackage theLtlPackage = (LtlPackage)EPackage.Registry.INSTANCE.getEPackage(LtlPackage.eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes

    // Initialize classes and features; add operations and parameters
    initEClass(documentEClass, Document.class, "Document", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getDocument_Ltss(), theLtsPackage.getLTS(), null, "ltss", null, 0, -1, Document.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocument_FormulaSet(), this.getFormulaSet(), null, "formulaSet", null, 0, 1, Document.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocument_CheckSet(), this.getCheckSet(), null, "checkSet", null, 0, 1, Document.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocument_LTLformulaSet(), this.getLTLFormulaSet(), null, "LTLformulaSet", null, 0, 1, Document.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocument_LTLcheckSet(), this.getLTLCheckSet(), null, "LTLcheckSet", null, 0, 1, Document.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(formulaSetEClass, FormulaSet.class, "FormulaSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getFormulaSet_Name(), ecorePackage.getEString(), "name", null, 0, 1, FormulaSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getFormulaSet_Formulae(), theCtlPackage.getFormulaDefinition(), null, "formulae", null, 0, -1, FormulaSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(checkSetEClass, CheckSet.class, "CheckSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCheckSet_Name(), ecorePackage.getEString(), "name", null, 0, 1, CheckSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCheckSet_Checks(), this.getCheck(), null, "checks", null, 0, -1, CheckSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(checkEClass, Check.class, "Check", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getCheck_Lts(), theLtsPackage.getLTS(), null, "lts", null, 0, 1, Check.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCheck_FormulaDefinition(), theCtlPackage.getFormulaDefinition(), null, "formulaDefinition", null, 0, 1, Check.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlFormulaSetEClass, LTLFormulaSet.class, "LTLFormulaSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getLTLFormulaSet_Name(), ecorePackage.getEString(), "name", null, 0, 1, LTLFormulaSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLTLFormulaSet_Formulae(), this.getLTLFormulaDefinition(), null, "formulae", null, 0, -1, LTLFormulaSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlFormulaDefinitionEClass, LTLFormulaDefinition.class, "LTLFormulaDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getLTLFormulaDefinition_Name(), ecorePackage.getEString(), "name", null, 0, 1, LTLFormulaDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLTLFormulaDefinition_Formula(), theLtlPackage.getFormula(), null, "formula", null, 0, 1, LTLFormulaDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlCheckSetEClass, LTLCheckSet.class, "LTLCheckSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getLTLCheckSet_Name(), ecorePackage.getEString(), "name", null, 0, 1, LTLCheckSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLTLCheckSet_Checks(), this.getLTLCheck(), null, "checks", null, 0, -1, LTLCheckSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(ltlCheckEClass, LTLCheck.class, "LTLCheck", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getLTLCheck_Lts(), theLtsPackage.getLTS(), null, "lts", null, 0, 1, LTLCheck.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLTLCheck_FormulaDefinition(), this.getLTLFormulaDefinition(), null, "formulaDefinition", null, 0, 1, LTLCheck.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);
  }

} //XLTSPackageImpl
