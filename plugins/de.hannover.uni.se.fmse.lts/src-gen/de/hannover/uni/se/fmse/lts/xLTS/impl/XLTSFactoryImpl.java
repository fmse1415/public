/**
 */
package de.hannover.uni.se.fmse.lts.xLTS.impl;

import de.hannover.uni.se.fmse.lts.xLTS.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class XLTSFactoryImpl extends EFactoryImpl implements XLTSFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static XLTSFactory init()
  {
    try
    {
      XLTSFactory theXLTSFactory = (XLTSFactory)EPackage.Registry.INSTANCE.getEFactory(XLTSPackage.eNS_URI);
      if (theXLTSFactory != null)
      {
        return theXLTSFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new XLTSFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public XLTSFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case XLTSPackage.DOCUMENT: return createDocument();
      case XLTSPackage.FORMULA_SET: return createFormulaSet();
      case XLTSPackage.CHECK_SET: return createCheckSet();
      case XLTSPackage.CHECK: return createCheck();
      case XLTSPackage.LTL_FORMULA_SET: return createLTLFormulaSet();
      case XLTSPackage.LTL_FORMULA_DEFINITION: return createLTLFormulaDefinition();
      case XLTSPackage.LTL_CHECK_SET: return createLTLCheckSet();
      case XLTSPackage.LTL_CHECK: return createLTLCheck();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Document createDocument()
  {
    DocumentImpl document = new DocumentImpl();
    return document;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FormulaSet createFormulaSet()
  {
    FormulaSetImpl formulaSet = new FormulaSetImpl();
    return formulaSet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CheckSet createCheckSet()
  {
    CheckSetImpl checkSet = new CheckSetImpl();
    return checkSet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Check createCheck()
  {
    CheckImpl check = new CheckImpl();
    return check;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLFormulaSet createLTLFormulaSet()
  {
    LTLFormulaSetImpl ltlFormulaSet = new LTLFormulaSetImpl();
    return ltlFormulaSet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLFormulaDefinition createLTLFormulaDefinition()
  {
    LTLFormulaDefinitionImpl ltlFormulaDefinition = new LTLFormulaDefinitionImpl();
    return ltlFormulaDefinition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLCheckSet createLTLCheckSet()
  {
    LTLCheckSetImpl ltlCheckSet = new LTLCheckSetImpl();
    return ltlCheckSet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLCheck createLTLCheck()
  {
    LTLCheckImpl ltlCheck = new LTLCheckImpl();
    return ltlCheck;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public XLTSPackage getXLTSPackage()
  {
    return (XLTSPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static XLTSPackage getPackage()
  {
    return XLTSPackage.eINSTANCE;
  }

} //XLTSFactoryImpl
