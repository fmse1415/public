/**
 */
package de.hannover.uni.se.fmse.lts.xLTS.impl;

import de.hannover.uni.se.fmse.lts.LTS;

import de.hannover.uni.se.fmse.lts.xLTS.LTLCheck;
import de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaDefinition;
import de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LTL Check</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.impl.LTLCheckImpl#getLts <em>Lts</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.impl.LTLCheckImpl#getFormulaDefinition <em>Formula Definition</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LTLCheckImpl extends MinimalEObjectImpl.Container implements LTLCheck
{
  /**
   * The cached value of the '{@link #getLts() <em>Lts</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLts()
   * @generated
   * @ordered
   */
  protected LTS lts;

  /**
   * The cached value of the '{@link #getFormulaDefinition() <em>Formula Definition</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFormulaDefinition()
   * @generated
   * @ordered
   */
  protected LTLFormulaDefinition formulaDefinition;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LTLCheckImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XLTSPackage.Literals.LTL_CHECK;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTS getLts()
  {
    if (lts != null && lts.eIsProxy())
    {
      InternalEObject oldLts = (InternalEObject)lts;
      lts = (LTS)eResolveProxy(oldLts);
      if (lts != oldLts)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, XLTSPackage.LTL_CHECK__LTS, oldLts, lts));
      }
    }
    return lts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTS basicGetLts()
  {
    return lts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLts(LTS newLts)
  {
    LTS oldLts = lts;
    lts = newLts;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XLTSPackage.LTL_CHECK__LTS, oldLts, lts));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLFormulaDefinition getFormulaDefinition()
  {
    if (formulaDefinition != null && formulaDefinition.eIsProxy())
    {
      InternalEObject oldFormulaDefinition = (InternalEObject)formulaDefinition;
      formulaDefinition = (LTLFormulaDefinition)eResolveProxy(oldFormulaDefinition);
      if (formulaDefinition != oldFormulaDefinition)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, XLTSPackage.LTL_CHECK__FORMULA_DEFINITION, oldFormulaDefinition, formulaDefinition));
      }
    }
    return formulaDefinition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LTLFormulaDefinition basicGetFormulaDefinition()
  {
    return formulaDefinition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFormulaDefinition(LTLFormulaDefinition newFormulaDefinition)
  {
    LTLFormulaDefinition oldFormulaDefinition = formulaDefinition;
    formulaDefinition = newFormulaDefinition;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XLTSPackage.LTL_CHECK__FORMULA_DEFINITION, oldFormulaDefinition, formulaDefinition));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case XLTSPackage.LTL_CHECK__LTS:
        if (resolve) return getLts();
        return basicGetLts();
      case XLTSPackage.LTL_CHECK__FORMULA_DEFINITION:
        if (resolve) return getFormulaDefinition();
        return basicGetFormulaDefinition();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case XLTSPackage.LTL_CHECK__LTS:
        setLts((LTS)newValue);
        return;
      case XLTSPackage.LTL_CHECK__FORMULA_DEFINITION:
        setFormulaDefinition((LTLFormulaDefinition)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case XLTSPackage.LTL_CHECK__LTS:
        setLts((LTS)null);
        return;
      case XLTSPackage.LTL_CHECK__FORMULA_DEFINITION:
        setFormulaDefinition((LTLFormulaDefinition)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case XLTSPackage.LTL_CHECK__LTS:
        return lts != null;
      case XLTSPackage.LTL_CHECK__FORMULA_DEFINITION:
        return formulaDefinition != null;
    }
    return super.eIsSet(featureID);
  }

} //LTLCheckImpl
