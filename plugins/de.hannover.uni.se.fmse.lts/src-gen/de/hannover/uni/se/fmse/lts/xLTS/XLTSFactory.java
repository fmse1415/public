/**
 */
package de.hannover.uni.se.fmse.lts.xLTS;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage
 * @generated
 */
public interface XLTSFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  XLTSFactory eINSTANCE = de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Document</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Document</em>'.
   * @generated
   */
  Document createDocument();

  /**
   * Returns a new object of class '<em>Formula Set</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Formula Set</em>'.
   * @generated
   */
  FormulaSet createFormulaSet();

  /**
   * Returns a new object of class '<em>Check Set</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Check Set</em>'.
   * @generated
   */
  CheckSet createCheckSet();

  /**
   * Returns a new object of class '<em>Check</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Check</em>'.
   * @generated
   */
  Check createCheck();

  /**
   * Returns a new object of class '<em>LTL Formula Set</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Formula Set</em>'.
   * @generated
   */
  LTLFormulaSet createLTLFormulaSet();

  /**
   * Returns a new object of class '<em>LTL Formula Definition</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Formula Definition</em>'.
   * @generated
   */
  LTLFormulaDefinition createLTLFormulaDefinition();

  /**
   * Returns a new object of class '<em>LTL Check Set</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Check Set</em>'.
   * @generated
   */
  LTLCheckSet createLTLCheckSet();

  /**
   * Returns a new object of class '<em>LTL Check</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>LTL Check</em>'.
   * @generated
   */
  LTLCheck createLTLCheck();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  XLTSPackage getXLTSPackage();

} //XLTSFactory
