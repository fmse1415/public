/**
 */
package de.hannover.uni.se.fmse.lts.xLTS;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSFactory
 * @model kind="package"
 * @generated
 */
public interface XLTSPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "xLTS";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.hannover.de/uni/se/fmse/lts/XLTS";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "xLTS";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  XLTSPackage eINSTANCE = de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl.init();

  /**
   * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.DocumentImpl <em>Document</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.DocumentImpl
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getDocument()
   * @generated
   */
  int DOCUMENT = 0;

  /**
   * The feature id for the '<em><b>Ltss</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__LTSS = 0;

  /**
   * The feature id for the '<em><b>Formula Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__FORMULA_SET = 1;

  /**
   * The feature id for the '<em><b>Check Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__CHECK_SET = 2;

  /**
   * The feature id for the '<em><b>LT Lformula Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__LT_LFORMULA_SET = 3;

  /**
   * The feature id for the '<em><b>LT Lcheck Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT__LT_LCHECK_SET = 4;

  /**
   * The number of structural features of the '<em>Document</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_FEATURE_COUNT = 5;

  /**
   * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.FormulaSetImpl <em>Formula Set</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.FormulaSetImpl
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getFormulaSet()
   * @generated
   */
  int FORMULA_SET = 1;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMULA_SET__NAME = 0;

  /**
   * The feature id for the '<em><b>Formulae</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMULA_SET__FORMULAE = 1;

  /**
   * The number of structural features of the '<em>Formula Set</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMULA_SET_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.CheckSetImpl <em>Check Set</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.CheckSetImpl
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getCheckSet()
   * @generated
   */
  int CHECK_SET = 2;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_SET__NAME = 0;

  /**
   * The feature id for the '<em><b>Checks</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_SET__CHECKS = 1;

  /**
   * The number of structural features of the '<em>Check Set</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_SET_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.CheckImpl <em>Check</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.CheckImpl
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getCheck()
   * @generated
   */
  int CHECK = 3;

  /**
   * The feature id for the '<em><b>Lts</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK__LTS = 0;

  /**
   * The feature id for the '<em><b>Formula Definition</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK__FORMULA_DEFINITION = 1;

  /**
   * The number of structural features of the '<em>Check</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.LTLFormulaSetImpl <em>LTL Formula Set</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.LTLFormulaSetImpl
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getLTLFormulaSet()
   * @generated
   */
  int LTL_FORMULA_SET = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_FORMULA_SET__NAME = 0;

  /**
   * The feature id for the '<em><b>Formulae</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_FORMULA_SET__FORMULAE = 1;

  /**
   * The number of structural features of the '<em>LTL Formula Set</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_FORMULA_SET_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.LTLFormulaDefinitionImpl <em>LTL Formula Definition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.LTLFormulaDefinitionImpl
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getLTLFormulaDefinition()
   * @generated
   */
  int LTL_FORMULA_DEFINITION = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_FORMULA_DEFINITION__NAME = 0;

  /**
   * The feature id for the '<em><b>Formula</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_FORMULA_DEFINITION__FORMULA = 1;

  /**
   * The number of structural features of the '<em>LTL Formula Definition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_FORMULA_DEFINITION_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.LTLCheckSetImpl <em>LTL Check Set</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.LTLCheckSetImpl
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getLTLCheckSet()
   * @generated
   */
  int LTL_CHECK_SET = 6;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_CHECK_SET__NAME = 0;

  /**
   * The feature id for the '<em><b>Checks</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_CHECK_SET__CHECKS = 1;

  /**
   * The number of structural features of the '<em>LTL Check Set</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_CHECK_SET_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.LTLCheckImpl <em>LTL Check</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.LTLCheckImpl
   * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getLTLCheck()
   * @generated
   */
  int LTL_CHECK = 7;

  /**
   * The feature id for the '<em><b>Lts</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_CHECK__LTS = 0;

  /**
   * The feature id for the '<em><b>Formula Definition</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_CHECK__FORMULA_DEFINITION = 1;

  /**
   * The number of structural features of the '<em>LTL Check</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LTL_CHECK_FEATURE_COUNT = 2;


  /**
   * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.xLTS.Document <em>Document</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Document</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.Document
   * @generated
   */
  EClass getDocument();

  /**
   * Returns the meta object for the containment reference list '{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getLtss <em>Ltss</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Ltss</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.Document#getLtss()
   * @see #getDocument()
   * @generated
   */
  EReference getDocument_Ltss();

  /**
   * Returns the meta object for the containment reference '{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getFormulaSet <em>Formula Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Formula Set</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.Document#getFormulaSet()
   * @see #getDocument()
   * @generated
   */
  EReference getDocument_FormulaSet();

  /**
   * Returns the meta object for the containment reference '{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getCheckSet <em>Check Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Check Set</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.Document#getCheckSet()
   * @see #getDocument()
   * @generated
   */
  EReference getDocument_CheckSet();

  /**
   * Returns the meta object for the containment reference '{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getLTLformulaSet <em>LT Lformula Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>LT Lformula Set</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.Document#getLTLformulaSet()
   * @see #getDocument()
   * @generated
   */
  EReference getDocument_LTLformulaSet();

  /**
   * Returns the meta object for the containment reference '{@link de.hannover.uni.se.fmse.lts.xLTS.Document#getLTLcheckSet <em>LT Lcheck Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>LT Lcheck Set</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.Document#getLTLcheckSet()
   * @see #getDocument()
   * @generated
   */
  EReference getDocument_LTLcheckSet();

  /**
   * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.xLTS.FormulaSet <em>Formula Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Formula Set</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.FormulaSet
   * @generated
   */
  EClass getFormulaSet();

  /**
   * Returns the meta object for the attribute '{@link de.hannover.uni.se.fmse.lts.xLTS.FormulaSet#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.FormulaSet#getName()
   * @see #getFormulaSet()
   * @generated
   */
  EAttribute getFormulaSet_Name();

  /**
   * Returns the meta object for the containment reference list '{@link de.hannover.uni.se.fmse.lts.xLTS.FormulaSet#getFormulae <em>Formulae</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Formulae</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.FormulaSet#getFormulae()
   * @see #getFormulaSet()
   * @generated
   */
  EReference getFormulaSet_Formulae();

  /**
   * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.xLTS.CheckSet <em>Check Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Check Set</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.CheckSet
   * @generated
   */
  EClass getCheckSet();

  /**
   * Returns the meta object for the attribute '{@link de.hannover.uni.se.fmse.lts.xLTS.CheckSet#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.CheckSet#getName()
   * @see #getCheckSet()
   * @generated
   */
  EAttribute getCheckSet_Name();

  /**
   * Returns the meta object for the containment reference list '{@link de.hannover.uni.se.fmse.lts.xLTS.CheckSet#getChecks <em>Checks</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Checks</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.CheckSet#getChecks()
   * @see #getCheckSet()
   * @generated
   */
  EReference getCheckSet_Checks();

  /**
   * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.xLTS.Check <em>Check</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Check</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.Check
   * @generated
   */
  EClass getCheck();

  /**
   * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.lts.xLTS.Check#getLts <em>Lts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Lts</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.Check#getLts()
   * @see #getCheck()
   * @generated
   */
  EReference getCheck_Lts();

  /**
   * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.lts.xLTS.Check#getFormulaDefinition <em>Formula Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Formula Definition</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.Check#getFormulaDefinition()
   * @see #getCheck()
   * @generated
   */
  EReference getCheck_FormulaDefinition();

  /**
   * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet <em>LTL Formula Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Formula Set</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet
   * @generated
   */
  EClass getLTLFormulaSet();

  /**
   * Returns the meta object for the attribute '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet#getName()
   * @see #getLTLFormulaSet()
   * @generated
   */
  EAttribute getLTLFormulaSet_Name();

  /**
   * Returns the meta object for the containment reference list '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet#getFormulae <em>Formulae</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Formulae</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet#getFormulae()
   * @see #getLTLFormulaSet()
   * @generated
   */
  EReference getLTLFormulaSet_Formulae();

  /**
   * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaDefinition <em>LTL Formula Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Formula Definition</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaDefinition
   * @generated
   */
  EClass getLTLFormulaDefinition();

  /**
   * Returns the meta object for the attribute '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaDefinition#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaDefinition#getName()
   * @see #getLTLFormulaDefinition()
   * @generated
   */
  EAttribute getLTLFormulaDefinition_Name();

  /**
   * Returns the meta object for the containment reference '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaDefinition#getFormula <em>Formula</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Formula</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaDefinition#getFormula()
   * @see #getLTLFormulaDefinition()
   * @generated
   */
  EReference getLTLFormulaDefinition_Formula();

  /**
   * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLCheckSet <em>LTL Check Set</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Check Set</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLCheckSet
   * @generated
   */
  EClass getLTLCheckSet();

  /**
   * Returns the meta object for the attribute '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLCheckSet#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLCheckSet#getName()
   * @see #getLTLCheckSet()
   * @generated
   */
  EAttribute getLTLCheckSet_Name();

  /**
   * Returns the meta object for the containment reference list '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLCheckSet#getChecks <em>Checks</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Checks</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLCheckSet#getChecks()
   * @see #getLTLCheckSet()
   * @generated
   */
  EReference getLTLCheckSet_Checks();

  /**
   * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLCheck <em>LTL Check</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>LTL Check</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLCheck
   * @generated
   */
  EClass getLTLCheck();

  /**
   * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLCheck#getLts <em>Lts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Lts</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLCheck#getLts()
   * @see #getLTLCheck()
   * @generated
   */
  EReference getLTLCheck_Lts();

  /**
   * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLCheck#getFormulaDefinition <em>Formula Definition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Formula Definition</em>'.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLCheck#getFormulaDefinition()
   * @see #getLTLCheck()
   * @generated
   */
  EReference getLTLCheck_FormulaDefinition();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  XLTSFactory getXLTSFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.DocumentImpl <em>Document</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.DocumentImpl
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getDocument()
     * @generated
     */
    EClass DOCUMENT = eINSTANCE.getDocument();

    /**
     * The meta object literal for the '<em><b>Ltss</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT__LTSS = eINSTANCE.getDocument_Ltss();

    /**
     * The meta object literal for the '<em><b>Formula Set</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT__FORMULA_SET = eINSTANCE.getDocument_FormulaSet();

    /**
     * The meta object literal for the '<em><b>Check Set</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT__CHECK_SET = eINSTANCE.getDocument_CheckSet();

    /**
     * The meta object literal for the '<em><b>LT Lformula Set</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT__LT_LFORMULA_SET = eINSTANCE.getDocument_LTLformulaSet();

    /**
     * The meta object literal for the '<em><b>LT Lcheck Set</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT__LT_LCHECK_SET = eINSTANCE.getDocument_LTLcheckSet();

    /**
     * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.FormulaSetImpl <em>Formula Set</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.FormulaSetImpl
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getFormulaSet()
     * @generated
     */
    EClass FORMULA_SET = eINSTANCE.getFormulaSet();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FORMULA_SET__NAME = eINSTANCE.getFormulaSet_Name();

    /**
     * The meta object literal for the '<em><b>Formulae</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FORMULA_SET__FORMULAE = eINSTANCE.getFormulaSet_Formulae();

    /**
     * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.CheckSetImpl <em>Check Set</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.CheckSetImpl
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getCheckSet()
     * @generated
     */
    EClass CHECK_SET = eINSTANCE.getCheckSet();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CHECK_SET__NAME = eINSTANCE.getCheckSet_Name();

    /**
     * The meta object literal for the '<em><b>Checks</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CHECK_SET__CHECKS = eINSTANCE.getCheckSet_Checks();

    /**
     * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.CheckImpl <em>Check</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.CheckImpl
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getCheck()
     * @generated
     */
    EClass CHECK = eINSTANCE.getCheck();

    /**
     * The meta object literal for the '<em><b>Lts</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CHECK__LTS = eINSTANCE.getCheck_Lts();

    /**
     * The meta object literal for the '<em><b>Formula Definition</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CHECK__FORMULA_DEFINITION = eINSTANCE.getCheck_FormulaDefinition();

    /**
     * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.LTLFormulaSetImpl <em>LTL Formula Set</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.LTLFormulaSetImpl
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getLTLFormulaSet()
     * @generated
     */
    EClass LTL_FORMULA_SET = eINSTANCE.getLTLFormulaSet();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LTL_FORMULA_SET__NAME = eINSTANCE.getLTLFormulaSet_Name();

    /**
     * The meta object literal for the '<em><b>Formulae</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_FORMULA_SET__FORMULAE = eINSTANCE.getLTLFormulaSet_Formulae();

    /**
     * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.LTLFormulaDefinitionImpl <em>LTL Formula Definition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.LTLFormulaDefinitionImpl
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getLTLFormulaDefinition()
     * @generated
     */
    EClass LTL_FORMULA_DEFINITION = eINSTANCE.getLTLFormulaDefinition();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LTL_FORMULA_DEFINITION__NAME = eINSTANCE.getLTLFormulaDefinition_Name();

    /**
     * The meta object literal for the '<em><b>Formula</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_FORMULA_DEFINITION__FORMULA = eINSTANCE.getLTLFormulaDefinition_Formula();

    /**
     * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.LTLCheckSetImpl <em>LTL Check Set</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.LTLCheckSetImpl
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getLTLCheckSet()
     * @generated
     */
    EClass LTL_CHECK_SET = eINSTANCE.getLTLCheckSet();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LTL_CHECK_SET__NAME = eINSTANCE.getLTLCheckSet_Name();

    /**
     * The meta object literal for the '<em><b>Checks</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_CHECK_SET__CHECKS = eINSTANCE.getLTLCheckSet_Checks();

    /**
     * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.xLTS.impl.LTLCheckImpl <em>LTL Check</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.LTLCheckImpl
     * @see de.hannover.uni.se.fmse.lts.xLTS.impl.XLTSPackageImpl#getLTLCheck()
     * @generated
     */
    EClass LTL_CHECK = eINSTANCE.getLTLCheck();

    /**
     * The meta object literal for the '<em><b>Lts</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_CHECK__LTS = eINSTANCE.getLTLCheck_Lts();

    /**
     * The meta object literal for the '<em><b>Formula Definition</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LTL_CHECK__FORMULA_DEFINITION = eINSTANCE.getLTLCheck_FormulaDefinition();

  }

} //XLTSPackage
