/*
 * generated by Xtext
 */
package de.hannover.uni.se.fmse.lts.validation;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.ecore.EPackage;

public class AbstractXLTSValidator extends org.eclipse.xtext.validation.AbstractDeclarativeValidator {

	@Override
	protected List<EPackage> getEPackages() {
	    List<EPackage> result = new ArrayList<EPackage>();
	    result.add(de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage.eINSTANCE);
	    result.add(EPackage.Registry.INSTANCE.getEPackage("http://www.hannover.de/uni/se/fmse/lts/LTS"));
	    result.add(EPackage.Registry.INSTANCE.getEPackage("http://www.hannover.de/uni/se/fmse/lts/CTL"));
	    result.add(EPackage.Registry.INSTANCE.getEPackage("http://www.hannover.de/uni/se/fmse/lts/LTL"));
		return result;
	}
}
