package de.hannover.uni.se.fmse.lts.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import de.hannover.uni.se.fmse.ctl.AND;
import de.hannover.uni.se.fmse.ctl.Constant;
import de.hannover.uni.se.fmse.ctl.CtlPackage;
import de.hannover.uni.se.fmse.ctl.EG;
import de.hannover.uni.se.fmse.ctl.EU;
import de.hannover.uni.se.fmse.ctl.EX;
import de.hannover.uni.se.fmse.ctl.FormulaDefinition;
import de.hannover.uni.se.fmse.ctl.NOT;
import de.hannover.uni.se.fmse.ctl.OR;
import de.hannover.uni.se.fmse.ctl.Proposition;
import de.hannover.uni.se.fmse.ltl.F;
import de.hannover.uni.se.fmse.ltl.G;
import de.hannover.uni.se.fmse.ltl.LTLProposition;
import de.hannover.uni.se.fmse.ltl.LtlPackage;
import de.hannover.uni.se.fmse.ltl.U;
import de.hannover.uni.se.fmse.ltl.X;
import de.hannover.uni.se.fmse.lts.AtomicProposition;
import de.hannover.uni.se.fmse.lts.BasicState;
import de.hannover.uni.se.fmse.lts.ComposedLTS;
import de.hannover.uni.se.fmse.lts.Event;
import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.LtsPackage;
import de.hannover.uni.se.fmse.lts.Transition;
import de.hannover.uni.se.fmse.lts.services.XLTSGrammarAccess;
import de.hannover.uni.se.fmse.lts.xLTS.Check;
import de.hannover.uni.se.fmse.lts.xLTS.CheckSet;
import de.hannover.uni.se.fmse.lts.xLTS.Document;
import de.hannover.uni.se.fmse.lts.xLTS.FormulaSet;
import de.hannover.uni.se.fmse.lts.xLTS.LTLCheck;
import de.hannover.uni.se.fmse.lts.xLTS.LTLCheckSet;
import de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaDefinition;
import de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet;
import de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class XLTSSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private XLTSGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == CtlPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case CtlPackage.AND:
				if(context == grammarAccess.getANDRule() ||
				   context == grammarAccess.getFormulaRule()) {
					sequence_AND(context, (AND) semanticObject); 
					return; 
				}
				else break;
			case CtlPackage.CONSTANT:
				if(context == grammarAccess.getConstantRule() ||
				   context == grammarAccess.getFormulaRule()) {
					sequence_Constant(context, (Constant) semanticObject); 
					return; 
				}
				else break;
			case CtlPackage.EG:
				if(context == grammarAccess.getEGRule() ||
				   context == grammarAccess.getFormulaRule()) {
					sequence_EG(context, (EG) semanticObject); 
					return; 
				}
				else break;
			case CtlPackage.EU:
				if(context == grammarAccess.getEURule() ||
				   context == grammarAccess.getFormulaRule()) {
					sequence_EU(context, (EU) semanticObject); 
					return; 
				}
				else break;
			case CtlPackage.EX:
				if(context == grammarAccess.getEXRule() ||
				   context == grammarAccess.getFormulaRule()) {
					sequence_EX(context, (EX) semanticObject); 
					return; 
				}
				else break;
			case CtlPackage.FORMULA_DEFINITION:
				if(context == grammarAccess.getFormulaDefinitionRule()) {
					sequence_FormulaDefinition(context, (FormulaDefinition) semanticObject); 
					return; 
				}
				else break;
			case CtlPackage.NOT:
				if(context == grammarAccess.getFormulaRule() ||
				   context == grammarAccess.getNOTRule()) {
					sequence_NOT(context, (NOT) semanticObject); 
					return; 
				}
				else break;
			case CtlPackage.OR:
				if(context == grammarAccess.getFormulaRule() ||
				   context == grammarAccess.getORRule()) {
					sequence_OR(context, (OR) semanticObject); 
					return; 
				}
				else break;
			case CtlPackage.PROPOSITION:
				if(context == grammarAccess.getFormulaRule() ||
				   context == grammarAccess.getPropositionRule()) {
					sequence_Proposition(context, (Proposition) semanticObject); 
					return; 
				}
				else break;
			}
		else if(semanticObject.eClass().getEPackage() == LtlPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case LtlPackage.AND:
				if(context == grammarAccess.getLTLANDRule() ||
				   context == grammarAccess.getLTLFormulaRule()) {
					sequence_LTLAND(context, (de.hannover.uni.se.fmse.ltl.AND) semanticObject); 
					return; 
				}
				else break;
			case LtlPackage.CONSTANT:
				if(context == grammarAccess.getLTLConstantRule() ||
				   context == grammarAccess.getLTLFormulaRule()) {
					sequence_LTLConstant(context, (de.hannover.uni.se.fmse.ltl.Constant) semanticObject); 
					return; 
				}
				else break;
			case LtlPackage.F:
				if(context == grammarAccess.getLTLFRule() ||
				   context == grammarAccess.getLTLFormulaRule()) {
					sequence_LTLF(context, (F) semanticObject); 
					return; 
				}
				else break;
			case LtlPackage.G:
				if(context == grammarAccess.getLTLFormulaRule() ||
				   context == grammarAccess.getLTLGRule()) {
					sequence_LTLG(context, (G) semanticObject); 
					return; 
				}
				else break;
			case LtlPackage.LTL_PROPOSITION:
				if(context == grammarAccess.getLTLFormulaRule() ||
				   context == grammarAccess.getLTLPropositionRule()) {
					sequence_LTLProposition(context, (LTLProposition) semanticObject); 
					return; 
				}
				else break;
			case LtlPackage.NOT:
				if(context == grammarAccess.getLTLFormulaRule() ||
				   context == grammarAccess.getLTLNOTRule()) {
					sequence_LTLNOT(context, (de.hannover.uni.se.fmse.ltl.NOT) semanticObject); 
					return; 
				}
				else break;
			case LtlPackage.OR:
				if(context == grammarAccess.getLTLFormulaRule() ||
				   context == grammarAccess.getLTLORRule()) {
					sequence_LTLOR(context, (de.hannover.uni.se.fmse.ltl.OR) semanticObject); 
					return; 
				}
				else break;
			case LtlPackage.U:
				if(context == grammarAccess.getLTLFormulaRule() ||
				   context == grammarAccess.getLTLURule()) {
					sequence_LTLU(context, (U) semanticObject); 
					return; 
				}
				else break;
			case LtlPackage.X:
				if(context == grammarAccess.getLTLFormulaRule() ||
				   context == grammarAccess.getLTLXRule()) {
					sequence_LTLX(context, (X) semanticObject); 
					return; 
				}
				else break;
			}
		else if(semanticObject.eClass().getEPackage() == LtsPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case LtsPackage.ATOMIC_PROPOSITION:
				if(context == grammarAccess.getAtomicPropositionRule()) {
					sequence_AtomicProposition(context, (AtomicProposition) semanticObject); 
					return; 
				}
				else break;
			case LtsPackage.BASIC_STATE:
				if(context == grammarAccess.getBasicStateRule()) {
					sequence_BasicState(context, (BasicState) semanticObject); 
					return; 
				}
				else break;
			case LtsPackage.COMPOSED_LTS:
				if(context == grammarAccess.getComposedLTSRule()) {
					sequence_ComposedLTS(context, (ComposedLTS) semanticObject); 
					return; 
				}
				else break;
			case LtsPackage.EVENT:
				if(context == grammarAccess.getEventRule()) {
					sequence_Event(context, (Event) semanticObject); 
					return; 
				}
				else break;
			case LtsPackage.LTS:
				if(context == grammarAccess.getLTSRule()) {
					sequence_LTS(context, (LTS) semanticObject); 
					return; 
				}
				else break;
			case LtsPackage.TRANSITION:
				if(context == grammarAccess.getTransitionRule()) {
					sequence_Transition(context, (Transition) semanticObject); 
					return; 
				}
				else break;
			}
		else if(semanticObject.eClass().getEPackage() == XLTSPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case XLTSPackage.CHECK:
				if(context == grammarAccess.getCheckRule()) {
					sequence_Check(context, (Check) semanticObject); 
					return; 
				}
				else break;
			case XLTSPackage.CHECK_SET:
				if(context == grammarAccess.getCheckSetRule()) {
					sequence_CheckSet(context, (CheckSet) semanticObject); 
					return; 
				}
				else break;
			case XLTSPackage.DOCUMENT:
				if(context == grammarAccess.getDocumentRule()) {
					sequence_Document(context, (Document) semanticObject); 
					return; 
				}
				else break;
			case XLTSPackage.FORMULA_SET:
				if(context == grammarAccess.getFormulaSetRule()) {
					sequence_FormulaSet(context, (FormulaSet) semanticObject); 
					return; 
				}
				else break;
			case XLTSPackage.LTL_CHECK:
				if(context == grammarAccess.getLTLCheckRule()) {
					sequence_LTLCheck(context, (LTLCheck) semanticObject); 
					return; 
				}
				else break;
			case XLTSPackage.LTL_CHECK_SET:
				if(context == grammarAccess.getLTLCheckSetRule()) {
					sequence_LTLCheckSet(context, (LTLCheckSet) semanticObject); 
					return; 
				}
				else break;
			case XLTSPackage.LTL_FORMULA_DEFINITION:
				if(context == grammarAccess.getLTLFormulaDefinitionRule()) {
					sequence_LTLFormulaDefinition(context, (LTLFormulaDefinition) semanticObject); 
					return; 
				}
				else break;
			case XLTSPackage.LTL_FORMULA_SET:
				if(context == grammarAccess.getLTLFormulaSetRule()) {
					sequence_LTLFormulaSet(context, (LTLFormulaSet) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (left=Formula right=Formula)
	 */
	protected void sequence_AND(EObject context, AND semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_AtomicProposition(EObject context, AtomicProposition semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LtsPackage.Literals.NAMED_ELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LtsPackage.Literals.NAMED_ELEMENT__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAtomicPropositionAccess().getNameIDTerminalRuleCall_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID (atomicPropositions+=[AtomicProposition|ID] atomicPropositions+=[AtomicProposition|ID]*)*)
	 */
	protected void sequence_BasicState(EObject context, BasicState semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID checks+=Check*)
	 */
	protected void sequence_CheckSet(EObject context, CheckSet semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (lts=[LTS|ID] formulaDefinition=[FormulaDefinition|ID])
	 */
	protected void sequence_Check(EObject context, Check semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, XLTSPackage.Literals.CHECK__LTS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, XLTSPackage.Literals.CHECK__LTS));
			if(transientValues.isValueTransient(semanticObject, XLTSPackage.Literals.CHECK__FORMULA_DEFINITION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, XLTSPackage.Literals.CHECK__FORMULA_DEFINITION));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getCheckAccess().getLtsLTSIDTerminalRuleCall_1_0_1(), semanticObject.getLts());
		feeder.accept(grammarAccess.getCheckAccess().getFormulaDefinitionFormulaDefinitionIDTerminalRuleCall_3_0_1(), semanticObject.getFormulaDefinition());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (ltsLeft=[LTS|ID] ltsRight=[LTS|ID] name=ID (events+=[Event|ID] events+=[Event|ID]*)?)
	 */
	protected void sequence_ComposedLTS(EObject context, ComposedLTS semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (value?='true'?)
	 */
	protected void sequence_Constant(EObject context, Constant semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     ((ltss+=LTS | ltss+=ComposedLTS)* formulaSet=FormulaSet? checkSet=CheckSet? LTLformulaSet=LTLFormulaSet? LTLcheckSet=LTLCheckSet?)
	 */
	protected void sequence_Document(EObject context, Document semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     nested=Formula
	 */
	protected void sequence_EG(EObject context, EG semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=Formula right=Formula)
	 */
	protected void sequence_EU(EObject context, EU semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     nested=Formula
	 */
	protected void sequence_EX(EObject context, EX semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_Event(EObject context, Event semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LtsPackage.Literals.NAMED_ELEMENT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LtsPackage.Literals.NAMED_ELEMENT__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getEventAccess().getNameIDTerminalRuleCall_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID formula=Formula)
	 */
	protected void sequence_FormulaDefinition(EObject context, FormulaDefinition semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, CtlPackage.Literals.FORMULA_DEFINITION__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CtlPackage.Literals.FORMULA_DEFINITION__NAME));
			if(transientValues.isValueTransient(semanticObject, CtlPackage.Literals.FORMULA_DEFINITION__FORMULA) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, CtlPackage.Literals.FORMULA_DEFINITION__FORMULA));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getFormulaDefinitionAccess().getNameIDTerminalRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getFormulaDefinitionAccess().getFormulaFormulaParserRuleCall_2_0(), semanticObject.getFormula());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID formulae+=FormulaDefinition*)
	 */
	protected void sequence_FormulaSet(EObject context, FormulaSet semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=LTLFormula right=LTLFormula)
	 */
	protected void sequence_LTLAND(EObject context, de.hannover.uni.se.fmse.ltl.AND semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID checks+=LTLCheck*)
	 */
	protected void sequence_LTLCheckSet(EObject context, LTLCheckSet semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (lts=[LTS|ID] formulaDefinition=[LTLFormulaDefinition|ID])
	 */
	protected void sequence_LTLCheck(EObject context, LTLCheck semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, XLTSPackage.Literals.LTL_CHECK__LTS) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, XLTSPackage.Literals.LTL_CHECK__LTS));
			if(transientValues.isValueTransient(semanticObject, XLTSPackage.Literals.LTL_CHECK__FORMULA_DEFINITION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, XLTSPackage.Literals.LTL_CHECK__FORMULA_DEFINITION));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getLTLCheckAccess().getLtsLTSIDTerminalRuleCall_1_0_1(), semanticObject.getLts());
		feeder.accept(grammarAccess.getLTLCheckAccess().getFormulaDefinitionLTLFormulaDefinitionIDTerminalRuleCall_3_0_1(), semanticObject.getFormulaDefinition());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (value?='true'?)
	 */
	protected void sequence_LTLConstant(EObject context, de.hannover.uni.se.fmse.ltl.Constant semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     nested=LTLFormula
	 */
	protected void sequence_LTLF(EObject context, F semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID formula=LTLFormula)
	 */
	protected void sequence_LTLFormulaDefinition(EObject context, LTLFormulaDefinition semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, XLTSPackage.Literals.LTL_FORMULA_DEFINITION__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, XLTSPackage.Literals.LTL_FORMULA_DEFINITION__NAME));
			if(transientValues.isValueTransient(semanticObject, XLTSPackage.Literals.LTL_FORMULA_DEFINITION__FORMULA) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, XLTSPackage.Literals.LTL_FORMULA_DEFINITION__FORMULA));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getLTLFormulaDefinitionAccess().getNameIDTerminalRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getLTLFormulaDefinitionAccess().getFormulaLTLFormulaParserRuleCall_2_0(), semanticObject.getFormula());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID formulae+=LTLFormulaDefinition*)
	 */
	protected void sequence_LTLFormulaSet(EObject context, LTLFormulaSet semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     nested=LTLFormula
	 */
	protected void sequence_LTLG(EObject context, G semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     nested=LTLFormula
	 */
	protected void sequence_LTLNOT(EObject context, de.hannover.uni.se.fmse.ltl.NOT semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=LTLFormula right=LTLFormula)
	 */
	protected void sequence_LTLOR(EObject context, de.hannover.uni.se.fmse.ltl.OR semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     atomicProposition=[AtomicProposition|ID]
	 */
	protected void sequence_LTLProposition(EObject context, LTLProposition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=LTLFormula right=LTLFormula)
	 */
	protected void sequence_LTLU(EObject context, U semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     nested=LTLFormula
	 */
	protected void sequence_LTLX(EObject context, X semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (
	 *         name=ID 
	 *         (atomicPropositions+=AtomicProposition atomicPropositions+=AtomicProposition*)? 
	 *         states+=BasicState 
	 *         states+=BasicState* 
	 *         initStates+=[State|ID] 
	 *         initStates+=[State|ID]* 
	 *         (alphabet+=Event alphabet+=Event*)? 
	 *         transitions+=Transition*
	 *     )
	 */
	protected void sequence_LTS(EObject context, LTS semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     nested=Formula
	 */
	protected void sequence_NOT(EObject context, NOT semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (left=Formula right=Formula)
	 */
	protected void sequence_OR(EObject context, OR semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     atomicProposition=[AtomicProposition|ID]
	 */
	protected void sequence_Proposition(EObject context, Proposition semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (source=[State|ID] destination=[State|ID] event=[Event|ID])
	 */
	protected void sequence_Transition(EObject context, Transition semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, LtsPackage.Literals.TRANSITION__EVENT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LtsPackage.Literals.TRANSITION__EVENT));
			if(transientValues.isValueTransient(semanticObject, LtsPackage.Literals.TRANSITION__SOURCE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LtsPackage.Literals.TRANSITION__SOURCE));
			if(transientValues.isValueTransient(semanticObject, LtsPackage.Literals.TRANSITION__DESTINATION) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, LtsPackage.Literals.TRANSITION__DESTINATION));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getTransitionAccess().getSourceStateIDTerminalRuleCall_0_0_1(), semanticObject.getSource());
		feeder.accept(grammarAccess.getTransitionAccess().getDestinationStateIDTerminalRuleCall_2_0_1(), semanticObject.getDestination());
		feeder.accept(grammarAccess.getTransitionAccess().getEventEventIDTerminalRuleCall_4_0_1(), semanticObject.getEvent());
		feeder.finish();
	}
}
