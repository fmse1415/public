/**
 */
package de.hannover.uni.se.fmse.lts.xLTS;

import de.hannover.uni.se.fmse.lts.LTS;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LTL Check</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.LTLCheck#getLts <em>Lts</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.xLTS.LTLCheck#getFormulaDefinition <em>Formula Definition</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getLTLCheck()
 * @model
 * @generated
 */
public interface LTLCheck extends EObject
{
  /**
   * Returns the value of the '<em><b>Lts</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Lts</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Lts</em>' reference.
   * @see #setLts(LTS)
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getLTLCheck_Lts()
   * @model
   * @generated
   */
  LTS getLts();

  /**
   * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLCheck#getLts <em>Lts</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Lts</em>' reference.
   * @see #getLts()
   * @generated
   */
  void setLts(LTS value);

  /**
   * Returns the value of the '<em><b>Formula Definition</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Formula Definition</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Formula Definition</em>' reference.
   * @see #setFormulaDefinition(LTLFormulaDefinition)
   * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage#getLTLCheck_FormulaDefinition()
   * @model
   * @generated
   */
  LTLFormulaDefinition getFormulaDefinition();

  /**
   * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLCheck#getFormulaDefinition <em>Formula Definition</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Formula Definition</em>' reference.
   * @see #getFormulaDefinition()
   * @generated
   */
  void setFormulaDefinition(LTLFormulaDefinition value);

} // LTLCheck
