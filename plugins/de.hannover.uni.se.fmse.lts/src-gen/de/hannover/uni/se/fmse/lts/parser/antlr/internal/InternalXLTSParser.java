package de.hannover.uni.se.fmse.lts.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import de.hannover.uni.se.fmse.lts.services.XLTSGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalXLTSParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'LTS'", "'{'", "'atomic'", "'propositions'", "','", "'states'", "'initial'", "'alphabet'", "'}'", "'->'", "':'", "'compose'", "'||'", "'as'", "'using'", "'CTL'", "'FormulaSet'", "':='", "'EX'", "'EG'", "'E'", "'['", "'U'", "']'", "'('", "'\\u22C0'", "')'", "'\\u22C1'", "'\\u00AC'", "'true'", "'false'", "'CheckSet'", "'check'", "'\\u22A8'", "'LTL'", "'X'", "'G'", "'F'"
    };
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int RULE_ID=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalXLTSParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalXLTSParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalXLTSParser.tokenNames; }
    public String getGrammarFileName() { return "../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g"; }



     	private XLTSGrammarAccess grammarAccess;
     	
        public InternalXLTSParser(TokenStream input, XLTSGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Document";	
       	}
       	
       	@Override
       	protected XLTSGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleDocument"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:67:1: entryRuleDocument returns [EObject current=null] : iv_ruleDocument= ruleDocument EOF ;
    public final EObject entryRuleDocument() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDocument = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:68:2: (iv_ruleDocument= ruleDocument EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:69:2: iv_ruleDocument= ruleDocument EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDocumentRule()); 
            }
            pushFollow(FOLLOW_ruleDocument_in_entryRuleDocument75);
            iv_ruleDocument=ruleDocument();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDocument; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleDocument85); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDocument"


    // $ANTLR start "ruleDocument"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:76:1: ruleDocument returns [EObject current=null] : ( ( ( (lv_ltss_0_1= ruleLTS | lv_ltss_0_2= ruleComposedLTS ) ) )* ( (lv_formulaSet_1_0= ruleFormulaSet ) )? ( (lv_checkSet_2_0= ruleCheckSet ) )? ( (lv_LTLformulaSet_3_0= ruleLTLFormulaSet ) )? ( (lv_LTLcheckSet_4_0= ruleLTLCheckSet ) )? ) ;
    public final EObject ruleDocument() throws RecognitionException {
        EObject current = null;

        EObject lv_ltss_0_1 = null;

        EObject lv_ltss_0_2 = null;

        EObject lv_formulaSet_1_0 = null;

        EObject lv_checkSet_2_0 = null;

        EObject lv_LTLformulaSet_3_0 = null;

        EObject lv_LTLcheckSet_4_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:79:28: ( ( ( ( (lv_ltss_0_1= ruleLTS | lv_ltss_0_2= ruleComposedLTS ) ) )* ( (lv_formulaSet_1_0= ruleFormulaSet ) )? ( (lv_checkSet_2_0= ruleCheckSet ) )? ( (lv_LTLformulaSet_3_0= ruleLTLFormulaSet ) )? ( (lv_LTLcheckSet_4_0= ruleLTLCheckSet ) )? ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:80:1: ( ( ( (lv_ltss_0_1= ruleLTS | lv_ltss_0_2= ruleComposedLTS ) ) )* ( (lv_formulaSet_1_0= ruleFormulaSet ) )? ( (lv_checkSet_2_0= ruleCheckSet ) )? ( (lv_LTLformulaSet_3_0= ruleLTLFormulaSet ) )? ( (lv_LTLcheckSet_4_0= ruleLTLCheckSet ) )? )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:80:1: ( ( ( (lv_ltss_0_1= ruleLTS | lv_ltss_0_2= ruleComposedLTS ) ) )* ( (lv_formulaSet_1_0= ruleFormulaSet ) )? ( (lv_checkSet_2_0= ruleCheckSet ) )? ( (lv_LTLformulaSet_3_0= ruleLTLFormulaSet ) )? ( (lv_LTLcheckSet_4_0= ruleLTLCheckSet ) )? )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:80:2: ( ( (lv_ltss_0_1= ruleLTS | lv_ltss_0_2= ruleComposedLTS ) ) )* ( (lv_formulaSet_1_0= ruleFormulaSet ) )? ( (lv_checkSet_2_0= ruleCheckSet ) )? ( (lv_LTLformulaSet_3_0= ruleLTLFormulaSet ) )? ( (lv_LTLcheckSet_4_0= ruleLTLCheckSet ) )?
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:80:2: ( ( (lv_ltss_0_1= ruleLTS | lv_ltss_0_2= ruleComposedLTS ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==11||LA2_0==22) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:81:1: ( (lv_ltss_0_1= ruleLTS | lv_ltss_0_2= ruleComposedLTS ) )
            	    {
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:81:1: ( (lv_ltss_0_1= ruleLTS | lv_ltss_0_2= ruleComposedLTS ) )
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:82:1: (lv_ltss_0_1= ruleLTS | lv_ltss_0_2= ruleComposedLTS )
            	    {
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:82:1: (lv_ltss_0_1= ruleLTS | lv_ltss_0_2= ruleComposedLTS )
            	    int alt1=2;
            	    int LA1_0 = input.LA(1);

            	    if ( (LA1_0==11) ) {
            	        alt1=1;
            	    }
            	    else if ( (LA1_0==22) ) {
            	        alt1=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 1, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt1) {
            	        case 1 :
            	            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:83:3: lv_ltss_0_1= ruleLTS
            	            {
            	            if ( state.backtracking==0 ) {
            	               
            	              	        newCompositeNode(grammarAccess.getDocumentAccess().getLtssLTSParserRuleCall_0_0_0()); 
            	              	    
            	            }
            	            pushFollow(FOLLOW_ruleLTS_in_ruleDocument133);
            	            lv_ltss_0_1=ruleLTS();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              	        if (current==null) {
            	              	            current = createModelElementForParent(grammarAccess.getDocumentRule());
            	              	        }
            	                     		add(
            	                     			current, 
            	                     			"ltss",
            	                      		lv_ltss_0_1, 
            	                      		"LTS");
            	              	        afterParserOrEnumRuleCall();
            	              	    
            	            }

            	            }
            	            break;
            	        case 2 :
            	            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:98:8: lv_ltss_0_2= ruleComposedLTS
            	            {
            	            if ( state.backtracking==0 ) {
            	               
            	              	        newCompositeNode(grammarAccess.getDocumentAccess().getLtssComposedLTSParserRuleCall_0_0_1()); 
            	              	    
            	            }
            	            pushFollow(FOLLOW_ruleComposedLTS_in_ruleDocument152);
            	            lv_ltss_0_2=ruleComposedLTS();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              	        if (current==null) {
            	              	            current = createModelElementForParent(grammarAccess.getDocumentRule());
            	              	        }
            	                     		add(
            	                     			current, 
            	                     			"ltss",
            	                      		lv_ltss_0_2, 
            	                      		"ComposedLTS");
            	              	        afterParserOrEnumRuleCall();
            	              	    
            	            }

            	            }
            	            break;

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:116:3: ( (lv_formulaSet_1_0= ruleFormulaSet ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==26) ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1==27) ) {
                    alt3=1;
                }
            }
            switch (alt3) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:117:1: (lv_formulaSet_1_0= ruleFormulaSet )
                    {
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:117:1: (lv_formulaSet_1_0= ruleFormulaSet )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:118:3: lv_formulaSet_1_0= ruleFormulaSet
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDocumentAccess().getFormulaSetFormulaSetParserRuleCall_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleFormulaSet_in_ruleDocument177);
                    lv_formulaSet_1_0=ruleFormulaSet();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDocumentRule());
                      	        }
                             		set(
                             			current, 
                             			"formulaSet",
                              		lv_formulaSet_1_0, 
                              		"FormulaSet");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:134:3: ( (lv_checkSet_2_0= ruleCheckSet ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==26) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:135:1: (lv_checkSet_2_0= ruleCheckSet )
                    {
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:135:1: (lv_checkSet_2_0= ruleCheckSet )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:136:3: lv_checkSet_2_0= ruleCheckSet
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDocumentAccess().getCheckSetCheckSetParserRuleCall_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleCheckSet_in_ruleDocument199);
                    lv_checkSet_2_0=ruleCheckSet();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDocumentRule());
                      	        }
                             		set(
                             			current, 
                             			"checkSet",
                              		lv_checkSet_2_0, 
                              		"CheckSet");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:152:3: ( (lv_LTLformulaSet_3_0= ruleLTLFormulaSet ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==45) ) {
                int LA5_1 = input.LA(2);

                if ( (LA5_1==27) ) {
                    alt5=1;
                }
            }
            switch (alt5) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:153:1: (lv_LTLformulaSet_3_0= ruleLTLFormulaSet )
                    {
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:153:1: (lv_LTLformulaSet_3_0= ruleLTLFormulaSet )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:154:3: lv_LTLformulaSet_3_0= ruleLTLFormulaSet
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDocumentAccess().getLTLformulaSetLTLFormulaSetParserRuleCall_3_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleLTLFormulaSet_in_ruleDocument221);
                    lv_LTLformulaSet_3_0=ruleLTLFormulaSet();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDocumentRule());
                      	        }
                             		set(
                             			current, 
                             			"LTLformulaSet",
                              		lv_LTLformulaSet_3_0, 
                              		"LTLFormulaSet");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:170:3: ( (lv_LTLcheckSet_4_0= ruleLTLCheckSet ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==45) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:171:1: (lv_LTLcheckSet_4_0= ruleLTLCheckSet )
                    {
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:171:1: (lv_LTLcheckSet_4_0= ruleLTLCheckSet )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:172:3: lv_LTLcheckSet_4_0= ruleLTLCheckSet
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getDocumentAccess().getLTLcheckSetLTLCheckSetParserRuleCall_4_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleLTLCheckSet_in_ruleDocument243);
                    lv_LTLcheckSet_4_0=ruleLTLCheckSet();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getDocumentRule());
                      	        }
                             		set(
                             			current, 
                             			"LTLcheckSet",
                              		lv_LTLcheckSet_4_0, 
                              		"LTLCheckSet");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDocument"


    // $ANTLR start "entryRuleLTS"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:196:1: entryRuleLTS returns [EObject current=null] : iv_ruleLTS= ruleLTS EOF ;
    public final EObject entryRuleLTS() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTS = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:197:2: (iv_ruleLTS= ruleLTS EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:198:2: iv_ruleLTS= ruleLTS EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTSRule()); 
            }
            pushFollow(FOLLOW_ruleLTS_in_entryRuleLTS280);
            iv_ruleLTS=ruleLTS();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTS; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTS290); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTS"


    // $ANTLR start "ruleLTS"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:205:1: ruleLTS returns [EObject current=null] : (otherlv_0= 'LTS' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'atomic' otherlv_4= 'propositions' ( (lv_atomicPropositions_5_0= ruleAtomicProposition ) ) (otherlv_6= ',' ( (lv_atomicPropositions_7_0= ruleAtomicProposition ) ) )* )? otherlv_8= 'states' ( (lv_states_9_0= ruleBasicState ) ) (otherlv_10= ',' ( (lv_states_11_0= ruleBasicState ) ) )* otherlv_12= 'initial' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* (otherlv_16= 'alphabet' ( (lv_alphabet_17_0= ruleEvent ) ) (otherlv_18= ',' ( (lv_alphabet_19_0= ruleEvent ) ) )* )? ( (lv_transitions_20_0= ruleTransition ) )* otherlv_21= '}' ) ;
    public final EObject ruleLTS() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_21=null;
        EObject lv_atomicPropositions_5_0 = null;

        EObject lv_atomicPropositions_7_0 = null;

        EObject lv_states_9_0 = null;

        EObject lv_states_11_0 = null;

        EObject lv_alphabet_17_0 = null;

        EObject lv_alphabet_19_0 = null;

        EObject lv_transitions_20_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:208:28: ( (otherlv_0= 'LTS' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'atomic' otherlv_4= 'propositions' ( (lv_atomicPropositions_5_0= ruleAtomicProposition ) ) (otherlv_6= ',' ( (lv_atomicPropositions_7_0= ruleAtomicProposition ) ) )* )? otherlv_8= 'states' ( (lv_states_9_0= ruleBasicState ) ) (otherlv_10= ',' ( (lv_states_11_0= ruleBasicState ) ) )* otherlv_12= 'initial' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* (otherlv_16= 'alphabet' ( (lv_alphabet_17_0= ruleEvent ) ) (otherlv_18= ',' ( (lv_alphabet_19_0= ruleEvent ) ) )* )? ( (lv_transitions_20_0= ruleTransition ) )* otherlv_21= '}' ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:209:1: (otherlv_0= 'LTS' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'atomic' otherlv_4= 'propositions' ( (lv_atomicPropositions_5_0= ruleAtomicProposition ) ) (otherlv_6= ',' ( (lv_atomicPropositions_7_0= ruleAtomicProposition ) ) )* )? otherlv_8= 'states' ( (lv_states_9_0= ruleBasicState ) ) (otherlv_10= ',' ( (lv_states_11_0= ruleBasicState ) ) )* otherlv_12= 'initial' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* (otherlv_16= 'alphabet' ( (lv_alphabet_17_0= ruleEvent ) ) (otherlv_18= ',' ( (lv_alphabet_19_0= ruleEvent ) ) )* )? ( (lv_transitions_20_0= ruleTransition ) )* otherlv_21= '}' )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:209:1: (otherlv_0= 'LTS' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'atomic' otherlv_4= 'propositions' ( (lv_atomicPropositions_5_0= ruleAtomicProposition ) ) (otherlv_6= ',' ( (lv_atomicPropositions_7_0= ruleAtomicProposition ) ) )* )? otherlv_8= 'states' ( (lv_states_9_0= ruleBasicState ) ) (otherlv_10= ',' ( (lv_states_11_0= ruleBasicState ) ) )* otherlv_12= 'initial' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* (otherlv_16= 'alphabet' ( (lv_alphabet_17_0= ruleEvent ) ) (otherlv_18= ',' ( (lv_alphabet_19_0= ruleEvent ) ) )* )? ( (lv_transitions_20_0= ruleTransition ) )* otherlv_21= '}' )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:209:3: otherlv_0= 'LTS' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' (otherlv_3= 'atomic' otherlv_4= 'propositions' ( (lv_atomicPropositions_5_0= ruleAtomicProposition ) ) (otherlv_6= ',' ( (lv_atomicPropositions_7_0= ruleAtomicProposition ) ) )* )? otherlv_8= 'states' ( (lv_states_9_0= ruleBasicState ) ) (otherlv_10= ',' ( (lv_states_11_0= ruleBasicState ) ) )* otherlv_12= 'initial' ( (otherlv_13= RULE_ID ) ) (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )* (otherlv_16= 'alphabet' ( (lv_alphabet_17_0= ruleEvent ) ) (otherlv_18= ',' ( (lv_alphabet_19_0= ruleEvent ) ) )* )? ( (lv_transitions_20_0= ruleTransition ) )* otherlv_21= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_ruleLTS327); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLTSAccess().getLTSKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:213:1: ( (lv_name_1_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:214:1: (lv_name_1_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:214:1: (lv_name_1_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:215:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleLTS344); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getLTSAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getLTSRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleLTS361); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getLTSAccess().getLeftCurlyBracketKeyword_2());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:235:1: (otherlv_3= 'atomic' otherlv_4= 'propositions' ( (lv_atomicPropositions_5_0= ruleAtomicProposition ) ) (otherlv_6= ',' ( (lv_atomicPropositions_7_0= ruleAtomicProposition ) ) )* )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==13) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:235:3: otherlv_3= 'atomic' otherlv_4= 'propositions' ( (lv_atomicPropositions_5_0= ruleAtomicProposition ) ) (otherlv_6= ',' ( (lv_atomicPropositions_7_0= ruleAtomicProposition ) ) )*
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_13_in_ruleLTS374); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getLTSAccess().getAtomicKeyword_3_0());
                          
                    }
                    otherlv_4=(Token)match(input,14,FOLLOW_14_in_ruleLTS386); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_4, grammarAccess.getLTSAccess().getPropositionsKeyword_3_1());
                          
                    }
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:243:1: ( (lv_atomicPropositions_5_0= ruleAtomicProposition ) )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:244:1: (lv_atomicPropositions_5_0= ruleAtomicProposition )
                    {
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:244:1: (lv_atomicPropositions_5_0= ruleAtomicProposition )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:245:3: lv_atomicPropositions_5_0= ruleAtomicProposition
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getLTSAccess().getAtomicPropositionsAtomicPropositionParserRuleCall_3_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleAtomicProposition_in_ruleLTS407);
                    lv_atomicPropositions_5_0=ruleAtomicProposition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getLTSRule());
                      	        }
                             		add(
                             			current, 
                             			"atomicPropositions",
                              		lv_atomicPropositions_5_0, 
                              		"AtomicProposition");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:261:2: (otherlv_6= ',' ( (lv_atomicPropositions_7_0= ruleAtomicProposition ) ) )*
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==15) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:261:4: otherlv_6= ',' ( (lv_atomicPropositions_7_0= ruleAtomicProposition ) )
                    	    {
                    	    otherlv_6=(Token)match(input,15,FOLLOW_15_in_ruleLTS420); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_6, grammarAccess.getLTSAccess().getCommaKeyword_3_3_0());
                    	          
                    	    }
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:265:1: ( (lv_atomicPropositions_7_0= ruleAtomicProposition ) )
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:266:1: (lv_atomicPropositions_7_0= ruleAtomicProposition )
                    	    {
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:266:1: (lv_atomicPropositions_7_0= ruleAtomicProposition )
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:267:3: lv_atomicPropositions_7_0= ruleAtomicProposition
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getLTSAccess().getAtomicPropositionsAtomicPropositionParserRuleCall_3_3_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleAtomicProposition_in_ruleLTS441);
                    	    lv_atomicPropositions_7_0=ruleAtomicProposition();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getLTSRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"atomicPropositions",
                    	              		lv_atomicPropositions_7_0, 
                    	              		"AtomicProposition");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_8=(Token)match(input,16,FOLLOW_16_in_ruleLTS457); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_8, grammarAccess.getLTSAccess().getStatesKeyword_4());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:287:1: ( (lv_states_9_0= ruleBasicState ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:288:1: (lv_states_9_0= ruleBasicState )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:288:1: (lv_states_9_0= ruleBasicState )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:289:3: lv_states_9_0= ruleBasicState
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLTSAccess().getStatesBasicStateParserRuleCall_5_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBasicState_in_ruleLTS478);
            lv_states_9_0=ruleBasicState();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLTSRule());
              	        }
                     		add(
                     			current, 
                     			"states",
                      		lv_states_9_0, 
                      		"BasicState");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:305:2: (otherlv_10= ',' ( (lv_states_11_0= ruleBasicState ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==15) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:305:4: otherlv_10= ',' ( (lv_states_11_0= ruleBasicState ) )
            	    {
            	    otherlv_10=(Token)match(input,15,FOLLOW_15_in_ruleLTS491); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_10, grammarAccess.getLTSAccess().getCommaKeyword_6_0());
            	          
            	    }
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:309:1: ( (lv_states_11_0= ruleBasicState ) )
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:310:1: (lv_states_11_0= ruleBasicState )
            	    {
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:310:1: (lv_states_11_0= ruleBasicState )
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:311:3: lv_states_11_0= ruleBasicState
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getLTSAccess().getStatesBasicStateParserRuleCall_6_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleBasicState_in_ruleLTS512);
            	    lv_states_11_0=ruleBasicState();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getLTSRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"states",
            	              		lv_states_11_0, 
            	              		"BasicState");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            otherlv_12=(Token)match(input,17,FOLLOW_17_in_ruleLTS526); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_12, grammarAccess.getLTSAccess().getInitialKeyword_7());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:331:1: ( (otherlv_13= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:332:1: (otherlv_13= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:332:1: (otherlv_13= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:333:3: otherlv_13= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getLTSRule());
              	        }
                      
            }
            otherlv_13=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleLTS546); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_13, grammarAccess.getLTSAccess().getInitStatesStateCrossReference_8_0()); 
              	
            }

            }


            }

            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:344:2: (otherlv_14= ',' ( (otherlv_15= RULE_ID ) ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==15) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:344:4: otherlv_14= ',' ( (otherlv_15= RULE_ID ) )
            	    {
            	    otherlv_14=(Token)match(input,15,FOLLOW_15_in_ruleLTS559); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_14, grammarAccess.getLTSAccess().getCommaKeyword_9_0());
            	          
            	    }
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:348:1: ( (otherlv_15= RULE_ID ) )
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:349:1: (otherlv_15= RULE_ID )
            	    {
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:349:1: (otherlv_15= RULE_ID )
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:350:3: otherlv_15= RULE_ID
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getLTSRule());
            	      	        }
            	              
            	    }
            	    otherlv_15=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleLTS579); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		newLeafNode(otherlv_15, grammarAccess.getLTSAccess().getInitStatesStateCrossReference_9_1_0()); 
            	      	
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:361:4: (otherlv_16= 'alphabet' ( (lv_alphabet_17_0= ruleEvent ) ) (otherlv_18= ',' ( (lv_alphabet_19_0= ruleEvent ) ) )* )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==18) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:361:6: otherlv_16= 'alphabet' ( (lv_alphabet_17_0= ruleEvent ) ) (otherlv_18= ',' ( (lv_alphabet_19_0= ruleEvent ) ) )*
                    {
                    otherlv_16=(Token)match(input,18,FOLLOW_18_in_ruleLTS594); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_16, grammarAccess.getLTSAccess().getAlphabetKeyword_10_0());
                          
                    }
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:365:1: ( (lv_alphabet_17_0= ruleEvent ) )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:366:1: (lv_alphabet_17_0= ruleEvent )
                    {
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:366:1: (lv_alphabet_17_0= ruleEvent )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:367:3: lv_alphabet_17_0= ruleEvent
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getLTSAccess().getAlphabetEventParserRuleCall_10_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleEvent_in_ruleLTS615);
                    lv_alphabet_17_0=ruleEvent();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getLTSRule());
                      	        }
                             		add(
                             			current, 
                             			"alphabet",
                              		lv_alphabet_17_0, 
                              		"Event");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:383:2: (otherlv_18= ',' ( (lv_alphabet_19_0= ruleEvent ) ) )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==15) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:383:4: otherlv_18= ',' ( (lv_alphabet_19_0= ruleEvent ) )
                    	    {
                    	    otherlv_18=(Token)match(input,15,FOLLOW_15_in_ruleLTS628); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_18, grammarAccess.getLTSAccess().getCommaKeyword_10_2_0());
                    	          
                    	    }
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:387:1: ( (lv_alphabet_19_0= ruleEvent ) )
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:388:1: (lv_alphabet_19_0= ruleEvent )
                    	    {
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:388:1: (lv_alphabet_19_0= ruleEvent )
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:389:3: lv_alphabet_19_0= ruleEvent
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getLTSAccess().getAlphabetEventParserRuleCall_10_2_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleEvent_in_ruleLTS649);
                    	    lv_alphabet_19_0=ruleEvent();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getLTSRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"alphabet",
                    	              		lv_alphabet_19_0, 
                    	              		"Event");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:405:6: ( (lv_transitions_20_0= ruleTransition ) )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==RULE_ID) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:406:1: (lv_transitions_20_0= ruleTransition )
            	    {
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:406:1: (lv_transitions_20_0= ruleTransition )
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:407:3: lv_transitions_20_0= ruleTransition
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getLTSAccess().getTransitionsTransitionParserRuleCall_11_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleTransition_in_ruleLTS674);
            	    lv_transitions_20_0=ruleTransition();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getLTSRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"transitions",
            	              		lv_transitions_20_0, 
            	              		"Transition");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            otherlv_21=(Token)match(input,19,FOLLOW_19_in_ruleLTS687); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_21, grammarAccess.getLTSAccess().getRightCurlyBracketKeyword_12());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTS"


    // $ANTLR start "entryRuleAtomicProposition"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:435:1: entryRuleAtomicProposition returns [EObject current=null] : iv_ruleAtomicProposition= ruleAtomicProposition EOF ;
    public final EObject entryRuleAtomicProposition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAtomicProposition = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:436:2: (iv_ruleAtomicProposition= ruleAtomicProposition EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:437:2: iv_ruleAtomicProposition= ruleAtomicProposition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAtomicPropositionRule()); 
            }
            pushFollow(FOLLOW_ruleAtomicProposition_in_entryRuleAtomicProposition723);
            iv_ruleAtomicProposition=ruleAtomicProposition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAtomicProposition; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAtomicProposition733); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAtomicProposition"


    // $ANTLR start "ruleAtomicProposition"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:444:1: ruleAtomicProposition returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleAtomicProposition() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:447:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:448:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:448:1: ( (lv_name_0_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:449:1: (lv_name_0_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:449:1: (lv_name_0_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:450:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleAtomicProposition774); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_0_0, grammarAccess.getAtomicPropositionAccess().getNameIDTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getAtomicPropositionRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"ID");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAtomicProposition"


    // $ANTLR start "entryRuleBasicState"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:474:1: entryRuleBasicState returns [EObject current=null] : iv_ruleBasicState= ruleBasicState EOF ;
    public final EObject entryRuleBasicState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBasicState = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:475:2: (iv_ruleBasicState= ruleBasicState EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:476:2: iv_ruleBasicState= ruleBasicState EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBasicStateRule()); 
            }
            pushFollow(FOLLOW_ruleBasicState_in_entryRuleBasicState814);
            iv_ruleBasicState=ruleBasicState();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBasicState; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBasicState824); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBasicState"


    // $ANTLR start "ruleBasicState"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:483:1: ruleBasicState returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '{' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '}' )* ) ;
    public final EObject ruleBasicState() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;

         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:486:28: ( ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '{' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '}' )* ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:487:1: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '{' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '}' )* )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:487:1: ( ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '{' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '}' )* )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:487:2: ( (lv_name_0_0= RULE_ID ) ) (otherlv_1= '{' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '}' )*
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:487:2: ( (lv_name_0_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:488:1: (lv_name_0_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:488:1: (lv_name_0_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:489:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleBasicState866); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_0_0, grammarAccess.getBasicStateAccess().getNameIDTerminalRuleCall_0_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBasicStateRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"ID");
              	    
            }

            }


            }

            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:505:2: (otherlv_1= '{' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '}' )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==12) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:505:4: otherlv_1= '{' ( (otherlv_2= RULE_ID ) ) (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )* otherlv_5= '}'
            	    {
            	    otherlv_1=(Token)match(input,12,FOLLOW_12_in_ruleBasicState884); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_1, grammarAccess.getBasicStateAccess().getLeftCurlyBracketKeyword_1_0());
            	          
            	    }
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:509:1: ( (otherlv_2= RULE_ID ) )
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:510:1: (otherlv_2= RULE_ID )
            	    {
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:510:1: (otherlv_2= RULE_ID )
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:511:3: otherlv_2= RULE_ID
            	    {
            	    if ( state.backtracking==0 ) {

            	      			if (current==null) {
            	      	            current = createModelElement(grammarAccess.getBasicStateRule());
            	      	        }
            	              
            	    }
            	    otherlv_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleBasicState904); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      		newLeafNode(otherlv_2, grammarAccess.getBasicStateAccess().getAtomicPropositionsAtomicPropositionCrossReference_1_1_0()); 
            	      	
            	    }

            	    }


            	    }

            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:522:2: (otherlv_3= ',' ( (otherlv_4= RULE_ID ) ) )*
            	    loop14:
            	    do {
            	        int alt14=2;
            	        int LA14_0 = input.LA(1);

            	        if ( (LA14_0==15) ) {
            	            alt14=1;
            	        }


            	        switch (alt14) {
            	    	case 1 :
            	    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:522:4: otherlv_3= ',' ( (otherlv_4= RULE_ID ) )
            	    	    {
            	    	    otherlv_3=(Token)match(input,15,FOLLOW_15_in_ruleBasicState917); if (state.failed) return current;
            	    	    if ( state.backtracking==0 ) {

            	    	          	newLeafNode(otherlv_3, grammarAccess.getBasicStateAccess().getCommaKeyword_1_2_0());
            	    	          
            	    	    }
            	    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:526:1: ( (otherlv_4= RULE_ID ) )
            	    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:527:1: (otherlv_4= RULE_ID )
            	    	    {
            	    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:527:1: (otherlv_4= RULE_ID )
            	    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:528:3: otherlv_4= RULE_ID
            	    	    {
            	    	    if ( state.backtracking==0 ) {

            	    	      			if (current==null) {
            	    	      	            current = createModelElement(grammarAccess.getBasicStateRule());
            	    	      	        }
            	    	              
            	    	    }
            	    	    otherlv_4=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleBasicState937); if (state.failed) return current;
            	    	    if ( state.backtracking==0 ) {

            	    	      		newLeafNode(otherlv_4, grammarAccess.getBasicStateAccess().getAtomicPropositionsAtomicPropositionCrossReference_1_2_1_0()); 
            	    	      	
            	    	    }

            	    	    }


            	    	    }


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop14;
            	        }
            	    } while (true);

            	    otherlv_5=(Token)match(input,19,FOLLOW_19_in_ruleBasicState951); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	          	newLeafNode(otherlv_5, grammarAccess.getBasicStateAccess().getRightCurlyBracketKeyword_1_3());
            	          
            	    }

            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBasicState"


    // $ANTLR start "entryRuleEvent"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:551:1: entryRuleEvent returns [EObject current=null] : iv_ruleEvent= ruleEvent EOF ;
    public final EObject entryRuleEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvent = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:552:2: (iv_ruleEvent= ruleEvent EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:553:2: iv_ruleEvent= ruleEvent EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEventRule()); 
            }
            pushFollow(FOLLOW_ruleEvent_in_entryRuleEvent989);
            iv_ruleEvent=ruleEvent();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEvent; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEvent999); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvent"


    // $ANTLR start "ruleEvent"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:560:1: ruleEvent returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleEvent() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:563:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:564:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:564:1: ( (lv_name_0_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:565:1: (lv_name_0_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:565:1: (lv_name_0_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:566:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleEvent1040); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_0_0, grammarAccess.getEventAccess().getNameIDTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getEventRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"ID");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvent"


    // $ANTLR start "entryRuleTransition"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:590:1: entryRuleTransition returns [EObject current=null] : iv_ruleTransition= ruleTransition EOF ;
    public final EObject entryRuleTransition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTransition = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:591:2: (iv_ruleTransition= ruleTransition EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:592:2: iv_ruleTransition= ruleTransition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTransitionRule()); 
            }
            pushFollow(FOLLOW_ruleTransition_in_entryRuleTransition1080);
            iv_ruleTransition=ruleTransition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTransition; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTransition1090); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTransition"


    // $ANTLR start "ruleTransition"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:599:1: ruleTransition returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) ) ;
    public final EObject ruleTransition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:602:28: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:603:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:603:1: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:603:2: ( (otherlv_0= RULE_ID ) ) otherlv_1= '->' ( (otherlv_2= RULE_ID ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:603:2: ( (otherlv_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:604:1: (otherlv_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:604:1: (otherlv_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:605:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getTransitionRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleTransition1135); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getTransitionAccess().getSourceStateCrossReference_0_0()); 
              	
            }

            }


            }

            otherlv_1=(Token)match(input,20,FOLLOW_20_in_ruleTransition1147); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getTransitionAccess().getHyphenMinusGreaterThanSignKeyword_1());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:620:1: ( (otherlv_2= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:621:1: (otherlv_2= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:621:1: (otherlv_2= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:622:3: otherlv_2= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getTransitionRule());
              	        }
                      
            }
            otherlv_2=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleTransition1167); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_2, grammarAccess.getTransitionAccess().getDestinationStateCrossReference_2_0()); 
              	
            }

            }


            }

            otherlv_3=(Token)match(input,21,FOLLOW_21_in_ruleTransition1179); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getTransitionAccess().getColonKeyword_3());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:637:1: ( (otherlv_4= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:638:1: (otherlv_4= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:638:1: (otherlv_4= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:639:3: otherlv_4= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getTransitionRule());
              	        }
                      
            }
            otherlv_4=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleTransition1199); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_4, grammarAccess.getTransitionAccess().getEventEventCrossReference_4_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTransition"


    // $ANTLR start "entryRuleComposedLTS"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:658:1: entryRuleComposedLTS returns [EObject current=null] : iv_ruleComposedLTS= ruleComposedLTS EOF ;
    public final EObject entryRuleComposedLTS() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComposedLTS = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:659:2: (iv_ruleComposedLTS= ruleComposedLTS EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:660:2: iv_ruleComposedLTS= ruleComposedLTS EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getComposedLTSRule()); 
            }
            pushFollow(FOLLOW_ruleComposedLTS_in_entryRuleComposedLTS1235);
            iv_ruleComposedLTS=ruleComposedLTS();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleComposedLTS; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleComposedLTS1245); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComposedLTS"


    // $ANTLR start "ruleComposedLTS"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:667:1: ruleComposedLTS returns [EObject current=null] : (otherlv_0= 'compose' ( (otherlv_1= RULE_ID ) ) otherlv_2= '||' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'as' ( (lv_name_5_0= RULE_ID ) ) (otherlv_6= 'using' otherlv_7= '{' ( (otherlv_8= RULE_ID ) ) (otherlv_9= ',' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= '}' )? ) ;
    public final EObject ruleComposedLTS() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_name_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;

         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:670:28: ( (otherlv_0= 'compose' ( (otherlv_1= RULE_ID ) ) otherlv_2= '||' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'as' ( (lv_name_5_0= RULE_ID ) ) (otherlv_6= 'using' otherlv_7= '{' ( (otherlv_8= RULE_ID ) ) (otherlv_9= ',' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= '}' )? ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:671:1: (otherlv_0= 'compose' ( (otherlv_1= RULE_ID ) ) otherlv_2= '||' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'as' ( (lv_name_5_0= RULE_ID ) ) (otherlv_6= 'using' otherlv_7= '{' ( (otherlv_8= RULE_ID ) ) (otherlv_9= ',' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= '}' )? )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:671:1: (otherlv_0= 'compose' ( (otherlv_1= RULE_ID ) ) otherlv_2= '||' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'as' ( (lv_name_5_0= RULE_ID ) ) (otherlv_6= 'using' otherlv_7= '{' ( (otherlv_8= RULE_ID ) ) (otherlv_9= ',' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= '}' )? )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:671:3: otherlv_0= 'compose' ( (otherlv_1= RULE_ID ) ) otherlv_2= '||' ( (otherlv_3= RULE_ID ) ) otherlv_4= 'as' ( (lv_name_5_0= RULE_ID ) ) (otherlv_6= 'using' otherlv_7= '{' ( (otherlv_8= RULE_ID ) ) (otherlv_9= ',' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= '}' )?
            {
            otherlv_0=(Token)match(input,22,FOLLOW_22_in_ruleComposedLTS1282); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getComposedLTSAccess().getComposeKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:675:1: ( (otherlv_1= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:676:1: (otherlv_1= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:676:1: (otherlv_1= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:677:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getComposedLTSRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleComposedLTS1302); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getComposedLTSAccess().getLtsLeftLTSCrossReference_1_0()); 
              	
            }

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_23_in_ruleComposedLTS1314); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getComposedLTSAccess().getVerticalLineVerticalLineKeyword_2());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:692:1: ( (otherlv_3= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:693:1: (otherlv_3= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:693:1: (otherlv_3= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:694:3: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getComposedLTSRule());
              	        }
                      
            }
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleComposedLTS1334); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_3, grammarAccess.getComposedLTSAccess().getLtsRightLTSCrossReference_3_0()); 
              	
            }

            }


            }

            otherlv_4=(Token)match(input,24,FOLLOW_24_in_ruleComposedLTS1346); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getComposedLTSAccess().getAsKeyword_4());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:709:1: ( (lv_name_5_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:710:1: (lv_name_5_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:710:1: (lv_name_5_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:711:3: lv_name_5_0= RULE_ID
            {
            lv_name_5_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleComposedLTS1363); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_5_0, grammarAccess.getComposedLTSAccess().getNameIDTerminalRuleCall_5_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getComposedLTSRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_5_0, 
                      		"ID");
              	    
            }

            }


            }

            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:727:2: (otherlv_6= 'using' otherlv_7= '{' ( (otherlv_8= RULE_ID ) ) (otherlv_9= ',' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= '}' )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==25) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:727:4: otherlv_6= 'using' otherlv_7= '{' ( (otherlv_8= RULE_ID ) ) (otherlv_9= ',' ( (otherlv_10= RULE_ID ) ) )* otherlv_11= '}'
                    {
                    otherlv_6=(Token)match(input,25,FOLLOW_25_in_ruleComposedLTS1381); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_6, grammarAccess.getComposedLTSAccess().getUsingKeyword_6_0());
                          
                    }
                    otherlv_7=(Token)match(input,12,FOLLOW_12_in_ruleComposedLTS1393); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_7, grammarAccess.getComposedLTSAccess().getLeftCurlyBracketKeyword_6_1());
                          
                    }
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:735:1: ( (otherlv_8= RULE_ID ) )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:736:1: (otherlv_8= RULE_ID )
                    {
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:736:1: (otherlv_8= RULE_ID )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:737:3: otherlv_8= RULE_ID
                    {
                    if ( state.backtracking==0 ) {

                      			if (current==null) {
                      	            current = createModelElement(grammarAccess.getComposedLTSRule());
                      	        }
                              
                    }
                    otherlv_8=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleComposedLTS1413); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      		newLeafNode(otherlv_8, grammarAccess.getComposedLTSAccess().getEventsEventCrossReference_6_2_0()); 
                      	
                    }

                    }


                    }

                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:748:2: (otherlv_9= ',' ( (otherlv_10= RULE_ID ) ) )*
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==15) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:748:4: otherlv_9= ',' ( (otherlv_10= RULE_ID ) )
                    	    {
                    	    otherlv_9=(Token)match(input,15,FOLLOW_15_in_ruleComposedLTS1426); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	          	newLeafNode(otherlv_9, grammarAccess.getComposedLTSAccess().getCommaKeyword_6_3_0());
                    	          
                    	    }
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:752:1: ( (otherlv_10= RULE_ID ) )
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:753:1: (otherlv_10= RULE_ID )
                    	    {
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:753:1: (otherlv_10= RULE_ID )
                    	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:754:3: otherlv_10= RULE_ID
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      			if (current==null) {
                    	      	            current = createModelElement(grammarAccess.getComposedLTSRule());
                    	      	        }
                    	              
                    	    }
                    	    otherlv_10=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleComposedLTS1446); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      		newLeafNode(otherlv_10, grammarAccess.getComposedLTSAccess().getEventsEventCrossReference_6_3_1_0()); 
                    	      	
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop16;
                        }
                    } while (true);

                    otherlv_11=(Token)match(input,19,FOLLOW_19_in_ruleComposedLTS1460); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_11, grammarAccess.getComposedLTSAccess().getRightCurlyBracketKeyword_6_4());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComposedLTS"


    // $ANTLR start "entryRuleFormulaSet"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:777:1: entryRuleFormulaSet returns [EObject current=null] : iv_ruleFormulaSet= ruleFormulaSet EOF ;
    public final EObject entryRuleFormulaSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFormulaSet = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:778:2: (iv_ruleFormulaSet= ruleFormulaSet EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:779:2: iv_ruleFormulaSet= ruleFormulaSet EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFormulaSetRule()); 
            }
            pushFollow(FOLLOW_ruleFormulaSet_in_entryRuleFormulaSet1498);
            iv_ruleFormulaSet=ruleFormulaSet();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFormulaSet; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFormulaSet1508); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFormulaSet"


    // $ANTLR start "ruleFormulaSet"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:786:1: ruleFormulaSet returns [EObject current=null] : (otherlv_0= 'CTL' otherlv_1= 'FormulaSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_formulae_4_0= ruleFormulaDefinition ) )* otherlv_5= '}' ) ;
    public final EObject ruleFormulaSet() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_formulae_4_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:789:28: ( (otherlv_0= 'CTL' otherlv_1= 'FormulaSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_formulae_4_0= ruleFormulaDefinition ) )* otherlv_5= '}' ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:790:1: (otherlv_0= 'CTL' otherlv_1= 'FormulaSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_formulae_4_0= ruleFormulaDefinition ) )* otherlv_5= '}' )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:790:1: (otherlv_0= 'CTL' otherlv_1= 'FormulaSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_formulae_4_0= ruleFormulaDefinition ) )* otherlv_5= '}' )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:790:3: otherlv_0= 'CTL' otherlv_1= 'FormulaSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_formulae_4_0= ruleFormulaDefinition ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,26,FOLLOW_26_in_ruleFormulaSet1545); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getFormulaSetAccess().getCTLKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,27,FOLLOW_27_in_ruleFormulaSet1557); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getFormulaSetAccess().getFormulaSetKeyword_1());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:798:1: ( (lv_name_2_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:799:1: (lv_name_2_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:799:1: (lv_name_2_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:800:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFormulaSet1574); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getFormulaSetAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getFormulaSetRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_12_in_ruleFormulaSet1591); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getFormulaSetAccess().getLeftCurlyBracketKeyword_3());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:820:1: ( (lv_formulae_4_0= ruleFormulaDefinition ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==RULE_ID) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:821:1: (lv_formulae_4_0= ruleFormulaDefinition )
            	    {
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:821:1: (lv_formulae_4_0= ruleFormulaDefinition )
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:822:3: lv_formulae_4_0= ruleFormulaDefinition
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getFormulaSetAccess().getFormulaeFormulaDefinitionParserRuleCall_4_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleFormulaDefinition_in_ruleFormulaSet1612);
            	    lv_formulae_4_0=ruleFormulaDefinition();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getFormulaSetRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"formulae",
            	              		lv_formulae_4_0, 
            	              		"FormulaDefinition");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            otherlv_5=(Token)match(input,19,FOLLOW_19_in_ruleFormulaSet1625); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getFormulaSetAccess().getRightCurlyBracketKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFormulaSet"


    // $ANTLR start "entryRuleFormulaDefinition"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:850:1: entryRuleFormulaDefinition returns [EObject current=null] : iv_ruleFormulaDefinition= ruleFormulaDefinition EOF ;
    public final EObject entryRuleFormulaDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFormulaDefinition = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:851:2: (iv_ruleFormulaDefinition= ruleFormulaDefinition EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:852:2: iv_ruleFormulaDefinition= ruleFormulaDefinition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFormulaDefinitionRule()); 
            }
            pushFollow(FOLLOW_ruleFormulaDefinition_in_entryRuleFormulaDefinition1661);
            iv_ruleFormulaDefinition=ruleFormulaDefinition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFormulaDefinition; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFormulaDefinition1671); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFormulaDefinition"


    // $ANTLR start "ruleFormulaDefinition"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:859:1: ruleFormulaDefinition returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':=' ( (lv_formula_2_0= ruleFormula ) ) ) ;
    public final EObject ruleFormulaDefinition() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        EObject lv_formula_2_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:862:28: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':=' ( (lv_formula_2_0= ruleFormula ) ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:863:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':=' ( (lv_formula_2_0= ruleFormula ) ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:863:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':=' ( (lv_formula_2_0= ruleFormula ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:863:2: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':=' ( (lv_formula_2_0= ruleFormula ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:863:2: ( (lv_name_0_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:864:1: (lv_name_0_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:864:1: (lv_name_0_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:865:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFormulaDefinition1713); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_0_0, grammarAccess.getFormulaDefinitionAccess().getNameIDTerminalRuleCall_0_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getFormulaDefinitionRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,28,FOLLOW_28_in_ruleFormulaDefinition1730); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getFormulaDefinitionAccess().getColonEqualsSignKeyword_1());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:885:1: ( (lv_formula_2_0= ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:886:1: (lv_formula_2_0= ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:886:1: (lv_formula_2_0= ruleFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:887:3: lv_formula_2_0= ruleFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getFormulaDefinitionAccess().getFormulaFormulaParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFormula_in_ruleFormulaDefinition1751);
            lv_formula_2_0=ruleFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getFormulaDefinitionRule());
              	        }
                     		set(
                     			current, 
                     			"formula",
                      		lv_formula_2_0, 
                      		"Formula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFormulaDefinition"


    // $ANTLR start "entryRuleFormula"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:911:1: entryRuleFormula returns [EObject current=null] : iv_ruleFormula= ruleFormula EOF ;
    public final EObject entryRuleFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFormula = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:912:2: (iv_ruleFormula= ruleFormula EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:913:2: iv_ruleFormula= ruleFormula EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFormulaRule()); 
            }
            pushFollow(FOLLOW_ruleFormula_in_entryRuleFormula1787);
            iv_ruleFormula=ruleFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFormula; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleFormula1797); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFormula"


    // $ANTLR start "ruleFormula"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:920:1: ruleFormula returns [EObject current=null] : (this_EX_0= ruleEX | this_EG_1= ruleEG | this_EU_2= ruleEU | this_NOT_3= ruleNOT | ( ( ruleAND )=>this_AND_4= ruleAND ) | this_OR_5= ruleOR | this_Proposition_6= ruleProposition | this_Constant_7= ruleConstant ) ;
    public final EObject ruleFormula() throws RecognitionException {
        EObject current = null;

        EObject this_EX_0 = null;

        EObject this_EG_1 = null;

        EObject this_EU_2 = null;

        EObject this_NOT_3 = null;

        EObject this_AND_4 = null;

        EObject this_OR_5 = null;

        EObject this_Proposition_6 = null;

        EObject this_Constant_7 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:923:28: ( (this_EX_0= ruleEX | this_EG_1= ruleEG | this_EU_2= ruleEU | this_NOT_3= ruleNOT | ( ( ruleAND )=>this_AND_4= ruleAND ) | this_OR_5= ruleOR | this_Proposition_6= ruleProposition | this_Constant_7= ruleConstant ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:924:1: (this_EX_0= ruleEX | this_EG_1= ruleEG | this_EU_2= ruleEU | this_NOT_3= ruleNOT | ( ( ruleAND )=>this_AND_4= ruleAND ) | this_OR_5= ruleOR | this_Proposition_6= ruleProposition | this_Constant_7= ruleConstant )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:924:1: (this_EX_0= ruleEX | this_EG_1= ruleEG | this_EU_2= ruleEU | this_NOT_3= ruleNOT | ( ( ruleAND )=>this_AND_4= ruleAND ) | this_OR_5= ruleOR | this_Proposition_6= ruleProposition | this_Constant_7= ruleConstant )
            int alt19=8;
            alt19 = dfa19.predict(input);
            switch (alt19) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:925:5: this_EX_0= ruleEX
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFormulaAccess().getEXParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleEX_in_ruleFormula1844);
                    this_EX_0=ruleEX();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EX_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:935:5: this_EG_1= ruleEG
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFormulaAccess().getEGParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleEG_in_ruleFormula1871);
                    this_EG_1=ruleEG();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EG_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:945:5: this_EU_2= ruleEU
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFormulaAccess().getEUParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleEU_in_ruleFormula1898);
                    this_EU_2=ruleEU();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_EU_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:955:5: this_NOT_3= ruleNOT
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFormulaAccess().getNOTParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleNOT_in_ruleFormula1925);
                    this_NOT_3=ruleNOT();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NOT_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:964:6: ( ( ruleAND )=>this_AND_4= ruleAND )
                    {
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:964:6: ( ( ruleAND )=>this_AND_4= ruleAND )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:964:7: ( ruleAND )=>this_AND_4= ruleAND
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFormulaAccess().getANDParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleAND_in_ruleFormula1958);
                    this_AND_4=ruleAND();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_AND_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }


                    }
                    break;
                case 6 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:975:5: this_OR_5= ruleOR
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFormulaAccess().getORParserRuleCall_5()); 
                          
                    }
                    pushFollow(FOLLOW_ruleOR_in_ruleFormula1986);
                    this_OR_5=ruleOR();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_OR_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:985:5: this_Proposition_6= ruleProposition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFormulaAccess().getPropositionParserRuleCall_6()); 
                          
                    }
                    pushFollow(FOLLOW_ruleProposition_in_ruleFormula2013);
                    this_Proposition_6=ruleProposition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Proposition_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 8 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:995:5: this_Constant_7= ruleConstant
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getFormulaAccess().getConstantParserRuleCall_7()); 
                          
                    }
                    pushFollow(FOLLOW_ruleConstant_in_ruleFormula2040);
                    this_Constant_7=ruleConstant();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Constant_7; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFormula"


    // $ANTLR start "entryRuleEX"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1011:1: entryRuleEX returns [EObject current=null] : iv_ruleEX= ruleEX EOF ;
    public final EObject entryRuleEX() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEX = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1012:2: (iv_ruleEX= ruleEX EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1013:2: iv_ruleEX= ruleEX EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEXRule()); 
            }
            pushFollow(FOLLOW_ruleEX_in_entryRuleEX2075);
            iv_ruleEX=ruleEX();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEX; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEX2085); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEX"


    // $ANTLR start "ruleEX"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1020:1: ruleEX returns [EObject current=null] : (otherlv_0= 'EX' ( (lv_nested_1_0= ruleFormula ) ) ) ;
    public final EObject ruleEX() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_nested_1_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1023:28: ( (otherlv_0= 'EX' ( (lv_nested_1_0= ruleFormula ) ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1024:1: (otherlv_0= 'EX' ( (lv_nested_1_0= ruleFormula ) ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1024:1: (otherlv_0= 'EX' ( (lv_nested_1_0= ruleFormula ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1024:3: otherlv_0= 'EX' ( (lv_nested_1_0= ruleFormula ) )
            {
            otherlv_0=(Token)match(input,29,FOLLOW_29_in_ruleEX2122); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getEXAccess().getEXKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1028:1: ( (lv_nested_1_0= ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1029:1: (lv_nested_1_0= ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1029:1: (lv_nested_1_0= ruleFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1030:3: lv_nested_1_0= ruleFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEXAccess().getNestedFormulaParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFormula_in_ruleEX2143);
            lv_nested_1_0=ruleFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getEXRule());
              	        }
                     		set(
                     			current, 
                     			"nested",
                      		lv_nested_1_0, 
                      		"Formula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEX"


    // $ANTLR start "entryRuleEG"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1054:1: entryRuleEG returns [EObject current=null] : iv_ruleEG= ruleEG EOF ;
    public final EObject entryRuleEG() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEG = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1055:2: (iv_ruleEG= ruleEG EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1056:2: iv_ruleEG= ruleEG EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEGRule()); 
            }
            pushFollow(FOLLOW_ruleEG_in_entryRuleEG2179);
            iv_ruleEG=ruleEG();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEG; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEG2189); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEG"


    // $ANTLR start "ruleEG"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1063:1: ruleEG returns [EObject current=null] : (otherlv_0= 'EG' ( (lv_nested_1_0= ruleFormula ) ) ) ;
    public final EObject ruleEG() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_nested_1_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1066:28: ( (otherlv_0= 'EG' ( (lv_nested_1_0= ruleFormula ) ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1067:1: (otherlv_0= 'EG' ( (lv_nested_1_0= ruleFormula ) ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1067:1: (otherlv_0= 'EG' ( (lv_nested_1_0= ruleFormula ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1067:3: otherlv_0= 'EG' ( (lv_nested_1_0= ruleFormula ) )
            {
            otherlv_0=(Token)match(input,30,FOLLOW_30_in_ruleEG2226); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getEGAccess().getEGKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1071:1: ( (lv_nested_1_0= ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1072:1: (lv_nested_1_0= ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1072:1: (lv_nested_1_0= ruleFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1073:3: lv_nested_1_0= ruleFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEGAccess().getNestedFormulaParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFormula_in_ruleEG2247);
            lv_nested_1_0=ruleFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getEGRule());
              	        }
                     		set(
                     			current, 
                     			"nested",
                      		lv_nested_1_0, 
                      		"Formula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEG"


    // $ANTLR start "entryRuleEU"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1097:1: entryRuleEU returns [EObject current=null] : iv_ruleEU= ruleEU EOF ;
    public final EObject entryRuleEU() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEU = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1098:2: (iv_ruleEU= ruleEU EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1099:2: iv_ruleEU= ruleEU EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEURule()); 
            }
            pushFollow(FOLLOW_ruleEU_in_entryRuleEU2283);
            iv_ruleEU=ruleEU();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEU; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEU2293); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEU"


    // $ANTLR start "ruleEU"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1106:1: ruleEU returns [EObject current=null] : (otherlv_0= 'E' otherlv_1= '[' ( (lv_left_2_0= ruleFormula ) ) otherlv_3= 'U' ( (lv_right_4_0= ruleFormula ) ) otherlv_5= ']' ) ;
    public final EObject ruleEU() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_left_2_0 = null;

        EObject lv_right_4_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1109:28: ( (otherlv_0= 'E' otherlv_1= '[' ( (lv_left_2_0= ruleFormula ) ) otherlv_3= 'U' ( (lv_right_4_0= ruleFormula ) ) otherlv_5= ']' ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1110:1: (otherlv_0= 'E' otherlv_1= '[' ( (lv_left_2_0= ruleFormula ) ) otherlv_3= 'U' ( (lv_right_4_0= ruleFormula ) ) otherlv_5= ']' )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1110:1: (otherlv_0= 'E' otherlv_1= '[' ( (lv_left_2_0= ruleFormula ) ) otherlv_3= 'U' ( (lv_right_4_0= ruleFormula ) ) otherlv_5= ']' )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1110:3: otherlv_0= 'E' otherlv_1= '[' ( (lv_left_2_0= ruleFormula ) ) otherlv_3= 'U' ( (lv_right_4_0= ruleFormula ) ) otherlv_5= ']'
            {
            otherlv_0=(Token)match(input,31,FOLLOW_31_in_ruleEU2330); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getEUAccess().getEKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,32,FOLLOW_32_in_ruleEU2342); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getEUAccess().getLeftSquareBracketKeyword_1());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1118:1: ( (lv_left_2_0= ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1119:1: (lv_left_2_0= ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1119:1: (lv_left_2_0= ruleFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1120:3: lv_left_2_0= ruleFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEUAccess().getLeftFormulaParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFormula_in_ruleEU2363);
            lv_left_2_0=ruleFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getEURule());
              	        }
                     		set(
                     			current, 
                     			"left",
                      		lv_left_2_0, 
                      		"Formula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,33,FOLLOW_33_in_ruleEU2375); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getEUAccess().getUKeyword_3());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1140:1: ( (lv_right_4_0= ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1141:1: (lv_right_4_0= ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1141:1: (lv_right_4_0= ruleFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1142:3: lv_right_4_0= ruleFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEUAccess().getRightFormulaParserRuleCall_4_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFormula_in_ruleEU2396);
            lv_right_4_0=ruleFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getEURule());
              	        }
                     		set(
                     			current, 
                     			"right",
                      		lv_right_4_0, 
                      		"Formula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_5=(Token)match(input,34,FOLLOW_34_in_ruleEU2408); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getEUAccess().getRightSquareBracketKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEU"


    // $ANTLR start "entryRuleAND"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1170:1: entryRuleAND returns [EObject current=null] : iv_ruleAND= ruleAND EOF ;
    public final EObject entryRuleAND() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAND = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1171:2: (iv_ruleAND= ruleAND EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1172:2: iv_ruleAND= ruleAND EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getANDRule()); 
            }
            pushFollow(FOLLOW_ruleAND_in_entryRuleAND2444);
            iv_ruleAND=ruleAND();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAND; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAND2454); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAND"


    // $ANTLR start "ruleAND"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1179:1: ruleAND returns [EObject current=null] : (otherlv_0= '(' ( (lv_left_1_0= ruleFormula ) ) otherlv_2= '\\u22C0' ( (lv_right_3_0= ruleFormula ) ) otherlv_4= ')' ) ;
    public final EObject ruleAND() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_left_1_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1182:28: ( (otherlv_0= '(' ( (lv_left_1_0= ruleFormula ) ) otherlv_2= '\\u22C0' ( (lv_right_3_0= ruleFormula ) ) otherlv_4= ')' ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1183:1: (otherlv_0= '(' ( (lv_left_1_0= ruleFormula ) ) otherlv_2= '\\u22C0' ( (lv_right_3_0= ruleFormula ) ) otherlv_4= ')' )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1183:1: (otherlv_0= '(' ( (lv_left_1_0= ruleFormula ) ) otherlv_2= '\\u22C0' ( (lv_right_3_0= ruleFormula ) ) otherlv_4= ')' )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1183:3: otherlv_0= '(' ( (lv_left_1_0= ruleFormula ) ) otherlv_2= '\\u22C0' ( (lv_right_3_0= ruleFormula ) ) otherlv_4= ')'
            {
            otherlv_0=(Token)match(input,35,FOLLOW_35_in_ruleAND2491); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getANDAccess().getLeftParenthesisKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1187:1: ( (lv_left_1_0= ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1188:1: (lv_left_1_0= ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1188:1: (lv_left_1_0= ruleFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1189:3: lv_left_1_0= ruleFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getANDAccess().getLeftFormulaParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFormula_in_ruleAND2512);
            lv_left_1_0=ruleFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getANDRule());
              	        }
                     		set(
                     			current, 
                     			"left",
                      		lv_left_1_0, 
                      		"Formula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,36,FOLLOW_36_in_ruleAND2524); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getANDAccess().getNAryLogicalAndKeyword_2());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1209:1: ( (lv_right_3_0= ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1210:1: (lv_right_3_0= ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1210:1: (lv_right_3_0= ruleFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1211:3: lv_right_3_0= ruleFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getANDAccess().getRightFormulaParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFormula_in_ruleAND2545);
            lv_right_3_0=ruleFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getANDRule());
              	        }
                     		set(
                     			current, 
                     			"right",
                      		lv_right_3_0, 
                      		"Formula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,37,FOLLOW_37_in_ruleAND2557); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getANDAccess().getRightParenthesisKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAND"


    // $ANTLR start "entryRuleOR"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1239:1: entryRuleOR returns [EObject current=null] : iv_ruleOR= ruleOR EOF ;
    public final EObject entryRuleOR() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOR = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1240:2: (iv_ruleOR= ruleOR EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1241:2: iv_ruleOR= ruleOR EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getORRule()); 
            }
            pushFollow(FOLLOW_ruleOR_in_entryRuleOR2593);
            iv_ruleOR=ruleOR();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOR; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOR2603); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOR"


    // $ANTLR start "ruleOR"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1248:1: ruleOR returns [EObject current=null] : (otherlv_0= '(' ( (lv_left_1_0= ruleFormula ) ) otherlv_2= '\\u22C1' ( (lv_right_3_0= ruleFormula ) ) otherlv_4= ')' ) ;
    public final EObject ruleOR() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_left_1_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1251:28: ( (otherlv_0= '(' ( (lv_left_1_0= ruleFormula ) ) otherlv_2= '\\u22C1' ( (lv_right_3_0= ruleFormula ) ) otherlv_4= ')' ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1252:1: (otherlv_0= '(' ( (lv_left_1_0= ruleFormula ) ) otherlv_2= '\\u22C1' ( (lv_right_3_0= ruleFormula ) ) otherlv_4= ')' )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1252:1: (otherlv_0= '(' ( (lv_left_1_0= ruleFormula ) ) otherlv_2= '\\u22C1' ( (lv_right_3_0= ruleFormula ) ) otherlv_4= ')' )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1252:3: otherlv_0= '(' ( (lv_left_1_0= ruleFormula ) ) otherlv_2= '\\u22C1' ( (lv_right_3_0= ruleFormula ) ) otherlv_4= ')'
            {
            otherlv_0=(Token)match(input,35,FOLLOW_35_in_ruleOR2640); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getORAccess().getLeftParenthesisKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1256:1: ( (lv_left_1_0= ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1257:1: (lv_left_1_0= ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1257:1: (lv_left_1_0= ruleFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1258:3: lv_left_1_0= ruleFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getORAccess().getLeftFormulaParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFormula_in_ruleOR2661);
            lv_left_1_0=ruleFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getORRule());
              	        }
                     		set(
                     			current, 
                     			"left",
                      		lv_left_1_0, 
                      		"Formula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,38,FOLLOW_38_in_ruleOR2673); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getORAccess().getNAryLogicalOrKeyword_2());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1278:1: ( (lv_right_3_0= ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1279:1: (lv_right_3_0= ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1279:1: (lv_right_3_0= ruleFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1280:3: lv_right_3_0= ruleFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getORAccess().getRightFormulaParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFormula_in_ruleOR2694);
            lv_right_3_0=ruleFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getORRule());
              	        }
                     		set(
                     			current, 
                     			"right",
                      		lv_right_3_0, 
                      		"Formula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,37,FOLLOW_37_in_ruleOR2706); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getORAccess().getRightParenthesisKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOR"


    // $ANTLR start "entryRuleNOT"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1308:1: entryRuleNOT returns [EObject current=null] : iv_ruleNOT= ruleNOT EOF ;
    public final EObject entryRuleNOT() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNOT = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1309:2: (iv_ruleNOT= ruleNOT EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1310:2: iv_ruleNOT= ruleNOT EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNOTRule()); 
            }
            pushFollow(FOLLOW_ruleNOT_in_entryRuleNOT2742);
            iv_ruleNOT=ruleNOT();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNOT; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNOT2752); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNOT"


    // $ANTLR start "ruleNOT"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1317:1: ruleNOT returns [EObject current=null] : (otherlv_0= '\\u00AC' ( (lv_nested_1_0= ruleFormula ) ) ) ;
    public final EObject ruleNOT() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_nested_1_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1320:28: ( (otherlv_0= '\\u00AC' ( (lv_nested_1_0= ruleFormula ) ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1321:1: (otherlv_0= '\\u00AC' ( (lv_nested_1_0= ruleFormula ) ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1321:1: (otherlv_0= '\\u00AC' ( (lv_nested_1_0= ruleFormula ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1321:3: otherlv_0= '\\u00AC' ( (lv_nested_1_0= ruleFormula ) )
            {
            otherlv_0=(Token)match(input,39,FOLLOW_39_in_ruleNOT2789); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getNOTAccess().getNotSignKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1325:1: ( (lv_nested_1_0= ruleFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1326:1: (lv_nested_1_0= ruleFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1326:1: (lv_nested_1_0= ruleFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1327:3: lv_nested_1_0= ruleFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getNOTAccess().getNestedFormulaParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleFormula_in_ruleNOT2810);
            lv_nested_1_0=ruleFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getNOTRule());
              	        }
                     		set(
                     			current, 
                     			"nested",
                      		lv_nested_1_0, 
                      		"Formula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNOT"


    // $ANTLR start "entryRuleProposition"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1351:1: entryRuleProposition returns [EObject current=null] : iv_ruleProposition= ruleProposition EOF ;
    public final EObject entryRuleProposition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProposition = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1352:2: (iv_ruleProposition= ruleProposition EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1353:2: iv_ruleProposition= ruleProposition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPropositionRule()); 
            }
            pushFollow(FOLLOW_ruleProposition_in_entryRuleProposition2846);
            iv_ruleProposition=ruleProposition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleProposition; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleProposition2856); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProposition"


    // $ANTLR start "ruleProposition"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1360:1: ruleProposition returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleProposition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1363:28: ( ( (otherlv_0= RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1364:1: ( (otherlv_0= RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1364:1: ( (otherlv_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1365:1: (otherlv_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1365:1: (otherlv_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1366:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getPropositionRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleProposition2900); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getPropositionAccess().getAtomicPropositionAtomicPropositionCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProposition"


    // $ANTLR start "entryRuleConstant"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1385:1: entryRuleConstant returns [EObject current=null] : iv_ruleConstant= ruleConstant EOF ;
    public final EObject entryRuleConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstant = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1386:2: (iv_ruleConstant= ruleConstant EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1387:2: iv_ruleConstant= ruleConstant EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getConstantRule()); 
            }
            pushFollow(FOLLOW_ruleConstant_in_entryRuleConstant2935);
            iv_ruleConstant=ruleConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleConstant; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleConstant2945); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstant"


    // $ANTLR start "ruleConstant"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1394:1: ruleConstant returns [EObject current=null] : ( () ( ( (lv_value_1_0= 'true' ) ) | otherlv_2= 'false' ) ) ;
    public final EObject ruleConstant() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1397:28: ( ( () ( ( (lv_value_1_0= 'true' ) ) | otherlv_2= 'false' ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1398:1: ( () ( ( (lv_value_1_0= 'true' ) ) | otherlv_2= 'false' ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1398:1: ( () ( ( (lv_value_1_0= 'true' ) ) | otherlv_2= 'false' ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1398:2: () ( ( (lv_value_1_0= 'true' ) ) | otherlv_2= 'false' )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1398:2: ()
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1399:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getConstantAccess().getConstantAction_0(),
                          current);
                  
            }

            }

            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1404:2: ( ( (lv_value_1_0= 'true' ) ) | otherlv_2= 'false' )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==40) ) {
                alt20=1;
            }
            else if ( (LA20_0==41) ) {
                alt20=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1404:3: ( (lv_value_1_0= 'true' ) )
                    {
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1404:3: ( (lv_value_1_0= 'true' ) )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1405:1: (lv_value_1_0= 'true' )
                    {
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1405:1: (lv_value_1_0= 'true' )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1406:3: lv_value_1_0= 'true'
                    {
                    lv_value_1_0=(Token)match(input,40,FOLLOW_40_in_ruleConstant2998); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_value_1_0, grammarAccess.getConstantAccess().getValueTrueKeyword_1_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getConstantRule());
                      	        }
                             		setWithLastConsumed(current, "value", true, "true");
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1420:7: otherlv_2= 'false'
                    {
                    otherlv_2=(Token)match(input,41,FOLLOW_41_in_ruleConstant3029); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getConstantAccess().getFalseKeyword_1_1());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstant"


    // $ANTLR start "entryRuleCheckSet"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1432:1: entryRuleCheckSet returns [EObject current=null] : iv_ruleCheckSet= ruleCheckSet EOF ;
    public final EObject entryRuleCheckSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCheckSet = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1433:2: (iv_ruleCheckSet= ruleCheckSet EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1434:2: iv_ruleCheckSet= ruleCheckSet EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCheckSetRule()); 
            }
            pushFollow(FOLLOW_ruleCheckSet_in_entryRuleCheckSet3066);
            iv_ruleCheckSet=ruleCheckSet();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCheckSet; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheckSet3076); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCheckSet"


    // $ANTLR start "ruleCheckSet"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1441:1: ruleCheckSet returns [EObject current=null] : (otherlv_0= 'CTL' otherlv_1= 'CheckSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_checks_4_0= ruleCheck ) )* otherlv_5= '}' ) ;
    public final EObject ruleCheckSet() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_checks_4_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1444:28: ( (otherlv_0= 'CTL' otherlv_1= 'CheckSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_checks_4_0= ruleCheck ) )* otherlv_5= '}' ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1445:1: (otherlv_0= 'CTL' otherlv_1= 'CheckSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_checks_4_0= ruleCheck ) )* otherlv_5= '}' )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1445:1: (otherlv_0= 'CTL' otherlv_1= 'CheckSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_checks_4_0= ruleCheck ) )* otherlv_5= '}' )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1445:3: otherlv_0= 'CTL' otherlv_1= 'CheckSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_checks_4_0= ruleCheck ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,26,FOLLOW_26_in_ruleCheckSet3113); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getCheckSetAccess().getCTLKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,42,FOLLOW_42_in_ruleCheckSet3125); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getCheckSetAccess().getCheckSetKeyword_1());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1453:1: ( (lv_name_2_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1454:1: (lv_name_2_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1454:1: (lv_name_2_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1455:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleCheckSet3142); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getCheckSetAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getCheckSetRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_12_in_ruleCheckSet3159); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getCheckSetAccess().getLeftCurlyBracketKeyword_3());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1475:1: ( (lv_checks_4_0= ruleCheck ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==43) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1476:1: (lv_checks_4_0= ruleCheck )
            	    {
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1476:1: (lv_checks_4_0= ruleCheck )
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1477:3: lv_checks_4_0= ruleCheck
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getCheckSetAccess().getChecksCheckParserRuleCall_4_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleCheck_in_ruleCheckSet3180);
            	    lv_checks_4_0=ruleCheck();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getCheckSetRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"checks",
            	              		lv_checks_4_0, 
            	              		"Check");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

            otherlv_5=(Token)match(input,19,FOLLOW_19_in_ruleCheckSet3193); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getCheckSetAccess().getRightCurlyBracketKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCheckSet"


    // $ANTLR start "entryRuleCheck"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1505:1: entryRuleCheck returns [EObject current=null] : iv_ruleCheck= ruleCheck EOF ;
    public final EObject entryRuleCheck() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCheck = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1506:2: (iv_ruleCheck= ruleCheck EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1507:2: iv_ruleCheck= ruleCheck EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCheckRule()); 
            }
            pushFollow(FOLLOW_ruleCheck_in_entryRuleCheck3229);
            iv_ruleCheck=ruleCheck();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCheck; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheck3239); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCheck"


    // $ANTLR start "ruleCheck"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1514:1: ruleCheck returns [EObject current=null] : (otherlv_0= 'check' ( (otherlv_1= RULE_ID ) ) otherlv_2= '\\u22A8' ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleCheck() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1517:28: ( (otherlv_0= 'check' ( (otherlv_1= RULE_ID ) ) otherlv_2= '\\u22A8' ( (otherlv_3= RULE_ID ) ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1518:1: (otherlv_0= 'check' ( (otherlv_1= RULE_ID ) ) otherlv_2= '\\u22A8' ( (otherlv_3= RULE_ID ) ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1518:1: (otherlv_0= 'check' ( (otherlv_1= RULE_ID ) ) otherlv_2= '\\u22A8' ( (otherlv_3= RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1518:3: otherlv_0= 'check' ( (otherlv_1= RULE_ID ) ) otherlv_2= '\\u22A8' ( (otherlv_3= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,43,FOLLOW_43_in_ruleCheck3276); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getCheckAccess().getCheckKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1522:1: ( (otherlv_1= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1523:1: (otherlv_1= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1523:1: (otherlv_1= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1524:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getCheckRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleCheck3296); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getCheckAccess().getLtsLTSCrossReference_1_0()); 
              	
            }

            }


            }

            otherlv_2=(Token)match(input,44,FOLLOW_44_in_ruleCheck3308); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getCheckAccess().getTrueKeyword_2());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1539:1: ( (otherlv_3= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1540:1: (otherlv_3= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1540:1: (otherlv_3= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1541:3: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getCheckRule());
              	        }
                      
            }
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleCheck3328); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_3, grammarAccess.getCheckAccess().getFormulaDefinitionFormulaDefinitionCrossReference_3_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCheck"


    // $ANTLR start "entryRuleLTLFormulaSet"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1560:1: entryRuleLTLFormulaSet returns [EObject current=null] : iv_ruleLTLFormulaSet= ruleLTLFormulaSet EOF ;
    public final EObject entryRuleLTLFormulaSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLFormulaSet = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1561:2: (iv_ruleLTLFormulaSet= ruleLTLFormulaSet EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1562:2: iv_ruleLTLFormulaSet= ruleLTLFormulaSet EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLFormulaSetRule()); 
            }
            pushFollow(FOLLOW_ruleLTLFormulaSet_in_entryRuleLTLFormulaSet3364);
            iv_ruleLTLFormulaSet=ruleLTLFormulaSet();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLFormulaSet; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLFormulaSet3374); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLFormulaSet"


    // $ANTLR start "ruleLTLFormulaSet"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1569:1: ruleLTLFormulaSet returns [EObject current=null] : (otherlv_0= 'LTL' otherlv_1= 'FormulaSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_formulae_4_0= ruleLTLFormulaDefinition ) )* otherlv_5= '}' ) ;
    public final EObject ruleLTLFormulaSet() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_formulae_4_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1572:28: ( (otherlv_0= 'LTL' otherlv_1= 'FormulaSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_formulae_4_0= ruleLTLFormulaDefinition ) )* otherlv_5= '}' ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1573:1: (otherlv_0= 'LTL' otherlv_1= 'FormulaSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_formulae_4_0= ruleLTLFormulaDefinition ) )* otherlv_5= '}' )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1573:1: (otherlv_0= 'LTL' otherlv_1= 'FormulaSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_formulae_4_0= ruleLTLFormulaDefinition ) )* otherlv_5= '}' )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1573:3: otherlv_0= 'LTL' otherlv_1= 'FormulaSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_formulae_4_0= ruleLTLFormulaDefinition ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,45,FOLLOW_45_in_ruleLTLFormulaSet3411); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLTLFormulaSetAccess().getLTLKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,27,FOLLOW_27_in_ruleLTLFormulaSet3423); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getLTLFormulaSetAccess().getFormulaSetKeyword_1());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1581:1: ( (lv_name_2_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1582:1: (lv_name_2_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1582:1: (lv_name_2_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1583:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleLTLFormulaSet3440); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getLTLFormulaSetAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getLTLFormulaSetRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_12_in_ruleLTLFormulaSet3457); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getLTLFormulaSetAccess().getLeftCurlyBracketKeyword_3());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1603:1: ( (lv_formulae_4_0= ruleLTLFormulaDefinition ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==RULE_ID) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1604:1: (lv_formulae_4_0= ruleLTLFormulaDefinition )
            	    {
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1604:1: (lv_formulae_4_0= ruleLTLFormulaDefinition )
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1605:3: lv_formulae_4_0= ruleLTLFormulaDefinition
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getLTLFormulaSetAccess().getFormulaeLTLFormulaDefinitionParserRuleCall_4_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleLTLFormulaDefinition_in_ruleLTLFormulaSet3478);
            	    lv_formulae_4_0=ruleLTLFormulaDefinition();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getLTLFormulaSetRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"formulae",
            	              		lv_formulae_4_0, 
            	              		"LTLFormulaDefinition");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

            otherlv_5=(Token)match(input,19,FOLLOW_19_in_ruleLTLFormulaSet3491); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getLTLFormulaSetAccess().getRightCurlyBracketKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLFormulaSet"


    // $ANTLR start "entryRuleLTLFormulaDefinition"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1633:1: entryRuleLTLFormulaDefinition returns [EObject current=null] : iv_ruleLTLFormulaDefinition= ruleLTLFormulaDefinition EOF ;
    public final EObject entryRuleLTLFormulaDefinition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLFormulaDefinition = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1634:2: (iv_ruleLTLFormulaDefinition= ruleLTLFormulaDefinition EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1635:2: iv_ruleLTLFormulaDefinition= ruleLTLFormulaDefinition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLFormulaDefinitionRule()); 
            }
            pushFollow(FOLLOW_ruleLTLFormulaDefinition_in_entryRuleLTLFormulaDefinition3527);
            iv_ruleLTLFormulaDefinition=ruleLTLFormulaDefinition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLFormulaDefinition; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLFormulaDefinition3537); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLFormulaDefinition"


    // $ANTLR start "ruleLTLFormulaDefinition"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1642:1: ruleLTLFormulaDefinition returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':=' ( (lv_formula_2_0= ruleLTLFormula ) ) ) ;
    public final EObject ruleLTLFormulaDefinition() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        EObject lv_formula_2_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1645:28: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':=' ( (lv_formula_2_0= ruleLTLFormula ) ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1646:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':=' ( (lv_formula_2_0= ruleLTLFormula ) ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1646:1: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':=' ( (lv_formula_2_0= ruleLTLFormula ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1646:2: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':=' ( (lv_formula_2_0= ruleLTLFormula ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1646:2: ( (lv_name_0_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1647:1: (lv_name_0_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1647:1: (lv_name_0_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1648:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleLTLFormulaDefinition3579); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_0_0, grammarAccess.getLTLFormulaDefinitionAccess().getNameIDTerminalRuleCall_0_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getLTLFormulaDefinitionRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,28,FOLLOW_28_in_ruleLTLFormulaDefinition3596); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getLTLFormulaDefinitionAccess().getColonEqualsSignKeyword_1());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1668:1: ( (lv_formula_2_0= ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1669:1: (lv_formula_2_0= ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1669:1: (lv_formula_2_0= ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1670:3: lv_formula_2_0= ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLTLFormulaDefinitionAccess().getFormulaLTLFormulaParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_ruleLTLFormulaDefinition3617);
            lv_formula_2_0=ruleLTLFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLTLFormulaDefinitionRule());
              	        }
                     		set(
                     			current, 
                     			"formula",
                      		lv_formula_2_0, 
                      		"LTLFormula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLFormulaDefinition"


    // $ANTLR start "entryRuleLTLFormula"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1694:1: entryRuleLTLFormula returns [EObject current=null] : iv_ruleLTLFormula= ruleLTLFormula EOF ;
    public final EObject entryRuleLTLFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLFormula = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1695:2: (iv_ruleLTLFormula= ruleLTLFormula EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1696:2: iv_ruleLTLFormula= ruleLTLFormula EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLFormulaRule()); 
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_entryRuleLTLFormula3653);
            iv_ruleLTLFormula=ruleLTLFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLFormula; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLFormula3663); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLFormula"


    // $ANTLR start "ruleLTLFormula"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1703:1: ruleLTLFormula returns [EObject current=null] : (this_LTLX_0= ruleLTLX | this_LTLG_1= ruleLTLG | this_LTLU_2= ruleLTLU | this_LTLF_3= ruleLTLF | this_LTLNOT_4= ruleLTLNOT | ( ( ruleLTLAND )=>this_LTLAND_5= ruleLTLAND ) | this_LTLOR_6= ruleLTLOR | this_LTLProposition_7= ruleLTLProposition | this_LTLConstant_8= ruleLTLConstant ) ;
    public final EObject ruleLTLFormula() throws RecognitionException {
        EObject current = null;

        EObject this_LTLX_0 = null;

        EObject this_LTLG_1 = null;

        EObject this_LTLU_2 = null;

        EObject this_LTLF_3 = null;

        EObject this_LTLNOT_4 = null;

        EObject this_LTLAND_5 = null;

        EObject this_LTLOR_6 = null;

        EObject this_LTLProposition_7 = null;

        EObject this_LTLConstant_8 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1706:28: ( (this_LTLX_0= ruleLTLX | this_LTLG_1= ruleLTLG | this_LTLU_2= ruleLTLU | this_LTLF_3= ruleLTLF | this_LTLNOT_4= ruleLTLNOT | ( ( ruleLTLAND )=>this_LTLAND_5= ruleLTLAND ) | this_LTLOR_6= ruleLTLOR | this_LTLProposition_7= ruleLTLProposition | this_LTLConstant_8= ruleLTLConstant ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1707:1: (this_LTLX_0= ruleLTLX | this_LTLG_1= ruleLTLG | this_LTLU_2= ruleLTLU | this_LTLF_3= ruleLTLF | this_LTLNOT_4= ruleLTLNOT | ( ( ruleLTLAND )=>this_LTLAND_5= ruleLTLAND ) | this_LTLOR_6= ruleLTLOR | this_LTLProposition_7= ruleLTLProposition | this_LTLConstant_8= ruleLTLConstant )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1707:1: (this_LTLX_0= ruleLTLX | this_LTLG_1= ruleLTLG | this_LTLU_2= ruleLTLU | this_LTLF_3= ruleLTLF | this_LTLNOT_4= ruleLTLNOT | ( ( ruleLTLAND )=>this_LTLAND_5= ruleLTLAND ) | this_LTLOR_6= ruleLTLOR | this_LTLProposition_7= ruleLTLProposition | this_LTLConstant_8= ruleLTLConstant )
            int alt23=9;
            alt23 = dfa23.predict(input);
            switch (alt23) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1708:5: this_LTLX_0= ruleLTLX
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLTLFormulaAccess().getLTLXParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleLTLX_in_ruleLTLFormula3710);
                    this_LTLX_0=ruleLTLX();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_LTLX_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1718:5: this_LTLG_1= ruleLTLG
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLTLFormulaAccess().getLTLGParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleLTLG_in_ruleLTLFormula3737);
                    this_LTLG_1=ruleLTLG();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_LTLG_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1728:5: this_LTLU_2= ruleLTLU
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLTLFormulaAccess().getLTLUParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleLTLU_in_ruleLTLFormula3764);
                    this_LTLU_2=ruleLTLU();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_LTLU_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1738:5: this_LTLF_3= ruleLTLF
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLTLFormulaAccess().getLTLFParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleLTLF_in_ruleLTLFormula3791);
                    this_LTLF_3=ruleLTLF();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_LTLF_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1748:5: this_LTLNOT_4= ruleLTLNOT
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLTLFormulaAccess().getLTLNOTParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleLTLNOT_in_ruleLTLFormula3818);
                    this_LTLNOT_4=ruleLTLNOT();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_LTLNOT_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1757:6: ( ( ruleLTLAND )=>this_LTLAND_5= ruleLTLAND )
                    {
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1757:6: ( ( ruleLTLAND )=>this_LTLAND_5= ruleLTLAND )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1757:7: ( ruleLTLAND )=>this_LTLAND_5= ruleLTLAND
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLTLFormulaAccess().getLTLANDParserRuleCall_5()); 
                          
                    }
                    pushFollow(FOLLOW_ruleLTLAND_in_ruleLTLFormula3851);
                    this_LTLAND_5=ruleLTLAND();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_LTLAND_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }


                    }
                    break;
                case 7 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1768:5: this_LTLOR_6= ruleLTLOR
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLTLFormulaAccess().getLTLORParserRuleCall_6()); 
                          
                    }
                    pushFollow(FOLLOW_ruleLTLOR_in_ruleLTLFormula3879);
                    this_LTLOR_6=ruleLTLOR();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_LTLOR_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 8 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1778:5: this_LTLProposition_7= ruleLTLProposition
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLTLFormulaAccess().getLTLPropositionParserRuleCall_7()); 
                          
                    }
                    pushFollow(FOLLOW_ruleLTLProposition_in_ruleLTLFormula3906);
                    this_LTLProposition_7=ruleLTLProposition();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_LTLProposition_7; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 9 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1788:5: this_LTLConstant_8= ruleLTLConstant
                    {
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLTLFormulaAccess().getLTLConstantParserRuleCall_8()); 
                          
                    }
                    pushFollow(FOLLOW_ruleLTLConstant_in_ruleLTLFormula3933);
                    this_LTLConstant_8=ruleLTLConstant();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_LTLConstant_8; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLFormula"


    // $ANTLR start "entryRuleLTLX"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1804:1: entryRuleLTLX returns [EObject current=null] : iv_ruleLTLX= ruleLTLX EOF ;
    public final EObject entryRuleLTLX() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLX = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1805:2: (iv_ruleLTLX= ruleLTLX EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1806:2: iv_ruleLTLX= ruleLTLX EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLXRule()); 
            }
            pushFollow(FOLLOW_ruleLTLX_in_entryRuleLTLX3968);
            iv_ruleLTLX=ruleLTLX();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLX; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLX3978); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLX"


    // $ANTLR start "ruleLTLX"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1813:1: ruleLTLX returns [EObject current=null] : (otherlv_0= 'X' ( (lv_nested_1_0= ruleLTLFormula ) ) ) ;
    public final EObject ruleLTLX() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_nested_1_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1816:28: ( (otherlv_0= 'X' ( (lv_nested_1_0= ruleLTLFormula ) ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1817:1: (otherlv_0= 'X' ( (lv_nested_1_0= ruleLTLFormula ) ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1817:1: (otherlv_0= 'X' ( (lv_nested_1_0= ruleLTLFormula ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1817:3: otherlv_0= 'X' ( (lv_nested_1_0= ruleLTLFormula ) )
            {
            otherlv_0=(Token)match(input,46,FOLLOW_46_in_ruleLTLX4015); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLTLXAccess().getXKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1821:1: ( (lv_nested_1_0= ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1822:1: (lv_nested_1_0= ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1822:1: (lv_nested_1_0= ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1823:3: lv_nested_1_0= ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLTLXAccess().getNestedLTLFormulaParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_ruleLTLX4036);
            lv_nested_1_0=ruleLTLFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLTLXRule());
              	        }
                     		set(
                     			current, 
                     			"nested",
                      		lv_nested_1_0, 
                      		"LTLFormula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLX"


    // $ANTLR start "entryRuleLTLG"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1847:1: entryRuleLTLG returns [EObject current=null] : iv_ruleLTLG= ruleLTLG EOF ;
    public final EObject entryRuleLTLG() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLG = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1848:2: (iv_ruleLTLG= ruleLTLG EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1849:2: iv_ruleLTLG= ruleLTLG EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLGRule()); 
            }
            pushFollow(FOLLOW_ruleLTLG_in_entryRuleLTLG4072);
            iv_ruleLTLG=ruleLTLG();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLG; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLG4082); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLG"


    // $ANTLR start "ruleLTLG"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1856:1: ruleLTLG returns [EObject current=null] : (otherlv_0= 'G' ( (lv_nested_1_0= ruleLTLFormula ) ) ) ;
    public final EObject ruleLTLG() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_nested_1_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1859:28: ( (otherlv_0= 'G' ( (lv_nested_1_0= ruleLTLFormula ) ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1860:1: (otherlv_0= 'G' ( (lv_nested_1_0= ruleLTLFormula ) ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1860:1: (otherlv_0= 'G' ( (lv_nested_1_0= ruleLTLFormula ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1860:3: otherlv_0= 'G' ( (lv_nested_1_0= ruleLTLFormula ) )
            {
            otherlv_0=(Token)match(input,47,FOLLOW_47_in_ruleLTLG4119); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLTLGAccess().getGKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1864:1: ( (lv_nested_1_0= ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1865:1: (lv_nested_1_0= ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1865:1: (lv_nested_1_0= ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1866:3: lv_nested_1_0= ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLTLGAccess().getNestedLTLFormulaParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_ruleLTLG4140);
            lv_nested_1_0=ruleLTLFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLTLGRule());
              	        }
                     		set(
                     			current, 
                     			"nested",
                      		lv_nested_1_0, 
                      		"LTLFormula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLG"


    // $ANTLR start "entryRuleLTLU"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1890:1: entryRuleLTLU returns [EObject current=null] : iv_ruleLTLU= ruleLTLU EOF ;
    public final EObject entryRuleLTLU() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLU = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1891:2: (iv_ruleLTLU= ruleLTLU EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1892:2: iv_ruleLTLU= ruleLTLU EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLURule()); 
            }
            pushFollow(FOLLOW_ruleLTLU_in_entryRuleLTLU4176);
            iv_ruleLTLU=ruleLTLU();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLU; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLU4186); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLU"


    // $ANTLR start "ruleLTLU"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1899:1: ruleLTLU returns [EObject current=null] : (otherlv_0= '[' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= 'U' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ']' ) ;
    public final EObject ruleLTLU() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_left_1_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1902:28: ( (otherlv_0= '[' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= 'U' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ']' ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1903:1: (otherlv_0= '[' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= 'U' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ']' )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1903:1: (otherlv_0= '[' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= 'U' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ']' )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1903:3: otherlv_0= '[' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= 'U' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,32,FOLLOW_32_in_ruleLTLU4223); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLTLUAccess().getLeftSquareBracketKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1907:1: ( (lv_left_1_0= ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1908:1: (lv_left_1_0= ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1908:1: (lv_left_1_0= ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1909:3: lv_left_1_0= ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLTLUAccess().getLeftLTLFormulaParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_ruleLTLU4244);
            lv_left_1_0=ruleLTLFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLTLURule());
              	        }
                     		set(
                     			current, 
                     			"left",
                      		lv_left_1_0, 
                      		"LTLFormula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,33,FOLLOW_33_in_ruleLTLU4256); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getLTLUAccess().getUKeyword_2());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1929:1: ( (lv_right_3_0= ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1930:1: (lv_right_3_0= ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1930:1: (lv_right_3_0= ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1931:3: lv_right_3_0= ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLTLUAccess().getRightLTLFormulaParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_ruleLTLU4277);
            lv_right_3_0=ruleLTLFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLTLURule());
              	        }
                     		set(
                     			current, 
                     			"right",
                      		lv_right_3_0, 
                      		"LTLFormula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,34,FOLLOW_34_in_ruleLTLU4289); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getLTLUAccess().getRightSquareBracketKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLU"


    // $ANTLR start "entryRuleLTLF"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1959:1: entryRuleLTLF returns [EObject current=null] : iv_ruleLTLF= ruleLTLF EOF ;
    public final EObject entryRuleLTLF() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLF = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1960:2: (iv_ruleLTLF= ruleLTLF EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1961:2: iv_ruleLTLF= ruleLTLF EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLFRule()); 
            }
            pushFollow(FOLLOW_ruleLTLF_in_entryRuleLTLF4325);
            iv_ruleLTLF=ruleLTLF();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLF; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLF4335); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLF"


    // $ANTLR start "ruleLTLF"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1968:1: ruleLTLF returns [EObject current=null] : (otherlv_0= 'F' ( (lv_nested_1_0= ruleLTLFormula ) ) ) ;
    public final EObject ruleLTLF() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_nested_1_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1971:28: ( (otherlv_0= 'F' ( (lv_nested_1_0= ruleLTLFormula ) ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1972:1: (otherlv_0= 'F' ( (lv_nested_1_0= ruleLTLFormula ) ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1972:1: (otherlv_0= 'F' ( (lv_nested_1_0= ruleLTLFormula ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1972:3: otherlv_0= 'F' ( (lv_nested_1_0= ruleLTLFormula ) )
            {
            otherlv_0=(Token)match(input,48,FOLLOW_48_in_ruleLTLF4372); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLTLFAccess().getFKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1976:1: ( (lv_nested_1_0= ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1977:1: (lv_nested_1_0= ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1977:1: (lv_nested_1_0= ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1978:3: lv_nested_1_0= ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLTLFAccess().getNestedLTLFormulaParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_ruleLTLF4393);
            lv_nested_1_0=ruleLTLFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLTLFRule());
              	        }
                     		set(
                     			current, 
                     			"nested",
                      		lv_nested_1_0, 
                      		"LTLFormula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLF"


    // $ANTLR start "entryRuleLTLNOT"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2002:1: entryRuleLTLNOT returns [EObject current=null] : iv_ruleLTLNOT= ruleLTLNOT EOF ;
    public final EObject entryRuleLTLNOT() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLNOT = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2003:2: (iv_ruleLTLNOT= ruleLTLNOT EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2004:2: iv_ruleLTLNOT= ruleLTLNOT EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLNOTRule()); 
            }
            pushFollow(FOLLOW_ruleLTLNOT_in_entryRuleLTLNOT4429);
            iv_ruleLTLNOT=ruleLTLNOT();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLNOT; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLNOT4439); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLNOT"


    // $ANTLR start "ruleLTLNOT"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2011:1: ruleLTLNOT returns [EObject current=null] : (otherlv_0= '\\u00AC' ( (lv_nested_1_0= ruleLTLFormula ) ) ) ;
    public final EObject ruleLTLNOT() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_nested_1_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2014:28: ( (otherlv_0= '\\u00AC' ( (lv_nested_1_0= ruleLTLFormula ) ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2015:1: (otherlv_0= '\\u00AC' ( (lv_nested_1_0= ruleLTLFormula ) ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2015:1: (otherlv_0= '\\u00AC' ( (lv_nested_1_0= ruleLTLFormula ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2015:3: otherlv_0= '\\u00AC' ( (lv_nested_1_0= ruleLTLFormula ) )
            {
            otherlv_0=(Token)match(input,39,FOLLOW_39_in_ruleLTLNOT4476); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLTLNOTAccess().getNotSignKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2019:1: ( (lv_nested_1_0= ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2020:1: (lv_nested_1_0= ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2020:1: (lv_nested_1_0= ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2021:3: lv_nested_1_0= ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLTLNOTAccess().getNestedLTLFormulaParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_ruleLTLNOT4497);
            lv_nested_1_0=ruleLTLFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLTLNOTRule());
              	        }
                     		set(
                     			current, 
                     			"nested",
                      		lv_nested_1_0, 
                      		"LTLFormula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLNOT"


    // $ANTLR start "entryRuleLTLAND"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2045:1: entryRuleLTLAND returns [EObject current=null] : iv_ruleLTLAND= ruleLTLAND EOF ;
    public final EObject entryRuleLTLAND() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLAND = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2046:2: (iv_ruleLTLAND= ruleLTLAND EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2047:2: iv_ruleLTLAND= ruleLTLAND EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLANDRule()); 
            }
            pushFollow(FOLLOW_ruleLTLAND_in_entryRuleLTLAND4533);
            iv_ruleLTLAND=ruleLTLAND();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLAND; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLAND4543); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLAND"


    // $ANTLR start "ruleLTLAND"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2054:1: ruleLTLAND returns [EObject current=null] : (otherlv_0= '(' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= '\\u22C0' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ')' ) ;
    public final EObject ruleLTLAND() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_left_1_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2057:28: ( (otherlv_0= '(' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= '\\u22C0' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ')' ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2058:1: (otherlv_0= '(' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= '\\u22C0' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ')' )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2058:1: (otherlv_0= '(' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= '\\u22C0' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ')' )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2058:3: otherlv_0= '(' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= '\\u22C0' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ')'
            {
            otherlv_0=(Token)match(input,35,FOLLOW_35_in_ruleLTLAND4580); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLTLANDAccess().getLeftParenthesisKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2062:1: ( (lv_left_1_0= ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2063:1: (lv_left_1_0= ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2063:1: (lv_left_1_0= ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2064:3: lv_left_1_0= ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLTLANDAccess().getLeftLTLFormulaParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_ruleLTLAND4601);
            lv_left_1_0=ruleLTLFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLTLANDRule());
              	        }
                     		set(
                     			current, 
                     			"left",
                      		lv_left_1_0, 
                      		"LTLFormula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,36,FOLLOW_36_in_ruleLTLAND4613); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getLTLANDAccess().getNAryLogicalAndKeyword_2());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2084:1: ( (lv_right_3_0= ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2085:1: (lv_right_3_0= ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2085:1: (lv_right_3_0= ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2086:3: lv_right_3_0= ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLTLANDAccess().getRightLTLFormulaParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_ruleLTLAND4634);
            lv_right_3_0=ruleLTLFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLTLANDRule());
              	        }
                     		set(
                     			current, 
                     			"right",
                      		lv_right_3_0, 
                      		"LTLFormula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,37,FOLLOW_37_in_ruleLTLAND4646); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getLTLANDAccess().getRightParenthesisKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLAND"


    // $ANTLR start "entryRuleLTLOR"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2114:1: entryRuleLTLOR returns [EObject current=null] : iv_ruleLTLOR= ruleLTLOR EOF ;
    public final EObject entryRuleLTLOR() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLOR = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2115:2: (iv_ruleLTLOR= ruleLTLOR EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2116:2: iv_ruleLTLOR= ruleLTLOR EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLORRule()); 
            }
            pushFollow(FOLLOW_ruleLTLOR_in_entryRuleLTLOR4682);
            iv_ruleLTLOR=ruleLTLOR();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLOR; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLOR4692); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLOR"


    // $ANTLR start "ruleLTLOR"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2123:1: ruleLTLOR returns [EObject current=null] : (otherlv_0= '(' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= '\\u22C1' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ')' ) ;
    public final EObject ruleLTLOR() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_left_1_0 = null;

        EObject lv_right_3_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2126:28: ( (otherlv_0= '(' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= '\\u22C1' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ')' ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2127:1: (otherlv_0= '(' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= '\\u22C1' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ')' )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2127:1: (otherlv_0= '(' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= '\\u22C1' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ')' )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2127:3: otherlv_0= '(' ( (lv_left_1_0= ruleLTLFormula ) ) otherlv_2= '\\u22C1' ( (lv_right_3_0= ruleLTLFormula ) ) otherlv_4= ')'
            {
            otherlv_0=(Token)match(input,35,FOLLOW_35_in_ruleLTLOR4729); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLTLORAccess().getLeftParenthesisKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2131:1: ( (lv_left_1_0= ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2132:1: (lv_left_1_0= ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2132:1: (lv_left_1_0= ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2133:3: lv_left_1_0= ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLTLORAccess().getLeftLTLFormulaParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_ruleLTLOR4750);
            lv_left_1_0=ruleLTLFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLTLORRule());
              	        }
                     		set(
                     			current, 
                     			"left",
                      		lv_left_1_0, 
                      		"LTLFormula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,38,FOLLOW_38_in_ruleLTLOR4762); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getLTLORAccess().getNAryLogicalOrKeyword_2());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2153:1: ( (lv_right_3_0= ruleLTLFormula ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2154:1: (lv_right_3_0= ruleLTLFormula )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2154:1: (lv_right_3_0= ruleLTLFormula )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2155:3: lv_right_3_0= ruleLTLFormula
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLTLORAccess().getRightLTLFormulaParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleLTLFormula_in_ruleLTLOR4783);
            lv_right_3_0=ruleLTLFormula();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLTLORRule());
              	        }
                     		set(
                     			current, 
                     			"right",
                      		lv_right_3_0, 
                      		"LTLFormula");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,37,FOLLOW_37_in_ruleLTLOR4795); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getLTLORAccess().getRightParenthesisKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLOR"


    // $ANTLR start "entryRuleLTLProposition"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2183:1: entryRuleLTLProposition returns [EObject current=null] : iv_ruleLTLProposition= ruleLTLProposition EOF ;
    public final EObject entryRuleLTLProposition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLProposition = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2184:2: (iv_ruleLTLProposition= ruleLTLProposition EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2185:2: iv_ruleLTLProposition= ruleLTLProposition EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLPropositionRule()); 
            }
            pushFollow(FOLLOW_ruleLTLProposition_in_entryRuleLTLProposition4831);
            iv_ruleLTLProposition=ruleLTLProposition();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLProposition; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLProposition4841); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLProposition"


    // $ANTLR start "ruleLTLProposition"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2192:1: ruleLTLProposition returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleLTLProposition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2195:28: ( ( (otherlv_0= RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2196:1: ( (otherlv_0= RULE_ID ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2196:1: ( (otherlv_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2197:1: (otherlv_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2197:1: (otherlv_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2198:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getLTLPropositionRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleLTLProposition4885); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getLTLPropositionAccess().getAtomicPropositionAtomicPropositionCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLProposition"


    // $ANTLR start "entryRuleLTLConstant"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2217:1: entryRuleLTLConstant returns [EObject current=null] : iv_ruleLTLConstant= ruleLTLConstant EOF ;
    public final EObject entryRuleLTLConstant() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLConstant = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2218:2: (iv_ruleLTLConstant= ruleLTLConstant EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2219:2: iv_ruleLTLConstant= ruleLTLConstant EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLConstantRule()); 
            }
            pushFollow(FOLLOW_ruleLTLConstant_in_entryRuleLTLConstant4920);
            iv_ruleLTLConstant=ruleLTLConstant();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLConstant; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLConstant4930); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLConstant"


    // $ANTLR start "ruleLTLConstant"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2226:1: ruleLTLConstant returns [EObject current=null] : ( () ( ( (lv_value_1_0= 'true' ) ) | otherlv_2= 'false' ) ) ;
    public final EObject ruleLTLConstant() throws RecognitionException {
        EObject current = null;

        Token lv_value_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2229:28: ( ( () ( ( (lv_value_1_0= 'true' ) ) | otherlv_2= 'false' ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2230:1: ( () ( ( (lv_value_1_0= 'true' ) ) | otherlv_2= 'false' ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2230:1: ( () ( ( (lv_value_1_0= 'true' ) ) | otherlv_2= 'false' ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2230:2: () ( ( (lv_value_1_0= 'true' ) ) | otherlv_2= 'false' )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2230:2: ()
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2231:5: 
            {
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getLTLConstantAccess().getConstantAction_0(),
                          current);
                  
            }

            }

            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2236:2: ( ( (lv_value_1_0= 'true' ) ) | otherlv_2= 'false' )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==40) ) {
                alt24=1;
            }
            else if ( (LA24_0==41) ) {
                alt24=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }
            switch (alt24) {
                case 1 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2236:3: ( (lv_value_1_0= 'true' ) )
                    {
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2236:3: ( (lv_value_1_0= 'true' ) )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2237:1: (lv_value_1_0= 'true' )
                    {
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2237:1: (lv_value_1_0= 'true' )
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2238:3: lv_value_1_0= 'true'
                    {
                    lv_value_1_0=(Token)match(input,40,FOLLOW_40_in_ruleLTLConstant4983); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_value_1_0, grammarAccess.getLTLConstantAccess().getValueTrueKeyword_1_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getLTLConstantRule());
                      	        }
                             		setWithLastConsumed(current, "value", true, "true");
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2252:7: otherlv_2= 'false'
                    {
                    otherlv_2=(Token)match(input,41,FOLLOW_41_in_ruleLTLConstant5014); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getLTLConstantAccess().getFalseKeyword_1_1());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLConstant"


    // $ANTLR start "entryRuleLTLCheckSet"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2264:1: entryRuleLTLCheckSet returns [EObject current=null] : iv_ruleLTLCheckSet= ruleLTLCheckSet EOF ;
    public final EObject entryRuleLTLCheckSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLCheckSet = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2265:2: (iv_ruleLTLCheckSet= ruleLTLCheckSet EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2266:2: iv_ruleLTLCheckSet= ruleLTLCheckSet EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLCheckSetRule()); 
            }
            pushFollow(FOLLOW_ruleLTLCheckSet_in_entryRuleLTLCheckSet5051);
            iv_ruleLTLCheckSet=ruleLTLCheckSet();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLCheckSet; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLCheckSet5061); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLCheckSet"


    // $ANTLR start "ruleLTLCheckSet"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2273:1: ruleLTLCheckSet returns [EObject current=null] : (otherlv_0= 'LTL' otherlv_1= 'CheckSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_checks_4_0= ruleLTLCheck ) )* otherlv_5= '}' ) ;
    public final EObject ruleLTLCheckSet() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_checks_4_0 = null;


         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2276:28: ( (otherlv_0= 'LTL' otherlv_1= 'CheckSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_checks_4_0= ruleLTLCheck ) )* otherlv_5= '}' ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2277:1: (otherlv_0= 'LTL' otherlv_1= 'CheckSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_checks_4_0= ruleLTLCheck ) )* otherlv_5= '}' )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2277:1: (otherlv_0= 'LTL' otherlv_1= 'CheckSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_checks_4_0= ruleLTLCheck ) )* otherlv_5= '}' )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2277:3: otherlv_0= 'LTL' otherlv_1= 'CheckSet' ( (lv_name_2_0= RULE_ID ) ) otherlv_3= '{' ( (lv_checks_4_0= ruleLTLCheck ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,45,FOLLOW_45_in_ruleLTLCheckSet5098); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLTLCheckSetAccess().getLTLKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,42,FOLLOW_42_in_ruleLTLCheckSet5110); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getLTLCheckSetAccess().getCheckSetKeyword_1());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2285:1: ( (lv_name_2_0= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2286:1: (lv_name_2_0= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2286:1: (lv_name_2_0= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2287:3: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleLTLCheckSet5127); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_2_0, grammarAccess.getLTLCheckSetAccess().getNameIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getLTLCheckSetRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_2_0, 
                      		"ID");
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_12_in_ruleLTLCheckSet5144); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getLTLCheckSetAccess().getLeftCurlyBracketKeyword_3());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2307:1: ( (lv_checks_4_0= ruleLTLCheck ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==43) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2308:1: (lv_checks_4_0= ruleLTLCheck )
            	    {
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2308:1: (lv_checks_4_0= ruleLTLCheck )
            	    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2309:3: lv_checks_4_0= ruleLTLCheck
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getLTLCheckSetAccess().getChecksLTLCheckParserRuleCall_4_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleLTLCheck_in_ruleLTLCheckSet5165);
            	    lv_checks_4_0=ruleLTLCheck();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getLTLCheckSetRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"checks",
            	              		lv_checks_4_0, 
            	              		"LTLCheck");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            otherlv_5=(Token)match(input,19,FOLLOW_19_in_ruleLTLCheckSet5178); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getLTLCheckSetAccess().getRightCurlyBracketKeyword_5());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLCheckSet"


    // $ANTLR start "entryRuleLTLCheck"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2337:1: entryRuleLTLCheck returns [EObject current=null] : iv_ruleLTLCheck= ruleLTLCheck EOF ;
    public final EObject entryRuleLTLCheck() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLTLCheck = null;


        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2338:2: (iv_ruleLTLCheck= ruleLTLCheck EOF )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2339:2: iv_ruleLTLCheck= ruleLTLCheck EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLTLCheckRule()); 
            }
            pushFollow(FOLLOW_ruleLTLCheck_in_entryRuleLTLCheck5214);
            iv_ruleLTLCheck=ruleLTLCheck();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLTLCheck; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLTLCheck5224); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLTLCheck"


    // $ANTLR start "ruleLTLCheck"
    // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2346:1: ruleLTLCheck returns [EObject current=null] : (otherlv_0= 'check' ( (otherlv_1= RULE_ID ) ) otherlv_2= '\\u22A8' ( (otherlv_3= RULE_ID ) ) ) ;
    public final EObject ruleLTLCheck() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2349:28: ( (otherlv_0= 'check' ( (otherlv_1= RULE_ID ) ) otherlv_2= '\\u22A8' ( (otherlv_3= RULE_ID ) ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2350:1: (otherlv_0= 'check' ( (otherlv_1= RULE_ID ) ) otherlv_2= '\\u22A8' ( (otherlv_3= RULE_ID ) ) )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2350:1: (otherlv_0= 'check' ( (otherlv_1= RULE_ID ) ) otherlv_2= '\\u22A8' ( (otherlv_3= RULE_ID ) ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2350:3: otherlv_0= 'check' ( (otherlv_1= RULE_ID ) ) otherlv_2= '\\u22A8' ( (otherlv_3= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,43,FOLLOW_43_in_ruleLTLCheck5261); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getLTLCheckAccess().getCheckKeyword_0());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2354:1: ( (otherlv_1= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2355:1: (otherlv_1= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2355:1: (otherlv_1= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2356:3: otherlv_1= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getLTLCheckRule());
              	        }
                      
            }
            otherlv_1=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleLTLCheck5281); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_1, grammarAccess.getLTLCheckAccess().getLtsLTSCrossReference_1_0()); 
              	
            }

            }


            }

            otherlv_2=(Token)match(input,44,FOLLOW_44_in_ruleLTLCheck5293); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getLTLCheckAccess().getTrueKeyword_2());
                  
            }
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2371:1: ( (otherlv_3= RULE_ID ) )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2372:1: (otherlv_3= RULE_ID )
            {
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2372:1: (otherlv_3= RULE_ID )
            // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:2373:3: otherlv_3= RULE_ID
            {
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getLTLCheckRule());
              	        }
                      
            }
            otherlv_3=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleLTLCheck5313); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_3, grammarAccess.getLTLCheckAccess().getFormulaDefinitionLTLFormulaDefinitionCrossReference_3_0()); 
              	
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLTLCheck"

    // $ANTLR start synpred1_InternalXLTS
    public final void synpred1_InternalXLTS_fragment() throws RecognitionException {   
        // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:964:7: ( ruleAND )
        // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:964:9: ruleAND
        {
        pushFollow(FOLLOW_ruleAND_in_synpred1_InternalXLTS1942);
        ruleAND();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred1_InternalXLTS

    // $ANTLR start synpred2_InternalXLTS
    public final void synpred2_InternalXLTS_fragment() throws RecognitionException {   
        // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1757:7: ( ruleLTLAND )
        // ../de.hannover.uni.se.fmse.lts/src-gen/de/hannover/uni/se/fmse/lts/parser/antlr/internal/InternalXLTS.g:1757:9: ruleLTLAND
        {
        pushFollow(FOLLOW_ruleLTLAND_in_synpred2_InternalXLTS3835);
        ruleLTLAND();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred2_InternalXLTS

    // Delegated rules

    public final boolean synpred1_InternalXLTS() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred1_InternalXLTS_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred2_InternalXLTS() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred2_InternalXLTS_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA19 dfa19 = new DFA19(this);
    protected DFA23 dfa23 = new DFA23(this);
    static final String DFA19_eotS =
        "\13\uffff";
    static final String DFA19_eofS =
        "\13\uffff";
    static final String DFA19_minS =
        "\1\4\4\uffff\1\0\5\uffff";
    static final String DFA19_maxS =
        "\1\51\4\uffff\1\0\5\uffff";
    static final String DFA19_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\1\uffff\1\7\1\10\1\uffff\1\5\1\6";
    static final String DFA19_specialS =
        "\5\uffff\1\0\5\uffff}>";
    static final String[] DFA19_transitionS = {
            "\1\6\30\uffff\1\1\1\2\1\3\3\uffff\1\5\3\uffff\1\4\2\7",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA19_eot = DFA.unpackEncodedString(DFA19_eotS);
    static final short[] DFA19_eof = DFA.unpackEncodedString(DFA19_eofS);
    static final char[] DFA19_min = DFA.unpackEncodedStringToUnsignedChars(DFA19_minS);
    static final char[] DFA19_max = DFA.unpackEncodedStringToUnsignedChars(DFA19_maxS);
    static final short[] DFA19_accept = DFA.unpackEncodedString(DFA19_acceptS);
    static final short[] DFA19_special = DFA.unpackEncodedString(DFA19_specialS);
    static final short[][] DFA19_transition;

    static {
        int numStates = DFA19_transitionS.length;
        DFA19_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA19_transition[i] = DFA.unpackEncodedString(DFA19_transitionS[i]);
        }
    }

    class DFA19 extends DFA {

        public DFA19(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 19;
            this.eot = DFA19_eot;
            this.eof = DFA19_eof;
            this.min = DFA19_min;
            this.max = DFA19_max;
            this.accept = DFA19_accept;
            this.special = DFA19_special;
            this.transition = DFA19_transition;
        }
        public String getDescription() {
            return "924:1: (this_EX_0= ruleEX | this_EG_1= ruleEG | this_EU_2= ruleEU | this_NOT_3= ruleNOT | ( ( ruleAND )=>this_AND_4= ruleAND ) | this_OR_5= ruleOR | this_Proposition_6= ruleProposition | this_Constant_7= ruleConstant )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA19_5 = input.LA(1);

                         
                        int index19_5 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred1_InternalXLTS()) ) {s = 9;}

                        else if ( (true) ) {s = 10;}

                         
                        input.seek(index19_5);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 19, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA23_eotS =
        "\14\uffff";
    static final String DFA23_eofS =
        "\14\uffff";
    static final String DFA23_minS =
        "\1\4\5\uffff\1\0\5\uffff";
    static final String DFA23_maxS =
        "\1\60\5\uffff\1\0\5\uffff";
    static final String DFA23_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\1\5\1\uffff\1\10\1\11\1\uffff\1\6\1\7";
    static final String DFA23_specialS =
        "\6\uffff\1\0\5\uffff}>";
    static final String[] DFA23_transitionS = {
            "\1\7\33\uffff\1\3\2\uffff\1\6\3\uffff\1\5\2\10\4\uffff\1\1"+
            "\1\2\1\4",
            "",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA23_eot = DFA.unpackEncodedString(DFA23_eotS);
    static final short[] DFA23_eof = DFA.unpackEncodedString(DFA23_eofS);
    static final char[] DFA23_min = DFA.unpackEncodedStringToUnsignedChars(DFA23_minS);
    static final char[] DFA23_max = DFA.unpackEncodedStringToUnsignedChars(DFA23_maxS);
    static final short[] DFA23_accept = DFA.unpackEncodedString(DFA23_acceptS);
    static final short[] DFA23_special = DFA.unpackEncodedString(DFA23_specialS);
    static final short[][] DFA23_transition;

    static {
        int numStates = DFA23_transitionS.length;
        DFA23_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA23_transition[i] = DFA.unpackEncodedString(DFA23_transitionS[i]);
        }
    }

    class DFA23 extends DFA {

        public DFA23(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 23;
            this.eot = DFA23_eot;
            this.eof = DFA23_eof;
            this.min = DFA23_min;
            this.max = DFA23_max;
            this.accept = DFA23_accept;
            this.special = DFA23_special;
            this.transition = DFA23_transition;
        }
        public String getDescription() {
            return "1707:1: (this_LTLX_0= ruleLTLX | this_LTLG_1= ruleLTLG | this_LTLU_2= ruleLTLU | this_LTLF_3= ruleLTLF | this_LTLNOT_4= ruleLTLNOT | ( ( ruleLTLAND )=>this_LTLAND_5= ruleLTLAND ) | this_LTLOR_6= ruleLTLOR | this_LTLProposition_7= ruleLTLProposition | this_LTLConstant_8= ruleLTLConstant )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA23_6 = input.LA(1);

                         
                        int index23_6 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred2_InternalXLTS()) ) {s = 10;}

                        else if ( (true) ) {s = 11;}

                         
                        input.seek(index23_6);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 23, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_ruleDocument_in_entryRuleDocument75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDocument85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTS_in_ruleDocument133 = new BitSet(new long[]{0x0000200004400802L});
    public static final BitSet FOLLOW_ruleComposedLTS_in_ruleDocument152 = new BitSet(new long[]{0x0000200004400802L});
    public static final BitSet FOLLOW_ruleFormulaSet_in_ruleDocument177 = new BitSet(new long[]{0x0000200004000002L});
    public static final BitSet FOLLOW_ruleCheckSet_in_ruleDocument199 = new BitSet(new long[]{0x0000200000000002L});
    public static final BitSet FOLLOW_ruleLTLFormulaSet_in_ruleDocument221 = new BitSet(new long[]{0x0000200000000002L});
    public static final BitSet FOLLOW_ruleLTLCheckSet_in_ruleDocument243 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTS_in_entryRuleLTS280 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTS290 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleLTS327 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleLTS344 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleLTS361 = new BitSet(new long[]{0x0000000000012000L});
    public static final BitSet FOLLOW_13_in_ruleLTS374 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleLTS386 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleAtomicProposition_in_ruleLTS407 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_15_in_ruleLTS420 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleAtomicProposition_in_ruleLTS441 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_16_in_ruleLTS457 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleBasicState_in_ruleLTS478 = new BitSet(new long[]{0x0000000000028000L});
    public static final BitSet FOLLOW_15_in_ruleLTS491 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleBasicState_in_ruleLTS512 = new BitSet(new long[]{0x0000000000028000L});
    public static final BitSet FOLLOW_17_in_ruleLTS526 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleLTS546 = new BitSet(new long[]{0x00000000000C8010L});
    public static final BitSet FOLLOW_15_in_ruleLTS559 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleLTS579 = new BitSet(new long[]{0x00000000000C8010L});
    public static final BitSet FOLLOW_18_in_ruleLTS594 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleEvent_in_ruleLTS615 = new BitSet(new long[]{0x0000000000088010L});
    public static final BitSet FOLLOW_15_in_ruleLTS628 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleEvent_in_ruleLTS649 = new BitSet(new long[]{0x0000000000088010L});
    public static final BitSet FOLLOW_ruleTransition_in_ruleLTS674 = new BitSet(new long[]{0x0000000000080010L});
    public static final BitSet FOLLOW_19_in_ruleLTS687 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAtomicProposition_in_entryRuleAtomicProposition723 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAtomicProposition733 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleAtomicProposition774 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBasicState_in_entryRuleBasicState814 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBasicState824 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleBasicState866 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_12_in_ruleBasicState884 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleBasicState904 = new BitSet(new long[]{0x0000000000088000L});
    public static final BitSet FOLLOW_15_in_ruleBasicState917 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleBasicState937 = new BitSet(new long[]{0x0000000000088000L});
    public static final BitSet FOLLOW_19_in_ruleBasicState951 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_ruleEvent_in_entryRuleEvent989 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEvent999 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleEvent1040 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTransition_in_entryRuleTransition1080 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTransition1090 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleTransition1135 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20_in_ruleTransition1147 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleTransition1167 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleTransition1179 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleTransition1199 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleComposedLTS_in_entryRuleComposedLTS1235 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleComposedLTS1245 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleComposedLTS1282 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleComposedLTS1302 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleComposedLTS1314 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleComposedLTS1334 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24_in_ruleComposedLTS1346 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleComposedLTS1363 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_25_in_ruleComposedLTS1381 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleComposedLTS1393 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleComposedLTS1413 = new BitSet(new long[]{0x0000000000088000L});
    public static final BitSet FOLLOW_15_in_ruleComposedLTS1426 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleComposedLTS1446 = new BitSet(new long[]{0x0000000000088000L});
    public static final BitSet FOLLOW_19_in_ruleComposedLTS1460 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormulaSet_in_entryRuleFormulaSet1498 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFormulaSet1508 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_ruleFormulaSet1545 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_27_in_ruleFormulaSet1557 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFormulaSet1574 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleFormulaSet1591 = new BitSet(new long[]{0x0000000000080010L});
    public static final BitSet FOLLOW_ruleFormulaDefinition_in_ruleFormulaSet1612 = new BitSet(new long[]{0x0000000000080010L});
    public static final BitSet FOLLOW_19_in_ruleFormulaSet1625 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormulaDefinition_in_entryRuleFormulaDefinition1661 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFormulaDefinition1671 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFormulaDefinition1713 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_ruleFormulaDefinition1730 = new BitSet(new long[]{0x00000388E0000010L});
    public static final BitSet FOLLOW_ruleFormula_in_ruleFormulaDefinition1751 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFormula_in_entryRuleFormula1787 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFormula1797 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEX_in_ruleFormula1844 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEG_in_ruleFormula1871 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEU_in_ruleFormula1898 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNOT_in_ruleFormula1925 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAND_in_ruleFormula1958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOR_in_ruleFormula1986 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProposition_in_ruleFormula2013 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_ruleFormula2040 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEX_in_entryRuleEX2075 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEX2085 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_ruleEX2122 = new BitSet(new long[]{0x00000388E0000010L});
    public static final BitSet FOLLOW_ruleFormula_in_ruleEX2143 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEG_in_entryRuleEG2179 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEG2189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_ruleEG2226 = new BitSet(new long[]{0x00000388E0000010L});
    public static final BitSet FOLLOW_ruleFormula_in_ruleEG2247 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEU_in_entryRuleEU2283 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEU2293 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_ruleEU2330 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_ruleEU2342 = new BitSet(new long[]{0x00000388E0000010L});
    public static final BitSet FOLLOW_ruleFormula_in_ruleEU2363 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_ruleEU2375 = new BitSet(new long[]{0x00000388E0000010L});
    public static final BitSet FOLLOW_ruleFormula_in_ruleEU2396 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_34_in_ruleEU2408 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAND_in_entryRuleAND2444 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAND2454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_ruleAND2491 = new BitSet(new long[]{0x00000388E0000010L});
    public static final BitSet FOLLOW_ruleFormula_in_ruleAND2512 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_ruleAND2524 = new BitSet(new long[]{0x00000388E0000010L});
    public static final BitSet FOLLOW_ruleFormula_in_ruleAND2545 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_ruleAND2557 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOR_in_entryRuleOR2593 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOR2603 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_ruleOR2640 = new BitSet(new long[]{0x00000388E0000010L});
    public static final BitSet FOLLOW_ruleFormula_in_ruleOR2661 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_ruleOR2673 = new BitSet(new long[]{0x00000388E0000010L});
    public static final BitSet FOLLOW_ruleFormula_in_ruleOR2694 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_ruleOR2706 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNOT_in_entryRuleNOT2742 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNOT2752 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_ruleNOT2789 = new BitSet(new long[]{0x00000388E0000010L});
    public static final BitSet FOLLOW_ruleFormula_in_ruleNOT2810 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleProposition_in_entryRuleProposition2846 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProposition2856 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleProposition2900 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleConstant_in_entryRuleConstant2935 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleConstant2945 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_ruleConstant2998 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_ruleConstant3029 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckSet_in_entryRuleCheckSet3066 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheckSet3076 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_ruleCheckSet3113 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_ruleCheckSet3125 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleCheckSet3142 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleCheckSet3159 = new BitSet(new long[]{0x0000080000080000L});
    public static final BitSet FOLLOW_ruleCheck_in_ruleCheckSet3180 = new BitSet(new long[]{0x0000080000080000L});
    public static final BitSet FOLLOW_19_in_ruleCheckSet3193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheck_in_entryRuleCheck3229 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheck3239 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_ruleCheck3276 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleCheck3296 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_44_in_ruleCheck3308 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleCheck3328 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormulaSet_in_entryRuleLTLFormulaSet3364 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLFormulaSet3374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_ruleLTLFormulaSet3411 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_27_in_ruleLTLFormulaSet3423 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleLTLFormulaSet3440 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleLTLFormulaSet3457 = new BitSet(new long[]{0x0000000000080010L});
    public static final BitSet FOLLOW_ruleLTLFormulaDefinition_in_ruleLTLFormulaSet3478 = new BitSet(new long[]{0x0000000000080010L});
    public static final BitSet FOLLOW_19_in_ruleLTLFormulaSet3491 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormulaDefinition_in_entryRuleLTLFormulaDefinition3527 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLFormulaDefinition3537 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleLTLFormulaDefinition3579 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_28_in_ruleLTLFormulaDefinition3596 = new BitSet(new long[]{0x0001C38900000010L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_ruleLTLFormulaDefinition3617 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_entryRuleLTLFormula3653 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLFormula3663 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLX_in_ruleLTLFormula3710 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLG_in_ruleLTLFormula3737 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLU_in_ruleLTLFormula3764 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLF_in_ruleLTLFormula3791 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLNOT_in_ruleLTLFormula3818 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLAND_in_ruleLTLFormula3851 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLOR_in_ruleLTLFormula3879 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLProposition_in_ruleLTLFormula3906 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLConstant_in_ruleLTLFormula3933 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLX_in_entryRuleLTLX3968 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLX3978 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_ruleLTLX4015 = new BitSet(new long[]{0x0001C38900000010L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_ruleLTLX4036 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLG_in_entryRuleLTLG4072 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLG4082 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_ruleLTLG4119 = new BitSet(new long[]{0x0001C38900000010L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_ruleLTLG4140 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLU_in_entryRuleLTLU4176 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLU4186 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_ruleLTLU4223 = new BitSet(new long[]{0x0001C38900000010L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_ruleLTLU4244 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_33_in_ruleLTLU4256 = new BitSet(new long[]{0x0001C38900000010L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_ruleLTLU4277 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_34_in_ruleLTLU4289 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLF_in_entryRuleLTLF4325 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLF4335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_ruleLTLF4372 = new BitSet(new long[]{0x0001C38900000010L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_ruleLTLF4393 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLNOT_in_entryRuleLTLNOT4429 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLNOT4439 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_ruleLTLNOT4476 = new BitSet(new long[]{0x0001C38900000010L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_ruleLTLNOT4497 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLAND_in_entryRuleLTLAND4533 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLAND4543 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_ruleLTLAND4580 = new BitSet(new long[]{0x0001C38900000010L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_ruleLTLAND4601 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_36_in_ruleLTLAND4613 = new BitSet(new long[]{0x0001C38900000010L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_ruleLTLAND4634 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_ruleLTLAND4646 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLOR_in_entryRuleLTLOR4682 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLOR4692 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_ruleLTLOR4729 = new BitSet(new long[]{0x0001C38900000010L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_ruleLTLOR4750 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_ruleLTLOR4762 = new BitSet(new long[]{0x0001C38900000010L});
    public static final BitSet FOLLOW_ruleLTLFormula_in_ruleLTLOR4783 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_ruleLTLOR4795 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLProposition_in_entryRuleLTLProposition4831 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLProposition4841 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleLTLProposition4885 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLConstant_in_entryRuleLTLConstant4920 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLConstant4930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_ruleLTLConstant4983 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_ruleLTLConstant5014 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLCheckSet_in_entryRuleLTLCheckSet5051 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLCheckSet5061 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_ruleLTLCheckSet5098 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_ruleLTLCheckSet5110 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleLTLCheckSet5127 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleLTLCheckSet5144 = new BitSet(new long[]{0x0000080000080000L});
    public static final BitSet FOLLOW_ruleLTLCheck_in_ruleLTLCheckSet5165 = new BitSet(new long[]{0x0000080000080000L});
    public static final BitSet FOLLOW_19_in_ruleLTLCheckSet5178 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLCheck_in_entryRuleLTLCheck5214 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLTLCheck5224 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_ruleLTLCheck5261 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleLTLCheck5281 = new BitSet(new long[]{0x0000100000000000L});
    public static final BitSet FOLLOW_44_in_ruleLTLCheck5293 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleLTLCheck5313 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAND_in_synpred1_InternalXLTS1942 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLTLAND_in_synpred2_InternalXLTS3835 = new BitSet(new long[]{0x0000000000000002L});

}