/**
 */
package de.hannover.uni.se.fmse.lts.xLTS.util;

import de.hannover.uni.se.fmse.lts.xLTS.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.hannover.uni.se.fmse.lts.xLTS.XLTSPackage
 * @generated
 */
public class XLTSAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static XLTSPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public XLTSAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = XLTSPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected XLTSSwitch<Adapter> modelSwitch =
    new XLTSSwitch<Adapter>()
    {
      @Override
      public Adapter caseDocument(Document object)
      {
        return createDocumentAdapter();
      }
      @Override
      public Adapter caseFormulaSet(FormulaSet object)
      {
        return createFormulaSetAdapter();
      }
      @Override
      public Adapter caseCheckSet(CheckSet object)
      {
        return createCheckSetAdapter();
      }
      @Override
      public Adapter caseCheck(Check object)
      {
        return createCheckAdapter();
      }
      @Override
      public Adapter caseLTLFormulaSet(LTLFormulaSet object)
      {
        return createLTLFormulaSetAdapter();
      }
      @Override
      public Adapter caseLTLFormulaDefinition(LTLFormulaDefinition object)
      {
        return createLTLFormulaDefinitionAdapter();
      }
      @Override
      public Adapter caseLTLCheckSet(LTLCheckSet object)
      {
        return createLTLCheckSetAdapter();
      }
      @Override
      public Adapter caseLTLCheck(LTLCheck object)
      {
        return createLTLCheckAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link de.hannover.uni.se.fmse.lts.xLTS.Document <em>Document</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.hannover.uni.se.fmse.lts.xLTS.Document
   * @generated
   */
  public Adapter createDocumentAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.hannover.uni.se.fmse.lts.xLTS.FormulaSet <em>Formula Set</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.hannover.uni.se.fmse.lts.xLTS.FormulaSet
   * @generated
   */
  public Adapter createFormulaSetAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.hannover.uni.se.fmse.lts.xLTS.CheckSet <em>Check Set</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.hannover.uni.se.fmse.lts.xLTS.CheckSet
   * @generated
   */
  public Adapter createCheckSetAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.hannover.uni.se.fmse.lts.xLTS.Check <em>Check</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.hannover.uni.se.fmse.lts.xLTS.Check
   * @generated
   */
  public Adapter createCheckAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet <em>LTL Formula Set</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaSet
   * @generated
   */
  public Adapter createLTLFormulaSetAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaDefinition <em>LTL Formula Definition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLFormulaDefinition
   * @generated
   */
  public Adapter createLTLFormulaDefinitionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLCheckSet <em>LTL Check Set</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLCheckSet
   * @generated
   */
  public Adapter createLTLCheckSetAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link de.hannover.uni.se.fmse.lts.xLTS.LTLCheck <em>LTL Check</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see de.hannover.uni.se.fmse.lts.xLTS.LTLCheck
   * @generated
   */
  public Adapter createLTLCheckAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //XLTSAdapterFactory
