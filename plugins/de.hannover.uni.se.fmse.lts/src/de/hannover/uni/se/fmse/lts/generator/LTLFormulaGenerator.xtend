package de.hannover.uni.se.fmse.lts.generator

import de.hannover.uni.se.fmse.ltl.AND
import de.hannover.uni.se.fmse.ltl.Binary
import de.hannover.uni.se.fmse.ltl.Constant
import de.hannover.uni.se.fmse.ltl.F
import de.hannover.uni.se.fmse.ltl.Formula
import de.hannover.uni.se.fmse.ltl.G
import de.hannover.uni.se.fmse.ltl.LTLProposition
import de.hannover.uni.se.fmse.ltl.NOT
import de.hannover.uni.se.fmse.ltl.OR
import de.hannover.uni.se.fmse.ltl.U
import de.hannover.uni.se.fmse.ltl.Unary
import de.hannover.uni.se.fmse.ltl.X

class LTLFormulaGenerator {
	public def static String generateLTLString(Formula f) {

		if (f instanceof Binary) {
			return generateLTLString(f.left) + " " + operator(f) + " " + generateLTLString(f.right)
		}
		if (f instanceof Unary) {
			return operator(f) + " " + "(" + generateLTLString(f.nested) + ")"
		}
		if (f instanceof Constant) {
			return if(f.value) "TRUE" else "FALSE"
		}
		if (f instanceof LTLProposition) {
			val ap = f.atomicProposition
			return ap.name
			//return f.atomicProposition.name
		}
		throw new IllegalArgumentException("Formula not supported")
	}

	def static String operator(Binary b) {
		if (b instanceof AND)
			return "&&"
		if (b instanceof OR)
			return "||"
		if (b instanceof U)
			return "U"
		throw new IllegalArgumentException("Operator not supported")
	}

	def static String operator(Unary u) {
		if (u instanceof NOT)
			return "!"
		if (u instanceof G)
			return "[]"
		if (u instanceof F)
			return "<>"
		if (u instanceof X)
			return "X"
		throw new IllegalArgumentException("Operator not supported")
	}
}
