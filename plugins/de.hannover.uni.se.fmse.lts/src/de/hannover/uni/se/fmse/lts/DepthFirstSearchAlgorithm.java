package de.hannover.uni.se.fmse.lts;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import de.hannover.uni.se.fmse.buechi.Automaton;

public class DepthFirstSearchAlgorithm {
	List<de.hannover.uni.se.fmse.buechi.Transition> blue, red;
	
	public List<de.hannover.uni.se.fmse.buechi.Transition> getBlueTransitions() {
		return blue;
	}
	public List<de.hannover.uni.se.fmse.buechi.Transition> getRedTransitions() {
		return red;
	}
	Queue<State> post = new ArrayDeque<State>();
	HashSet<State> visited = new HashSet<State>();
	Automaton a = null;
	State initState = null;
	Map<State, Transition> predecessor = new HashMap<>();

	public State getCycleState() {
		return initState;
	}

	public DepthFirstSearchAlgorithm(Automaton buechi) {
		a = buechi;
	}

	/**
	 * Deal with multiple initial states later..
	 * 
	 * @param a
	 * @return
	 */
	public boolean search(State s) {
		visited.add(s);
		for (Transition t : s.getOutgoing()) {
			if (!visited.contains(t.getDestination())) {
				predecessor.put(t.getDestination(), t);
				return search(t.getDestination());
			} else {
				InnerDFS i = new InnerDFS(a);
				if (a.getFinalStates().contains(s)
						&& i.search(s)) {
					blue = new ArrayList<>(); red = new ArrayList<>();
					Transition tt=i.predecessor.get(i.initState);
					do {
						red.add((de.hannover.uni.se.fmse.buechi.Transition) tt);
						tt = i.predecessor.get(tt.getSource());
					}while(tt.getSource() != i.initState);
					red.add((de.hannover.uni.se.fmse.buechi.Transition) tt);
					tt = predecessor.get(s);
					do{
						blue.add((de.hannover.uni.se.fmse.buechi.Transition) tt);
						 tt = predecessor.get(tt.getSource());
					} while(tt!= null && !a.getInitStates().contains(tt.getSource()));
						blue.add((de.hannover.uni.se.fmse.buechi.Transition) tt);
					return true;
				}
			}
		}
		return false;
	}

	class InnerDFS extends DepthFirstSearchAlgorithm {

		public InnerDFS(Automaton buechi) {
			super(buechi);
		}

		@Override
		public boolean search(State s) {
			if (visited.isEmpty())
				initState = s;
			visited.add(s);

			for (Transition t : s.getOutgoing()) {
				if (!visited.contains(t.getDestination())) {
					predecessor.put(t.getDestination(), t);
					return search(t.getDestination());
				} else if(t.getDestination()==initState){
					predecessor.put(initState, t);
					return true;
				}
			}
			return false;
		}
	}
}
