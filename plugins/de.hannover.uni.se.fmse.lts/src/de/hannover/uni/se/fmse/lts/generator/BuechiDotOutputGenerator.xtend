package de.hannover.uni.se.fmse.lts.generator

import de.hannover.uni.se.fmse.buechi.Automaton
import de.hannover.uni.se.fmse.buechi.Transition
import de.hannover.uni.se.fmse.lts.AtomicProposition
import java.util.List

class BuechiDotOutputGenerator {
	static def generate(Automaton a) {'''
		digraph {
		«FOR i: 0..a.states.size-1»
			«val state=a.states.get(i)»
			«IF state.incoming.size>0||a.initStates.contains(state)»state_«i» [ style="filled" label="«state.name»" «IF a.finalStates.contains(state)»shape=doublecircle«ENDIF» ]«ENDIF»
		«ENDFOR»
		«FOR i: 0..a.initStates.size-1»
			«val state=a.initStates.get(i)»
			state_init_«i» [ style="invisible" ]
			state_init_«i» -> state_«a.states.indexOf(state)»
		«ENDFOR»
		«FOR i: 0..a.transitions.size-1»
			«val t=a.transitions.get(i)»
			«IF t.source.incoming.size>0||a.initStates.contains(t.source)»state_«a.states.indexOf(t.source)» -> state_«a.states.indexOf(t.destination)» [ label="«IF (t as Transition).atomicPropositions.findFirst[ap|ap.name.equals("___<SIGMA>") ]!=null»Σ"«ELSE»«FOR ap:(t as Transition).atomicPropositions SEPARATOR ','» «ap.name» «ENDFOR»"«ENDIF» ] «ENDIF»
		«ENDFOR»
	}
	'''
	}
	
	static def generateCounterexample(Automaton a, List<Transition> blue, List<Transition>red) {'''
		digraph {
		«FOR i: 0..a.states.size-1»
			«val state=a.states.get(i)»
			«IF state.incoming.size>0||a.initStates.contains(state)»state_«i» [ style="filled" label="«state.name»" «IF a.finalStates.contains(state)»shape=doublecircle«ENDIF» ]«ENDIF»
		«ENDFOR»
		«FOR i: 0..a.initStates.size-1»
			«val state=a.initStates.get(i)»
			state_init_«i» [ style="invisible" ]
			state_init_«i» -> state_«a.states.indexOf(state)»
		«ENDFOR»
		«FOR i: 0..a.transitions.size-1»
			«val t=a.transitions.get(i)»
			«IF t.source.incoming.size>0||a.initStates.contains(t.source)»state_«a.states.indexOf(t.source)» -> state_«a.states.indexOf(t.destination)» [ label="«IF (t as Transition).atomicPropositions.findFirst[ap|ap.name.equals("___<SIGMA>") ]!=null»Σ"«ELSE»«FOR ap:(t as Transition).atomicPropositions SEPARATOR ','» «ap.name» «ENDFOR»"«ENDIF» «IF red.contains(t)&&blue.contains(t)»color=purple«ELSEIF red.contains(t)»color=red«ELSEIF blue.contains(t)»color=blue«ENDIF»] «ENDIF»
		«ENDFOR»
	}
	'''
	}
}