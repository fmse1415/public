grammar de.hannover.uni.se.fmse.lts.XLTS with org.eclipse.xtext.common.Terminals

generate xLTS "http://www.hannover.de/uni/se/fmse/lts/XLTS"
import "http://www.hannover.de/uni/se/fmse/lts/LTS"
import "http://www.hannover.de/uni/se/fmse/lts/CTL"
import "http://www.hannover.de/uni/se/fmse/lts/LTL" as ltl

Document:
	ltss+=(LTS | ComposedLTS)*
	formulaSet=FormulaSet?
	checkSet=CheckSet?
	LTLformulaSet=LTLFormulaSet?
	LTLcheckSet=LTLCheckSet?;

LTS returns LTS:
	'LTS' name=ID '{'
	('atomic' 'propositions' atomicPropositions+=AtomicProposition (',' atomicPropositions+=AtomicProposition)*)?
	'states' states+=BasicState (',' states+=BasicState)*
	'initial' initStates+=[State] (',' initStates+=[State])*
	('alphabet' alphabet+=Event (',' alphabet+=Event)*)?
	transitions+=Transition*
	'}';

AtomicProposition returns AtomicProposition:
	name=ID;

BasicState returns BasicState:
	name=ID ('{' atomicPropositions+=[AtomicProposition] (',' atomicPropositions+=[AtomicProposition])* '}')*;

Event returns Event:
	name=ID;

Transition returns Transition:
	source=[State] '->' destination=[State] ':' event=[Event];

ComposedLTS returns ComposedLTS:
	'compose' ltsLeft=[LTS] '||' ltsRight=[LTS] 'as' name=ID ('using' '{' events+=[Event] (',' events+=[Event])*
	'}')?;





FormulaSet:
	'CTL' 'FormulaSet' name=ID '{'
	formulae+=FormulaDefinition*
	'}';

FormulaDefinition returns FormulaDefinition:
	name=ID ':=' formula=Formula;

Formula returns Formula:
	EX | EG | EU | NOT | => AND | OR | Proposition | Constant;

EX returns EX:
	'EX' nested=Formula;

EG returns EG:
	'EG' nested=Formula;

EU returns EU:
	'E' '[' left=Formula 'U' right=Formula ']';

AND returns AND:
	'(' left=Formula '⋀' right=Formula ')';

OR returns OR:
	'(' left=Formula '⋁' right=Formula ')';

NOT returns NOT:
	'¬' nested=Formula;

Proposition returns Proposition:
	atomicProposition=[AtomicProposition];

Constant returns Constant:
	{Constant}
	((value?='true') | 'false');





CheckSet:
	'CTL' 'CheckSet' name=ID '{'
	checks+=Check*
	'}';

Check:
	'check' lts=[LTS] '⊨' formulaDefinition=[FormulaDefinition];

	
	
	
	
LTLFormulaSet:
	'LTL' 'FormulaSet' name=ID '{'
	formulae+=LTLFormulaDefinition*
	'}';

LTLFormulaDefinition:
	name=ID ':=' formula=LTLFormula;

LTLFormula returns ltl::Formula:
	LTLX | LTLG | LTLU | LTLF | LTLNOT | => LTLAND | LTLOR | LTLProposition | LTLConstant;

LTLX returns ltl::X:
	'X' nested=LTLFormula;

LTLG returns ltl::G:
	'G' nested=LTLFormula;

LTLU returns ltl::U:
	'[' left=LTLFormula 'U' right=LTLFormula ']';

LTLF returns ltl::F:
	'F' nested=LTLFormula;

LTLNOT returns ltl::NOT:
	'¬' nested=LTLFormula;

LTLAND returns ltl::AND:
	'(' left=LTLFormula '⋀' right=LTLFormula ')';

LTLOR returns ltl::OR:
	'(' left=LTLFormula '⋁' right=LTLFormula ')';

LTLProposition returns ltl::LTLProposition:
	atomicProposition=[AtomicProposition];

LTLConstant returns ltl::Constant:
	{ltl::Constant}
	((value?='true') | 'false');
	

	

	

LTLCheckSet:
	'LTL' 'CheckSet' name=ID '{'
	checks+=LTLCheck*
	'}';

LTLCheck:
	'check' lts=[LTS] '⊨' formulaDefinition=[LTLFormulaDefinition];
	