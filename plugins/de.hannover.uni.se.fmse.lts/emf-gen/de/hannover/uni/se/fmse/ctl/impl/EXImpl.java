/**
 */
package de.hannover.uni.se.fmse.ctl.impl;

import de.hannover.uni.se.fmse.ctl.CtlPackage;
import de.hannover.uni.se.fmse.ctl.EX;
import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.State;
import de.hannover.uni.se.fmse.lts.Transition;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EX</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class EXImpl extends UnaryImpl implements EX {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EXImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CtlPackage.Literals.EX;
	}
	
	@Override
	public boolean evaluate(LTS lts) {
		nested.evaluate(lts);
		for (State state : lts.getStates()) {
			if (nested.getSatisfyingStates().contains(state)) {
				for (Transition transition : state.getIncoming())
					getSatisfyingStates().add(transition.getSource());
			}
		}
		return getSatisfyingStates().containsAll(lts.getInitStates());
	}
} //EXImpl
