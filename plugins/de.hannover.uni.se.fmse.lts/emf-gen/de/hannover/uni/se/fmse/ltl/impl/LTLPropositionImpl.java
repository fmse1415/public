/**
 */
package de.hannover.uni.se.fmse.ltl.impl;

import de.hannover.uni.se.fmse.ltl.LTLProposition;
import de.hannover.uni.se.fmse.ltl.LtlPackage;

import de.hannover.uni.se.fmse.lts.AtomicProposition;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LTL Proposition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.ltl.impl.LTLPropositionImpl#getAtomicProposition <em>Atomic Proposition</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LTLPropositionImpl extends AtomicImpl implements LTLProposition {
	/**
	 * The cached value of the '{@link #getAtomicProposition() <em>Atomic Proposition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicProposition()
	 * @generated
	 * @ordered
	 */
	protected AtomicProposition atomicProposition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LTLPropositionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LtlPackage.Literals.LTL_PROPOSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicProposition getAtomicProposition() {
		if (atomicProposition != null && atomicProposition.eIsProxy()) {
			InternalEObject oldAtomicProposition = (InternalEObject)atomicProposition;
			atomicProposition = (AtomicProposition)eResolveProxy(oldAtomicProposition);
			if (atomicProposition != oldAtomicProposition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LtlPackage.LTL_PROPOSITION__ATOMIC_PROPOSITION, oldAtomicProposition, atomicProposition));
			}
		}
		return atomicProposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicProposition basicGetAtomicProposition() {
		return atomicProposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicProposition(AtomicProposition newAtomicProposition) {
		AtomicProposition oldAtomicProposition = atomicProposition;
		atomicProposition = newAtomicProposition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtlPackage.LTL_PROPOSITION__ATOMIC_PROPOSITION, oldAtomicProposition, atomicProposition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LtlPackage.LTL_PROPOSITION__ATOMIC_PROPOSITION:
				if (resolve) return getAtomicProposition();
				return basicGetAtomicProposition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LtlPackage.LTL_PROPOSITION__ATOMIC_PROPOSITION:
				setAtomicProposition((AtomicProposition)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LtlPackage.LTL_PROPOSITION__ATOMIC_PROPOSITION:
				setAtomicProposition((AtomicProposition)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LtlPackage.LTL_PROPOSITION__ATOMIC_PROPOSITION:
				return atomicProposition != null;
		}
		return super.eIsSet(featureID);
	}

} //LTLPropositionImpl
