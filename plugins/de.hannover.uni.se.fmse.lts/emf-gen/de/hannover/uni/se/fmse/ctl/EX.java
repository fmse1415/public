/**
 */
package de.hannover.uni.se.fmse.ctl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EX</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hannover.uni.se.fmse.ctl.CtlPackage#getEX()
 * @model
 * @generated
 */
public interface EX extends Unary {
} // EX
