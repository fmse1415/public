/**
 */
package de.hannover.uni.se.fmse.lts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getEvent()
 * @model
 * @generated
 */
public interface Event extends NamedElement {
} // Event
