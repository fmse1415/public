/**
 */
package de.hannover.uni.se.fmse.ctl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Atomic</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hannover.uni.se.fmse.ctl.CtlPackage#getAtomic()
 * @model abstract="true"
 * @generated
 */
public interface Atomic extends Formula {
} // Atomic
