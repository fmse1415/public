/**
 */
package de.hannover.uni.se.fmse.ltl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OR</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hannover.uni.se.fmse.ltl.LtlPackage#getOR()
 * @model
 * @generated
 */
public interface OR extends Binary {
} // OR
