/**
 */
package de.hannover.uni.se.fmse.ctl;

import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.State;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.ctl.Formula#getSatisfyingStates <em>Satisfying States</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.ctl.CtlPackage#getFormula()
 * @model abstract="true"
 * @generated
 */
public interface Formula extends EObject {
	/**
	 * Returns the value of the '<em><b>Satisfying States</b></em>' reference list.
	 * The list contents are of type {@link de.hannover.uni.se.fmse.lts.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Satisfying States</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Satisfying States</em>' reference list.
	 * @see de.hannover.uni.se.fmse.ctl.CtlPackage#getFormula_SatisfyingStates()
	 * @model
	 * @generated
	 */
	EList<State> getSatisfyingStates();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean evaluate(LTS lts);

} // Formula
