/**
 */
package de.hannover.uni.se.fmse.buechi;

import de.hannover.uni.se.fmse.lts.LtsPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hannover.uni.se.fmse.buechi.BuechiFactory
 * @model kind="package"
 * @generated
 */
public interface BuechiPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "buechi";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.hannover.de/uni/se/fmse/lts/Buechi";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "buechi";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BuechiPackage eINSTANCE = de.hannover.uni.se.fmse.buechi.impl.BuechiPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.buechi.impl.AutomatonImpl <em>Automaton</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.buechi.impl.AutomatonImpl
	 * @see de.hannover.uni.se.fmse.buechi.impl.BuechiPackageImpl#getAutomaton()
	 * @generated
	 */
	int AUTOMATON = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATON__NAME = LtsPackage.LTS__NAME;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATON__STATES = LtsPackage.LTS__STATES;

	/**
	 * The feature id for the '<em><b>Init States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATON__INIT_STATES = LtsPackage.LTS__INIT_STATES;

	/**
	 * The feature id for the '<em><b>Alphabet</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATON__ALPHABET = LtsPackage.LTS__ALPHABET;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATON__TRANSITIONS = LtsPackage.LTS__TRANSITIONS;

	/**
	 * The feature id for the '<em><b>Atomic Propositions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATON__ATOMIC_PROPOSITIONS = LtsPackage.LTS__ATOMIC_PROPOSITIONS;

	/**
	 * The feature id for the '<em><b>Final States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATON__FINAL_STATES = LtsPackage.LTS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Automaton</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATON_FEATURE_COUNT = LtsPackage.LTS_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Automaton</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATON_OPERATION_COUNT = LtsPackage.LTS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.buechi.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.buechi.impl.TransitionImpl
	 * @see de.hannover.uni.se.fmse.buechi.impl.BuechiPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 1;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__EVENT = LtsPackage.TRANSITION__EVENT;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__SOURCE = LtsPackage.TRANSITION__SOURCE;

	/**
	 * The feature id for the '<em><b>Destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__DESTINATION = LtsPackage.TRANSITION__DESTINATION;

	/**
	 * The feature id for the '<em><b>Atomic Propositions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ATOMIC_PROPOSITIONS = LtsPackage.TRANSITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = LtsPackage.TRANSITION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_OPERATION_COUNT = LtsPackage.TRANSITION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.buechi.Automaton <em>Automaton</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Automaton</em>'.
	 * @see de.hannover.uni.se.fmse.buechi.Automaton
	 * @generated
	 */
	EClass getAutomaton();

	/**
	 * Returns the meta object for the reference list '{@link de.hannover.uni.se.fmse.buechi.Automaton#getFinalStates <em>Final States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Final States</em>'.
	 * @see de.hannover.uni.se.fmse.buechi.Automaton#getFinalStates()
	 * @see #getAutomaton()
	 * @generated
	 */
	EReference getAutomaton_FinalStates();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.buechi.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see de.hannover.uni.se.fmse.buechi.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the reference list '{@link de.hannover.uni.se.fmse.buechi.Transition#getAtomicPropositions <em>Atomic Propositions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Atomic Propositions</em>'.
	 * @see de.hannover.uni.se.fmse.buechi.Transition#getAtomicPropositions()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_AtomicPropositions();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BuechiFactory getBuechiFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.buechi.impl.AutomatonImpl <em>Automaton</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.buechi.impl.AutomatonImpl
		 * @see de.hannover.uni.se.fmse.buechi.impl.BuechiPackageImpl#getAutomaton()
		 * @generated
		 */
		EClass AUTOMATON = eINSTANCE.getAutomaton();

		/**
		 * The meta object literal for the '<em><b>Final States</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AUTOMATON__FINAL_STATES = eINSTANCE.getAutomaton_FinalStates();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.buechi.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.buechi.impl.TransitionImpl
		 * @see de.hannover.uni.se.fmse.buechi.impl.BuechiPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Atomic Propositions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__ATOMIC_PROPOSITIONS = eINSTANCE.getTransition_AtomicPropositions();

	}

} //BuechiPackage
