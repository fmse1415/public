/**
 */
package de.hannover.uni.se.fmse.lts.impl;

import de.hannover.uni.se.fmse.lts.AtomicProposition;
import de.hannover.uni.se.fmse.lts.Event;
import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.LtsPackage;
import de.hannover.uni.se.fmse.lts.State;
import de.hannover.uni.se.fmse.lts.Transition;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LTS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.LTSImpl#getStates <em>States</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.LTSImpl#getInitStates <em>Init States</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.LTSImpl#getAlphabet <em>Alphabet</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.LTSImpl#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.LTSImpl#getAtomicPropositions <em>Atomic Propositions</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LTSImpl extends NamedElementImpl implements LTS {
	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> states;

	/**
	 * The cached value of the '{@link #getInitStates() <em>Init States</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> initStates;

	/**
	 * The cached value of the '{@link #getAlphabet() <em>Alphabet</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlphabet()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> alphabet;

	/**
	 * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> transitions;

	/**
	 * The cached value of the '{@link #getAtomicPropositions() <em>Atomic Propositions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicPropositions()
	 * @generated
	 * @ordered
	 */
	protected EList<AtomicProposition> atomicPropositions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LTSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LtsPackage.Literals.LTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getStates() {
		if (states == null) {
			states = new EObjectContainmentEList<State>(State.class, this, LtsPackage.LTS__STATES);
		}
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getInitStates() {
		if (initStates == null) {
			initStates = new EObjectResolvingEList<State>(State.class, this, LtsPackage.LTS__INIT_STATES);
		}
		return initStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getAlphabet() {
		if (alphabet == null) {
			alphabet = new EObjectContainmentEList<Event>(Event.class, this, LtsPackage.LTS__ALPHABET);
		}
		return alphabet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getTransitions() {
		if (transitions == null) {
			transitions = new EObjectContainmentEList<Transition>(Transition.class, this, LtsPackage.LTS__TRANSITIONS);
		}
		return transitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicProposition> getAtomicPropositions() {
		if (atomicPropositions == null) {
			atomicPropositions = new EObjectContainmentEList<AtomicProposition>(AtomicProposition.class, this, LtsPackage.LTS__ATOMIC_PROPOSITIONS);
		}
		return atomicPropositions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LtsPackage.LTS__STATES:
				return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
			case LtsPackage.LTS__ALPHABET:
				return ((InternalEList<?>)getAlphabet()).basicRemove(otherEnd, msgs);
			case LtsPackage.LTS__TRANSITIONS:
				return ((InternalEList<?>)getTransitions()).basicRemove(otherEnd, msgs);
			case LtsPackage.LTS__ATOMIC_PROPOSITIONS:
				return ((InternalEList<?>)getAtomicPropositions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LtsPackage.LTS__STATES:
				return getStates();
			case LtsPackage.LTS__INIT_STATES:
				return getInitStates();
			case LtsPackage.LTS__ALPHABET:
				return getAlphabet();
			case LtsPackage.LTS__TRANSITIONS:
				return getTransitions();
			case LtsPackage.LTS__ATOMIC_PROPOSITIONS:
				return getAtomicPropositions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LtsPackage.LTS__STATES:
				getStates().clear();
				getStates().addAll((Collection<? extends State>)newValue);
				return;
			case LtsPackage.LTS__INIT_STATES:
				getInitStates().clear();
				getInitStates().addAll((Collection<? extends State>)newValue);
				return;
			case LtsPackage.LTS__ALPHABET:
				getAlphabet().clear();
				getAlphabet().addAll((Collection<? extends Event>)newValue);
				return;
			case LtsPackage.LTS__TRANSITIONS:
				getTransitions().clear();
				getTransitions().addAll((Collection<? extends Transition>)newValue);
				return;
			case LtsPackage.LTS__ATOMIC_PROPOSITIONS:
				getAtomicPropositions().clear();
				getAtomicPropositions().addAll((Collection<? extends AtomicProposition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LtsPackage.LTS__STATES:
				getStates().clear();
				return;
			case LtsPackage.LTS__INIT_STATES:
				getInitStates().clear();
				return;
			case LtsPackage.LTS__ALPHABET:
				getAlphabet().clear();
				return;
			case LtsPackage.LTS__TRANSITIONS:
				getTransitions().clear();
				return;
			case LtsPackage.LTS__ATOMIC_PROPOSITIONS:
				getAtomicPropositions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LtsPackage.LTS__STATES:
				return states != null && !states.isEmpty();
			case LtsPackage.LTS__INIT_STATES:
				return initStates != null && !initStates.isEmpty();
			case LtsPackage.LTS__ALPHABET:
				return alphabet != null && !alphabet.isEmpty();
			case LtsPackage.LTS__TRANSITIONS:
				return transitions != null && !transitions.isEmpty();
			case LtsPackage.LTS__ATOMIC_PROPOSITIONS:
				return atomicPropositions != null && !atomicPropositions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LTSImpl
