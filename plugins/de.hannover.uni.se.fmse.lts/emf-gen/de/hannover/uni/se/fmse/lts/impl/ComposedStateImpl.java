/**
 */
package de.hannover.uni.se.fmse.lts.impl;

import de.hannover.uni.se.fmse.lts.AtomicProposition;
import de.hannover.uni.se.fmse.lts.ComposedState;
import de.hannover.uni.se.fmse.lts.LtsPackage;
import de.hannover.uni.se.fmse.lts.State;
import de.hannover.uni.se.fmse.lts.Transition;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composed State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.ComposedStateImpl#getOutgoing <em>Outgoing</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.ComposedStateImpl#getIncoming <em>Incoming</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.ComposedStateImpl#getAtomicPropositions <em>Atomic Propositions</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.ComposedStateImpl#getPart1 <em>Part1</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.ComposedStateImpl#getPart2 <em>Part2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComposedStateImpl extends MinimalEObjectImpl.Container implements ComposedState {
	/**
	 * The cached value of the '{@link #getOutgoing() <em>Outgoing</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutgoing()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> outgoing;

	/**
	 * The cached value of the '{@link #getIncoming() <em>Incoming</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncoming()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> incoming;

	/**
	 * The cached value of the '{@link #getAtomicPropositions() <em>Atomic Propositions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicPropositions()
	 * @generated
	 * @ordered
	 */
	protected EList<AtomicProposition> atomicPropositions;

	/**
	 * The cached value of the '{@link #getPart1() <em>Part1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPart1()
	 * @generated
	 * @ordered
	 */
	protected State part1;

	/**
	 * The cached value of the '{@link #getPart2() <em>Part2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPart2()
	 * @generated
	 * @ordered
	 */
	protected State part2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LtsPackage.Literals.COMPOSED_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getOutgoing() {
		if (outgoing == null) {
			outgoing = new EObjectWithInverseResolvingEList<Transition>(Transition.class, this, LtsPackage.COMPOSED_STATE__OUTGOING, LtsPackage.TRANSITION__SOURCE);
		}
		return outgoing;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getIncoming() {
		if (incoming == null) {
			incoming = new EObjectWithInverseResolvingEList<Transition>(Transition.class, this, LtsPackage.COMPOSED_STATE__INCOMING, LtsPackage.TRANSITION__DESTINATION);
		}
		return incoming;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicProposition> getAtomicPropositions() {
		if (atomicPropositions == null) {
			atomicPropositions = new EObjectResolvingEList<AtomicProposition>(AtomicProposition.class, this, LtsPackage.COMPOSED_STATE__ATOMIC_PROPOSITIONS);
		}
		return atomicPropositions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getPart1() {
		if (part1 != null && part1.eIsProxy()) {
			InternalEObject oldPart1 = (InternalEObject)part1;
			part1 = (State)eResolveProxy(oldPart1);
			if (part1 != oldPart1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LtsPackage.COMPOSED_STATE__PART1, oldPart1, part1));
			}
		}
		return part1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetPart1() {
		return part1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPart1(State newPart1) {
		State oldPart1 = part1;
		part1 = newPart1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.COMPOSED_STATE__PART1, oldPart1, part1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getPart2() {
		if (part2 != null && part2.eIsProxy()) {
			InternalEObject oldPart2 = (InternalEObject)part2;
			part2 = (State)eResolveProxy(oldPart2);
			if (part2 != oldPart2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LtsPackage.COMPOSED_STATE__PART2, oldPart2, part2));
			}
		}
		return part2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetPart2() {
		return part2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPart2(State newPart2) {
		State oldPart2 = part2;
		part2 = newPart2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.COMPOSED_STATE__PART2, oldPart2, part2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getName() {
		return getPart1().getName() + "," + getPart2().getName();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LtsPackage.COMPOSED_STATE__OUTGOING:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOutgoing()).basicAdd(otherEnd, msgs);
			case LtsPackage.COMPOSED_STATE__INCOMING:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIncoming()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LtsPackage.COMPOSED_STATE__OUTGOING:
				return ((InternalEList<?>)getOutgoing()).basicRemove(otherEnd, msgs);
			case LtsPackage.COMPOSED_STATE__INCOMING:
				return ((InternalEList<?>)getIncoming()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LtsPackage.COMPOSED_STATE__OUTGOING:
				return getOutgoing();
			case LtsPackage.COMPOSED_STATE__INCOMING:
				return getIncoming();
			case LtsPackage.COMPOSED_STATE__ATOMIC_PROPOSITIONS:
				return getAtomicPropositions();
			case LtsPackage.COMPOSED_STATE__PART1:
				if (resolve) return getPart1();
				return basicGetPart1();
			case LtsPackage.COMPOSED_STATE__PART2:
				if (resolve) return getPart2();
				return basicGetPart2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LtsPackage.COMPOSED_STATE__OUTGOING:
				getOutgoing().clear();
				getOutgoing().addAll((Collection<? extends Transition>)newValue);
				return;
			case LtsPackage.COMPOSED_STATE__INCOMING:
				getIncoming().clear();
				getIncoming().addAll((Collection<? extends Transition>)newValue);
				return;
			case LtsPackage.COMPOSED_STATE__ATOMIC_PROPOSITIONS:
				getAtomicPropositions().clear();
				getAtomicPropositions().addAll((Collection<? extends AtomicProposition>)newValue);
				return;
			case LtsPackage.COMPOSED_STATE__PART1:
				setPart1((State)newValue);
				return;
			case LtsPackage.COMPOSED_STATE__PART2:
				setPart2((State)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LtsPackage.COMPOSED_STATE__OUTGOING:
				getOutgoing().clear();
				return;
			case LtsPackage.COMPOSED_STATE__INCOMING:
				getIncoming().clear();
				return;
			case LtsPackage.COMPOSED_STATE__ATOMIC_PROPOSITIONS:
				getAtomicPropositions().clear();
				return;
			case LtsPackage.COMPOSED_STATE__PART1:
				setPart1((State)null);
				return;
			case LtsPackage.COMPOSED_STATE__PART2:
				setPart2((State)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LtsPackage.COMPOSED_STATE__OUTGOING:
				return outgoing != null && !outgoing.isEmpty();
			case LtsPackage.COMPOSED_STATE__INCOMING:
				return incoming != null && !incoming.isEmpty();
			case LtsPackage.COMPOSED_STATE__ATOMIC_PROPOSITIONS:
				return atomicPropositions != null && !atomicPropositions.isEmpty();
			case LtsPackage.COMPOSED_STATE__PART1:
				return part1 != null;
			case LtsPackage.COMPOSED_STATE__PART2:
				return part2 != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case LtsPackage.COMPOSED_STATE___GET_NAME:
				return getName();
		}
		return super.eInvoke(operationID, arguments);
	}

} //ComposedStateImpl
