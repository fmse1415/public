/**
 */
package de.hannover.uni.se.fmse.lts;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.State#getOutgoing <em>Outgoing</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.State#getIncoming <em>Incoming</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.State#getAtomicPropositions <em>Atomic Propositions</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getState()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface State extends EObject {
	/**
	 * Returns the value of the '<em><b>Outgoing</b></em>' reference list.
	 * The list contents are of type {@link de.hannover.uni.se.fmse.lts.Transition}.
	 * It is bidirectional and its opposite is '{@link de.hannover.uni.se.fmse.lts.Transition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outgoing</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outgoing</em>' reference list.
	 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getState_Outgoing()
	 * @see de.hannover.uni.se.fmse.lts.Transition#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<Transition> getOutgoing();

	/**
	 * Returns the value of the '<em><b>Incoming</b></em>' reference list.
	 * The list contents are of type {@link de.hannover.uni.se.fmse.lts.Transition}.
	 * It is bidirectional and its opposite is '{@link de.hannover.uni.se.fmse.lts.Transition#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming</em>' reference list.
	 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getState_Incoming()
	 * @see de.hannover.uni.se.fmse.lts.Transition#getDestination
	 * @model opposite="destination"
	 * @generated
	 */
	EList<Transition> getIncoming();

	/**
	 * Returns the value of the '<em><b>Atomic Propositions</b></em>' reference list.
	 * The list contents are of type {@link de.hannover.uni.se.fmse.lts.AtomicProposition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Propositions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Propositions</em>' reference list.
	 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getState_AtomicPropositions()
	 * @model
	 * @generated
	 */
	EList<AtomicProposition> getAtomicPropositions();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getName();

} // State
