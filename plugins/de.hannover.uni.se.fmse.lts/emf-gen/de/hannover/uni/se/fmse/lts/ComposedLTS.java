/**
 */
package de.hannover.uni.se.fmse.lts;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composed LTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.ComposedLTS#getLtsLeft <em>Lts Left</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.ComposedLTS#getLtsRight <em>Lts Right</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.ComposedLTS#getEvents <em>Events</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getComposedLTS()
 * @model
 * @generated
 */
public interface ComposedLTS extends LTS {
	/**
	 * Returns the value of the '<em><b>Lts Left</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lts Left</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lts Left</em>' reference.
	 * @see #setLtsLeft(LTS)
	 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getComposedLTS_LtsLeft()
	 * @model
	 * @generated
	 */
	LTS getLtsLeft();

	/**
	 * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.ComposedLTS#getLtsLeft <em>Lts Left</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lts Left</em>' reference.
	 * @see #getLtsLeft()
	 * @generated
	 */
	void setLtsLeft(LTS value);

	/**
	 * Returns the value of the '<em><b>Lts Right</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lts Right</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lts Right</em>' reference.
	 * @see #setLtsRight(LTS)
	 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getComposedLTS_LtsRight()
	 * @model
	 * @generated
	 */
	LTS getLtsRight();

	/**
	 * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.ComposedLTS#getLtsRight <em>Lts Right</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lts Right</em>' reference.
	 * @see #getLtsRight()
	 * @generated
	 */
	void setLtsRight(LTS value);

	/**
	 * Returns the value of the '<em><b>Events</b></em>' reference list.
	 * The list contents are of type {@link de.hannover.uni.se.fmse.lts.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' reference list.
	 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getComposedLTS_Events()
	 * @model
	 * @generated
	 */
	EList<Event> getEvents();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	LTS compose();

} // ComposedLTS
