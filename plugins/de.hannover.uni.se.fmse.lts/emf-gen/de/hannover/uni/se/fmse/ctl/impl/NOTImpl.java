/**
 */
package de.hannover.uni.se.fmse.ctl.impl;

import de.hannover.uni.se.fmse.ctl.CtlPackage;
import de.hannover.uni.se.fmse.ctl.NOT;
import de.hannover.uni.se.fmse.lts.LTS;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>NOT</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class NOTImpl extends UnaryImpl implements NOT {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NOTImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CtlPackage.Literals.NOT;
	}
	
	@Override
	public boolean evaluate(LTS lts) {
		boolean b = nested.evaluate(lts);
		getSatisfyingStates().addAll(lts.getStates());
		getSatisfyingStates().removeAll(nested.getSatisfyingStates());
		return !b;
	}

} //NOTImpl
