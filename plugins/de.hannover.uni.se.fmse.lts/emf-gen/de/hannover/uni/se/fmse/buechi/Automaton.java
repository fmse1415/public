/**
 */
package de.hannover.uni.se.fmse.buechi;

import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.State;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Automaton</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.buechi.Automaton#getFinalStates <em>Final States</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.buechi.BuechiPackage#getAutomaton()
 * @model
 * @generated
 */
public interface Automaton extends LTS {
	/**
	 * Returns the value of the '<em><b>Final States</b></em>' reference list.
	 * The list contents are of type {@link de.hannover.uni.se.fmse.lts.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final States</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final States</em>' reference list.
	 * @see de.hannover.uni.se.fmse.buechi.BuechiPackage#getAutomaton_FinalStates()
	 * @model
	 * @generated
	 */
	EList<State> getFinalStates();

} // Automaton
