/**
 */
package de.hannover.uni.se.fmse.ctl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.ctl.Binary#getLeft <em>Left</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.ctl.Binary#getRight <em>Right</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.ctl.CtlPackage#getBinary()
 * @model abstract="true"
 * @generated
 */
public interface Binary extends Formula {
	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(Formula)
	 * @see de.hannover.uni.se.fmse.ctl.CtlPackage#getBinary_Left()
	 * @model containment="true"
	 * @generated
	 */
	Formula getLeft();

	/**
	 * Sets the value of the '{@link de.hannover.uni.se.fmse.ctl.Binary#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(Formula value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(Formula)
	 * @see de.hannover.uni.se.fmse.ctl.CtlPackage#getBinary_Right()
	 * @model containment="true"
	 * @generated
	 */
	Formula getRight();

	/**
	 * Sets the value of the '{@link de.hannover.uni.se.fmse.ctl.Binary#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(Formula value);

} // Binary
