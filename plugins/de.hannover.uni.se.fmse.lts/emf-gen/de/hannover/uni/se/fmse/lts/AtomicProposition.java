/**
 */
package de.hannover.uni.se.fmse.lts;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Atomic Proposition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getAtomicProposition()
 * @model
 * @generated
 */
public interface AtomicProposition extends NamedElement {

} // AtomicProposition
