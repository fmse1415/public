/**
 */
package de.hannover.uni.se.fmse.lts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composed State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.ComposedState#getPart1 <em>Part1</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.ComposedState#getPart2 <em>Part2</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getComposedState()
 * @model
 * @generated
 */
public interface ComposedState extends State {
	/**
	 * Returns the value of the '<em><b>Part1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Part1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Part1</em>' reference.
	 * @see #setPart1(State)
	 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getComposedState_Part1()
	 * @model required="true"
	 * @generated
	 */
	State getPart1();

	/**
	 * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.ComposedState#getPart1 <em>Part1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Part1</em>' reference.
	 * @see #getPart1()
	 * @generated
	 */
	void setPart1(State value);

	/**
	 * Returns the value of the '<em><b>Part2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Part2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Part2</em>' reference.
	 * @see #setPart2(State)
	 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getComposedState_Part2()
	 * @model required="true"
	 * @generated
	 */
	State getPart2();

	/**
	 * Sets the value of the '{@link de.hannover.uni.se.fmse.lts.ComposedState#getPart2 <em>Part2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Part2</em>' reference.
	 * @see #getPart2()
	 * @generated
	 */
	void setPart2(State value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getName();

} // ComposedState
