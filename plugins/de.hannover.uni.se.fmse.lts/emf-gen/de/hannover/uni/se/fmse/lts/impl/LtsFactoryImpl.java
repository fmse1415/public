/**
 */
package de.hannover.uni.se.fmse.lts.impl;

import de.hannover.uni.se.fmse.lts.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LtsFactoryImpl extends EFactoryImpl implements LtsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LtsFactory init() {
		try {
			LtsFactory theLtsFactory = (LtsFactory)EPackage.Registry.INSTANCE.getEFactory(LtsPackage.eNS_URI);
			if (theLtsFactory != null) {
				return theLtsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new LtsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LtsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case LtsPackage.LTS: return createLTS();
			case LtsPackage.TRANSITION: return createTransition();
			case LtsPackage.EVENT: return createEvent();
			case LtsPackage.LTS_COMPOSITOR: return createLTSCompositor();
			case LtsPackage.BASIC_STATE: return createBasicState();
			case LtsPackage.COMPOSED_STATE: return createComposedState();
			case LtsPackage.ATOMIC_PROPOSITION: return createAtomicProposition();
			case LtsPackage.COMPOSED_LTS: return createComposedLTS();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LTS createLTS() {
		LTSImpl lts = new LTSImpl();
		return lts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition createTransition() {
		TransitionImpl transition = new TransitionImpl();
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event createEvent() {
		EventImpl event = new EventImpl();
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LTSCompositor createLTSCompositor() {
		LTSCompositorImpl ltsCompositor = new LTSCompositorImpl();
		return ltsCompositor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicState createBasicState() {
		BasicStateImpl basicState = new BasicStateImpl();
		return basicState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposedState createComposedState() {
		ComposedStateImpl composedState = new ComposedStateImpl();
		return composedState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicProposition createAtomicProposition() {
		AtomicPropositionImpl atomicProposition = new AtomicPropositionImpl();
		return atomicProposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposedLTS createComposedLTS() {
		ComposedLTSImpl composedLTS = new ComposedLTSImpl();
		return composedLTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LtsPackage getLtsPackage() {
		return (LtsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static LtsPackage getPackage() {
		return LtsPackage.eINSTANCE;
	}

} //LtsFactoryImpl
