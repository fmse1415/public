/**
 */
package de.hannover.uni.se.fmse.lts;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.hannover.uni.se.fmse.lts.LtsPackage
 * @generated
 */
public interface LtsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LtsFactory eINSTANCE = de.hannover.uni.se.fmse.lts.impl.LtsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>LTS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LTS</em>'.
	 * @generated
	 */
	LTS createLTS();

	/**
	 * Returns a new object of class '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transition</em>'.
	 * @generated
	 */
	Transition createTransition();

	/**
	 * Returns a new object of class '<em>Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event</em>'.
	 * @generated
	 */
	Event createEvent();

	/**
	 * Returns a new object of class '<em>LTS Compositor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LTS Compositor</em>'.
	 * @generated
	 */
	LTSCompositor createLTSCompositor();

	/**
	 * Returns a new object of class '<em>Basic State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Basic State</em>'.
	 * @generated
	 */
	BasicState createBasicState();

	/**
	 * Returns a new object of class '<em>Composed State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composed State</em>'.
	 * @generated
	 */
	ComposedState createComposedState();

	/**
	 * Returns a new object of class '<em>Atomic Proposition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Atomic Proposition</em>'.
	 * @generated
	 */
	AtomicProposition createAtomicProposition();

	/**
	 * Returns a new object of class '<em>Composed LTS</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composed LTS</em>'.
	 * @generated
	 */
	ComposedLTS createComposedLTS();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	LtsPackage getLtsPackage();

} //LtsFactory
