/**
 */
package de.hannover.uni.se.fmse.ltl.impl;

import de.hannover.uni.se.fmse.ltl.Atomic;
import de.hannover.uni.se.fmse.ltl.LtlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Atomic</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class AtomicImpl extends FormulaImpl implements Atomic {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AtomicImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LtlPackage.Literals.ATOMIC;
	}

} //AtomicImpl
