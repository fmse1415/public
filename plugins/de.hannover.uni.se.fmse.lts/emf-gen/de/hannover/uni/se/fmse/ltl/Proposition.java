/**
 */
package de.hannover.uni.se.fmse.ltl;

import de.hannover.uni.se.fmse.lts.AtomicProposition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Proposition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.ltl.Proposition#getAtomicProposition <em>Atomic Proposition</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.ltl.LtlPackage#getProposition()
 * @model
 * @generated
 */
public interface Proposition extends Atomic {
	/**
	 * Returns the value of the '<em><b>Atomic Proposition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Proposition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Proposition</em>' reference.
	 * @see #setAtomicProposition(AtomicProposition)
	 * @see de.hannover.uni.se.fmse.ltl.LtlPackage#getProposition_AtomicProposition()
	 * @model
	 * @generated
	 */
	AtomicProposition getAtomicProposition();

	/**
	 * Sets the value of the '{@link de.hannover.uni.se.fmse.ltl.Proposition#getAtomicProposition <em>Atomic Proposition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atomic Proposition</em>' reference.
	 * @see #getAtomicProposition()
	 * @generated
	 */
	void setAtomicProposition(AtomicProposition value);

} // Proposition
