/**
 */
package de.hannover.uni.se.fmse.buechi;

import de.hannover.uni.se.fmse.lts.AtomicProposition;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.buechi.Transition#getAtomicPropositions <em>Atomic Propositions</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.buechi.BuechiPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends de.hannover.uni.se.fmse.lts.Transition {
	/**
	 * Returns the value of the '<em><b>Atomic Propositions</b></em>' containment reference list.
	 * The list contents are of type {@link de.hannover.uni.se.fmse.lts.AtomicProposition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Propositions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Propositions</em>' containment reference list.
	 * @see de.hannover.uni.se.fmse.buechi.BuechiPackage#getTransition_AtomicPropositions()
	 * @model containment="true"
	 * @generated
	 */
	EList<AtomicProposition> getAtomicPropositions();

} // Transition
