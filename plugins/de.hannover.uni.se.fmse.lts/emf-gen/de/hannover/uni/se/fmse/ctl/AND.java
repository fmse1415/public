/**
 */
package de.hannover.uni.se.fmse.ctl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AND</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hannover.uni.se.fmse.ctl.CtlPackage#getAND()
 * @model
 * @generated
 */
public interface AND extends Binary {
} // AND
