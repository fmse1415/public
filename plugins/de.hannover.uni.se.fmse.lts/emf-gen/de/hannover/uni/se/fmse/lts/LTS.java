/**
 */
package de.hannover.uni.se.fmse.lts;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LTS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.LTS#getStates <em>States</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.LTS#getInitStates <em>Init States</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.LTS#getAlphabet <em>Alphabet</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.LTS#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.LTS#getAtomicPropositions <em>Atomic Propositions</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getLTS()
 * @model
 * @generated
 */
public interface LTS extends NamedElement {
	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link de.hannover.uni.se.fmse.lts.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getLTS_States()
	 * @model containment="true"
	 * @generated
	 */
	EList<State> getStates();

	/**
	 * Returns the value of the '<em><b>Init States</b></em>' reference list.
	 * The list contents are of type {@link de.hannover.uni.se.fmse.lts.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Init States</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Init States</em>' reference list.
	 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getLTS_InitStates()
	 * @model
	 * @generated
	 */
	EList<State> getInitStates();

	/**
	 * Returns the value of the '<em><b>Alphabet</b></em>' containment reference list.
	 * The list contents are of type {@link de.hannover.uni.se.fmse.lts.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alphabet</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alphabet</em>' containment reference list.
	 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getLTS_Alphabet()
	 * @model containment="true"
	 * @generated
	 */
	EList<Event> getAlphabet();

	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link de.hannover.uni.se.fmse.lts.Transition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transitions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getLTS_Transitions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Transition> getTransitions();

	/**
	 * Returns the value of the '<em><b>Atomic Propositions</b></em>' containment reference list.
	 * The list contents are of type {@link de.hannover.uni.se.fmse.lts.AtomicProposition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atomic Propositions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atomic Propositions</em>' containment reference list.
	 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getLTS_AtomicPropositions()
	 * @model containment="true"
	 * @generated
	 */
	EList<AtomicProposition> getAtomicPropositions();

} // LTS
