/**
 */
package de.hannover.uni.se.fmse.ctl.impl;

import java.util.ArrayList;
import java.util.List;

import de.hannover.uni.se.fmse.ctl.CtlPackage;
import de.hannover.uni.se.fmse.ctl.OR;
import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.State;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OR</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ORImpl extends BinaryImpl implements OR {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ORImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CtlPackage.Literals.OR;
	}

	@Override
	public boolean evaluate(LTS lts) {
		boolean b1 = left.evaluate(lts);
		boolean b2 = right.evaluate(lts);
		List<State> A = new ArrayList<State>(left.getSatisfyingStates());
		List<State> B = right.getSatisfyingStates();
		List<State> B_A = new ArrayList<State>(B); // B\A
		B_A.removeAll(A);
		getSatisfyingStates().addAll(A);
		getSatisfyingStates().addAll(B_A);
		
		return b1 || b2;
	}
	
} //ORImpl
