/**
 */
package de.hannover.uni.se.fmse.ctl.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import de.hannover.uni.se.fmse.ctl.CtlPackage;
import de.hannover.uni.se.fmse.ctl.EG;
import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.State;
import de.hannover.uni.se.fmse.lts.Transition;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EG</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class EGImpl extends UnaryImpl implements EG {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EGImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CtlPackage.Literals.EG;
	}
	
	@Override
	/**
	 * Evaluates if the LTS is satisfying EG formula.
	 * It checks for all states that are satisfying the formula that have a path to a scc 
	 * and where all states in that path is satisfying the formula.
	 * This is evaluated from the scc states to the outside of the LTS.
	 * @return Returns if all initStates of the LTS are satisfying the EG formula
	 */
	public boolean evaluate(LTS lts) {
		Set<State> temporarySatisfyingStates = new HashSet<State>();
		getNested().evaluate(lts);
		// add all states in which the formula is satisfied
		for (State state: lts.getStates()){
		/*	if (getNested().evaluate(lts))
				temporarySatisfyingStates.add(state);*/
				if(getNested().getSatisfyingStates().contains(state))
					temporarySatisfyingStates.add(state);
		}		
		// add all states to the returned list that are in a scc
		List<List<State>> sccStates = new SCCFinder(temporarySatisfyingStates).getSccStates();
		for (List<State> states: sccStates){
			for (State state: states){
				getSatisfyingStates().add(state);
			}
		}		
		temporarySatisfyingStates.removeAll(getSatisfyingStates());	// all formula satisfying states without SCC
		Set<State> unvisitedStates = temporarySatisfyingStates; // as above checks for all satisfying states not in scc
		List<State> temporalStates = getSatisfyingStates(); // all SCC states
		List<State> additionalStates = new ArrayList<>(); // states that have a satisfying path to scc
		while (!unvisitedStates.isEmpty()){ // while there are still unvisited states
			for (State state: temporalStates){ // for all scc states in the first loop and afterwards all additional satisfying states
				for (Transition trans: state.getIncoming()){ // get incoming states
					if (temporarySatisfyingStates.contains(trans.getSource())){ // if incoming state is satisfying formula
						additionalStates.add(trans.getSource()); // add incoming state
						unvisitedStates.remove(trans.getSource()); // remove incoming state from unvisited
					}
				}
			}
			temporalStates.clear(); // clear satisfying states, since they have been checked already
			temporalStates.addAll(additionalStates); // add new satisfying states that have a path to scc
			getSatisfyingStates().addAll(additionalStates); // add new satisfying states to returned list
			
			if (additionalStates.isEmpty()) // if no new additional states are added it means there are no paths anymore
				break;
			else
				additionalStates.clear(); // clear additional states, so they don't get added again
		}
		return getSatisfyingStates().containsAll(lts.getInitStates());
	}

	public class SCCFinder {

		Stack<State> stack = new Stack<>();
		HashMap<State, Integer> indexMap = new HashMap<>();
		HashMap<State, Integer> lowlinkMap = new HashMap<>();
		private int index;
		Set<State> explorableStates;
		List<List<State>> sccStates = new ArrayList<List<State>>();
		
		public List<List<State>> getSccStates() {
			return sccStates;
		}

		public SCCFinder(Set<State> states){
			index = 0;
			explorableStates= states;
			for(State state:states){
				if (!indexMap.containsKey(state)){
					sccStates.add(strongConnect(state));
				}
			}
		}

		private List<State> strongConnect(State state) {
			indexMap.put(state, index);
			lowlinkMap.put(state, index);
			index++;
			stack.push(state);			
			for(Transition trans: state.getOutgoing()){
				State target = trans.getDestination();
				if(!this.explorableStates.contains(target)) continue;
				if (!indexMap.containsKey(target)){
					strongConnect(target);
					lowlinkMap.put(state, Math.min(lowlinkMap.get(state), lowlinkMap.get(target)));
				}
				else if (stack.contains(target)){
					lowlinkMap.put(state, Math.min(lowlinkMap.get(state), indexMap.get(target)));
				}
			}			  
			List<State> scc = new ArrayList<>();
			if (indexMap.get(state).equals(lowlinkMap.get(state))){
				State w;
				do {
					w = stack.pop();
					scc.add(w);
				} while (state != w);
			}			
			return scc;
		}
	}
} //EGImpl
