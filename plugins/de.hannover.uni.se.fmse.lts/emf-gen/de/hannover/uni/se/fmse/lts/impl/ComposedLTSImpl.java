/**
 */
package de.hannover.uni.se.fmse.lts.impl;

import de.hannover.uni.se.fmse.lts.AtomicProposition;
import de.hannover.uni.se.fmse.lts.ComposedLTS;
import de.hannover.uni.se.fmse.lts.ComposedState;
import de.hannover.uni.se.fmse.lts.Event;
import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.LtsFactory;
import de.hannover.uni.se.fmse.lts.LtsPackage;
import de.hannover.uni.se.fmse.lts.State;
import de.hannover.uni.se.fmse.lts.Transition;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Queue;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Composed LTS</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.ComposedLTSImpl#getLtsLeft <em>Lts Left</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.ComposedLTSImpl#getLtsRight <em>Lts Right</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.ComposedLTSImpl#getEvents <em>Events</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComposedLTSImpl extends LTSImpl implements ComposedLTS {
	/**
	 * The cached value of the '{@link #getLtsLeft() <em>Lts Left</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLtsLeft()
	 * @generated
	 * @ordered
	 */
	protected LTS ltsLeft;

	/**
	 * The cached value of the '{@link #getLtsRight() <em>Lts Right</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLtsRight()
	 * @generated
	 * @ordered
	 */
	protected LTS ltsRight;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedLTSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LtsPackage.Literals.COMPOSED_LTS;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LTS getLtsLeft() {
		if (ltsLeft != null && ltsLeft.eIsProxy()) {
			InternalEObject oldLtsLeft = (InternalEObject)ltsLeft;
			ltsLeft = (LTS)eResolveProxy(oldLtsLeft);
			if (ltsLeft != oldLtsLeft) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LtsPackage.COMPOSED_LTS__LTS_LEFT, oldLtsLeft, ltsLeft));
			}
		}
		return ltsLeft;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LTS basicGetLtsLeft() {
		return ltsLeft;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setLtsLeft(LTS newLtsLeft) {
		LTS oldLtsLeft = ltsLeft;
		ltsLeft = newLtsLeft;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.COMPOSED_LTS__LTS_LEFT, oldLtsLeft, ltsLeft));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LTS getLtsRight() {
		if (ltsRight != null && ltsRight.eIsProxy()) {
			InternalEObject oldLtsRight = (InternalEObject)ltsRight;
			ltsRight = (LTS)eResolveProxy(oldLtsRight);
			if (ltsRight != oldLtsRight) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LtsPackage.COMPOSED_LTS__LTS_RIGHT, oldLtsRight, ltsRight));
			}
		}
		return ltsRight;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LTS basicGetLtsRight() {
		return ltsRight;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setLtsRight(LTS newLtsRight) {
		LTS oldLtsRight = ltsRight;
		ltsRight = newLtsRight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.COMPOSED_LTS__LTS_RIGHT, oldLtsRight, ltsRight));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents() {
		if (events == null) {
			events = new EObjectResolvingEList<Event>(Event.class, this, LtsPackage.COMPOSED_LTS__EVENTS);
		}
		return events;
	}

	private Queue<State> unvisited;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public LTS compose() {
		if (ltsLeft == null || ltsRight == null)
			return null;
		if (name != null)
			setName(name);
		else
			setName(ltsLeft.getName() + ltsRight.getName());

		unvisited = new ArrayDeque<State>();

		composeAlphabet();
		composeAtomicPropositions();
		composeInitstates();
		states.addAll(initStates);

		unvisited.addAll(initStates);
		while (!unvisited.isEmpty()) {
			ComposedState sourceState = (ComposedState) unvisited.poll();
			State partialState1 = sourceState.getPart1(), partialState2 = sourceState
					.getPart2();

			for (Event a : getAlphabet()) {
				for (Transition t1 : partialState1.getOutgoing()) {
					for (Transition t2 : partialState2.getOutgoing()) {

						boolean aElementOfH = getEvents().stream().anyMatch(
								e -> e.getName().equals(a.getName()));

						ComposedState destinationState;
						if (aElementOfH
								&& t1.getEvent().getName().equals(a.getName())
								&& t2.getEvent().getName().equals(a.getName())) {
							destinationState = retrieveOrCreateComposedState(
									t1.getDestination(), t2.getDestination());
						} else if (!aElementOfH
								&& t1.getEvent().getName().equals(a.getName())) {
							destinationState = retrieveOrCreateComposedState(
									t1.getDestination(), t2.getSource());
						} else if (!aElementOfH
								&& t2.getEvent().getName().equals(a.getName())) {
							destinationState = retrieveOrCreateComposedState(
									t1.getSource(), t2.getDestination());
						} else {
							continue;
						}
						retrieveOrAddTransition(sourceState, destinationState,
								a);

					}
				}
			}

			/*
			 * for (Transition t1 : partialState1.getOutgoing()) { for
			 * (Transition t2 : partialState2.getOutgoing()) { if
			 * (!t2.getEvent().getName() .equals(t1.getEvent().getName()))
			 * continue;
			 * 
			 * if (isHandshakingEvent(t1.getEvent()) && t1.getEvent().getName()
			 * .equals(t2.getEvent().getName())) { ComposedState dest =
			 * retrieveOrCreateComposedState( t1.getDestination(),
			 * t2.getDestination()); retrieveOrAddTransition(sourceState, dest,
			 * EcoreUtil.copy(t1.getEvent())); } else if (!isHandshakingEvent) {
			 * ComposedState dest = getComposedState(output,
			 * t1.getDestination(), partialState2); addTransition(output,
			 * sourceState, dest, t1.getEvent()); } }
			 * 
			 * } for (Transition partialTransition2 :
			 * partialState2.getOutgoing()) { boolean isHandshakingEvent =
			 * H.contains(partialTransition2 .getEvent()); if
			 * (!isHandshakingEvent) { ComposedState dest =
			 * getComposedState(output, partialState1,
			 * partialTransition2.getDestination()); addTransition(output,
			 * sourceState, dest, partialTransition2.getEvent()); } } }
			 */

		}

		return this;

	}

	private void composeInitstates() {
		for (State s1 : getLtsLeft().getInitStates())
			for (State s2 : getLtsRight().getInitStates())
				retrieveOrCreateComposedState(s1, s2);
		getInitStates().addAll(getStates());
	}

	private void composeAtomicPropositions() {
		for (AtomicProposition ap1 : getLtsLeft().getAtomicPropositions())
			getAtomicPropositions().add(EcoreUtil.copy(ap1));
		for (AtomicProposition ap2 : getLtsRight().getAtomicPropositions()) {
			if (getAtomicPropositions().stream().noneMatch(
					ap -> ap.getName().equals(ap2.getName()))) {
				getAtomicPropositions().add(EcoreUtil.copy(ap2));
			}
		}
	}

	private void composeAlphabet() {
		for (Event e1 : getLtsLeft().getAlphabet())
			getAlphabet().add(EcoreUtil.copy(e1));
		for (Event e2 : getLtsRight().getAlphabet()) {
			if (getAlphabet().stream().noneMatch(
					e -> e.getName().equals(e2.getName()))) {
				getAlphabet().add(EcoreUtil.copy(e2));
			}
		}
	}

	private Transition retrieveOrAddTransition(ComposedState sourceState,
			ComposedState destinationState, Event event) {
		for (Transition transition : getTransitions()) {
			if (transition.getSource().getName().equals(sourceState.getName())
					&& transition.getDestination().getName()
							.equals(destinationState.getName())
					&& transition.getEvent().getName().equals(event.getName())) {
				return transition;
			}
		}
		Transition newTransition = LtsFactory.eINSTANCE.createTransition();
		newTransition.setSource(sourceState);
		newTransition.setDestination(destinationState);
		newTransition.setEvent(getAlphabet().stream()
				.filter(e -> e.getName().equals(event.getName())).findFirst()
				.get());
		getTransitions().add(newTransition);
		return null;
	}

	private ComposedState retrieveOrCreateComposedState(State state1,
			State state2) {
		for (State state : getStates()) {
			ComposedState composedstate = (ComposedState) state;
			if (composedstate.getPart1().getName().equals(state1.getName())
					&& composedstate.getPart2().getName()
							.equals(state2.getName())) {
				return composedstate;
			}
		}
		ComposedState newState = LtsFactory.eINSTANCE.createComposedState();
		newState.setPart1(state1);
		newState.setPart2(state2);
		for (AtomicProposition ap : state1.getAtomicPropositions()) {
			newState.getAtomicPropositions().add(
					getAtomicPropositions().stream()
							.filter(a -> a.getName().equals(ap.getName()))
							.findFirst().get()); // AP always exists
		}
		for (AtomicProposition ap : state2.getAtomicPropositions()) {
			newState.getAtomicPropositions().add(
					getAtomicPropositions().stream()
							.filter(a -> a.getName().equals(ap.getName()))
							.findFirst().get());
		}
		getStates().add(newState);
		unvisited.add(newState);
		return newState;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LtsPackage.COMPOSED_LTS__LTS_LEFT:
				if (resolve) return getLtsLeft();
				return basicGetLtsLeft();
			case LtsPackage.COMPOSED_LTS__LTS_RIGHT:
				if (resolve) return getLtsRight();
				return basicGetLtsRight();
			case LtsPackage.COMPOSED_LTS__EVENTS:
				return getEvents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LtsPackage.COMPOSED_LTS__LTS_LEFT:
				setLtsLeft((LTS)newValue);
				return;
			case LtsPackage.COMPOSED_LTS__LTS_RIGHT:
				setLtsRight((LTS)newValue);
				return;
			case LtsPackage.COMPOSED_LTS__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends Event>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LtsPackage.COMPOSED_LTS__LTS_LEFT:
				setLtsLeft((LTS)null);
				return;
			case LtsPackage.COMPOSED_LTS__LTS_RIGHT:
				setLtsRight((LTS)null);
				return;
			case LtsPackage.COMPOSED_LTS__EVENTS:
				getEvents().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LtsPackage.COMPOSED_LTS__LTS_LEFT:
				return ltsLeft != null;
			case LtsPackage.COMPOSED_LTS__LTS_RIGHT:
				return ltsRight != null;
			case LtsPackage.COMPOSED_LTS__EVENTS:
				return events != null && !events.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case LtsPackage.COMPOSED_LTS___COMPOSE:
				return compose();
		}
		return super.eInvoke(operationID, arguments);
	}

} // ComposedLTSImpl
