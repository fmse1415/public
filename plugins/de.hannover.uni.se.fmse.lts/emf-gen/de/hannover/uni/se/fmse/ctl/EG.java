/**
 */
package de.hannover.uni.se.fmse.ctl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EG</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hannover.uni.se.fmse.ctl.CtlPackage#getEG()
 * @model
 * @generated
 */
public interface EG extends Unary {
} // EG
