/**
 */
package de.hannover.uni.se.fmse.buechi.impl;

import de.hannover.uni.se.fmse.buechi.BuechiPackage;
import de.hannover.uni.se.fmse.buechi.Transition;
import de.hannover.uni.se.fmse.lts.AtomicProposition;
import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.buechi.impl.TransitionImpl#getAtomicPropositions <em>Atomic Propositions</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TransitionImpl extends de.hannover.uni.se.fmse.lts.impl.TransitionImpl implements Transition {
	/**
	 * The cached value of the '{@link #getAtomicPropositions() <em>Atomic Propositions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicPropositions()
	 * @generated
	 * @ordered
	 */
	protected EList<AtomicProposition> atomicPropositions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BuechiPackage.Literals.TRANSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AtomicProposition> getAtomicPropositions() {
		if (atomicPropositions == null) {
			atomicPropositions = new EObjectResolvingEList<AtomicProposition>(AtomicProposition.class, this, BuechiPackage.TRANSITION__ATOMIC_PROPOSITIONS);
		}
		return atomicPropositions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BuechiPackage.TRANSITION__ATOMIC_PROPOSITIONS:
				return getAtomicPropositions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BuechiPackage.TRANSITION__ATOMIC_PROPOSITIONS:
				getAtomicPropositions().clear();
				getAtomicPropositions().addAll((Collection<? extends AtomicProposition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BuechiPackage.TRANSITION__ATOMIC_PROPOSITIONS:
				getAtomicPropositions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BuechiPackage.TRANSITION__ATOMIC_PROPOSITIONS:
				return atomicPropositions != null && !atomicPropositions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TransitionImpl
