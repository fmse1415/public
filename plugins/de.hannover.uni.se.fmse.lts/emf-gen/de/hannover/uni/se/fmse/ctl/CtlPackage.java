/**
 */
package de.hannover.uni.se.fmse.ctl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hannover.uni.se.fmse.ctl.CtlFactory
 * @model kind="package"
 * @generated
 */
public interface CtlPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ctl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.hannover.de/uni/se/fmse/lts/CTL";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ctl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CtlPackage eINSTANCE = de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ctl.impl.FormulaDefinitionImpl <em>Formula Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ctl.impl.FormulaDefinitionImpl
	 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getFormulaDefinition()
	 * @generated
	 */
	int FORMULA_DEFINITION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_DEFINITION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_DEFINITION__FORMULA = 1;

	/**
	 * The number of structural features of the '<em>Formula Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_DEFINITION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Formula Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_DEFINITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ctl.impl.FormulaImpl <em>Formula</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ctl.impl.FormulaImpl
	 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getFormula()
	 * @generated
	 */
	int FORMULA = 1;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA__SATISFYING_STATES = 0;

	/**
	 * The number of structural features of the '<em>Formula</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA___EVALUATE__LTS = 0;

	/**
	 * The number of operations of the '<em>Formula</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ctl.impl.UnaryImpl <em>Unary</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ctl.impl.UnaryImpl
	 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getUnary()
	 * @generated
	 */
	int UNARY = 5;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY__SATISFYING_STATES = FORMULA__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Nested</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY__NESTED = FORMULA_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unary</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY___EVALUATE__LTS = FORMULA___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>Unary</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATION_COUNT = FORMULA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ctl.impl.EXImpl <em>EX</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ctl.impl.EXImpl
	 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getEX()
	 * @generated
	 */
	int EX = 2;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EX__SATISFYING_STATES = UNARY__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Nested</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EX__NESTED = UNARY__NESTED;

	/**
	 * The number of structural features of the '<em>EX</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EX_FEATURE_COUNT = UNARY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EX___EVALUATE__LTS = UNARY___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>EX</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EX_OPERATION_COUNT = UNARY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ctl.impl.EGImpl <em>EG</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ctl.impl.EGImpl
	 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getEG()
	 * @generated
	 */
	int EG = 3;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EG__SATISFYING_STATES = UNARY__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Nested</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EG__NESTED = UNARY__NESTED;

	/**
	 * The number of structural features of the '<em>EG</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EG_FEATURE_COUNT = UNARY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EG___EVALUATE__LTS = UNARY___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>EG</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EG_OPERATION_COUNT = UNARY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ctl.impl.EUImpl <em>EU</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ctl.impl.EUImpl
	 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getEU()
	 * @generated
	 */
	int EU = 4;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ctl.impl.BinaryImpl <em>Binary</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ctl.impl.BinaryImpl
	 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getBinary()
	 * @generated
	 */
	int BINARY = 6;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY__SATISFYING_STATES = FORMULA__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY__LEFT = FORMULA_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY__RIGHT = FORMULA_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Binary</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY___EVALUATE__LTS = FORMULA___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>Binary</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATION_COUNT = FORMULA_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EU__SATISFYING_STATES = BINARY__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EU__LEFT = BINARY__LEFT;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EU__RIGHT = BINARY__RIGHT;

	/**
	 * The number of structural features of the '<em>EU</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EU_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EU___EVALUATE__LTS = BINARY___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>EU</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EU_OPERATION_COUNT = BINARY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ctl.impl.ANDImpl <em>AND</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ctl.impl.ANDImpl
	 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getAND()
	 * @generated
	 */
	int AND = 7;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__SATISFYING_STATES = BINARY__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__LEFT = BINARY__LEFT;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__RIGHT = BINARY__RIGHT;

	/**
	 * The number of structural features of the '<em>AND</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND___EVALUATE__LTS = BINARY___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>AND</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_OPERATION_COUNT = BINARY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ctl.impl.ORImpl <em>OR</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ctl.impl.ORImpl
	 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getOR()
	 * @generated
	 */
	int OR = 8;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__SATISFYING_STATES = BINARY__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__LEFT = BINARY__LEFT;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__RIGHT = BINARY__RIGHT;

	/**
	 * The number of structural features of the '<em>OR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR___EVALUATE__LTS = BINARY___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>OR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_OPERATION_COUNT = BINARY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ctl.impl.NOTImpl <em>NOT</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ctl.impl.NOTImpl
	 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getNOT()
	 * @generated
	 */
	int NOT = 9;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT__SATISFYING_STATES = UNARY__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Nested</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT__NESTED = UNARY__NESTED;

	/**
	 * The number of structural features of the '<em>NOT</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_FEATURE_COUNT = UNARY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT___EVALUATE__LTS = UNARY___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>NOT</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_OPERATION_COUNT = UNARY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ctl.impl.AtomicImpl <em>Atomic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ctl.impl.AtomicImpl
	 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getAtomic()
	 * @generated
	 */
	int ATOMIC = 11;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC__SATISFYING_STATES = FORMULA__SATISFYING_STATES;

	/**
	 * The number of structural features of the '<em>Atomic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC___EVALUATE__LTS = FORMULA___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>Atomic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_OPERATION_COUNT = FORMULA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ctl.impl.PropositionImpl <em>Proposition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ctl.impl.PropositionImpl
	 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getProposition()
	 * @generated
	 */
	int PROPOSITION = 10;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPOSITION__SATISFYING_STATES = ATOMIC__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Atomic Proposition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPOSITION__ATOMIC_PROPOSITION = ATOMIC_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Proposition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPOSITION_FEATURE_COUNT = ATOMIC_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPOSITION___EVALUATE__LTS = ATOMIC___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>Proposition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPOSITION_OPERATION_COUNT = ATOMIC_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ctl.impl.ConstantImpl <em>Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ctl.impl.ConstantImpl
	 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getConstant()
	 * @generated
	 */
	int CONSTANT = 12;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT__SATISFYING_STATES = ATOMIC__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT__VALUE = ATOMIC_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FEATURE_COUNT = ATOMIC_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT___EVALUATE__LTS = ATOMIC___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_OPERATION_COUNT = ATOMIC_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ctl.FormulaDefinition <em>Formula Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Formula Definition</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.FormulaDefinition
	 * @generated
	 */
	EClass getFormulaDefinition();

	/**
	 * Returns the meta object for the attribute '{@link de.hannover.uni.se.fmse.ctl.FormulaDefinition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.FormulaDefinition#getName()
	 * @see #getFormulaDefinition()
	 * @generated
	 */
	EAttribute getFormulaDefinition_Name();

	/**
	 * Returns the meta object for the containment reference '{@link de.hannover.uni.se.fmse.ctl.FormulaDefinition#getFormula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Formula</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.FormulaDefinition#getFormula()
	 * @see #getFormulaDefinition()
	 * @generated
	 */
	EReference getFormulaDefinition_Formula();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ctl.Formula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Formula</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.Formula
	 * @generated
	 */
	EClass getFormula();

	/**
	 * Returns the meta object for the reference list '{@link de.hannover.uni.se.fmse.ctl.Formula#getSatisfyingStates <em>Satisfying States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Satisfying States</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.Formula#getSatisfyingStates()
	 * @see #getFormula()
	 * @generated
	 */
	EReference getFormula_SatisfyingStates();

	/**
	 * Returns the meta object for the '{@link de.hannover.uni.se.fmse.ctl.Formula#evaluate(de.hannover.uni.se.fmse.lts.LTS) <em>Evaluate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Evaluate</em>' operation.
	 * @see de.hannover.uni.se.fmse.ctl.Formula#evaluate(de.hannover.uni.se.fmse.lts.LTS)
	 * @generated
	 */
	EOperation getFormula__Evaluate__LTS();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ctl.EX <em>EX</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EX</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.EX
	 * @generated
	 */
	EClass getEX();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ctl.EG <em>EG</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EG</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.EG
	 * @generated
	 */
	EClass getEG();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ctl.EU <em>EU</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EU</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.EU
	 * @generated
	 */
	EClass getEU();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ctl.Unary <em>Unary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.Unary
	 * @generated
	 */
	EClass getUnary();

	/**
	 * Returns the meta object for the containment reference '{@link de.hannover.uni.se.fmse.ctl.Unary#getNested <em>Nested</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Nested</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.Unary#getNested()
	 * @see #getUnary()
	 * @generated
	 */
	EReference getUnary_Nested();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ctl.Binary <em>Binary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.Binary
	 * @generated
	 */
	EClass getBinary();

	/**
	 * Returns the meta object for the containment reference '{@link de.hannover.uni.se.fmse.ctl.Binary#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.Binary#getLeft()
	 * @see #getBinary()
	 * @generated
	 */
	EReference getBinary_Left();

	/**
	 * Returns the meta object for the containment reference '{@link de.hannover.uni.se.fmse.ctl.Binary#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.Binary#getRight()
	 * @see #getBinary()
	 * @generated
	 */
	EReference getBinary_Right();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ctl.AND <em>AND</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AND</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.AND
	 * @generated
	 */
	EClass getAND();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ctl.OR <em>OR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OR</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.OR
	 * @generated
	 */
	EClass getOR();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ctl.NOT <em>NOT</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NOT</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.NOT
	 * @generated
	 */
	EClass getNOT();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ctl.Proposition <em>Proposition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Proposition</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.Proposition
	 * @generated
	 */
	EClass getProposition();

	/**
	 * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.ctl.Proposition#getAtomicProposition <em>Atomic Proposition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Atomic Proposition</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.Proposition#getAtomicProposition()
	 * @see #getProposition()
	 * @generated
	 */
	EReference getProposition_AtomicProposition();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ctl.Atomic <em>Atomic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Atomic</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.Atomic
	 * @generated
	 */
	EClass getAtomic();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ctl.Constant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.Constant
	 * @generated
	 */
	EClass getConstant();

	/**
	 * Returns the meta object for the attribute '{@link de.hannover.uni.se.fmse.ctl.Constant#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see de.hannover.uni.se.fmse.ctl.Constant#isValue()
	 * @see #getConstant()
	 * @generated
	 */
	EAttribute getConstant_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CtlFactory getCtlFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ctl.impl.FormulaDefinitionImpl <em>Formula Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ctl.impl.FormulaDefinitionImpl
		 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getFormulaDefinition()
		 * @generated
		 */
		EClass FORMULA_DEFINITION = eINSTANCE.getFormulaDefinition();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FORMULA_DEFINITION__NAME = eINSTANCE.getFormulaDefinition_Name();

		/**
		 * The meta object literal for the '<em><b>Formula</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORMULA_DEFINITION__FORMULA = eINSTANCE.getFormulaDefinition_Formula();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ctl.impl.FormulaImpl <em>Formula</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ctl.impl.FormulaImpl
		 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getFormula()
		 * @generated
		 */
		EClass FORMULA = eINSTANCE.getFormula();

		/**
		 * The meta object literal for the '<em><b>Satisfying States</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORMULA__SATISFYING_STATES = eINSTANCE.getFormula_SatisfyingStates();

		/**
		 * The meta object literal for the '<em><b>Evaluate</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FORMULA___EVALUATE__LTS = eINSTANCE.getFormula__Evaluate__LTS();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ctl.impl.EXImpl <em>EX</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ctl.impl.EXImpl
		 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getEX()
		 * @generated
		 */
		EClass EX = eINSTANCE.getEX();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ctl.impl.EGImpl <em>EG</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ctl.impl.EGImpl
		 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getEG()
		 * @generated
		 */
		EClass EG = eINSTANCE.getEG();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ctl.impl.EUImpl <em>EU</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ctl.impl.EUImpl
		 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getEU()
		 * @generated
		 */
		EClass EU = eINSTANCE.getEU();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ctl.impl.UnaryImpl <em>Unary</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ctl.impl.UnaryImpl
		 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getUnary()
		 * @generated
		 */
		EClass UNARY = eINSTANCE.getUnary();

		/**
		 * The meta object literal for the '<em><b>Nested</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY__NESTED = eINSTANCE.getUnary_Nested();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ctl.impl.BinaryImpl <em>Binary</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ctl.impl.BinaryImpl
		 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getBinary()
		 * @generated
		 */
		EClass BINARY = eINSTANCE.getBinary();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY__LEFT = eINSTANCE.getBinary_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY__RIGHT = eINSTANCE.getBinary_Right();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ctl.impl.ANDImpl <em>AND</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ctl.impl.ANDImpl
		 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getAND()
		 * @generated
		 */
		EClass AND = eINSTANCE.getAND();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ctl.impl.ORImpl <em>OR</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ctl.impl.ORImpl
		 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getOR()
		 * @generated
		 */
		EClass OR = eINSTANCE.getOR();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ctl.impl.NOTImpl <em>NOT</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ctl.impl.NOTImpl
		 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getNOT()
		 * @generated
		 */
		EClass NOT = eINSTANCE.getNOT();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ctl.impl.PropositionImpl <em>Proposition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ctl.impl.PropositionImpl
		 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getProposition()
		 * @generated
		 */
		EClass PROPOSITION = eINSTANCE.getProposition();

		/**
		 * The meta object literal for the '<em><b>Atomic Proposition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROPOSITION__ATOMIC_PROPOSITION = eINSTANCE.getProposition_AtomicProposition();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ctl.impl.AtomicImpl <em>Atomic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ctl.impl.AtomicImpl
		 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getAtomic()
		 * @generated
		 */
		EClass ATOMIC = eINSTANCE.getAtomic();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ctl.impl.ConstantImpl <em>Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ctl.impl.ConstantImpl
		 * @see de.hannover.uni.se.fmse.ctl.impl.CtlPackageImpl#getConstant()
		 * @generated
		 */
		EClass CONSTANT = eINSTANCE.getConstant();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT__VALUE = eINSTANCE.getConstant_Value();

	}

} //CtlPackage
