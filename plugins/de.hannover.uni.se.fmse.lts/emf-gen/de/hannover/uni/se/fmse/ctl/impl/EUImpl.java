/**
 */
package de.hannover.uni.se.fmse.ctl.impl;

import java.util.HashSet;
import java.util.Set;
import de.hannover.uni.se.fmse.ctl.CtlPackage;
import de.hannover.uni.se.fmse.ctl.EU;
import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.State;
import de.hannover.uni.se.fmse.lts.Transition;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EU</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class EUImpl extends BinaryImpl implements EU {
	
	private Set<State> visitedStates;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EUImpl() {
		super();
	}

	@Override
	public boolean evaluate(LTS lts) {
		
		this.visitedStates = new HashSet<State>();

		getRight().evaluate(lts);
		getLeft().evaluate(lts);
		
		
		for (State state : getRight().getSatisfyingStates()){
			for (Transition t : state.getIncoming()){
				this.visitedStates.add(t.getSource());
				getAllIncomingStatesTillHold(new HashSet<State>(), t.getSource(), 2*getRight().getSatisfyingStates().size());
			}
		}
		
		
		return getSatisfyingStates().containsAll(lts.getInitStates());
	}
	
	private void getAllIncomingStatesTillHold(Set<State> tmpStateSet, State state, int counter){
		if (getRight().getSatisfyingStates().contains(state)){
			tmpStateSet.add(state);

			if (state.getIncoming().size() > 0){
				for (Transition t : state.getIncoming()){
					if (counter > 0){
						getAllIncomingStatesTillHold(getNewHashSet(tmpStateSet), t.getSource(), --counter);
					}
				}
			}
			else {
				getSatisfyingStates().addAll(tmpStateSet);
			}
			
		} else {
			tmpStateSet.add(state);
			getSatisfyingStates().addAll(tmpStateSet);
		}
	}
	
	private Set<State> getNewHashSet(Set<State> set){
		Set<State> states = new HashSet<State>();
		
		for (State s : set)
			states.add(s);
		
		return states;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CtlPackage.Literals.EU;
	}

} //EUImpl
