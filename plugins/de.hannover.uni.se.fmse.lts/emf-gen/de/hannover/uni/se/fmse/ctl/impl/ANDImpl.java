/**
 */
package de.hannover.uni.se.fmse.ctl.impl;

import de.hannover.uni.se.fmse.ctl.AND;
import de.hannover.uni.se.fmse.ctl.CtlPackage;
import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.State;
import org.eclipse.emf.ecore.EClass;
import java.util.HashSet;
import java.util.Set;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>AND</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ANDImpl extends BinaryImpl implements AND {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ANDImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CtlPackage.Literals.AND;
	}

	@Override
	public boolean evaluate(LTS lts) {
		boolean b1 = left.evaluate(lts);
		boolean b2 = right.evaluate(lts);
		
		Set<State> set = new HashSet<State>(left.getSatisfyingStates());
		set.retainAll(right.getSatisfyingStates());
		getSatisfyingStates().addAll(set);
		
//		List<State> A = new ArrayList<State>(left.getSatisfyingStates());
//		List<State> B = new ArrayList<State>(right.getSatisfyingStates());
//		List<State> B_A = new ArrayList<State>(B); // B\A
//		B_A.removeAll(A);
//		List<State> A_B = new ArrayList<State>(A); // A\B
//		B_A.removeAll(B);
//		
//		A_B.addAll(B_A); // A\B v B\A
//		getSatisfyingStates().addAll(A);
//		getSatisfyingStates().addAll(B);
//		getSatisfyingStates().removeAll(A_B);
		
		return b1 && b2;
	}

} // ANDImpl
