/**
 */
package de.hannover.uni.se.fmse.lts;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hannover.uni.se.fmse.lts.LtsFactory
 * @model kind="package"
 * @generated
 */
public interface LtsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "lts";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.hannover.de/uni/se/fmse/lts/LTS";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "lts";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LtsPackage eINSTANCE = de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.lts.impl.NamedElementImpl
	 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.impl.LTSImpl <em>LTS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.lts.impl.LTSImpl
	 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getLTS()
	 * @generated
	 */
	int LTS = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__STATES = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Init States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__INIT_STATES = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Alphabet</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__ALPHABET = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__TRANSITIONS = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Atomic Propositions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS__ATOMIC_PROPOSITIONS = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>LTS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>LTS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.State <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.lts.State
	 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getState()
	 * @generated
	 */
	int STATE = 1;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__OUTGOING = 0;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__INCOMING = 1;

	/**
	 * The feature id for the '<em><b>Atomic Propositions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ATOMIC_PROPOSITIONS = 2;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE___GET_NAME = 0;

	/**
	 * The number of operations of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.lts.impl.TransitionImpl
	 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 2;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__EVENT = 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__SOURCE = 1;

	/**
	 * The feature id for the '<em><b>Destination</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__DESTINATION = 2;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.lts.impl.EventImpl
	 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.impl.LTSCompositorImpl <em>LTS Compositor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.lts.impl.LTSCompositorImpl
	 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getLTSCompositor()
	 * @generated
	 */
	int LTS_COMPOSITOR = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_COMPOSITOR__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Lts Left</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_COMPOSITOR__LTS_LEFT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lts Right</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_COMPOSITOR__LTS_RIGHT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_COMPOSITOR__EVENTS = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>LTS Compositor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_COMPOSITOR_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Compose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_COMPOSITOR___COMPOSE = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LTS Compositor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTS_COMPOSITOR_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.impl.BasicStateImpl <em>Basic State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.lts.impl.BasicStateImpl
	 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getBasicState()
	 * @generated
	 */
	int BASIC_STATE = 5;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_STATE__OUTGOING = STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_STATE__INCOMING = STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Atomic Propositions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_STATE__ATOMIC_PROPOSITIONS = STATE__ATOMIC_PROPOSITIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_STATE__NAME = STATE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Basic State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_STATE_FEATURE_COUNT = STATE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_STATE___GET_NAME = STATE___GET_NAME;

	/**
	 * The number of operations of the '<em>Basic State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_STATE_OPERATION_COUNT = STATE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.impl.ComposedStateImpl <em>Composed State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.lts.impl.ComposedStateImpl
	 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getComposedState()
	 * @generated
	 */
	int COMPOSED_STATE = 6;

	/**
	 * The feature id for the '<em><b>Outgoing</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_STATE__OUTGOING = STATE__OUTGOING;

	/**
	 * The feature id for the '<em><b>Incoming</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_STATE__INCOMING = STATE__INCOMING;

	/**
	 * The feature id for the '<em><b>Atomic Propositions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_STATE__ATOMIC_PROPOSITIONS = STATE__ATOMIC_PROPOSITIONS;

	/**
	 * The feature id for the '<em><b>Part1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_STATE__PART1 = STATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Part2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_STATE__PART2 = STATE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Composed State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_STATE_FEATURE_COUNT = STATE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_STATE___GET_NAME = STATE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Composed State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_STATE_OPERATION_COUNT = STATE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.impl.AtomicPropositionImpl <em>Atomic Proposition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.lts.impl.AtomicPropositionImpl
	 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getAtomicProposition()
	 * @generated
	 */
	int ATOMIC_PROPOSITION = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_PROPOSITION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Atomic Proposition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_PROPOSITION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;


	/**
	 * The number of operations of the '<em>Atomic Proposition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_PROPOSITION_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.lts.impl.ComposedLTSImpl <em>Composed LTS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.lts.impl.ComposedLTSImpl
	 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getComposedLTS()
	 * @generated
	 */
	int COMPOSED_LTS = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__NAME = LTS__NAME;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__STATES = LTS__STATES;

	/**
	 * The feature id for the '<em><b>Init States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__INIT_STATES = LTS__INIT_STATES;

	/**
	 * The feature id for the '<em><b>Alphabet</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__ALPHABET = LTS__ALPHABET;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__TRANSITIONS = LTS__TRANSITIONS;

	/**
	 * The feature id for the '<em><b>Atomic Propositions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__ATOMIC_PROPOSITIONS = LTS__ATOMIC_PROPOSITIONS;

	/**
	 * The feature id for the '<em><b>Lts Left</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__LTS_LEFT = LTS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lts Right</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__LTS_RIGHT = LTS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS__EVENTS = LTS_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Composed LTS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS_FEATURE_COUNT = LTS_FEATURE_COUNT + 3;


	/**
	 * The operation id for the '<em>Compose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS___COMPOSE = LTS_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Composed LTS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSED_LTS_OPERATION_COUNT = LTS_OPERATION_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.LTS <em>LTS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LTS</em>'.
	 * @see de.hannover.uni.se.fmse.lts.LTS
	 * @generated
	 */
	EClass getLTS();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hannover.uni.se.fmse.lts.LTS#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see de.hannover.uni.se.fmse.lts.LTS#getStates()
	 * @see #getLTS()
	 * @generated
	 */
	EReference getLTS_States();

	/**
	 * Returns the meta object for the reference list '{@link de.hannover.uni.se.fmse.lts.LTS#getInitStates <em>Init States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Init States</em>'.
	 * @see de.hannover.uni.se.fmse.lts.LTS#getInitStates()
	 * @see #getLTS()
	 * @generated
	 */
	EReference getLTS_InitStates();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hannover.uni.se.fmse.lts.LTS#getAlphabet <em>Alphabet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Alphabet</em>'.
	 * @see de.hannover.uni.se.fmse.lts.LTS#getAlphabet()
	 * @see #getLTS()
	 * @generated
	 */
	EReference getLTS_Alphabet();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hannover.uni.se.fmse.lts.LTS#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see de.hannover.uni.se.fmse.lts.LTS#getTransitions()
	 * @see #getLTS()
	 * @generated
	 */
	EReference getLTS_Transitions();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hannover.uni.se.fmse.lts.LTS#getAtomicPropositions <em>Atomic Propositions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Atomic Propositions</em>'.
	 * @see de.hannover.uni.se.fmse.lts.LTS#getAtomicPropositions()
	 * @see #getLTS()
	 * @generated
	 */
	EReference getLTS_AtomicPropositions();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see de.hannover.uni.se.fmse.lts.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the reference list '{@link de.hannover.uni.se.fmse.lts.State#getOutgoing <em>Outgoing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outgoing</em>'.
	 * @see de.hannover.uni.se.fmse.lts.State#getOutgoing()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Outgoing();

	/**
	 * Returns the meta object for the reference list '{@link de.hannover.uni.se.fmse.lts.State#getIncoming <em>Incoming</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Incoming</em>'.
	 * @see de.hannover.uni.se.fmse.lts.State#getIncoming()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_Incoming();

	/**
	 * Returns the meta object for the reference list '{@link de.hannover.uni.se.fmse.lts.State#getAtomicPropositions <em>Atomic Propositions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Atomic Propositions</em>'.
	 * @see de.hannover.uni.se.fmse.lts.State#getAtomicPropositions()
	 * @see #getState()
	 * @generated
	 */
	EReference getState_AtomicPropositions();

	/**
	 * Returns the meta object for the '{@link de.hannover.uni.se.fmse.lts.State#getName() <em>Get Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Name</em>' operation.
	 * @see de.hannover.uni.se.fmse.lts.State#getName()
	 * @generated
	 */
	EOperation getState__GetName();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see de.hannover.uni.se.fmse.lts.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.lts.Transition#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see de.hannover.uni.se.fmse.lts.Transition#getEvent()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Event();

	/**
	 * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.lts.Transition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see de.hannover.uni.se.fmse.lts.Transition#getSource()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Source();

	/**
	 * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.lts.Transition#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Destination</em>'.
	 * @see de.hannover.uni.se.fmse.lts.Transition#getDestination()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Destination();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see de.hannover.uni.se.fmse.lts.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.LTSCompositor <em>LTS Compositor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LTS Compositor</em>'.
	 * @see de.hannover.uni.se.fmse.lts.LTSCompositor
	 * @generated
	 */
	EClass getLTSCompositor();

	/**
	 * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.lts.LTSCompositor#getLtsLeft <em>Lts Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Lts Left</em>'.
	 * @see de.hannover.uni.se.fmse.lts.LTSCompositor#getLtsLeft()
	 * @see #getLTSCompositor()
	 * @generated
	 */
	EReference getLTSCompositor_LtsLeft();

	/**
	 * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.lts.LTSCompositor#getLtsRight <em>Lts Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Lts Right</em>'.
	 * @see de.hannover.uni.se.fmse.lts.LTSCompositor#getLtsRight()
	 * @see #getLTSCompositor()
	 * @generated
	 */
	EReference getLTSCompositor_LtsRight();

	/**
	 * Returns the meta object for the reference list '{@link de.hannover.uni.se.fmse.lts.LTSCompositor#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Events</em>'.
	 * @see de.hannover.uni.se.fmse.lts.LTSCompositor#getEvents()
	 * @see #getLTSCompositor()
	 * @generated
	 */
	EReference getLTSCompositor_Events();

	/**
	 * Returns the meta object for the '{@link de.hannover.uni.se.fmse.lts.LTSCompositor#compose() <em>Compose</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Compose</em>' operation.
	 * @see de.hannover.uni.se.fmse.lts.LTSCompositor#compose()
	 * @generated
	 */
	EOperation getLTSCompositor__Compose();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.BasicState <em>Basic State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic State</em>'.
	 * @see de.hannover.uni.se.fmse.lts.BasicState
	 * @generated
	 */
	EClass getBasicState();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.ComposedState <em>Composed State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composed State</em>'.
	 * @see de.hannover.uni.se.fmse.lts.ComposedState
	 * @generated
	 */
	EClass getComposedState();

	/**
	 * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.lts.ComposedState#getPart1 <em>Part1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Part1</em>'.
	 * @see de.hannover.uni.se.fmse.lts.ComposedState#getPart1()
	 * @see #getComposedState()
	 * @generated
	 */
	EReference getComposedState_Part1();

	/**
	 * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.lts.ComposedState#getPart2 <em>Part2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Part2</em>'.
	 * @see de.hannover.uni.se.fmse.lts.ComposedState#getPart2()
	 * @see #getComposedState()
	 * @generated
	 */
	EReference getComposedState_Part2();

	/**
	 * Returns the meta object for the '{@link de.hannover.uni.se.fmse.lts.ComposedState#getName() <em>Get Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Name</em>' operation.
	 * @see de.hannover.uni.se.fmse.lts.ComposedState#getName()
	 * @generated
	 */
	EOperation getComposedState__GetName();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.AtomicProposition <em>Atomic Proposition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Atomic Proposition</em>'.
	 * @see de.hannover.uni.se.fmse.lts.AtomicProposition
	 * @generated
	 */
	EClass getAtomicProposition();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see de.hannover.uni.se.fmse.lts.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link de.hannover.uni.se.fmse.lts.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.hannover.uni.se.fmse.lts.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.lts.ComposedLTS <em>Composed LTS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composed LTS</em>'.
	 * @see de.hannover.uni.se.fmse.lts.ComposedLTS
	 * @generated
	 */
	EClass getComposedLTS();

	/**
	 * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.lts.ComposedLTS#getLtsLeft <em>Lts Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Lts Left</em>'.
	 * @see de.hannover.uni.se.fmse.lts.ComposedLTS#getLtsLeft()
	 * @see #getComposedLTS()
	 * @generated
	 */
	EReference getComposedLTS_LtsLeft();

	/**
	 * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.lts.ComposedLTS#getLtsRight <em>Lts Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Lts Right</em>'.
	 * @see de.hannover.uni.se.fmse.lts.ComposedLTS#getLtsRight()
	 * @see #getComposedLTS()
	 * @generated
	 */
	EReference getComposedLTS_LtsRight();

	/**
	 * Returns the meta object for the reference list '{@link de.hannover.uni.se.fmse.lts.ComposedLTS#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Events</em>'.
	 * @see de.hannover.uni.se.fmse.lts.ComposedLTS#getEvents()
	 * @see #getComposedLTS()
	 * @generated
	 */
	EReference getComposedLTS_Events();

	/**
	 * Returns the meta object for the '{@link de.hannover.uni.se.fmse.lts.ComposedLTS#compose() <em>Compose</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Compose</em>' operation.
	 * @see de.hannover.uni.se.fmse.lts.ComposedLTS#compose()
	 * @generated
	 */
	EOperation getComposedLTS__Compose();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	LtsFactory getLtsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.impl.LTSImpl <em>LTS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.lts.impl.LTSImpl
		 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getLTS()
		 * @generated
		 */
		EClass LTS = eINSTANCE.getLTS();

		/**
		 * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS__STATES = eINSTANCE.getLTS_States();

		/**
		 * The meta object literal for the '<em><b>Init States</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS__INIT_STATES = eINSTANCE.getLTS_InitStates();

		/**
		 * The meta object literal for the '<em><b>Alphabet</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS__ALPHABET = eINSTANCE.getLTS_Alphabet();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS__TRANSITIONS = eINSTANCE.getLTS_Transitions();

		/**
		 * The meta object literal for the '<em><b>Atomic Propositions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS__ATOMIC_PROPOSITIONS = eINSTANCE.getLTS_AtomicPropositions();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.State <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.lts.State
		 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>Outgoing</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__OUTGOING = eINSTANCE.getState_Outgoing();

		/**
		 * The meta object literal for the '<em><b>Incoming</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__INCOMING = eINSTANCE.getState_Incoming();

		/**
		 * The meta object literal for the '<em><b>Atomic Propositions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__ATOMIC_PROPOSITIONS = eINSTANCE.getState_AtomicPropositions();

		/**
		 * The meta object literal for the '<em><b>Get Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation STATE___GET_NAME = eINSTANCE.getState__GetName();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.lts.impl.TransitionImpl
		 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__EVENT = eINSTANCE.getTransition_Event();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__SOURCE = eINSTANCE.getTransition_Source();

		/**
		 * The meta object literal for the '<em><b>Destination</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__DESTINATION = eINSTANCE.getTransition_Destination();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.lts.impl.EventImpl
		 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.impl.LTSCompositorImpl <em>LTS Compositor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.lts.impl.LTSCompositorImpl
		 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getLTSCompositor()
		 * @generated
		 */
		EClass LTS_COMPOSITOR = eINSTANCE.getLTSCompositor();

		/**
		 * The meta object literal for the '<em><b>Lts Left</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS_COMPOSITOR__LTS_LEFT = eINSTANCE.getLTSCompositor_LtsLeft();

		/**
		 * The meta object literal for the '<em><b>Lts Right</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS_COMPOSITOR__LTS_RIGHT = eINSTANCE.getLTSCompositor_LtsRight();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTS_COMPOSITOR__EVENTS = eINSTANCE.getLTSCompositor_Events();

		/**
		 * The meta object literal for the '<em><b>Compose</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LTS_COMPOSITOR___COMPOSE = eINSTANCE.getLTSCompositor__Compose();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.impl.BasicStateImpl <em>Basic State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.lts.impl.BasicStateImpl
		 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getBasicState()
		 * @generated
		 */
		EClass BASIC_STATE = eINSTANCE.getBasicState();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.impl.ComposedStateImpl <em>Composed State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.lts.impl.ComposedStateImpl
		 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getComposedState()
		 * @generated
		 */
		EClass COMPOSED_STATE = eINSTANCE.getComposedState();

		/**
		 * The meta object literal for the '<em><b>Part1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSED_STATE__PART1 = eINSTANCE.getComposedState_Part1();

		/**
		 * The meta object literal for the '<em><b>Part2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSED_STATE__PART2 = eINSTANCE.getComposedState_Part2();

		/**
		 * The meta object literal for the '<em><b>Get Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COMPOSED_STATE___GET_NAME = eINSTANCE.getComposedState__GetName();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.impl.AtomicPropositionImpl <em>Atomic Proposition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.lts.impl.AtomicPropositionImpl
		 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getAtomicProposition()
		 * @generated
		 */
		EClass ATOMIC_PROPOSITION = eINSTANCE.getAtomicProposition();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.lts.impl.NamedElementImpl
		 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.lts.impl.ComposedLTSImpl <em>Composed LTS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.lts.impl.ComposedLTSImpl
		 * @see de.hannover.uni.se.fmse.lts.impl.LtsPackageImpl#getComposedLTS()
		 * @generated
		 */
		EClass COMPOSED_LTS = eINSTANCE.getComposedLTS();

		/**
		 * The meta object literal for the '<em><b>Lts Left</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSED_LTS__LTS_LEFT = eINSTANCE.getComposedLTS_LtsLeft();

		/**
		 * The meta object literal for the '<em><b>Lts Right</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSED_LTS__LTS_RIGHT = eINSTANCE.getComposedLTS_LtsRight();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSED_LTS__EVENTS = eINSTANCE.getComposedLTS_Events();

		/**
		 * The meta object literal for the '<em><b>Compose</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COMPOSED_LTS___COMPOSE = eINSTANCE.getComposedLTS__Compose();

	}

} //LtsPackage
