/**
 */
package de.hannover.uni.se.fmse.lts.impl;

import de.hannover.uni.se.fmse.lts.AtomicProposition;
import de.hannover.uni.se.fmse.lts.ComposedState;
import de.hannover.uni.se.fmse.lts.Event;
import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.LTSCompositor;
import de.hannover.uni.se.fmse.lts.LtsFactory;
import de.hannover.uni.se.fmse.lts.LtsPackage;
import de.hannover.uni.se.fmse.lts.State;
import de.hannover.uni.se.fmse.lts.Transition;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Queue;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>LTS Compositor</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.LTSCompositorImpl#getLtsLeft <em>Lts Left</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.LTSCompositorImpl#getLtsRight <em>Lts Right</em>}</li>
 *   <li>{@link de.hannover.uni.se.fmse.lts.impl.LTSCompositorImpl#getEvents <em>Events</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LTSCompositorImpl extends NamedElementImpl implements
		LTSCompositor {
	/**
	 * The cached value of the '{@link #getLtsLeft() <em>Lts Left</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLtsLeft()
	 * @generated
	 * @ordered
	 */
	protected LTS ltsLeft;

	/**
	 * The cached value of the '{@link #getLtsRight() <em>Lts Right</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLtsRight()
	 * @generated
	 * @ordered
	 */
	protected LTS ltsRight;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected LTSCompositorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LtsPackage.Literals.LTS_COMPOSITOR;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LTS getLtsLeft() {
		if (ltsLeft != null && ltsLeft.eIsProxy()) {
			InternalEObject oldLtsLeft = (InternalEObject)ltsLeft;
			ltsLeft = (LTS)eResolveProxy(oldLtsLeft);
			if (ltsLeft != oldLtsLeft) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LtsPackage.LTS_COMPOSITOR__LTS_LEFT, oldLtsLeft, ltsLeft));
			}
		}
		return ltsLeft;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LTS basicGetLtsLeft() {
		return ltsLeft;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setLtsLeft(LTS newLtsLeft) {
		LTS oldLtsLeft = ltsLeft;
		ltsLeft = newLtsLeft;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.LTS_COMPOSITOR__LTS_LEFT, oldLtsLeft, ltsLeft));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LTS getLtsRight() {
		if (ltsRight != null && ltsRight.eIsProxy()) {
			InternalEObject oldLtsRight = (InternalEObject)ltsRight;
			ltsRight = (LTS)eResolveProxy(oldLtsRight);
			if (ltsRight != oldLtsRight) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LtsPackage.LTS_COMPOSITOR__LTS_RIGHT, oldLtsRight, ltsRight));
			}
		}
		return ltsRight;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public LTS basicGetLtsRight() {
		return ltsRight;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setLtsRight(LTS newLtsRight) {
		LTS oldLtsRight = ltsRight;
		ltsRight = newLtsRight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LtsPackage.LTS_COMPOSITOR__LTS_RIGHT, oldLtsRight, ltsRight));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents() {
		if (events == null) {
			events = new EObjectResolvingEList<Event>(Event.class, this, LtsPackage.LTS_COMPOSITOR__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	private Queue<ComposedState> unvisited = new ArrayDeque<ComposedState>();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public LTS compose() {
		unvisited = new ArrayDeque<ComposedState>();
		LTS output = LtsFactory.eINSTANCE.createLTS();
		if (name != null)
			output.setName(name);
		else
			output.setName(ltsLeft.getName() + ltsRight.getName());
		output.getAlphabet().addAll(computeAlphabet(ltsLeft, ltsRight));
		output.getAtomicPropositions().addAll(
				computeAtomicPropositions(ltsLeft, ltsRight));
		List<ComposedState> composedInitStates = computeInitialComposedStates(
				ltsLeft, ltsRight);
		output.getInitStates().addAll(composedInitStates);
		output.getStates().addAll(composedInitStates);
		unvisited.addAll(composedInitStates);
		while (!unvisited.isEmpty()) {
			ComposedState sourceState = unvisited.poll();
			State partialState1 = sourceState.getPart1(), partialState2 = sourceState
					.getPart2();
			for (Transition partialTransition1 : partialState1.getOutgoing()) {
				boolean isHandshakingEvent = getEvents().contains(
						partialTransition1.getEvent());
				Transition partialTransition2 = partialState2
						.getOutgoing()
						.stream()
						.filter(t -> t.getEvent().equals(
								partialTransition1.getEvent())).findFirst()
						.orElse(null);
				if (isHandshakingEvent && partialTransition2 != null) {
					ComposedState dest = getComposedState(output,
							partialTransition1.getDestination(),
							partialTransition2.getDestination());

					addTransition(output, sourceState, dest,
							partialTransition1.getEvent());
				} else if (!isHandshakingEvent) {
					ComposedState dest = getComposedState(output,
							partialTransition1.getDestination(), partialState2);
					addTransition(output, sourceState, dest,
							partialTransition1.getEvent());
				}
			}
			for (Transition partialTransition2 : partialState2.getOutgoing()) {
				boolean isHandshakingEvent = getEvents().contains(
						partialTransition2.getEvent());
				if (!isHandshakingEvent) {
					ComposedState dest = getComposedState(output,
							partialState1, partialTransition2.getDestination());
					addTransition(output, sourceState, dest,
							partialTransition2.getEvent());
				}
			}
		}

		return output;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	private Collection<? extends AtomicProposition> computeAtomicPropositions(
			LTS l1, LTS l2) {
		List<AtomicProposition> result = new ArrayList<AtomicProposition>();
		for (AtomicProposition a1 : l1.getAtomicPropositions()) {
			result.add(EcoreUtil.copy(a1));
		}
		for (AtomicProposition a2 : l2.getAtomicPropositions()) {
			if (result.stream().anyMatch(
					ap -> ap.getName().equals(a2.getName()))) {
				continue;
			}
			result.add(EcoreUtil.copy(a2));
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	private ComposedState getComposedState(LTS output, State destination,
			State destination2) {
		ComposedState newState = LtsFactory.eINSTANCE.createComposedState();
		newState.setPart1(destination);
		newState.setPart2(destination2);
		ComposedState result = (ComposedState) output.getStates().stream()
				.filter(s -> s.equals(newState)).findAny().orElse(null);
		if (result == null) {
			result = newState;
			result.getAtomicPropositions().addAll(
					computeCombinedAtomicPropositions(output, destination,
							destination2));
			output.getStates().add(newState);
			unvisited.add(newState);
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	private Collection<? extends AtomicProposition> computeCombinedAtomicPropositions(
			LTS output, State s1, State s2) {
		List<AtomicProposition> result = new ArrayList<AtomicProposition>();
		for (AtomicProposition a1 : s1.getAtomicPropositions()) {
			for (AtomicProposition a : output.getAtomicPropositions()) {
				if (a.getName().equals(a1.getName()))
					result.add(a);
			}
		}
		for (AtomicProposition a2 : s2.getAtomicPropositions()) {
			for (AtomicProposition a : output.getAtomicPropositions()) {
				if (a.getName().equals(a2.getName()) && !result.contains(a))
					result.add(a);
			}
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	private void addTransition(LTS lts, ComposedState source,
			ComposedState destination, Event event) {
		boolean transitionExists = source
				.getOutgoing()
				.stream()
				.anyMatch(
						transition -> transition.getDestination().equals(
								destination)
								&& transition.getEvent().equals(event));
		if (!transitionExists) {
			Transition t = LtsFactory.eINSTANCE.createTransition();
			t.setEvent(event);
			t.setDestination(destination);
			t.setSource(source);
			lts.getTransitions().add(t);
		}
	}

	/**
	 * Builds the union of the alphabets of l1 and l2.
	 * 
	 * @param l1
	 * @param l2
	 * @return
	 */
	private List<Event> computeAlphabet(LTS l1, LTS l2) {
		List<Event> result = new ArrayList<Event>();
		for (Event e1 : l1.getAlphabet()) {
			result.add(EcoreUtil.copy(e1));
		}
		for (Event e2 : l2.getAlphabet()) {
			for (Event e : l1.getAlphabet()) {
				if (!e2.equals(e)) {
					result.add(EcoreUtil.copy(e2));
				}
			}
		}
		return result;
	}

	/**
	 * Creates a list of composed states from the initial states of l1 and l2
	 * 
	 * @param l1
	 * @param l2
	 * @return
	 */
	private List<ComposedState> computeInitialComposedStates(LTS l1, LTS l2) {
		List<ComposedState> result = new ArrayList<ComposedState>();
		for (State s1 : l1.getInitStates()) {
			for (State s2 : l2.getInitStates()) {
				ComposedState cs = LtsFactory.eINSTANCE.createComposedState();
				cs.setPart1(s1);
				cs.setPart2(s2);
				result.add(cs);
			}
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LtsPackage.LTS_COMPOSITOR__LTS_LEFT:
				if (resolve) return getLtsLeft();
				return basicGetLtsLeft();
			case LtsPackage.LTS_COMPOSITOR__LTS_RIGHT:
				if (resolve) return getLtsRight();
				return basicGetLtsRight();
			case LtsPackage.LTS_COMPOSITOR__EVENTS:
				return getEvents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LtsPackage.LTS_COMPOSITOR__LTS_LEFT:
				setLtsLeft((LTS)newValue);
				return;
			case LtsPackage.LTS_COMPOSITOR__LTS_RIGHT:
				setLtsRight((LTS)newValue);
				return;
			case LtsPackage.LTS_COMPOSITOR__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends Event>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LtsPackage.LTS_COMPOSITOR__LTS_LEFT:
				setLtsLeft((LTS)null);
				return;
			case LtsPackage.LTS_COMPOSITOR__LTS_RIGHT:
				setLtsRight((LTS)null);
				return;
			case LtsPackage.LTS_COMPOSITOR__EVENTS:
				getEvents().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LtsPackage.LTS_COMPOSITOR__LTS_LEFT:
				return ltsLeft != null;
			case LtsPackage.LTS_COMPOSITOR__LTS_RIGHT:
				return ltsRight != null;
			case LtsPackage.LTS_COMPOSITOR__EVENTS:
				return events != null && !events.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case LtsPackage.LTS_COMPOSITOR___COMPOSE:
				return compose();
		}
		return super.eInvoke(operationID, arguments);
	}

} // LTSCompositorImpl
