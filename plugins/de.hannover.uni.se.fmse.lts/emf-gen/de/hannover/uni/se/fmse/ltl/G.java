/**
 */
package de.hannover.uni.se.fmse.ltl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>G</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hannover.uni.se.fmse.ltl.LtlPackage#getG()
 * @model
 * @generated
 */
public interface G extends Unary {
} // G
