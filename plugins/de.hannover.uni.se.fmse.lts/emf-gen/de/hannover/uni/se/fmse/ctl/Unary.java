/**
 */
package de.hannover.uni.se.fmse.ctl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.ctl.Unary#getNested <em>Nested</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.hannover.uni.se.fmse.ctl.CtlPackage#getUnary()
 * @model abstract="true"
 * @generated
 */
public interface Unary extends Formula {
	/**
	 * Returns the value of the '<em><b>Nested</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nested</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nested</em>' containment reference.
	 * @see #setNested(Formula)
	 * @see de.hannover.uni.se.fmse.ctl.CtlPackage#getUnary_Nested()
	 * @model containment="true"
	 * @generated
	 */
	Formula getNested();

	/**
	 * Sets the value of the '{@link de.hannover.uni.se.fmse.ctl.Unary#getNested <em>Nested</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nested</em>' containment reference.
	 * @see #getNested()
	 * @generated
	 */
	void setNested(Formula value);

} // Unary
