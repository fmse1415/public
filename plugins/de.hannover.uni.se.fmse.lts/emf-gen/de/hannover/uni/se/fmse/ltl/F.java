/**
 */
package de.hannover.uni.se.fmse.ltl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>F</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hannover.uni.se.fmse.ltl.LtlPackage#getF()
 * @model
 * @generated
 */
public interface F extends Unary {
} // F
