/**
 */
package de.hannover.uni.se.fmse.ltl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hannover.uni.se.fmse.ltl.LtlFactory
 * @model kind="package"
 * @generated
 */
public interface LtlPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ltl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.hannover.de/uni/se/fmse/lts/LTL";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ltl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LtlPackage eINSTANCE = de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.FormulaDefinitionImpl <em>Formula Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.FormulaDefinitionImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getFormulaDefinition()
	 * @generated
	 */
	int FORMULA_DEFINITION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_DEFINITION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_DEFINITION__FORMULA = 1;

	/**
	 * The number of structural features of the '<em>Formula Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_DEFINITION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Formula Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_DEFINITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.FormulaImpl <em>Formula</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.FormulaImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getFormula()
	 * @generated
	 */
	int FORMULA = 1;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA__SATISFYING_STATES = 0;

	/**
	 * The number of structural features of the '<em>Formula</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA___EVALUATE__LTS = 0;

	/**
	 * The number of operations of the '<em>Formula</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.UnaryImpl <em>Unary</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.UnaryImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getUnary()
	 * @generated
	 */
	int UNARY = 6;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY__SATISFYING_STATES = FORMULA__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Nested</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY__NESTED = FORMULA_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unary</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY___EVALUATE__LTS = FORMULA___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>Unary</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATION_COUNT = FORMULA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.XImpl <em>X</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.XImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getX()
	 * @generated
	 */
	int X = 2;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int X__SATISFYING_STATES = UNARY__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Nested</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int X__NESTED = UNARY__NESTED;

	/**
	 * The number of structural features of the '<em>X</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int X_FEATURE_COUNT = UNARY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int X___EVALUATE__LTS = UNARY___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>X</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int X_OPERATION_COUNT = UNARY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.GImpl <em>G</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.GImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getG()
	 * @generated
	 */
	int G = 3;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G__SATISFYING_STATES = UNARY__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Nested</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G__NESTED = UNARY__NESTED;

	/**
	 * The number of structural features of the '<em>G</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G_FEATURE_COUNT = UNARY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G___EVALUATE__LTS = UNARY___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>G</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int G_OPERATION_COUNT = UNARY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.BinaryImpl <em>Binary</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.BinaryImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getBinary()
	 * @generated
	 */
	int BINARY = 7;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY__SATISFYING_STATES = FORMULA__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY__LEFT = FORMULA_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY__RIGHT = FORMULA_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Binary</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY___EVALUATE__LTS = FORMULA___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>Binary</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATION_COUNT = FORMULA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.UImpl <em>U</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.UImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getU()
	 * @generated
	 */
	int U = 4;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int U__SATISFYING_STATES = BINARY__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int U__LEFT = BINARY__LEFT;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int U__RIGHT = BINARY__RIGHT;

	/**
	 * The number of structural features of the '<em>U</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int U_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int U___EVALUATE__LTS = BINARY___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>U</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int U_OPERATION_COUNT = BINARY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.FImpl <em>F</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.FImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getF()
	 * @generated
	 */
	int F = 5;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F__SATISFYING_STATES = UNARY__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Nested</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F__NESTED = UNARY__NESTED;

	/**
	 * The number of structural features of the '<em>F</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F_FEATURE_COUNT = UNARY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F___EVALUATE__LTS = UNARY___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>F</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int F_OPERATION_COUNT = UNARY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.ANDImpl <em>AND</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.ANDImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getAND()
	 * @generated
	 */
	int AND = 8;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__SATISFYING_STATES = BINARY__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__LEFT = BINARY__LEFT;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__RIGHT = BINARY__RIGHT;

	/**
	 * The number of structural features of the '<em>AND</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND___EVALUATE__LTS = BINARY___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>AND</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_OPERATION_COUNT = BINARY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.ORImpl <em>OR</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.ORImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getOR()
	 * @generated
	 */
	int OR = 9;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__SATISFYING_STATES = BINARY__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__LEFT = BINARY__LEFT;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__RIGHT = BINARY__RIGHT;

	/**
	 * The number of structural features of the '<em>OR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_FEATURE_COUNT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR___EVALUATE__LTS = BINARY___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>OR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_OPERATION_COUNT = BINARY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.NOTImpl <em>NOT</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.NOTImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getNOT()
	 * @generated
	 */
	int NOT = 10;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT__SATISFYING_STATES = UNARY__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Nested</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT__NESTED = UNARY__NESTED;

	/**
	 * The number of structural features of the '<em>NOT</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_FEATURE_COUNT = UNARY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT___EVALUATE__LTS = UNARY___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>NOT</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_OPERATION_COUNT = UNARY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.AtomicImpl <em>Atomic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.AtomicImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getAtomic()
	 * @generated
	 */
	int ATOMIC = 12;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC__SATISFYING_STATES = FORMULA__SATISFYING_STATES;

	/**
	 * The number of structural features of the '<em>Atomic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC___EVALUATE__LTS = FORMULA___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>Atomic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_OPERATION_COUNT = FORMULA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.LTLPropositionImpl <em>LTL Proposition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.LTLPropositionImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getLTLProposition()
	 * @generated
	 */
	int LTL_PROPOSITION = 11;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTL_PROPOSITION__SATISFYING_STATES = ATOMIC__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Atomic Proposition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTL_PROPOSITION__ATOMIC_PROPOSITION = ATOMIC_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LTL Proposition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTL_PROPOSITION_FEATURE_COUNT = ATOMIC_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTL_PROPOSITION___EVALUATE__LTS = ATOMIC___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>LTL Proposition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LTL_PROPOSITION_OPERATION_COUNT = ATOMIC_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hannover.uni.se.fmse.ltl.impl.ConstantImpl <em>Constant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hannover.uni.se.fmse.ltl.impl.ConstantImpl
	 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getConstant()
	 * @generated
	 */
	int CONSTANT = 13;

	/**
	 * The feature id for the '<em><b>Satisfying States</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT__SATISFYING_STATES = ATOMIC__SATISFYING_STATES;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT__VALUE = ATOMIC_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_FEATURE_COUNT = ATOMIC_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Evaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT___EVALUATE__LTS = ATOMIC___EVALUATE__LTS;

	/**
	 * The number of operations of the '<em>Constant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_OPERATION_COUNT = ATOMIC_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.FormulaDefinition <em>Formula Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Formula Definition</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.FormulaDefinition
	 * @generated
	 */
	EClass getFormulaDefinition();

	/**
	 * Returns the meta object for the attribute '{@link de.hannover.uni.se.fmse.ltl.FormulaDefinition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.FormulaDefinition#getName()
	 * @see #getFormulaDefinition()
	 * @generated
	 */
	EAttribute getFormulaDefinition_Name();

	/**
	 * Returns the meta object for the containment reference '{@link de.hannover.uni.se.fmse.ltl.FormulaDefinition#getFormula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Formula</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.FormulaDefinition#getFormula()
	 * @see #getFormulaDefinition()
	 * @generated
	 */
	EReference getFormulaDefinition_Formula();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.Formula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Formula</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.Formula
	 * @generated
	 */
	EClass getFormula();

	/**
	 * Returns the meta object for the reference list '{@link de.hannover.uni.se.fmse.ltl.Formula#getSatisfyingStates <em>Satisfying States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Satisfying States</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.Formula#getSatisfyingStates()
	 * @see #getFormula()
	 * @generated
	 */
	EReference getFormula_SatisfyingStates();

	/**
	 * Returns the meta object for the '{@link de.hannover.uni.se.fmse.ltl.Formula#evaluate(de.hannover.uni.se.fmse.lts.LTS) <em>Evaluate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Evaluate</em>' operation.
	 * @see de.hannover.uni.se.fmse.ltl.Formula#evaluate(de.hannover.uni.se.fmse.lts.LTS)
	 * @generated
	 */
	EOperation getFormula__Evaluate__LTS();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.X <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>X</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.X
	 * @generated
	 */
	EClass getX();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.G <em>G</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>G</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.G
	 * @generated
	 */
	EClass getG();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.U <em>U</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>U</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.U
	 * @generated
	 */
	EClass getU();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.F <em>F</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>F</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.F
	 * @generated
	 */
	EClass getF();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.Unary <em>Unary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.Unary
	 * @generated
	 */
	EClass getUnary();

	/**
	 * Returns the meta object for the containment reference '{@link de.hannover.uni.se.fmse.ltl.Unary#getNested <em>Nested</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Nested</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.Unary#getNested()
	 * @see #getUnary()
	 * @generated
	 */
	EReference getUnary_Nested();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.Binary <em>Binary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.Binary
	 * @generated
	 */
	EClass getBinary();

	/**
	 * Returns the meta object for the containment reference '{@link de.hannover.uni.se.fmse.ltl.Binary#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.Binary#getLeft()
	 * @see #getBinary()
	 * @generated
	 */
	EReference getBinary_Left();

	/**
	 * Returns the meta object for the containment reference '{@link de.hannover.uni.se.fmse.ltl.Binary#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.Binary#getRight()
	 * @see #getBinary()
	 * @generated
	 */
	EReference getBinary_Right();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.AND <em>AND</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AND</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.AND
	 * @generated
	 */
	EClass getAND();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.OR <em>OR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OR</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.OR
	 * @generated
	 */
	EClass getOR();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.NOT <em>NOT</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>NOT</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.NOT
	 * @generated
	 */
	EClass getNOT();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.LTLProposition <em>LTL Proposition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LTL Proposition</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.LTLProposition
	 * @generated
	 */
	EClass getLTLProposition();

	/**
	 * Returns the meta object for the reference '{@link de.hannover.uni.se.fmse.ltl.LTLProposition#getAtomicProposition <em>Atomic Proposition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Atomic Proposition</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.LTLProposition#getAtomicProposition()
	 * @see #getLTLProposition()
	 * @generated
	 */
	EReference getLTLProposition_AtomicProposition();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.Atomic <em>Atomic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Atomic</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.Atomic
	 * @generated
	 */
	EClass getAtomic();

	/**
	 * Returns the meta object for class '{@link de.hannover.uni.se.fmse.ltl.Constant <em>Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.Constant
	 * @generated
	 */
	EClass getConstant();

	/**
	 * Returns the meta object for the attribute '{@link de.hannover.uni.se.fmse.ltl.Constant#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see de.hannover.uni.se.fmse.ltl.Constant#isValue()
	 * @see #getConstant()
	 * @generated
	 */
	EAttribute getConstant_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	LtlFactory getLtlFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.FormulaDefinitionImpl <em>Formula Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.FormulaDefinitionImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getFormulaDefinition()
		 * @generated
		 */
		EClass FORMULA_DEFINITION = eINSTANCE.getFormulaDefinition();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FORMULA_DEFINITION__NAME = eINSTANCE.getFormulaDefinition_Name();

		/**
		 * The meta object literal for the '<em><b>Formula</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORMULA_DEFINITION__FORMULA = eINSTANCE.getFormulaDefinition_Formula();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.FormulaImpl <em>Formula</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.FormulaImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getFormula()
		 * @generated
		 */
		EClass FORMULA = eINSTANCE.getFormula();

		/**
		 * The meta object literal for the '<em><b>Satisfying States</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORMULA__SATISFYING_STATES = eINSTANCE.getFormula_SatisfyingStates();

		/**
		 * The meta object literal for the '<em><b>Evaluate</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FORMULA___EVALUATE__LTS = eINSTANCE.getFormula__Evaluate__LTS();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.XImpl <em>X</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.XImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getX()
		 * @generated
		 */
		EClass X = eINSTANCE.getX();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.GImpl <em>G</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.GImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getG()
		 * @generated
		 */
		EClass G = eINSTANCE.getG();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.UImpl <em>U</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.UImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getU()
		 * @generated
		 */
		EClass U = eINSTANCE.getU();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.FImpl <em>F</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.FImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getF()
		 * @generated
		 */
		EClass F = eINSTANCE.getF();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.UnaryImpl <em>Unary</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.UnaryImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getUnary()
		 * @generated
		 */
		EClass UNARY = eINSTANCE.getUnary();

		/**
		 * The meta object literal for the '<em><b>Nested</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY__NESTED = eINSTANCE.getUnary_Nested();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.BinaryImpl <em>Binary</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.BinaryImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getBinary()
		 * @generated
		 */
		EClass BINARY = eINSTANCE.getBinary();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY__LEFT = eINSTANCE.getBinary_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY__RIGHT = eINSTANCE.getBinary_Right();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.ANDImpl <em>AND</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.ANDImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getAND()
		 * @generated
		 */
		EClass AND = eINSTANCE.getAND();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.ORImpl <em>OR</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.ORImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getOR()
		 * @generated
		 */
		EClass OR = eINSTANCE.getOR();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.NOTImpl <em>NOT</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.NOTImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getNOT()
		 * @generated
		 */
		EClass NOT = eINSTANCE.getNOT();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.LTLPropositionImpl <em>LTL Proposition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.LTLPropositionImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getLTLProposition()
		 * @generated
		 */
		EClass LTL_PROPOSITION = eINSTANCE.getLTLProposition();

		/**
		 * The meta object literal for the '<em><b>Atomic Proposition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LTL_PROPOSITION__ATOMIC_PROPOSITION = eINSTANCE.getLTLProposition_AtomicProposition();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.AtomicImpl <em>Atomic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.AtomicImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getAtomic()
		 * @generated
		 */
		EClass ATOMIC = eINSTANCE.getAtomic();

		/**
		 * The meta object literal for the '{@link de.hannover.uni.se.fmse.ltl.impl.ConstantImpl <em>Constant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hannover.uni.se.fmse.ltl.impl.ConstantImpl
		 * @see de.hannover.uni.se.fmse.ltl.impl.LtlPackageImpl#getConstant()
		 * @generated
		 */
		EClass CONSTANT = eINSTANCE.getConstant();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT__VALUE = eINSTANCE.getConstant_Value();

	}

} //LtlPackage
