/**
 */
package de.hannover.uni.se.fmse.lts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.hannover.uni.se.fmse.lts.LtsPackage#getBasicState()
 * @model
 * @generated
 */
public interface BasicState extends State, NamedElement {

} // BasicState
