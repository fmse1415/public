/**
 */
package de.hannover.uni.se.fmse.ctl.impl;

import de.hannover.uni.se.fmse.ctl.CtlPackage;
import de.hannover.uni.se.fmse.ctl.Proposition;
import de.hannover.uni.se.fmse.lts.AtomicProposition;
import de.hannover.uni.se.fmse.lts.LTS;
import de.hannover.uni.se.fmse.lts.State;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Proposition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.hannover.uni.se.fmse.ctl.impl.PropositionImpl#getAtomicProposition <em>Atomic Proposition</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PropositionImpl extends AtomicImpl implements Proposition {
	/**
	 * The cached value of the '{@link #getAtomicProposition() <em>Atomic Proposition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAtomicProposition()
	 * @generated
	 * @ordered
	 */
	protected AtomicProposition atomicProposition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropositionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CtlPackage.Literals.PROPOSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicProposition getAtomicProposition() {
		if (atomicProposition != null && atomicProposition.eIsProxy()) {
			InternalEObject oldAtomicProposition = (InternalEObject)atomicProposition;
			atomicProposition = (AtomicProposition)eResolveProxy(oldAtomicProposition);
			if (atomicProposition != oldAtomicProposition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CtlPackage.PROPOSITION__ATOMIC_PROPOSITION, oldAtomicProposition, atomicProposition));
			}
		}
		return atomicProposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicProposition basicGetAtomicProposition() {
		return atomicProposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAtomicProposition(AtomicProposition newAtomicProposition) {
		AtomicProposition oldAtomicProposition = atomicProposition;
		atomicProposition = newAtomicProposition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CtlPackage.PROPOSITION__ATOMIC_PROPOSITION, oldAtomicProposition, atomicProposition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CtlPackage.PROPOSITION__ATOMIC_PROPOSITION:
				if (resolve) return getAtomicProposition();
				return basicGetAtomicProposition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CtlPackage.PROPOSITION__ATOMIC_PROPOSITION:
				setAtomicProposition((AtomicProposition)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CtlPackage.PROPOSITION__ATOMIC_PROPOSITION:
				setAtomicProposition((AtomicProposition)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CtlPackage.PROPOSITION__ATOMIC_PROPOSITION:
				return atomicProposition != null;
		}
		return super.eIsSet(featureID);
	}
	
	@Override
	public boolean evaluate(LTS lts) {
		for(State state : lts.getStates()) {
			if (state.getAtomicPropositions().stream().anyMatch(ap->ap.getName().equals(getAtomicProposition().getName())))
				getSatisfyingStates().add(state);
		}
		return getSatisfyingStates().containsAll(lts.getInitStates());
	}

} //PropositionImpl
