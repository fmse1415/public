# README #

## Third mini project ##
* After starting a new eclipse instance with the given plugins, you have to create an *.xlts file in any open project. 
* Into this file, you can enter one or more labeled transition system definitions, as well as one or more ctl or ltl formulae (see example project de.hannover.uni.se.fmse.train). 
* Which formulae should be checked for which transition systems can be defined inside an ltl checkset or a ctl checkset. 
* After saving the xlts file, a new directory "src-gen" should show up in the project explorer (if not, refresh the project explorer view). 
* Inside the src-gen directory is, among other files, a "ltl checkset". By opening the context menu for the checkset file and clicking on the "Check Formulae" menu entry in the sub menu "LTL", you can check the validity of the ltl formulae.